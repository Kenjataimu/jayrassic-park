---- Minecraft Crash Report ----
// Hi. I'm Minecraft, and I'm a crashaholic.

Time: 2020-01-06 18:47:31 CST
Description: Exception ticking world

java.lang.IllegalArgumentException: bound must be positive
    at java.util.Random.nextInt(Random.java:388)
    at alexiy.secure.contain.protect.events.ServerEvents.generateContainmentChambers(ServerEvents.java:108)
    at net.minecraftforge.fml.common.eventhandler.ASMEventHandler_1763_ServerEvents_generateContainmentChambers_Post.invoke(.dynamic)
    at net.minecraftforge.fml.common.eventhandler.ASMEventHandler.invoke(ASMEventHandler.java:90)
    at net.minecraftforge.fml.common.eventhandler.EventBus.post(EventBus.java:182)
    at net.minecraft.world.biome.BiomeDecorator.genDecorations(BiomeDecorator.java:366)
    at net.minecraft.world.biome.BiomeDecorator.decorate(BiomeDecorator.java:88)
    at net.minecraft.world.biome.Biome.decorate(Biome.java:225)
    at net.minecraft.world.biome.BiomeHills.decorate(BiomeHills.java:41)
    at mcjty.lostcities.dimensions.world.LostCityChunkGenerator.populate(LostCityChunkGenerator.java:605)
    at net.minecraft.world.chunk.Chunk.populate(Chunk.java:1019)
    at net.minecraft.world.chunk.Chunk.populate(Chunk.java:990)
    at net.minecraft.world.gen.ChunkProviderServer.provideChunk(ChunkProviderServer.java:157)
    at net.minecraft.server.management.PlayerChunkMapEntry.providePlayerChunk(PlayerChunkMapEntry.java:126)
    at net.minecraft.server.management.PlayerChunkMap.tick(SourceFile:147)
    at net.minecraft.world.WorldServer.tick(WorldServer.java:227)
    at net.minecraft.server.MinecraftServer.updateTimeLightAndEntities(MinecraftServer.java:756)
    at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:668)
    at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:185)
    at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:526)
    at java.lang.Thread.run(Thread.java:745)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Affected level --
  Level name: testing scp facilities
  All players: 1 total; [EntityPlayerMP['A3delta'/12845, l='testing scp facilities', x=244.50, y=82.00, z=236.50]]
  Chunk stats: ServerChunkCache: 7 Drop: 0
  Level seed: 448911165654327182
  Level generator: ID 07 - lc_RTG, ver 0. Features enabled: true
  Level generator options: 
  Level spawn location: World: (245,64,232), Chunk: (at 5,4,8 in 15,14; contains blocks 240,0,224 to 255,255,239), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
  Level time: 6 game time, 6 day time
  Level dimension: 0
  Level storage version: 0x04ABD - Anvil
  Level weather: Rain time: 138869 (now: false), thunder time: 92644 (now: false)
  Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: true

-- System Details --
  Minecraft Version: 1.12.2
  Operating System: Windows 10 (amd64) version 10.0
  Java Version: 1.8.0_51, Oracle Corporation
  Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
  Memory: 1168218264 bytes (1114 MB) / 3009179648 bytes (2869 MB) up to 6210977792 bytes (5923 MB)
  JVM Flags: 29 total; -XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump -Xmx6400m -Xms256m -XX:PermSize=256m -Xmn768m -Xmx6000M -Xms512M -XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:+UseNUMA -XX:+CMSParallelRemarkEnabled -XX:MaxTenuringThreshold=15 -XX:MaxGCPauseMillis=30 -XX:GCPauseIntervalMillis=150 -XX:+UseAdaptiveGCBoundary -XX:-UseGCOverheadLimit -XX:+UseBiasedLocking -XX:SurvivorRatio=8 -XX:TargetSurvivorRatio=90 -XX:MaxTenuringThreshold=15 -XX:+UseFastAccessorMethods -XX:+UseCompressedOops -XX:+OptimizeStringConcat -XX:+AggressiveOpts -XX:ReservedCodeCacheSize=2048m -XX:+UseCodeCacheFlushing -XX:SoftRefLRUPolicyMSPerMB=10000 -XX:ParallelGCThreads=10
  IntCache: cache: 0, tcache: 0, allocated: 17, tallocated: 122
  FML: MCP 9.42 Powered by Forge 14.23.5.2847 150 mods loaded, 150 mods active
       States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored
       
       | State  | ID                                | Version                   | Source                                                    | Signature                                |
       |:------ |:--------------------------------- |:------------------------- |:--------------------------------------------------------- |:---------------------------------------- |
       | LCHIJA | minecraft                         | 1.12.2                    | minecraft.jar                                             | None                                     |
       | LCHIJA | mcp                               | 9.42                      | minecraft.jar                                             | None                                     |
       | LCHIJA | FML                               | 8.0.99.99                 | forge-1.12.2-14.23.5.2847.jar                             | None                                     |
       | LCHIJA | forge                             | 14.23.5.2847              | forge-1.12.2-14.23.5.2847.jar                             | None                                     |
       | LCHIJA | advancedrocketrycore              | 1                         | minecraft.jar                                             | None                                     |
       | LCHIJA | smoothfontcore                    | mc1.12.2-2.1.1            | minecraft.jar                                             | None                                     |
       | LCHIJA | foamfixcore                       | 7.7.4                     | minecraft.jar                                             | None                                     |
       | LCHIJA | techguns_core                     | 1.12.2-1.0                | minecraft.jar                                             | None                                     |
       | LCHIJA | smoothfont                        | mc1.12.2-2.1.1            | SmoothFont-mc1.12.2-2.1.1.jar                             | None                                     |
       | LCHIJA | quarryplus                        | 12.3.0                    | AdditionalEnchantedMiner-1.12.2-12.3.0-universal.jar      | 617a4e95f0af9de5402bb9883abe0f53a6bfa230 |
       | LCHIJA | quarryplus_ct                     | 12.3.0                    | AdditionalEnchantedMiner-1.12.2-12.3.0-universal.jar      | 617a4e95f0af9de5402bb9883abe0f53a6bfa230 |
       | LCHIJA | ic2                               | 2.8.170-ex112             | industrialcraft-2-2.8.170-ex112.jar                       | de041f9f6187debbc77034a344134053277aa3b0 |
       | LCHIJA | advanced_machines                 | 61.0.1                    | Advanced Machines-61.0.1.jar                              | None                                     |
       | LCHIJA | libvulpes                         | 0.4.2.-69                 | LibVulpes-1.12.2-0.4.2-69-universal.jar                   | None                                     |
       | LCHIJA | advancedrocketry                  | 1.6.0.-193                | AdvancedRocketry-1.12.2-1.6.0-193-universal.jar           | None                                     |
       | LCHIJA | aiimprovements                    | 0.0.1.3                   | AIImprovements-1.12-0.0.1b3.jar                           | None                                     |
       | LCHIJA | akashictome                       | 1.2-12                    | AkashicTome-1.2-12.jar                                    | None                                     |
       | LCHIJA | forgelin                          | 1.8.4                     | Forgelin-1.8.4.jar                                        | None                                     |
       | LCHIJA | alib                              | 1.0.12                    | alib-1.0.12.jar                                           | None                                     |
       | LCHIJA | crafttweaker                      | 4.1.20                    | CraftTweaker2-1.12-4.1.20.jar                             | None                                     |
       | LCHIJA | alchemistry                       | 1.0.36                    | alchemistry-1.0.36.jar                                    | None                                     |
       | LCHIJA | placebo                           | 1.6.0                     | Placebo-1.12.2-1.6.0.jar                                  | None                                     |
       | LCHIJA | apotheosis                        | 1.12.4                    | Apotheosis-1.12.2-1.12.4.jar                              | None                                     |
       | LCHIJA | aroma1997core                     | 2.0.0.2                   | Aroma1997Core-1.12.2-2.0.0.2.jar                          | dfbfe4c473253d8c5652417689848f650b2cbe32 |
       | LCHIJA | endertweaker                      | 1.1.7                     | EnderTweaker-1.12.2-1.1.7.jar                             | None                                     |
       | LCHIJA | mtlib                             | 3.0.6                     | MTLib-3.0.6.jar                                           | None                                     |
       | LCHIJA | modtweaker                        | 4.0.17                    | modtweaker-4.0.17.jar                                     | None                                     |
       | LCHIJA | jei                               | 4.15.0.291                | jei_1.12.2-4.15.0.291.jar                                 | None                                     |
       | LCHIJA | art                               | 1.0.0                     | ARTweaker-1.12.2-1.0.0.jar                                | None                                     |
       | LCHIJA | aom                               | 0.5 - MC 1.12.2           | Attack on Minecraft-Beta 0.5.jar                          | None                                     |
       | LCHIJA | attributefix                      | 1.0.4                     | AttributeFix-1.12.2-1.0.4.jar                             | d476d1b22b218a10d845928d1665d45fce301b27 |
       | LCHIJA | morphtool                         | 1.2-21                    | Morph-o-Tool-1.2-21.jar                                   | None                                     |
       | LCHIJA | quark                             | r1.6-177                  | Quark-r1.6-177.jar                                        | None                                     |
       | LCHIJA | autoreglib                        | 1.3-32                    | AutoRegLib-1.3-32.jar                                     | None                                     |
       | LCHIJA | betterbuilderswands               | 0.11.1                    | BetterBuildersWands-1.12-0.11.1.245+69d0d70.jar           | None                                     |
       | LCHIJA | bettercaves                       | 1.12.2                    | bettercaves-1.12.2-1.6.0.jar                              | None                                     |
       | LCHIJA | bettercombatmod                   | 1.5.6                     | BetterCombat-1.12.2-1.5.6.jar                             | None                                     |
       | LCHIJA | bnbgaminglib                      | 2.17.6                    | BNBGamingLib-1.12.2-2.17.6.jar                            | None                                     |
       | LCHIJA | bookshelf                         | 2.3.590                   | Bookshelf-1.12.2-2.3.590.jar                              | d476d1b22b218a10d845928d1665d45fce301b27 |
       | LCHIJA | bookworm                          | 1.12.2-2.3.0              | bookworm-1.12.2-2.3.0.jar                                 | None                                     |
       | LCHIJA | codechickenlib                    | 3.2.3.358                 | CodeChickenLib-1.12.2-3.2.3.358-universal.jar             | f1850c39b2516232a2108a7bd84d1cb5df93b261 |
       | LCHIJA | redstoneflux                      | 2.1.0                     | RedstoneFlux-1.12-2.1.0.6-universal.jar                   | 8a6abf2cb9e141b866580d369ba6548732eff25f |
       | LCHIJA | brandonscore                      | 2.4.17                    | BrandonsCore-1.12.2-2.4.17.208-universal.jar              | None                                     |
       | LCHIJA | caliper                           | 1.1.44                    | Caliper-1.12.2-1.1.44.jar                                 | d476d1b22b218a10d845928d1665d45fce301b27 |
       | LCHIJA | calmdownzombieguy                 | 1.0.0                     | CalmDownZombieGuy-1.12.2-1.0.0.jar                        | None                                     |
       | LCHIJA | car                               | 1.2.12                    | car-1.12.2-1.2.12.jar                                     | None                                     |
       | LCHIJA | chameleon                         | 1.12-4.1.3                | Chameleon-1.12-4.1.3.jar                                  | None                                     |
       | LCHIJA | ctm                               | MC1.12.2-1.0.1.30         | CTM-MC1.12.2-1.0.1.30.jar                                 | None                                     |
       | LCHIJA | chisel                            | MC1.12.2-1.0.1.44         | Chisel-MC1.12.2-1.0.1.44.jar                              | None                                     |
       | LCHIJA | chunkpregenerator                 | 2.1                       | Chunk Pregenerator V1.12-2.2.jar                          | None                                     |
       | LCHIJA | classiccombat                     | 1.0.1                     | ClassicCombat-1.0.1.jar                                   | None                                     |
       | LCHIJA | clumps                            | 3.1.2                     | Clumps-3.1.2.jar                                          | None                                     |
       | LCHIJA | controlling                       | 3.0.7                     | Controlling-3.0.7.jar                                     | None                                     |
       | LCHIJA | ctgui                             | 1.0.0                     | CraftTweaker2-1.12-4.1.20.jar                             | None                                     |
       | LCHIJA | crafttweakerjei                   | 2.0.3                     | CraftTweaker2-1.12-4.1.20.jar                             | None                                     |
       | LCHIJA | croploadcore                      | @version@                 | Crops++[1.12.2]1.12.2-0.0.1-1.12.2-0.0.1.jar              | None                                     |
       | LCHIJA | berriespp                         | 1.12.2-0.0.1              | Crops++[1.12.2]1.12.2-0.0.1-1.12.2-0.0.1.jar              | None                                     |
       | LCHIJA | csokicraftutil                    | 1.3.1                     | CsokiCraftUtil-1.3.6.jar                                  | None                                     |
       | LCHIJA | csokicraftutil-11                 | 1.3.1                     | CsokiCraftUtil-1.3.6.jar                                  | None                                     |
       | LCHIJA | custommainmenu                    | 2.0.9                     | CustomMainMenu-MC1.12.2-2.0.9.jar                         | None                                     |
       | LCHIJA | journeymap                        | 1.12.2-5.5.6              | journeymap-1.12.2-5.5.6.jar                               | None                                     |
       | LCHIJA | defaultoptions                    | 9.2.8                     | DefaultOptions_1.12.2-9.2.8.jar                           | None                                     |
       | LCHIJA | dimensionalcontrol                | 2.13.0                    | DimensionalControl-1.12.2-2.13.0.jar                      | None                                     |
       | LCHIJA | draconicevolution                 | 2.3.24                    | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar        | None                                     |
       | LCHIJA | enchdesc                          | 1.1.15                    | EnchantmentDescriptions-1.12.2-1.1.15.jar                 | d476d1b22b218a10d845928d1665d45fce301b27 |
       | LCHIJA | endercore                         | 1.12.2-0.5.73             | EnderCore-1.12.2-0.5.73.jar                               | None                                     |
       | LCHIJA | enderio                           | 5.1.52                    | EnderIO-1.12.2-5.1.52.jar                                 | None                                     |
       | LCHIJA | enderiointegrationtic             | 5.1.52                    | EnderIO-1.12.2-5.1.52.jar                                 | None                                     |
       | LCHIJA | enderiobase                       | 5.1.52                    | EnderIO-1.12.2-5.1.52.jar                                 | None                                     |
       | LCHIJA | enderioconduits                   | 5.1.52                    | EnderIO-1.12.2-5.1.52.jar                                 | None                                     |
       | LCHIJA | enderioconduitsappliedenergistics | 5.1.52                    | EnderIO-1.12.2-5.1.52.jar                                 | None                                     |
       | LCHIJA | enderioconduitsopencomputers      | 5.1.52                    | EnderIO-1.12.2-5.1.52.jar                                 | None                                     |
       | LCHIJA | refinedstorage                    | 1.6.15                    | refinedstorage-1.6.15.jar                                 | 57893d5b90a7336e8c63fe1c1e1ce472c3d59578 |
       | LCHIJA | enderioconduitsrefinedstorage     | 5.1.52                    | EnderIO-1.12.2-5.1.52.jar                                 | None                                     |
       | LCHIJA | enderiointegrationforestry        | 5.1.52                    | EnderIO-1.12.2-5.1.52.jar                                 | None                                     |
       | LCHIJA | enderiointegrationticlate         | 5.1.52                    | EnderIO-1.12.2-5.1.52.jar                                 | None                                     |
       | LCHIJA | enderiomachines                   | 5.1.52                    | EnderIO-1.12.2-5.1.52.jar                                 | None                                     |
       | LCHIJA | enderiopowertools                 | 5.1.52                    | EnderIO-1.12.2-5.1.52.jar                                 | None                                     |
       | LCHIJA | fastfurnace                       | 1.3.1                     | FastFurnace-1.12.2-1.3.1.jar                              | None                                     |
       | LCHIJA | fastleafdecay                     | v14                       | FastLeafDecay-v14.jar                                     | None                                     |
       | LCHIJA | fastbench                         | 1.7.3                     | FastWorkbench-1.12.2-1.7.3.jar                            | None                                     |
       | LCHIJA | foamfix                           | 0.10.10-1.12.2            | foamfix-0.10.10-1.12.2.jar                                | None                                     |
       | LCHIJA | frogcraftrebirth                  | 2.2.3                     | FrogCraft-Rebirth-2.2.3.jar                               | None                                     |
       | LCHIJA | gravisuite                        | 3.1.1                     | Gravitation Suite-3.1.1.jar                               | None                                     |
       | LCHIJA | icbmclassic                       | 1.12.2-3.3.0.63           | ICBM-classic-1.12.2-3.3.0b63.jar                          | None                                     |
       | LCHIJA | incontrol                         | 3.9.16                    | incontrol-1.12-3.9.16.jar                                 | None                                     |
       | LCHIJA | instantunify                      | 1.1.2                     | instantunify-1.12.2-1.1.2.jar                             | None                                     |
       | LCHIJA | inventorysorter                   | 1.13.3+57                 | inventorysorter-1.12.2-1.13.3+57.jar                      | None                                     |
       | LCHIJA | jaopca                            | 1.12.2-2.2.8.103          | JAOPCA-1.12.2-2.2.8.103.jar                               | None                                     |
       | LCHIJA | oredictinit                       | 1.12.2-2.2.1.71           | JAOPCA-1.12.2-2.2.8.103.jar                               | None                                     |
       | LCHIJA | justenoughdimensions              | 1.6.0-dev.20191019.172643 | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | 2b03e1423915a189b8094816baa18f239d576dff |
       | LCHIJA | loottweaker                       | 0.1.4                     | LootTweaker-0.1.4+MC1.12.2.jar                            | None                                     |
       | LCHIJA | jeresources                       | 0.9.2.60                  | JustEnoughResources-1.12.2-0.9.2.60.jar                   | None                                     |
       | LCHIJA | lockdown_additions                | 1.0.4                     | LA_1.12.2-1.0.5.jar                                       | None                                     |
       | LCHIJA | loadingprofiler                   | 1.1                       | LoadingProfiler-MC1.12-1.3.jar                            | None                                     |
       | LCHIJA | lostcities                        | 2.0.21                    | lostcities-1.12-2.0.21.jar                                | None                                     |
       | LCHIJA | lunatriuscore                     | 1.2.0.42                  | LunatriusCore-1.12.2-1.2.0.42-universal.jar               | None                                     |
       | LCHIJA | mekanism                          | 1.12.2-9.8.3.390          | Mekanism-1.12.2-9.8.3.390.jar                             | None                                     |
       | LCHIJA | nuclearcraft                      | 2.18o                     | NuclearCraft-2.18o-1.12.2.jar                             | None                                     |
       | LCHIJA | techguns                          | 2.0.2.0                   | techguns-1.12.2-2.0.2.0_pre3.1.jar                        | None                                     |
       | LCHIJA | mekores                           | 2.0.12                    | mekores-2.0.12.jar                                        | None                                     |
       | LCHIJA | moarboats                         | 4.1.2.1                   | moarboats-1.12.2-4.1.2.1.jar                              | None                                     |
       | LCHIJA | modnametooltip                    | 1.10.1                    | modnametooltip_1.12.2-1.10.1.jar                          | None                                     |
       | LCHIJA | moreoverlays                      | 1.15.1                    | moreoverlays-1.15.1-mc1.12.2.jar                          | None                                     |
       | LCHIJA | morpheus                          | 1.12.2-3.5.106            | Morpheus-1.12.2-3.5.106.jar                               | None                                     |
       | LCHIJA | mousetweaks                       | 2.10                      | MouseTweaks-2.10-mc1.12.2.jar                             | None                                     |
       | LCHIJA | notenoughrtgs                     | 1.2                       | notenoughrtgs-1.2a.jar                                    | None                                     |
       | LCHIJA | omlib                             | 3.1.3-246                 | omlib-1.12.2-3.1.3-246.jar                                | None                                     |
       | LCHIJA | openmodularturrets                | 3.1.4-356                 | openmodularturrets-1.12.2-3.1.4-356.jar                   | None                                     |
       | LCHIJA | oreveins                          | 2.0.15                    | oreveins-2.0.15.jar                                       | None                                     |
       | LCHIJA | harvestcraft                      | 1.12.2zb                  | Pam's HarvestCraft 1.12.2zf.jar                           | None                                     |
       | LCHIJA | pneumaticcraft                    | 1.12.2-0.11.7-375         | pneumaticcraft-repressurized-1.12.2-0.11.7-375.jar        | None                                     |
       | LCHIJA | progressiveautomation             | 1.7.8                     | ProgressiveAutomation-1.12.2-1.7.8.jar                    | None                                     |
       | LCHIJA | quarkoddities                     | 1                         | QuarkOddities-1.12.2.jar                                  | None                                     |
       | LCHIJA | refinedstorageaddons              | 0.4.4                     | refinedstorageaddons-0.4.4.jar                            | None                                     |
       | LCHIJA | refinedstoragerequestify          | ${version}                | refinedstoragerequestify-1.12.2-1.0.2-3.jar               | None                                     |
       | LCHIJA | resourceloader                    | 1.5.3                     | ResourceLoader-MC1.12.1-1.5.3.jar                         | d72e0dd57935b3e9476212aea0c0df352dd76291 |
       | LCHIJA | scannable                         | 1.6.3.24                  | Scannable-MC1.12.2-1.6.3.24.jar                           | None                                     |
       | LCHIJA | scp_containment_protocol          | 1.0.0                     | Scp Containment Protocol 1.0.4.jar                        | None                                     |
       | LCHIJA | scp_heavy_containment_expansion   | 1.0.0                     | SCP Lockdown Heavy Expansion 1.3.jar                      | None                                     |
       | LCHIJA | scp                               | 2.1.1                     | SCP Lockdown-1.12.2-2.1.1-beta.jar                        | None                                     |
       | LCHIJA | scpua                             | 1.6.1                     | SCPL-xk-unity-1.6.1.jar                                   | None                                     |
       | LCHIJA | stackie                           | 1.6.0.48                  | Stackie-1.12.2-1.6.0.48-universal.jar                     | None                                     |
       | LCHIJA | stg                               | 1.12.2-1.2.3              | stg-1.12.2-1.2.3.jar                                      | None                                     |
       | LCHIJA | storagedrawers                    | 1.12.2-5.4.0              | StorageDrawers-1.12.2-5.4.0.jar                           | None                                     |
       | LCHIJA | surge                             | 2.0.77                    | Surge-1.12.2-2.0.77.jar                                   | d476d1b22b218a10d845928d1665d45fce301b27 |
       | LCHIJA | tatw                              | 1.0.1.2                   | Tatw-1.12.2-1.0.1.2.jar                                   | dfbfe4c473253d8c5652417689848f650b2cbe32 |
       | LCHIJA | tellme                            | 0.7.0-dev.20190610.165828 | tellme-1.12.2-0.7.0-dev.20190610.165828.jar               | 2b03e1423915a189b8094816baa18f239d576dff |
       | LCHIJA | texfix                            | 4.0                       | TexFix V-1.12-4.0.jar                                     | None                                     |
       | LCHIJA | theoneprobe                       | 1.4.28                    | theoneprobe-1.12-1.4.28.jar                               | None                                     |
       | LCHIJA | tickdynamic                       | 1.0.2                     | TickDynamic-1.12.2-1.0.2.jar                              | None                                     |
       | LCHIJA | tipthescales                      | 1.0.4                     | TipTheScales-1.12.2-1.0.4.jar                             | None                                     |
       | LCHIJA | toastcontrol                      | 1.8.1                     | Toast Control-1.12.2-1.8.1.jar                            | None                                     |
       | LCHIJA | tombmanygraves                    | 1.12-4.2.0                | TombManyGraves-1.12-4.2.0.jar                             | None                                     |
       | LCHIJA | tombmanygraves2api                | 1.12.2-1.0.0              | tombmanygraves2api-1.12.2-1.0.0.jar                       | None                                     |
       | LCHIJA | tombmanypluginstechguns           | @VERSION@                 | tombmanypluginstechguns-1.0.0.jar                         | None                                     |
       | LCHIJA | thebetweenlands                   | 3.5.4                     | TheBetweenlands-3.5.4-universal.jar                       | 38067d6878811efb38b6a045521cfd80b9b60b38 |
       | LCHIJA | tombmanypluginsthebetweenlands    | 1.0.0                     | tombmanypluginsthebetweenlands-1.0.0.jar                  | None                                     |
       | LCHIJA | topaddons                         | 1.12.2-1.12.0             | topaddons-1.12.2-1.12.0.jar                               | None                                     |
       | LCHIJA | torchmaster                       | 1.8.1.81                  | torchmaster_1.12.2-1.8.1.81.jar                           | 5e9a436b366831c8f54a7e80b015784da69278c6 |
       | LCHIJA | unloader                          | 1.2.0                     | unloader-1.2.0.jar                                        | None                                     |
       | LCHIJA | vanillafix                        | 1.0.10-SNAPSHOT           | VanillaFix-1.0.10-99.jar                                  | None                                     |
       | LCHIJA | wanionlib                         | 1.12.2-2.4                | WanionLib-1.12.2-2.4.jar                                  | None                                     |
       | LCHIJA | worldstripper                     | 1.6.0-1.12.2              | World-Stripper-1.6.0-1.12.2.jar                           | None                                     |
       | LCHIJA | zerascp                           | 1.0.0                     | zeras-SCPs-1.6.jar                                        | None                                     |
       | LCHIJA | phosphor-lighting                 | 1.12.2-0.2.6              | phosphor-1.12.2-0.2.6+build50-universal.jar               | f0387d288626cc2d937daa504e74af570c52a2f1 |
       | LCHIJA | reauth                            | 3.6.0                     | reauth-3.6.0.jar                                          | daba0ec4df71b6da841768c49fb873def208a1e3 |
       | LCHIJA | rtg                               | 6.1.0.0-snapshot.1        | RTG-1.12.2-6.1.0.0-snapshot.1.jar                         | None                                     |
       | LCHIJA | unidict                           | 1.12.2-2.9.3              | UniDict-1.12.2-2.9.3.jar                                  | None                                     |
       | LCHIJA | wrapup                            | 1.12-1.1.3                | WrapUp-1.12-1.1.3.jar                                     | None                                     |
  Loaded coremods (and transformers): Quark Plugin (Quark-r1.6-177.jar)
                                        vazkii.quark.base.asm.ClassTransformer
                                      CTMCorePlugin (CTM-MC1.12.2-1.0.1.30.jar)
                                        team.chisel.ctm.client.asm.CTMTransformer
                                      ForgelinPlugin (Forgelin-1.8.4.jar)
                                        
                                      MekanismCoremod (Mekanism-1.12.2-9.8.3.390.jar)
                                        mekanism.coremod.KeybindingMigrationHelper
                                      LoadingPlugin (LoadingProfiler-MC1.12-1.3.jar)
                                        lumien.loadingprofiler.asm.ClassTransformer
                                      EnderCorePlugin (EnderCore-1.12.2-0.5.73-core.jar)
                                        com.enderio.core.common.transform.EnderCoreTransformer
                                        com.enderio.core.common.transform.SimpleMixinPatcher
                                      TheBetweenlandsLoadingPlugin (TheBetweenlands-3.5.4-core.jar)
                                        thebetweenlands.core.TheBetweenlandsClassTransformer
                                      AdvancedRocketryPlugin (AdvancedRocketry-1.12.2-1.6.0-193-universal.jar)
                                        zmaster587.advancedRocketry.asm.ClassTransformer
                                      Born in a Barn (Born In A Barn V1.8-1.12-1.1.jar)
                                        com.chocohead.biab.BornInABarn
                                      Techguns Core (techguns-1.12.2-2.0.2.0_pre3.1.jar)
                                        techguns.core.TechgunsASMTransformer
                                      CorePlugin (SmoothFont-mc1.12.2-2.1.1.jar)
                                        bre.smoothfont.asm.Transformer
                                      ApotheosisCore (Apotheosis-1.12.2-1.12.4.jar)
                                        shadows.ApotheosisTransformer
                                      PhosphorFMLLoadingPlugin (phosphor-1.12.2-0.2.6+build50-universal.jar)
                                        
                                      VanillaFixLoadingPlugin (VanillaFix-1.0.10-99.jar)
                                        
                                      Do not report to Forge! (If you haven't disabled the FoamFix coremod, try disabling it in the config! Note that this bit of text will still appear.) (foamfix-0.10.10-1.12.2.jar)
                                        pl.asie.foamfix.coremod.FoamFixTransformer
                                      LoadingPlugin (TickDynamic-1.12.2-1.0.2.jar)
                                        com.wildex999.patcher.TransformerPatcher
                                        com.wildex999.patcher.EntityInjector
                                      LoadingPlugin (ResourceLoader-MC1.12.1-1.5.3.jar)
                                        lumien.resourceloader.asm.ClassTransformer
                                      SurgeLoadingPlugin (Surge-1.12.2-2.0.77.jar)
                                        
                                      CoreMod (Aroma1997Core-1.12.2-2.0.0.2.jar)
  GL info: ~~ERROR~~ RuntimeException: No OpenGL context found in the current thread.
  Ender IO: No known problems detected.
            
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !!!You are looking at the diagnostics information, not at the crash.       !!!
            !!!Scroll up until you see the line with '---- Minecraft Crash Report ----'!!!
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  Suspected Mods: SCP: Lockdown (scp), The Lost Cities (lostcities)
  Profiler Position: N/A (disabled)
  Player Count: 1 / 8; [EntityPlayerMP['A3delta'/12845, l='testing scp facilities', x=244.50, y=82.00, z=236.50]]
  Type: Integrated Server (map_client.txt)
  Is Modded: Definitely; Client brand changed to 'fml,forge'
