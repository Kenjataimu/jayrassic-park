# Load Time Info

This file contains an analysis of mod load times. If you are using the vanilla
(or twitch) launcher, this file may show 0.0s for all the mods due to a bug in
that launcher. This data is anonymous, and is not automatically submitted to any
online service.



Signed Mods: 22 (12.0%)

| Mod                                          | Total Time | Pre Init | Init | IMC  | Post Init | Valid Signature | File Name                                                 |
|----------------------------------------------|------------|----------|------|------|-----------|-----------------|-----------------------------------------------------------|
| ReAuth                                       | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | reauth-3.6.0.jar                                          |
| RFTools                                      | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | rftools-1.12-7.72.jar                                     |
| MoreOverlays                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | moreoverlays-1.15.1-mc1.12.2.jar                          |
| Obsidian Animator                            | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | ObsidianAnimator_v1.0.0.jar                               |
| FastFurnace                                  | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | FastFurnace-1.12.2-1.3.1.jar                              |
| Vulpes library                               | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | LibVulpes-1.12.2-0.4.2-69-universal.jar                   |
| OreExcavation Integration                    | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | oeintegration-2.3.4.jar                                   |
| Chunk Pregenerator                           | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Chunk Pregenerator V1.12-2.2.jar                          |
| Enchantment Descriptions                     | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | EnchantmentDescriptions-1.12.2-1.1.15.jar                 |
| InfinityLib                                  | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | infinitylib-1.12.0.jar                                    |
| MekanismOres                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | mekores-2.0.12.jar                                        |
| Ender IO Integration with Forestry           | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | EnderIO-1.12.2-5.1.55.jar                                 |
| Forge Mod Loader                             | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | forge-1.12.2-14.23.5.2847.jar                             |
| TextureFix                                   | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | TexFix V-1.12-4.0.jar                                     |
| TombManyGraves2 API                          | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | tombmanygraves2api-1.12.2-1.0.0.jar                       |
| Ender IO Machines                            | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | EnderIO-1.12.2-5.1.55.jar                                 |
| Not Enough RTGs                              | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | notenoughrtgs-1.2a.jar                                    |
| Academy Monster                              | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | AcademyMonster-1.1.0a.jar                                 |
| JEI Integration                              | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | jeiintegration_1.12.2-1.5.1.36.jar                        |
| Rebind Narrator                              | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | rebindnarrator-1.0.jar                                    |
| Custom Backgrounds                           | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | CustomBackgrounds-MC1.12-1.1.1.jar                        |
| Progressive Automation                       | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | ProgressiveAutomation-1.12.2-1.7.8.jar                    |
| FastWorkbench                                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | FastWorkbench-1.12.2-1.7.3.jar                            |
| Open Modular Turrets                         | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | openmodularturrets-1.12.2-3.1.4-356.jar                   |
| Tech Reborn Mod Compatibility                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | TechReborn-ModCompatibility-1.12.2-1.3.2.64.jar           |
| GasConduits                                  | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | GasConduits-1.12.2-1.2.2.jar                              |
| Ore Veins                                    | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | oreveins-2.0.15.jar                                       |
| Redstone Flux                                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | RedstoneFlux-1.12-2.1.0.6-universal.jar                   |
| Caliper                                      | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | Caliper-1.12.2-1.1.44.jar                                 |
| BNBGamingLib                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | BNBGamingLib-1.12.2-2.17.6.jar                            |
| Academy Craft                                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | AcademyCraft-1.1.2.jar                                    |
| YUNG's Better Caves                          | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | bettercaves-1.12.2-1.6.0.jar                              |
| LunatriusCore                                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | LunatriusCore-1.12.2-1.2.0.42-universal.jar               |
| Ender IO Powertools                          | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | EnderIO-1.12.2-5.1.55.jar                                 |
| Minecraft Coder Pack                         | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | minecraft.jar                                             |
| OreDictInit                                  | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | JAOPCA-1.12.2-2.2.8.103.jar                               |
| Ender IO Applied Energistics Conduits        | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | EnderIO-1.12.2-5.1.55.jar                                 |
| LLibrary                                     | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | llibrary-1.7.19-1.12.2.jar                                |
| AI Improvements                              | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | AIImprovements-1.12-0.0.1b3.jar                           |
| Apotheosis                                   | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Apotheosis-1.12.2-1.12.4.jar                              |
| MTLib                                        | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | MTLib-3.0.6.jar                                           |
| Quark                                        | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Quark-r1.6-177.jar                                        |
| Scannable                                    | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Scannable-MC1.12.2-1.6.3.24.jar                           |
| ICBM-Classic                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | ICBM-classic-1.12.2-3.3.0b63.jar                          |
| Flood Lights                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | FloodLights-1.12.2-1.4.1-17.jar                           |
| Grimoire of Gaia 3                           | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | GrimoireOfGaia3-1.12.2-1.6.9.3.jar                        |
| McJtyLib                                     | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | mcjtylib-1.12-3.5.4.jar                                   |
| CodeChicken Lib                              | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | CodeChickenLib-1.12.2-3.2.3.358-universal.jar             |
| JAOPCA                                       | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | JAOPCA-1.12.2-2.2.8.103.jar                               |
| TombManyPlugins Techguns                     | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | tombmanypluginstechguns-1.0.0.jar                         |
| Unloader                                     | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | unloader-1.2.0.jar                                        |
| Akashic Tome                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | AkashicTome-1.2-12.jar                                    |
| WrapUp                                       | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | WrapUp-1.12-1.1.3.jar                                     |
| Techguns Core                                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | minecraft.jar                                             |
| Chameleon                                    | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Chameleon-1.12-4.1.3.jar                                  |
| Ender IO Open Computers Conduits             | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | EnderIO-1.12.2-5.1.55.jar                                 |
| Ender IO Conduits                            | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | EnderIO-1.12.2-5.1.55.jar                                 |
| JourneyMap                                   | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | journeymap-1.12.2-5.5.6.jar                               |
| Refined Storage                              | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | refinedstorage-1.6.15.jar                                 |
| csokicraftutil-11                            | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | CsokiCraftUtil-1.3.6.jar                                  |
| MCMultiPart                                  | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | MCMultiPart-2.5.3.jar                                     |
| Calm Down Zombie Guy                         | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | CalmDownZombieGuy-1.12.2-1.0.0.jar                        |
| The One Probe                                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | theoneprobe-1.12-1.4.28.jar                               |
| Advanced Rocketry Core                       | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | minecraft.jar                                             |
| Practical Logistics 2                        | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | practicallogistics2-1.12.2-3.0.8-11.jar                   |
| TombManyPlugins The Betweenlands             | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | tombmanypluginsthebetweenlands-1.0.0.jar                  |
| TOP Addons                                   | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | topaddons-1.12.2-1.12.0.jar                               |
| Alchemistry                                  | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | alchemistry-1.0.36.jar                                    |
| Chisel                                       | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Chisel-MC1.12.2-1.0.1.44.jar                              |
| Custom Main Menu                             | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | CustomMainMenu-MC1.12.2-2.0.9.1.jar                       |
| InControl                                    | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | incontrol-1.12-3.9.16.jar                                 |
| Minecraft                                    | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | minecraft.jar                                             |
| Reborn Core                                  | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | RebornCore-1.12.2-3.16.4.478-universal.jar                |
| Classic Combat                               | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | ClassicCombat-1.0.1.jar                                   |
| Tell Me                                      | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | tellme-1.12.2-0.7.0-dev.20190610.165828.jar               |
| Shadowfacts' Forgelin                        | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Forgelin-1.8.4.jar                                        |
| Phosphor Lighting Engine                     | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | phosphor-1.12.2-0.2.6+build50-universal.jar               |
| Ender IO Refined Storage Conduits            | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | EnderIO-1.12.2-5.1.55.jar                                 |
| AutoRegLib                                   | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | AutoRegLib-1.3-32.jar                                     |
| Attribute Fix                                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | AttributeFix-1.12.2-1.0.4.jar                             |
| SwingThroughGrass                            | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | stg-1.12.2-1.2.3.jar                                      |
| Item Blacklist                               | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | ItemBlacklist-1.4.3.jar                                   |
| RefinedStorageRequestify                     | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | refinedstoragerequestify-1.12.2-1.0.2-3.jar               |
| LootTweaker                                  | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | LootTweaker-0.1.4+MC1.12.2.jar                            |
| Techguns                                     | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | techguns-1.12.2-2.0.2.0_pre3.1.jar                        |
| Ice and Fire                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | iceandfire-1.8.3.jar                                      |
| Simple Inventory sorting                     | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | inventorysorter-1.12.2-1.13.3+57.jar                      |
| Brandon's Core                               | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | BrandonsCore-1.12.2-2.4.17.208-universal.jar              |
| InstantUnify                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | instantunify-1.12.2-1.1.2.jar                             |
| Controlling                                  | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Controlling-3.0.8.jar                                     |
| QuarryPlus                                   | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | AdditionalEnchantedMiner-1.12.2-12.3.0-universal.jar      |
| EnderCore                                    | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | EnderCore-1.12.2-0.5.73.jar                               |
| Just Enough Resources                        | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | JustEnoughResources-1.12.2-0.9.2.60.jar                   |
| Loading Profiler                             | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | LoadingProfiler-MC1.12-1.3.jar                            |
| Epic Siege Mod                               | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | EpicSiegeMod-13.165.jar                                   |
| Tinkers' Construct                           | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | TConstruct-1.12.2-2.13.0.171.jar                          |
| lambdalib2                                   | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | AcademyCraft-1.1.2.jar                                    |
| VanillaFix                                   | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | VanillaFix-1.0.10-99.jar                                  |
| Mod Name Tooltip                             | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | modnametooltip_1.12.2-1.10.1.jar                          |
| Resource Loader                              | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | ResourceLoader-MC1.12.1-1.5.3.jar                         |
| NuclearCraft                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | NuclearCraft-2.18o-1.12.2.jar                             |
| air_support                                  | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | AirSupport-0.3.0.jar                                      |
| Ender IO                                     | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | EnderIO-1.12.2-5.1.55.jar                                 |
| MineTweakerRecipeMaker                       | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | MineTweakerRecipeMaker-1.12.2-1.2.2.30.jar                |
| Stackie                                      | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Stackie-1.12.2-1.6.0.48-universal.jar                     |
| Building Gadgets                             | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | BuildingGadgets-2.7.4.jar                                 |
| Storage Drawers                              | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | StorageDrawers-1.12.2-5.4.0.jar                           |
| Smooth Font Core                             | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | minecraft.jar                                             |
| Mantle                                       | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Mantle-1.12-1.3.3.55.jar                                  |
| Tomb Many Graves                             | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | TombManyGraves-1.12-4.2.0.jar                             |
| Obsidian API                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | ObsidianAnimator_v1.0.0.jar                               |
| Tick Dynamic                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | TickDynamic-1.12.2-1.0.2.jar                              |
| Tech Reborn                                  | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | TechReborn-1.12.2-2.24.3.1043-universal.jar               |
| CraftTweaker JEI Support                     | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | CraftTweaker2-1.12-4.1.20.jar                             |
| A Lib                                        | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | alib-1.0.12.jar                                           |
| UniDict                                      | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | UniDict-1.12.2-2.9.3.jar                                  |
| Morpheus                                     | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Morpheus-1.12.2-3.5.106.jar                               |
| Just Enough HarvestCraft                     | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | just-enough-harvestcraft-1.12.2-1.7.0.jar                 |
| PlusTiC                                      | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | plustic-7.1.6.1.jar                                       |
| AgriCraft                                    | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | AgriCraft-2.12.0-1.12.0-a6.jar                            |
| Ender IO Integration with Tinkers' Construct | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | EnderIO-1.12.2-5.1.55.jar                                 |
| FoamFixCore                                  | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | minecraft.jar                                             |
| TorchMaster                                  | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | torchmaster_1.12.2-1.8.1.81.jar                           |
| Better Builder's Wands                       | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | BetterBuildersWands-1.12-0.11.1.245+69d0d70.jar           |
| CT-GUI                                       | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | CraftTweaker2-1.12-4.1.20.jar                             |
| Placebo                                      | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Placebo-1.12.2-1.6.0.jar                                  |
| Pam's HarvestCraft                           | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Pam's HarvestCraft 1.12.2zf.jar                           |
| Compact Machines 3                           | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | compactmachines3-1.12.2-3.0.18-b278.jar                   |
| Mouse Tweaks                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | MouseTweaks-2.10-mc1.12.2.jar                             |
| OMLib                                        | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | omlib-1.12.2-3.1.3-246.jar                                |
| Mutant Beasts                                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | MutantBeasts-1.12.2-0.6.0.jar                             |
| Dimensional Control                          | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | DimensionalControl-1.12.2-2.13.0.jar                      |
| TipTheScales                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | TipTheScales-1.12.2-1.0.4.jar                             |
| HelpFixer                                    | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | HelpFixer-1.12.1-1.5.18.jar                               |
| Fast Leaf Decay                              | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | FastLeafDecay-v14.jar                                     |
| Surge                                        | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | Surge-1.12.2-2.0.77.jar                                   |
| CraftTweaker2                                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | CraftTweaker2-1.12-4.1.20.jar                             |
| Default Options                              | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | DefaultOptions_1.12.2-9.2.8.jar                           |
| Just Enough Dimensions                       | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar |
| SonarCore                                    | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | sonarcore-1.12.2-5.0.19-20.jar                            |
| World Stripper                               | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | World-Stripper-1.6.0-1.12.2.jar                           |
| LambdaLib2|Core                              | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | minecraft.jar                                             |
| Aroma1997Core                                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | Aroma1997Core-1.12.2-2.0.0.2.jar                          |
| Tinkers Tool Leveling                        | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | TinkerToolLeveling-1.12.2-1.1.0.jar                       |
| Mekanism                                     | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Mekanism-1.12.2-9.8.3.390.jar                             |
| Minecraft Forge                              | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | forge-1.12.2-14.23.5.2847.jar                             |
| Just Enough Items                            | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | jei_1.12.2-4.15.0.291.jar                                 |
| QuarryPlus_CT                                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | AdditionalEnchantedMiner-1.12.2-12.3.0-universal.jar      |
| Bad Wither No Cookie! Reloaded               | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | badwithernocookiereloaded-1.12.2-3.3.16.jar               |
| Clumps                                       | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Clumps-3.1.2.jar                                          |
| SCPCraft:Decline                             | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | SCPCraft-1.1-Decline.jar                                  |
| csokicraftutil                               | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | CsokiCraftUtil-1.3.6.jar                                  |
| WanionLib                                    | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | WanionLib-1.12.2-2.4.jar                                  |
| Ender IO Base                                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | EnderIO-1.12.2-5.1.55.jar                                 |
| Prehistoric Eclipse                          | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Prehistoric Eclipsev0.9-BETA.jar                          |
| Mod Tweaker                                  | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | modtweaker-4.0.17.jar                                     |
| Morph-o-Tool                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Morph-o-Tool-1.2-21.jar                                   |
| Advanced Rocketry                            | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar           |
| Refined Storage Addons                       | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | refinedstorageaddons-0.4.4.jar                            |
| Quark Oddities                               | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | QuarkOddities-1.12.2.jar                                  |
| Better Combat Rebirth                        | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | BetterCombat-1.12.2-1.5.6.jar                             |
| Toast Control                                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Toast Control-1.12.2-1.8.1.jar                            |
| FoamFix                                      | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | foamfix-0.10.10-1.12.2.jar                                |
| No Recipe Book                               | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | noRecipeBook_v1.2.2formc1.12.2.jar                        |
| Anvil Patch - Lawful                         | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | anvilpatch-1.0.0.jar                                      |
| Smooth Font                                  | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | SmoothFont-mc1.12.2-2.1.1.jar                             |
| The Lost Cities                              | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | lostcities-1.12-2.0.21.jar                                |
| OreExcavation                                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | OreExcavation-1.4.143.jar                                 |
| Patchouli                                    | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Patchouli-1.0-20.jar                                      |
| The Betweenlands                             | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | TheBetweenlands-3.5.5-universal.jar                       |
| LagGoggles                                   | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | LagGoggles-FAT-1.12.2-4.9.jar                             |
| Bookworm API                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | bookworm-1.12.2-2.3.0.jar                                 |
| Bookshelf                                    | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | true            | Bookshelf-1.12.2-2.3.590.jar                              |
| PneumaticCraft: Repressurized                | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar        |
| Tinker's JEI                                 | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | tinkersjei-1.2.jar                                        |
| CTM                                          | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | CTM-MC1.12.2-1.0.1.30.jar                                 |
| Draconic Evolution                           | 0.0s       | 0.0s     | 0.0s | 0.0s | 0.0s      | false           | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar        |
