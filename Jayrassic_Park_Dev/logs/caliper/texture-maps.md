# Texture Map Analysis

This file contains an analysis on the texture maps in the game, and which mods
are adding textures to them. The size value is meant to represent the amount of
graphical memory used by the texture and not the file size on your hard drive.
Size is based on the assumption that every pixel is four bytes. This data is
anonymous, and is not automatically submitted to any online service.


## Texture: minecraft:textures/atlas/blocks.png
| Mod ID                   | Textures | Pixels  | Est. Size |
|--------------------------|----------|---------|-----------|
| chisel                   | 2732     | 1459200 | 5 MB      |
| pneumaticcraft           | 281      | 1284608 | 4 MB      |
| alchemistry              | 151      | 1177600 | 4 MB      |
| tconstruct               | 4310     | 1107200 | 4 MB      |
| icbmclassic              | 127      | 712192  | 2 MB      |
| mekanism                 | 708      | 642432  | 2 MB      |
| thebetweenlands          | 1725     | 582912  | 2 MB      |
| techguns                 | 369      | 501760  | 1 MB      |
| harvestcraft             | 1784     | 456704  | 1 MB      |
| minecraft                | 813      | 229632  | 897 KB    |
| nuclearcraft             | 699      | 211712  | 827 KB    |
| sonarcore                | 804      | 205824  | 804 KB    |
| techreborn               | 665      | 186368  | 728 KB    |
| enderio                  | 526      | 180224  | 704 KB    |
| practicallogistics2      | 74       | 170752  | 667 KB    |
| academy                  | 102      | 168960  | 660 KB    |
| iceandfire               | 451      | 129280  | 505 KB    |
| grimoireofgaia           | 128      | 111104  | 434 KB    |
| rftools                  | 236      | 102656  | 401 KB    |
| quark                    | 366      | 95232   | 372 KB    |
| plustic                  | 354      | 91392   | 357 KB    |
| libvulpes                | 35       | 79616   | 311 KB    |
| draconicevolution        | 156      | 79104   | 309 KB    |
| advancedrocketry         | 171      | 66048   | 258 KB    |
| openmodularturrets       | 64       | 65536   | 256 KB    |
| agricraft                | 194      | 54272   | 212 KB    |
| storagedrawers           | 113      | 53504   | 209 KB    |
| refinedstorage           | 198      | 50688   | 198 KB    |
| scpcraft                 | 109      | 27904   | 109 KB    |
| quarryplus               | 78       | 26880   | 105 KB    |
| compactmachines3         | 31       | 22528   | 88 KB     |
| air_support              | 8        | 17408   | 68 KB     |
| progressiveautomation    | 60       | 15360   | 60 KB     |
| gasconduits              | 9        | 12800   | 50 KB     |
| pe                       | 29       | 11264   | 44 KB     |
| floodlights              | 35       | 9728    | 38 KB     |
| buildinggadgets          | 33       | 8448    | 33 KB     |
| jaopca                   | 29       | 8192    | 32 KB     |
| omlib                    | 2        | 5120    | 20 KB     |
| apotheosis               | 4        | 4864    | 19 KB     |
| mutantbeasts             | 18       | 4416    | 17 KB     |
| notenoughrtgs            | 16       | 4096    | 16 KB     |
| scannable                | 12       | 3072    | 12 KB     |
| torchmaster              | 9        | 2304    | 9 KB      |
| mekores                  | 5        | 1280    | 5 KB      |
| tombmanygraves           | 1        | 1024    | 4 KB      |
| reborncore               | 4        | 1024    | 4 KB      |
| betterbuilderswands      | 4        | 1024    | 4 KB      |
| forge                    | 4        | 1024    | 4 KB      |
| theoneprobe              | 4        | 1024    | 4 KB      |
| enderiomachines          | 4        | 1024    | 4 KB      |
| itemblacklist            | 1        | 1024    | 4 KB      |
| topaddons                | 4        | 1024    | 4 KB      |
| oeintegration            | 1        | 1024    | 4 KB      |
| endercore                | 3        | 768     | 3 KB      |
| refinedstoragerequestify | 2        | 512     | 2 KB      |
| refinedstorageaddons     | 2        | 512     | 2 KB      |
| missingno                | 1        | 256     | 1 KB      |
| akashictome              | 1        | 256     | 1 KB      |
| patchouli                | 1        | 256     | 1 KB      |
| morphtool                | 1        | 256     | 1 KB      |

## Texture: jei:textures
| Mod ID    | Textures | Pixels | Est. Size |
|-----------|----------|--------|-----------|
| jei       | 20       | 15035  | 58 KB     |
| missingno | 1        | 256    | 1 KB      |
## Large Texture Files

The following table of textures are ones that are considered very large. While
some of these may be justified, it's possible that these are frivolous.

| Texture Name                       | Dimensions  | Pixels  | Est. Size |
|------------------------------------|-------------|---------|-----------|
| alchemistry:items/periodic_diagram | 1024 X 1024 | 1048576 | 4 MB      |
