# Event Analysis

This file contains an analysis on the various forge event busses. This first
table lists events that have listeners, in order of most listeners. Further
specifics on the listenrs. This data is anonymous, and is not automatically
submitted to any online service.

| Event Name                                             | Listener Count |
|--------------------------------------------------------|----------------|
| TickEvent$ClientTickEvent                              | 89             |
| ConfigChangedEvent$OnConfigChangedEvent                | 67             |
| InputEvent$KeyInputEvent                               | 53             |
| EntityJoinWorldEvent                                   | 47             |
| PlayerInteractEvent$RightClickBlock                    | 44             |
| ItemTooltipEvent                                       | 44             |
| TickEvent$ServerTickEvent                              | 41             |
| TickEvent$PlayerTickEvent                              | 41             |
| RenderWorldLastEvent                                   | 40             |
| AttachCapabilitiesEvent                                | 37             |
| LivingEvent$LivingUpdateEvent                          | 37             |
| PlayerEvent$PlayerLoggedInEvent                        | 36             |
| ModelBakeEvent                                         | 36             |
| WorldEvent$Unload                                      | 35             |
| TickEvent$WorldTickEvent                               | 29             |
| GuiOpenEvent                                           | 29             |
| RenderGameOverlayEvent$Post                            | 27             |
| WorldEvent$Load                                        | 26             |
| LivingDropsEvent                                       | 25             |
| LivingDeathEvent                                       | 22             |
| LivingHurtEvent                                        | 22             |
| PlayerEvent$BreakSpeed                                 | 20             |
| EnderIOLifecycleEvent$PreInit                          | 20             |
| GuiScreenEvent$InitGuiEvent$Post                       | 20             |
| TextureStitchEvent$Pre                                 | 20             |
| BlockEvent$HarvestDropsEvent                           | 20             |
| BlockEvent$BreakEvent                                  | 19             |
| TickEvent$RenderTickEvent                              | 19             |
| PlayerInteractEvent$EntityInteract                     | 19             |
| LootTableLoadEvent                                     | 17             |
| LivingAttackEvent                                      | 17             |
| FMLNetworkEvent$ClientDisconnectionFromServerEvent     | 16             |
| PlayerEvent$Clone                                      | 15             |
| LivingSpawnEvent$CheckSpawn                            | 15             |
| EnderIOLifecycleEvent$Init$Pre                         | 14             |
| DrawBlockHighlightEvent                                | 13             |
| PlayerEvent$PlayerLoggedOutEvent                       | 12             |
| MouseEvent                                             | 12             |
| PlayerInteractEvent$RightClickItem                     | 12             |
| AnvilUpdateEvent                                       | 11             |
| RenderGameOverlayEvent$Pre                             | 11             |
| PlayerEvent$ItemCraftedEvent                           | 11             |
| GuiScreenEvent$KeyboardInputEvent$Post                 | 11             |
| GuiScreenEvent$DrawScreenEvent$Post                    | 11             |
| PlayerEvent$PlayerChangedDimensionEvent                | 11             |
| FOVUpdateEvent                                         | 11             |
| RenderPlayerEvent$Pre                                  | 11             |
| RenderTooltipEvent$PostText                            | 10             |
| AttackEntityEvent                                      | 10             |
| WorldEvent$Save                                        | 10             |
| EnderTeleportEvent                                     | 9              |
| RenderLivingEvent$Pre                                  | 9              |
| LivingFallEvent                                        | 9              |
| ChunkDataEvent$Load                                    | 8              |
| ChunkEvent$Load                                        | 8              |
| ConfigFileChangedEvent                                 | 8              |
| BlockEvent$PlaceEvent                                  | 8              |
| GuiScreenEvent$MouseInputEvent$Post                    | 8              |
| GuiScreenEvent$MouseInputEvent$Pre                     | 8              |
| PlayerInteractEvent$LeftClickBlock                     | 8              |
| RenderGameOverlayEvent                                 | 8              |
| ChunkDataEvent$Save                                    | 8              |
| CalcEvent$MaxCP                                        | 8              |
| ChunkEvent$Unload                                      | 7              |
| PlayerEvent$StartTracking                              | 7              |
| LivingEvent$LivingJumpEvent                            | 7              |
| RenderPlayerEvent$Post                                 | 7              |
| GuiScreenEvent$ActionPerformedEvent$Pre                | 7              |
| PlaySoundEvent                                         | 7              |
| PlayerDropsEvent                                       | 7              |
| RegistryEvent$NewRegistry                              | 7              |
| EntityItemPickupEvent                                  | 7              |
| PlayerEvent$PlayerRespawnEvent                         | 7              |
| RenderLivingEvent$Post                                 | 6              |
| PopulateChunkEvent$Post                                | 6              |
| PlayerInteractEvent                                    | 6              |
| FMLNetworkEvent$ClientConnectedToServerEvent           | 6              |
| PlayerSleepInBedEvent                                  | 6              |
| ItemTossEvent                                          | 6              |
| EntityViewRenderEvent$FogColors                        | 6              |
| FillBucketEvent                                        | 5              |
| PlayerEvent$ItemPickupEvent                            | 5              |
| LivingEntityUseItemEvent$Start                         | 5              |
| Apotheosis$ApotheosisInit                              | 5              |
| OreGenEvent$GenerateMinable                            | 5              |
| PlayerInteractEvent$EntityInteractSpecific             | 5              |
| InputEvent                                             | 5              |
| LivingEntityUseItemEvent$Tick                          | 5              |
| Apotheosis$ApotheosisPreInit                           | 5              |
| FurnaceFuelBurnTimeEvent                               | 5              |
| TextureStitchEvent$Post                                | 5              |
| LivingEntityUseItemEvent$Finish                        | 4              |
| WorldEvent$CreateSpawnPosition                         | 4              |
| WorldEvent$PotentialSpawns                             | 4              |
| GuiScreenEvent$KeyboardInputEvent$Pre                  | 4              |
| PlayerDestroyItemEvent                                 | 4              |
| RenderSpecificHandEvent                                | 4              |
| GuiScreenEvent$InitGuiEvent$Pre                        | 4              |
| TextureStitchEvent                                     | 4              |
| CalcEvent$MaxOverload                                  | 4              |
| CalcEvent$CPRecoverSpeed                               | 4              |
| CategoryChangeEvent                                    | 4              |
| OreDictionary$OreRegisterEvent                         | 4              |
| ColorHandlerEvent$Block                                | 4              |
| EntityEvent$EntityConstructing                         | 4              |
| ExplosionEvent$Detonate                                | 4              |
| SkillLearnEvent                                        | 4              |
| EntityMountEvent                                       | 4              |
| EntityViewRenderEvent$FogDensity                       | 4              |
| LivingSetAttackTargetEvent                             | 4              |
| ColorHandlerEvent$Item                                 | 3              |
| LivingExperienceDropEvent                              | 3              |
| TinkerEvent$OnItemBuilding                             | 3              |
| EnderIOLifecycleEvent$Init$Normal                      | 3              |
| AdvancementEvent                                       | 3              |
| GuiScreenEvent$InitGuiEvent                            | 3              |
| FMLNetworkEvent$ServerConnectionFromClientEvent        | 3              |
| RenderHandEvent                                        | 3              |
| GuiContainerEvent$DrawForeground                       | 3              |
| RenderLivingEvent$Specials$Post                        | 3              |
| Apotheosis$ApotheosisRecipeEvent                       | 3              |
| ItemExpireEvent                                        | 3              |
| PopulateChunkEvent$Pre                                 | 3              |
| PlayerContainerEvent$Open                              | 3              |
| PlayerEvent$ItemSmeltedEvent                           | 3              |
| ArmSwingSpeedEvent                                     | 3              |
| ArrowLooseEvent                                        | 3              |
| RenderBlockOverlayEvent                                | 3              |
| TinkerToolEvent$OnBowShoot                             | 3              |
| EntityViewRenderEvent$RenderFogEvent                   | 3              |
| ConfigChangedEvent                                     | 3              |
| EntityFilterRegisterEvent$MegaTorch                    | 3              |
| PlayerEvent$StopTracking                               | 3              |
| ConfigModifyEvent                                      | 3              |
| LevelChangeEvent                                       | 3              |
| GuiScreenEvent$ActionPerformedEvent$Post               | 2              |
| PresetSwitchEvent                                      | 2              |
| RenderTooltipEvent$Pre                                 | 2              |
| ChunkWatchEvent$Watch                                  | 2              |
| OverloadEvent                                          | 2              |
| RayShootingEvent                                       | 2              |
| AnvilRepairEvent                                       | 2              |
| NetworkEvent$ConnectedTile                             | 2              |
| LivingDestroyBlockEvent                                | 2              |
| PlayerJoinedWorldEvent                                 | 2              |
| EntityViewRenderEvent$FOVModifier                      | 2              |
| LivingEquipmentChangeEvent                             | 2              |
| InputEvent$MouseInputEvent                             | 2              |
| GuiScreenEvent$DrawScreenEvent$Pre                     | 2              |
| EnderIOLifecycleEvent$Config$Post                      | 2              |
| CommandEvent                                           | 2              |
| GuiScreenEvent$KeyboardInputEvent                      | 2              |
| EnderIOLifecycleEvent$Config$Pre                       | 2              |
| NetworkEvent$DisconnectedTile                          | 2              |
| GuiScreenEvent$PotionShiftEvent                        | 2              |
| InitMapGenEvent                                        | 2              |
| EntityViewRenderEvent$CameraSetup                      | 2              |
| RenderItemInFrameEvent                                 | 2              |
| LivingSpawnEvent$AllowDespawn                          | 2              |
| TinkerRegisterEvent$MeltingRegisterEvent               | 2              |
| PlayerInteractEvent$LeftClickEmpty                     | 2              |
| RenderGameOverlayEvent$Text                            | 2              |
| PlayerPickupXpEvent                                    | 2              |
| ChunkWatchEvent$UnWatch                                | 2              |
| BonemealEvent                                          | 2              |
| GetCollisionBoxesEvent                                 | 2              |
| ZombieEvent$SummonAidEvent                             | 2              |
| BlockEvent$NeighborNotifyEvent                         | 2              |
| DecorateBiomeEvent$Decorate                            | 2              |
| UpdateFogEvent                                         | 2              |
| CoinThrowEvent                                         | 2              |
| RegisterPacketEvent                                    | 2              |
| PlayerEvent$NameFormat                                 | 2              |
| NoteBlockEvent$Play                                    | 1              |
| LootingLevelEvent                                      | 1              |
| EquipmentChangedEvent                                  | 1              |
| ChunkGeneratorEvent$ReplaceBiomeBlocks                 | 1              |
| OverlayToggleEvent                                     | 1              |
| MinecartUpdateEvent                                    | 1              |
| MekanismAPI$BoxBlacklistEvent                          | 1              |
| InventoryTrackEvent                                    | 1              |
| TeleportEntityEvent                                    | 1              |
| EntityEvent$EnteringChunk                              | 1              |
| RenderAllEntityEvent                                   | 1              |
| CollectEntityDataEvent                                 | 1              |
| FMLNetworkEvent$ServerDisconnectionFromClientEvent     | 1              |
| DynamicNetwork$ClientTickUpdate                        | 1              |
| ProjectileImpactEvent                                  | 1              |
| GasNetwork$GasTransferEvent                            | 1              |
| PlayerWakeUpEvent                                      | 1              |
| AbilityActivateEvent                                   | 1              |
| MatterUnitHarvestEvent                                 | 1              |
| DrawMultipartHighlightEvent                            | 1              |
| NetworkEvent$DisconnectedNetwork                       | 1              |
| ClientChatReceivedEvent                                | 1              |
| SkillExpAddedEvent                                     | 1              |
| LivingKnockBackEvent                                   | 1              |
| FlushControlEvent                                      | 1              |
| RocketEvent$RocketLaunchEvent                          | 1              |
| PlayerEvent$LoadFromFile                               | 1              |
| LivingSpawnEvent$SpecialSpawn                          | 1              |
| SoundLoadEvent                                         | 1              |
| SurvivalTabClickEvent                                  | 1              |
| AmadronRetrievalEvent                                  | 1              |
| TinkerEvent$OnToolPartReplacement                      | 1              |
| ScreenshotEvent                                        | 1              |
| NetworkPartEvent$RemovedPart                           | 1              |
| DecorateBiomeEvent$Pre                                 | 1              |
| PlayerBrewedPotionEvent                                | 1              |
| EnderIOLifecycleEvent$Init$Post                        | 1              |
| DroneSuicideEvent                                      | 1              |
| GuiScreenEvent$BackgroundDrawnEvent                    | 1              |
| AnalyticSkillEvent                                     | 1              |
| RenderGameOverlayEvent$BossInfo                        | 1              |
| BlockEvent$CropGrowEvent$Pre                           | 1              |
| EnderIOLifecycleEvent$PostInit$Post                    | 1              |
| BlockDestroyEvent                                      | 1              |
| SpecialVariableRetrievalEvent$CoordinateVariable$Drone | 1              |
| TransformCategoryEvent                                 | 1              |
| LivingEntityUseItemEvent$Stop                          | 1              |
| RocketEvent$RocketDeOrbitingEvent                      | 1              |
| PotionColorCalculationEvent                            | 1              |
| PresetUpdateEvent                                      | 1              |
| TinkerToolEvent$OnScytheHarvest                        | 1              |
| AbilityDeactivateEvent                                 | 1              |
| UseHoeEvent                                            | 1              |
| TinkerRegisterEvent$ModifierRegisterEvent              | 1              |
| EntityFilterRegisterEvent$DreadLamp                    | 1              |
| TPSkillHelper$TPCritHitEvent                           | 1              |
| BiomeEvent$GetFoliageColor                             | 1              |
| BiomeEvent$GetGrassColor                               | 1              |
| NetworkEvent$ConnectedLocalProvider                    | 1              |
| DecorateBiomeEvent$Post                                | 1              |
| PlayerSetSpawnEvent                                    | 1              |
| TinkerToolEvent$OnShovelMakePath                       | 1              |
| RenderGameOverlayEvent$Chat                            | 1              |
| UnlinkUserEvent                                        | 1              |
| DynamicNetwork$TransmittersAddedEvent                  | 1              |
| BlockEvent$FarmlandTrampleEvent                        | 1              |
| AnimalTameEvent                                        | 1              |
| EntityTravelToDimensionEvent                           | 1              |
| NetworkEvent$ConnectedNetwork                          | 1              |
| ExplosionEvent$Start                                   | 1              |
| DynamicNetwork$NetworkClientRequest                    | 1              |
| ConfigChangedEvent$PostConfigChangedEvent              | 1              |
| TinkerToolEvent$ExtraBlockBreak                        | 1              |
| LinkNodeEvent                                          | 1              |
| DestroyNetworkEvent                                    | 1              |
| FluidNetwork$FluidTransferEvent                        | 1              |
| InfoEvent$ListChanged                                  | 1              |
| AnalyticLevelUpEvent                                   | 1              |
| InputUpdateEvent                                       | 1              |
| BiomeEvent$GetWaterColor                               | 1              |
| EntityStruckByLightningEvent                           | 1              |
| CriticalHitEvent                                       | 1              |
| PlaySoundSourceEvent                                   | 1              |
| LightOverlayReloadHandlerEvent                         | 1              |
| TinkerToolEvent$OnRepair                               | 1              |
| EnergyNetwork$EnergyTransferEvent                      | 1              |
| PopulateChunkEvent$Populate                            | 1              |
| EventExcavate$Pre                                      | 1              |
| PreRenderShadersEvent                                  | 1              |
| TutorialActivatedEvent                                 | 1              |
| ConfigRegistryFactory$RebornRegistryEvent              | 1              |
| LinkUserEvent                                          | 1              |
| GameRuleChangeEvent                                    | 1              |
| ArrowNockEvent                                         | 1              |
| SplashPotionEvent                                      | 1              |
| EntityTrackEvent                                       | 1              |
| PlayerEvent$HarvestCheck                               | 1              |
| NetworkCableEvent$AddedCable                           | 1              |
| NetworkEvent$DisconnectedLocalProvider                 | 1              |
| ProjectileEvent$OnHitBlock                             | 1              |
| CTGUIEvent                                             | 1              |
| ServerChatEvent                                        | 1              |
| LivingEntityUseItemEvent                               | 1              |
| PlayerSPPushOutOfBlocksEvent                           | 1              |
| NetworkPartEvent$AddedPart                             | 1              |
| CreateNetworkEvent                                     | 1              |
| BlockEvent$EntityPlaceEvent                            | 1              |
| TinkerRegisterEvent$TableCastingRegisterEvent          | 1              |
| InfoEvent$InfoChanged                                  | 1              |
| SoundEvent$SoundSourceEvent                            | 1              |
| TextureCollectedEvent                                  | 1              |
| UnlinkNodeEvent                                        | 1              |
| EnchantmentLevelSetEvent                               | 1              |
| GuiScreenEvent$ActionPerformedEvent                    | 1              |
| PlayerContainerEvent                                   | 1              |
| EditModeToggleEvent                                    | 1              |
| NetworkCableEvent$RemovedCable                         | 1              |
| ModSoundRegisterEvent                                  | 1              |
| ChangePassEvent                                        | 1              |
| TinkerToolEvent$OnMattockHoe                           | 1              |


## GuiScreenEvent$ActionPerformedEvent$Post
| Owner              | Method          | Location                               | Priority | Source                           | RecieveCanceled |
|--------------------|-----------------|----------------------------------------|----------|----------------------------------|-----------------|
| Chunk Pregenerator | onButtonPressed | pregenerator.impl.client.ClientHandler | normal   | Chunk Pregenerator V1.12-2.2.jar | false           |
| ReAuth             | action          | technicianlp.reauth.GuiHandler         | normal   | reauth-3.6.0.jar                 | false           |


## NoteBlockEvent$Play
| Owner | Method          | Location                                      | Priority | Source             | RecieveCanceled |
|-------|-----------------|-----------------------------------------------|----------|--------------------|-----------------|
| Quark | noteBlockPlayed | vazkii.quark.misc.feature.NoteBlocksMobSounds | normal   | Quark-r1.6-177.jar | false           |


## ChunkDataEvent$Load
| Owner                         | Method               | Location                                                  | Priority | Source                                             | RecieveCanceled |
|-------------------------------|----------------------|-----------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | onChunkLoad          | me.desht.pneumaticcraft.common.semiblock.SemiBlockManager | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Chunk Pregenerator            | onChunkLoad          | pregenerator.impl.retrogen.RetrogenHandler                | normal   | Chunk Pregenerator V1.12-2.2.jar                   | false           |
| RFTools                       | handleChunkLoadEvent | mcjty.rftools.world.RFToolsWorldGenerator                 | normal   | rftools-1.12-7.72.jar                              | false           |
| Draconic Evolution            | chunkLoadEvent       | com.brandon3055.draconicevolution.world.DEWorldGenHandler | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| Mekanism                      | onChunkDataLoad      | mekanism.common.Mekanism                                  | normal   | Mekanism-1.12.2-9.8.3.390.jar                      | false           |
| Tech Reborn                   | onChunkLoad          | TechRebornRetroGen{chunksToRetroGen=[]}                   | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar        | false           |
| Chisel                        | onChunkLoad          | INSTANCE                                                  | normal   | Chisel-MC1.12.2-1.0.1.44.jar                       | false           |
| The Betweenlands              | onChunkRead          | thebetweenlands.common.handler.WorldEventHandler          | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |


## AnvilUpdateEvent
| Owner                | Method           | Location                                                        | Priority | Source                              | RecieveCanceled |
|----------------------|------------------|-----------------------------------------------------------------|----------|-------------------------------------|-----------------|
| Anvil Patch - Lawful | anvilUpdate      | lumberwizard.anvilpatch.common.AnvilEventHandler                | lowest   | anvilpatch-1.0.0.jar                | false           |
| Apotheosis           | anvilEvent       | shadows.ench.EnchModule                                         | normal   | Apotheosis-1.12.2-1.12.4.jar        | false           |
| Quark                | onAnvilUpdate    | vazkii.quark.vanity.feature.DyeItemNames                        | normal   | Quark-r1.6-177.jar                  | false           |
| Building Gadgets     | onAnvilUpdate    | com.direwolf20.buildinggadgets.common.events.AnvilRepairHandler | normal   | BuildingGadgets-2.7.4.jar           | false           |
| Quark                | onAnvilUpdate    | vazkii.quark.misc.feature.AncientTomes                          | normal   | Quark-r1.6-177.jar                  | false           |
| Quark                | onAnvilUpdate    | vazkii.quark.misc.feature.ColorRunes                            | normal   | Quark-r1.6-177.jar                  | false           |
| Ender IO             | handleAnvilEvent | crazypants.enderio.base.handler.darksteel.DarkSteelRepairRecipe | normal   | EnderIO-1.12.2-5.1.55.jar           | false           |
| Bookshelf            | onAnvilUpdate    | net.darkhax.bookshelf.Bookshelf                                 | normal   | Bookshelf-1.12.2-2.3.590.jar        | false           |
| The Betweenlands     | onAnvilUpdate    | thebetweenlands.common.handler.AnvilEventHandler                | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| Ender IO             | handleAnvilEvent | crazypants.enderio.base.loot.AnvilCapacitorRecipe               | normal   | EnderIO-1.12.2-5.1.55.jar           | false           |
| Ender IO             | handleAnvilEvent | Block{minecraft:air}                                            | normal   | EnderIO-1.12.2-5.1.55.jar           | false           |


## LootingLevelEvent
| Owner              | Method    | Location                                 | Priority | Source                           | RecieveCanceled |
|--------------------|-----------|------------------------------------------|----------|----------------------------------|-----------------|
| Tinkers' Construct | onLooting | slimeknights.tconstruct.tools.ToolEvents | normal   | TConstruct-1.12.2-2.13.0.171.jar | false           |


## PresetSwitchEvent
| Owner      | Method         | Location | Priority | Source                 | RecieveCanceled |
|------------|----------------|----------|----------|------------------------|-----------------|
| lambdalib2 | onSwitchPreset | null     | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | presetSwitch   | instance | normal   | AcademyCraft-1.1.2.jar | false           |


## TickEvent$ServerTickEvent
| Owner                         | Method            | Location                                                         | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-------------------|------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| CodeChicken Lib               | onTickEnd         | codechicken.lib.internal.CCLLog                                  | lowest   | CodeChickenLib-1.12.2-3.2.3.358-universal.jar      | false           |
| OreExcavation                 | onTick            | oreexcavation.handlers.EventHandler                              | normal   | OreExcavation-1.4.143.jar                          | false           |
| lambdalib2                    | __onServerTick    | instance                                                         | normal   | AcademyCraft-1.1.2.jar                             | false           |
| Advanced Rocketry             | onTick            | zmaster587.advancedRocketry.event.CableTickHandler               | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| Unloader                      | tick              | com.unnoen.unloader.UnloadHandler                                | normal   | unloader-1.2.0.jar                                 | false           |
| Practical Logistics 2         | onServerTick      | sonar.logistics.base.events.PL2Events                            | normal   | practicallogistics2-1.12.2-3.0.8-11.jar            | false           |
| Tick Dynamic                  | tickEventEnd      | com.wildex999.tickdynamic.TickDynamicMod                         | lowest   | TickDynamic-1.12.2-1.0.2.jar                       | false           |
| The Betweenlands              | onServerTick      | thebetweenlands.common.world.storage.OfflinePlayerHandlerImpl    | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Academy Monster               | onTick            | cn.paindar.academymonster.core.support.tile.AbilityInterfManager | normal   | AcademyMonster-1.1.0a.jar                          | false           |
| Ender IO                      | onServerTick      | crazypants.enderio.base.item.spawner.BrokenSpawnerHandler        | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| The Betweenlands              | onServerTick      | thebetweenlands.common.world.biome.spawning.WorldMobSpawner      | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| The Betweenlands              | onServerTick      | thebetweenlands.common.handler.WorldEventHandler                 | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| MCMultiPart                   | onServerTick      | mcmultipart.client.MCMPClientProxy                               | normal   | MCMultiPart-2.5.3.jar                              | false           |
| Ender IO                      | onServerTick      | crazypants.enderio.base.handler.ServerTickHandler                | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Ender IO                      | onServerTick      | crazypants.enderio.base.teleport.ChunkTicket                     | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| PneumaticCraft: Repressurized | tickAll           | me.desht.pneumaticcraft.common.TemporaryBlockManager             | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onServerTick      | me.desht.pneumaticcraft.common.event.HackTickHandler             | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Stackie                       | tick              | com.github.lunatrius.stackie.handler.StackingHandlerTick         | normal   | Stackie-1.12.2-1.6.0.48-universal.jar              | false           |
| Advanced Rocketry             | onServerTick      | zmaster587.advancedRocketry.stations.SpaceObjectManager          | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| Advanced Rocketry             | tick              | zmaster587.advancedRocketry.event.PlanetEventHandler             | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| Chunk Pregenerator            | onServerTickEvent | pregenerator.impl.processor.ChunkProcessor                       | normal   | Chunk Pregenerator V1.12-2.2.jar                   | false           |
| Chunk Pregenerator            | onServerTickEvent | pregenerator.impl.processor.deleter.DeleteProcessor              | normal   | Chunk Pregenerator V1.12-2.2.jar                   | false           |
| Minecraft Forge               | onServerTick      | net.minecraftforge.common.ForgeInternalHandler                   | normal   | forge-1.12.2-14.23.5.2847.jar                      | false           |
| TorchMaster                   | onGlobalTick      | net.xalcon.torchmaster.common.EventHandlerServer                 | normal   | torchmaster_1.12.2-1.8.1.81.jar                    | false           |
| PneumaticCraft: Repressurized | onServerTickEnd   | me.desht.pneumaticcraft.common.event.TickHandlerPneumaticCraft   | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Advanced Rocketry             | tick              | zmaster587.advancedRocketry.event.PlanetEventHandler             | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| lambdalib2                    | onServerTick      | cn.academy.energy.impl.WirelessSystem                            | normal   | AcademyCraft-1.1.2.jar                             | false           |
| The Betweenlands              | onServerTick      | thebetweenlands.common.handler.EnvironmentEventOverridesHandler  | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Tick Dynamic                  | tickEventStart    | com.wildex999.tickdynamic.TickDynamicMod                         | highest  | TickDynamic-1.12.2-1.0.2.jar                       | false           |
| Chunk Pregenerator            | onServerTick      | pregenerator.impl.tracking.ServerTracker                         | normal   | Chunk Pregenerator V1.12-2.2.jar                   | false           |
| Mekanism                      | onTick            | Registry:
[]                                                     | normal   | Mekanism-1.12.2-9.8.3.390.jar                      | false           |
| Ender IO                      | onTick            | crazypants.enderio.base.init.TickTimer                           | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Vulpes library                | tick              | zmaster587.libVulpes.LibVulpes                                   | normal   | LibVulpes-1.12.2-0.4.2-69-universal.jar            | false           |
| Draconic Evolution            | serverTick        | com.brandon3055.draconicevolution.handlers.DEEventHandler        | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| LLibrary                      | onServerTickEvent | INSTANCE                                                         | normal   | llibrary-1.7.19-1.12.2.jar                         | false           |
| Ender IO                      | onServerTick      | crazypants.enderio.base.TileEntityEio                            | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Brandon's Core                | onServerTick      | com.brandon3055.brandonscore.handlers.ProcessHandler             | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar       | false           |
| EnderCore                     | onServerTick      | com.enderio.core.common.util.Scheduler                           | normal   | EnderCore-1.12.2-0.5.73.jar                        | false           |
| The Betweenlands              | onServerTickEvent | thebetweenlands.common.capability.base.EntityCapabilityHandler   | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Aroma1997Core                 | serverTick        | aroma1997.core.util.registry.TickRegistry$3                      | normal   | Aroma1997Core-1.12.2-2.0.0.2.jar                   | false           |
| PneumaticCraft: Repressurized | onServerTick      | me.desht.pneumaticcraft.common.semiblock.SemiBlockManager        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## EquipmentChangedEvent
| Owner            | Method            | Location                                                   | Priority | Source                              | RecieveCanceled |
|------------------|-------------------|------------------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onEquipmentChange | thebetweenlands.client.handler.equipment.RadialMenuHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |


## ChunkGeneratorEvent$ReplaceBiomeBlocks
| Owner               | Method                  | Location                                                       | Priority | Source                               | RecieveCanceled |
|---------------------|-------------------------|----------------------------------------------------------------|----------|--------------------------------------|-----------------|
| Dimensional Control | onBiomeBlockReplacement | com.bloodnbonesgaming.dimensionalcontrol.events.DCEventHandler | highest  | DimensionalControl-1.12.2-2.13.0.jar | false           |


## OverlayToggleEvent
| Owner             | Method          | Location                     | Priority | Source                    | RecieveCanceled |
|-------------------|-----------------|------------------------------|----------|---------------------------|-----------------|
| Just Enough Items | onOverlayToggle | mezz.jei.gui.GuiEventHandler | normal   | jei_1.12.2-4.15.0.291.jar | false           |


## MinecartUpdateEvent
| Owner | Method           | Location                                     | Priority | Source             | RecieveCanceled |
|-------|------------------|----------------------------------------------|----------|--------------------|-----------------|
| Quark | onMinecartUpdate | vazkii.quark.automation.feature.ChainLinkage | normal   | Quark-r1.6-177.jar | false           |


## RenderTooltipEvent$Pre
| Owner        | Method           | Location                                         | Priority | Source                           | RecieveCanceled |
|--------------|------------------|--------------------------------------------------|----------|----------------------------------|-----------------|
| Quark        | drawTooltipEvent | vazkii.quark.client.feature.ChestSearchBar       | normal   | Quark-r1.6-177.jar               | false           |
| MoreOverlays | onRenderTooltip  | at.feldim2425.moreoverlays.itemsearch.GuiHandler | normal   | moreoverlays-1.15.1-mc1.12.2.jar | false           |


## MekanismAPI$BoxBlacklistEvent
| Owner    | Method            | Location                 | Priority | Source                        | RecieveCanceled |
|----------|-------------------|--------------------------|----------|-------------------------------|-----------------|
| Mekanism | onBlacklistUpdate | mekanism.common.Mekanism | normal   | Mekanism-1.12.2-9.8.3.390.jar | false           |


## ChunkWatchEvent$Watch
| Owner                 | Method         | Location                                                 | Priority | Source                                  | RecieveCanceled |
|-----------------------|----------------|----------------------------------------------------------|----------|-----------------------------------------|-----------------|
| The Betweenlands      | onWatchChunk   | thebetweenlands.common.handler.WorldEventHandler         | normal   | TheBetweenlands-3.5.5-universal.jar     | false           |
| Practical Logistics 2 | onChunkWatched | sonar.logistics.core.tiles.displays.DisplayViewerHandler | normal   | practicallogistics2-1.12.2-3.0.8-11.jar | false           |


## InventoryTrackEvent
| Owner                         | Method              | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|---------------------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | onInventoryTracking | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## TickEvent$ClientTickEvent
| Owner                         | Method              | Location                                                            | Priority | Source                                             | RecieveCanceled |
|-------------------------------|---------------------|---------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Obsidian Animator             | onClientTick        | pl.asie.foamfix.client.ModelLoaderCleanup$Ticker                    | normal   | ObsidianAnimator_v1.0.0.jar                        | false           |
| Reborn Core                   | onClientTick        | reborncore.common.multiblock.MultiblockClientTickHandler            | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar         | false           |
| Ender IO                      | onTick              | crazypants.enderio.base.init.TickTimer                              | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| MoreOverlays                  | onClientTick        | at.feldim2425.moreoverlays.chunkbounds.ChunkBoundsHandler           | normal   | moreoverlays-1.15.1-mc1.12.2.jar                   | false           |
| CodeChicken Lib               | clientTick          | codechicken.lib.render.CCRenderEventHandler                         | normal   | CodeChickenLib-1.12.2-3.2.3.358-universal.jar      | false           |
| TorchMaster                   | onGlobalTickEvent   | net.xalcon.torchmaster.client.EventHandlerClient                    | normal   | torchmaster_1.12.2-1.8.1.81.jar                    | false           |
| Ender IO                      | onClientTick        | crazypants.enderio.base.handler.darksteel.DarkSteelController       | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Toast Control                 | clientTick          | shadows.toaster.ToastControl                                        | normal   | Toast Control-1.12.2-1.8.1.jar                     | false           |
| Quark                         | clientTick          | vazkii.quark.tweaks.feature.AutomaticRecipeUnlock                   | normal   | Quark-r1.6-177.jar                                 | false           |
| Building Gadgets              | onClientTick        | com.direwolf20.buildinggadgets.client.events.EventClientTick        | normal   | BuildingGadgets-2.7.4.jar                          | false           |
| The Betweenlands              | onClientTick        | thebetweenlands.client.handler.ArmSwingSpeedHandler                 | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| CodeChicken Lib               | onTickEnd           | codechicken.lib.internal.CCLLog                                     | lowest   | CodeChickenLib-1.12.2-3.2.3.358-universal.jar      | false           |
| The Betweenlands              | onClientTick        | thebetweenlands.client.handler.WorldRenderHandler                   | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| lambdalib2                    | onClientTick        | INSTANCE                                                            | normal   | AcademyCraft-1.1.2.jar                             | false           |
| FoamFix                       | clientTick          | pl.asie.foamfix.common.WorldNuller$ClientNuller                     | normal   | foamfix-0.10.10-1.12.2.jar                         | false           |
| The Betweenlands              | onClientTick        | thebetweenlands.common.handler.FoodSicknessHandler                  | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| The Betweenlands              | onClientTick        | thebetweenlands.common.item.misc.ItemBarkAmulet                     | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| The Betweenlands              | onClientTick        | thebetweenlands.common.handler.PlayerPortalHandler                  | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| EnderCore                     | onClientTick        | com.enderio.core.client.handlers.ClientHandler                      | normal   | EnderCore-1.12.2-0.5.73.jar                        | false           |
| LLibrary                      | onClientUpdate      | INSTANCE                                                            | normal   | llibrary-1.7.19-1.12.2.jar                         | false           |
| Bookshelf                     | onClientTick        | net.darkhax.bookshelf.Bookshelf                                     | normal   | Bookshelf-1.12.2-2.3.590.jar                       | false           |
| Chunk Pregenerator            | onClientTickEvent   | pregenerator.impl.client.ClientHandler                              | normal   | Chunk Pregenerator V1.12-2.2.jar                   | false           |
| Draconic Evolution            | tickEnd             | com.brandon3055.draconicevolution.client.handler.ClientEventHandler | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| Custom Main Menu              | tick                | lumien.custommainmenu.handler.CMMEventHandler                       | normal   | CustomMainMenu-MC1.12.2-2.0.9.1.jar                | false           |
| Ender IO                      | onClientTick        | crazypants.enderio.base.item.darksteel.upgrade.sound.SoundDetector  | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Brandon's Core                | tickEnd             | com.brandon3055.brandonscore.client.BCClientEventHandler            | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar       | false           |
| The Betweenlands              | onClientTick        | thebetweenlands.client.handler.FogHandler                           | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                         | clientTick          | vazkii.quark.oddities.feature.Backpacks                             | normal   | Quark-r1.6-177.jar                                 | false           |
| Brandon's Core                | clientTick          | com.brandon3055.brandonscore.utils.BCProfiler                       | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar       | false           |
| lambdalib2                    | __onClientTick      | instance                                                            | normal   | AcademyCraft-1.1.2.jar                             | false           |
| Advanced Rocketry             | tickClient          | zmaster587.advancedRocketry.event.PlanetEventHandler                | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| The Betweenlands              | onClientTick        | thebetweenlands.common.world.event.EventSpoopy                      | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| PneumaticCraft: Repressurized | tickEnd             | me.desht.pneumaticcraft.client.AreaShowManager                      | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Quark                         | onTick              | vazkii.quark.management.feature.ChangeHotbarKeybind                 | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | clientTick          | vazkii.quark.client.feature.UsageTicker                             | normal   | Quark-r1.6-177.jar                                 | false           |
| The Betweenlands              | onClientTick        | thebetweenlands.client.render.sky.BLSkyRenderer                     | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| MoreOverlays                  | onClientTick        | at.feldim2425.moreoverlays.lightoverlay.LightOverlayHandler         | normal   | moreoverlays-1.15.1-mc1.12.2.jar                   | false           |
| lambdalib2                    | onClientTick        | instance                                                            | normal   | AcademyCraft-1.1.2.jar                             | false           |
| Quark                         | clientTick          | vazkii.quark.misc.feature.ExtraPotions                              | normal   | Quark-r1.6-177.jar                                 | false           |
| Smooth Font                   | onClientTick        | bre.smoothfont.handler.TimerEventHandler                            | normal   | SmoothFont-mc1.12.2-2.1.1.jar                      | false           |
| The Betweenlands              | onClientTick        | thebetweenlands.client.handler.ElixirClientHandler                  | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                         | update              | vazkii.quark.management.feature.StoreToChests                       | normal   | Quark-r1.6-177.jar                                 | false           |
| PneumaticCraft: Repressurized | tickEnd             | me.desht.pneumaticcraft.client.ClientTickHandler                    | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| The Betweenlands              | onTick              | thebetweenlands.common.handler.EnvironmentEventHandler              | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Ender IO                      | onClientTick        | crazypants.enderio.base.TileEntityEio                               | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| The Betweenlands              | onTick              | thebetweenlands.client.handler.BrightnessHandler                    | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| The Betweenlands              | onClientTick        | thebetweenlands.common.handler.WorldEventHandler                    | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Patchouli                     | clientTickEnd       | vazkii.patchouli.client.base.ClientTicker                           | normal   | Patchouli-1.0-20.jar                               | false           |
| The Betweenlands              | onTick              | thebetweenlands.client.handler.equipment.RadialMenuHandler          | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                         | update              | vazkii.quark.management.feature.DeleteItems                         | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | onClientTick        | vazkii.quark.client.feature.AutoJumpHotkey                          | normal   | Quark-r1.6-177.jar                                 | false           |
| Minecraft Forge               | checkSettings       | net.minecraftforge.common.ForgeInternalHandler                      | normal   | forge-1.12.2-14.23.5.2847.jar                      | false           |
| Patchouli                     | onTick              | vazkii.patchouli.client.base.ClientAdvancements                     | normal   | Patchouli-1.0-20.jar                               | false           |
| Academy Monster               | clientTick          | cn.lambdalib2.auxgui.AuxGuiHandler                                  | normal   | AcademyMonster-1.1.0a.jar                          | false           |
| FoamFix                       | onTick              | pl.asie.foamfix.coremod.VertexLighterOverrideHandler                | normal   | foamfix-0.10.10-1.12.2.jar                         | false           |
| Obsidian Animator             | onClientTick        | com.dabigjoe.obsidianAnimator.EventHandler                          | normal   | ObsidianAnimator_v1.0.0.jar                        | false           |
| The Betweenlands              | onTick              | thebetweenlands.client.handler.MusicHandler                         | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Academy Craft                 | _onEvent            | cn.academy.util.ACKeyManager                                        | normal   | AcademyCraft-1.1.2.jar                             | false           |
| TextureFix                    | onClientTick        | texFix.TextureFix                                                   | normal   | TexFix V-1.12-4.0.jar                              | false           |
| OreExcavation                 | onClientTick        | oreexcavation.handlers.EventHandler                                 | normal   | OreExcavation-1.4.143.jar                          | false           |
| Advanced Rocketry             | tickClient          | zmaster587.advancedRocketry.event.PlanetEventHandler                | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| The Betweenlands              | onClientTick        | thebetweenlands.util.RenderUtils                                    | lowest   | TheBetweenlands-3.5.5-universal.jar                | false           |
| AutoRegLib                    | clientTickEnd       | vazkii.arl.util.ClientTicker                                        | normal   | AutoRegLib-1.3-32.jar                              | false           |
| PneumaticCraft: Repressurized | onClientTick        | me.desht.pneumaticcraft.common.semiblock.SemiBlockManager           | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| MoreOverlays                  | onClientTick        | at.feldim2425.moreoverlays.itemsearch.GuiHandler                    | normal   | moreoverlays-1.15.1-mc1.12.2.jar                   | false           |
| Quark                         | onTick              | vazkii.quark.client.feature.BetterFireEffect                        | normal   | Quark-r1.6-177.jar                                 | false           |
| lambdalib2                    | __onClientTick      | instance                                                            | normal   | AcademyCraft-1.1.2.jar                             | false           |
| Ender IO                      | onClientTick        | crazypants.enderio.base.teleport.TravelController                   | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Academy Craft                 | _onEvent            | cn.lambdalib2.input.KeyManager                                      | normal   | AcademyCraft-1.1.2.jar                             | false           |
| Ender IO                      | onTick              | crazypants.enderio.base.paint.YetaUtil                              | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Patchouli                     | onClientTick        | vazkii.patchouli.client.handler.MultiblockVisualizationHandler      | normal   | Patchouli-1.0-20.jar                               | false           |
| Draconic Evolution            | tick                | com.brandon3055.draconicevolution.lib.DEImageHandler                | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| Practical Logistics 2         | onClientTick        | sonar.logistics.base.events.PL2Events                               | normal   | practicallogistics2-1.12.2-3.0.8-11.jar            | false           |
| The Betweenlands              | onClientTick        | thebetweenlands.common.world.event.EventWinter                      | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Mekanism                      | onTick              | mekanism.client.ClientTickHandler                                   | normal   | Mekanism-1.12.2-9.8.3.390.jar                      | false           |
| Scannable                     | onClientTick        | INSTANCE                                                            | normal   | Scannable-MC1.12.2-1.6.3.24.jar                    | false           |
| Brandon's Core                | clientTick          | com.brandon3055.brandonscore.client.particle.BCEffectHandler        | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar       | false           |
| The Betweenlands              | onClientTick        | thebetweenlands.client.handler.CameraPositionHandler                | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Brandon's Core                | onClientTick        | com.brandon3055.brandonscore.client.ProcessHandlerClient            | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar       | false           |
| The Betweenlands              | onTick              | thebetweenlands.client.handler.ThemHandler                          | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Aroma1997Core                 | clientTick          | aroma1997.core.util.registry.TickRegistry$4                         | normal   | Aroma1997Core-1.12.2-2.0.0.2.jar                   | false           |
| JourneyMap                    | onClientTick        | journeymap.client.forge.event.StateTickHandler                      | normal   | journeymap-1.12.2-5.5.6.jar                        | false           |
| EnderCore                     | onClientTick        | com.enderio.core.common.util.Scheduler                              | normal   | EnderCore-1.12.2-0.5.73.jar                        | false           |
| CodeChicken Lib               | clientTick          | codechicken.lib.internal.ExceptionMessageEventHandler               | normal   | CodeChickenLib-1.12.2-3.2.3.358-universal.jar      | false           |
| Techguns                      | TickParticleSystems | techguns.events.TGTickHandler                                       | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| Academy Monster               | onTick              | cn.paindar.academymonster.network.NetworkManager                    | normal   | AcademyMonster-1.1.0a.jar                          | false           |
| Just Enough Items             | onClientTick        | mezz.jei.gui.GuiEventHandler                                        | normal   | jei_1.12.2-4.15.0.291.jar                          | false           |
| Quark                         | clientUpdateTick    | vazkii.quark.automation.feature.ChainLinkage                        | normal   | Quark-r1.6-177.jar                                 | false           |
| The Betweenlands              | onClientTick        | thebetweenlands.client.handler.ScreenRenderHandler                  | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |


## ColorHandlerEvent$Item
| Owner                         | Method                    | Location                                     | Priority | Source                                             | RecieveCanceled |
|-------------------------------|---------------------------|----------------------------------------------|----------|----------------------------------------------------|-----------------|
| AutoRegLib                    | onItemColorRegister       | vazkii.arl.util.ModelHandler                 | normal   | AutoRegLib-1.3-32.jar                              | false           |
| PneumaticCraft: Repressurized | registerItemColorHandlers | me.desht.pneumaticcraft.common.item.Itemss   | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Minecraft Forge               | registerItemHandlers      | net.minecraftforge.client.ForgeClientHandler | normal   | forge-1.12.2-14.23.5.2847.jar                      | false           |


## TeleportEntityEvent
| Owner             | Method     | Location                                                               | Priority | Source                    | RecieveCanceled |
|-------------------|------------|------------------------------------------------------------------------|----------|---------------------------|-----------------|
| Ender IO Machines | onTeleport | crazypants.enderio.machines.machine.obelisk.inhibitor.InhibitorHandler | normal   | EnderIO-1.12.2-5.1.55.jar | false           |


## LivingExperienceDropEvent
| Owner        | Method                 | Location                                              | Priority | Source                       | RecieveCanceled |
|--------------|------------------------|-------------------------------------------------------|----------|------------------------------|-----------------|
| Apotheosis   | xp                     | shadows.potion.PotionModule                           | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |
| InControl    | onLivingExperienceDrop | mcjty.incontrol.ForgeEventHandlers                    | lowest   | incontrol-1.12-3.9.16.jar    | false           |
| Ice and Fire | onXpDrop               | slimeknights.tconstruct.tools.traits.TraitEstablished | normal   | iceandfire-1.8.3.jar         | false           |


## EntityEvent$EnteringChunk
| Owner | Method       | Location                                         | Priority | Source             | RecieveCanceled |
|-------|--------------|--------------------------------------------------|----------|--------------------|-----------------|
| Quark | onEnterChunk | vazkii.quark.misc.feature.EndermitesIntoShulkers | normal   | Quark-r1.6-177.jar | false           |


## ChunkEvent$Unload
| Owner                         | Method        | Location                                                  | Priority | Source                                             | RecieveCanceled |
|-------------------------------|---------------|-----------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Chunk Pregenerator            | onChunkUnload | pregenerator.impl.retrogen.RetrogenHandler                | normal   | Chunk Pregenerator V1.12-2.2.jar                   | false           |
| ICBM-Classic                  | chunkUnload   | icbm.classic.lib.radar.RadarRegistry                      | normal   | ICBM-classic-1.12.2-3.3.0b63.jar                   | false           |
| PneumaticCraft: Repressurized | onChunkUnLoad | me.desht.pneumaticcraft.common.semiblock.SemiBlockManager | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| The Betweenlands              | onChunkUnload | thebetweenlands.common.item.misc.ItemRingOfGathering      | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Minecraft Forge               | onChunkUnload | net.minecraftforge.common.ForgeInternalHandler            | normal   | forge-1.12.2-14.23.5.2847.jar                      | false           |
| The Betweenlands              | onChunkUnload | thebetweenlands.common.handler.WorldEventHandler          | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| JourneyMap                    | onChunkUnload | INSTANCE                                                  | normal   | journeymap-1.12.2-5.5.6.jar                        | false           |


## RenderAllEntityEvent
| Owner         | Method           | Location                     | Priority | Source                 | RecieveCanceled |
|---------------|------------------|------------------------------|----------|------------------------|-----------------|
| Academy Craft | postRenderEntity | cn.lambdalib2.util.DebugDraw | normal   | AcademyCraft-1.1.2.jar | false           |


## WorldEvent$Unload
| Owner                         | Method                   | Location                                                                   | Priority | Source                                                    | RecieveCanceled |
|-------------------------------|--------------------------|----------------------------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| Tick Dynamic                  | onDimensionUnload        | com.wildex999.tickdynamic.WorldEventHandler                                | lowest   | TickDynamic-1.12.2-1.0.2.jar                              | false           |
| ICBM-Classic                  | onWorldUnload            | icbm.classic.content.missile.MissileTrackerHandler                         | normal   | ICBM-classic-1.12.2-3.3.0b63.jar                          | false           |
| ICBM-Classic                  | worldUnload              | icbm.classic.lib.radio.RadioRegistry                                       | normal   | ICBM-classic-1.12.2-3.3.0b63.jar                          | false           |
| Ender IO                      | onWorldUnload            | crazypants.enderio.base.teleport.ChunkTicket                               | normal   | EnderIO-1.12.2-5.1.55.jar                                 | false           |
| Just Enough Dimensions        | onWorldUnload            | fi.dy.masa.justenoughdimensions.event.JEDEventHandler                      | normal   | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |
| Brandon's Core                | onWorldClose             | com.brandon3055.brandonscore.client.ProcessHandlerClient                   | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar              | false           |
| Just Enough Items             | onWorldUnload            | thebetweenlands.compat.jei.DynamicJEIRecipeHandler                         | normal   | jei_1.12.2-4.15.0.291.jar                                 | false           |
| Tech Reborn                   | onWorldUnload            | reborncore.common.multiblock.MultiblockEventHandler                        | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar               | false           |
| Draconic Evolution            | worldUnload              | com.brandon3055.draconicevolution.world.WorldTickHandler                   | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar        | false           |
| Epic Siege Mod                | onWorldUnload            | funwayguy.epicsiegemod.handlers.entities.GeneralEntityHandler              | normal   | EpicSiegeMod-13.165.jar                                   | false           |
| The Betweenlands              | onWorldUnload            | thebetweenlands.common.handler.BossHandler                                 | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |
| Minecraft Forge               | onDimensionUnload        | net.minecraftforge.common.ForgeInternalHandler                             | highest  | forge-1.12.2-14.23.5.2847.jar                             | false           |
| JourneyMap                    | onUnload                 | journeymap.client.forge.event.WorldEventHandler                            | normal   | journeymap-1.12.2-5.5.6.jar                               | false           |
| PneumaticCraft: Repressurized | onWorldUnloaded          | me.desht.pneumaticcraft.common.util.GlobalTileEntityCacheManager           | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar        | false           |
| ICBM-Classic                  | worldUnload              | icbm.classic.content.explosive.ExplosiveHandler                            | normal   | ICBM-classic-1.12.2-3.3.0b63.jar                          | false           |
| Reborn Core                   | worldUnloaded            | reborncore.client.multiblock.MultiblockRenderEvent                         | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar                | false           |
| ICBM-Classic                  | onWorldUnload            | icbm.classic.content.explosive.thread2.BlockEditHandler                    | normal   | ICBM-classic-1.12.2-3.3.0b63.jar                          | false           |
| ICBM-Classic                  | worldUnload              | icbm.classic.lib.radar.RadarRegistry                                       | normal   | ICBM-classic-1.12.2-3.3.0b63.jar                          | false           |
| Ender IO Powertools           | unload                   | crazypants.enderio.powertools.machine.capbank.network.ClientNetworkManager | normal   | EnderIO-1.12.2-5.1.55.jar                                 | false           |
| Refined Storage               | onPlayerLogoutEvent      | com.raoulvdberge.refinedstorage.proxy.ProxyClient                          | normal   | refinedstorage-1.6.15.jar                                 | false           |
| Mekanism                      | onWorldUnload            | mekanism.common.Mekanism                                                   | normal   | Mekanism-1.12.2-9.8.3.390.jar                             | false           |
| Advanced Rocketry             | worldUnloadEvent         | zmaster587.advancedRocketry.event.PlanetEventHandler                       | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar           | false           |
| Chunk Pregenerator            | onWorldUnload            | pregenerator.impl.tracking.ServerTracker                                   | normal   | Chunk Pregenerator V1.12-2.2.jar                          | false           |
| Chunk Pregenerator            | onWorldUnload            | pregenerator.impl.structure.StructureManager                               | normal   | Chunk Pregenerator V1.12-2.2.jar                          | false           |
| OreExcavation                 | onWorldUnload            | oreexcavation.handlers.EventHandler                                        | normal   | OreExcavation-1.4.143.jar                                 | false           |
| Progressive Automation        | onPlayerChangedDimension | com.vanhal.progressiveautomation.client.events.EventRenderWorld            | normal   | ProgressiveAutomation-1.12.2-1.7.8.jar                    | false           |
| Ender IO                      | unload                   | crazypants.enderio.base.render.util.DynaTextureProvider$Unloader           | normal   | EnderIO-1.12.2-5.1.55.jar                                 | false           |
| PneumaticCraft: Repressurized | onWorldUnload            | me.desht.pneumaticcraft.common.TemporaryBlockManager                       | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar        | false           |
| Advanced Rocketry             | worldUnloadEvent         | zmaster587.advancedRocketry.event.PlanetEventHandler                       | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar           | false           |
| JourneyMap                    | onWorldUnload            | INSTANCE                                                                   | normal   | journeymap-1.12.2-5.5.6.jar                               | false           |
| Reborn Core                   | onWorldUnload            | reborncore.common.multiblock.MultiblockEventHandler                        | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar                | false           |
| QuarryPlus                    | onWorldUnload            | com.yogpc.qp.QuarryPlus                                                    | normal   | AdditionalEnchantedMiner-1.12.2-12.3.0-universal.jar      | false           |
| FoamFix                       | queueWorldNull           | pl.asie.foamfix.common.WorldNuller$ClientNuller                            | normal   | foamfix-0.10.10-1.12.2.jar                                | false           |
| The Betweenlands              | onWorldUnload            | thebetweenlands.client.handler.AmbienceSoundPlayHandler                    | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |
| The Betweenlands              | onWorldUnload            | thebetweenlands.common.world.storage.OfflinePlayerHandlerImpl              | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |


## LivingEntityUseItemEvent$Finish
| Owner         | Method                           | Location                                      | Priority | Source                        | RecieveCanceled |
|---------------|----------------------------------|-----------------------------------------------|----------|-------------------------------|-----------------|
| Quark         | finishEvent                      | vazkii.quark.tweaks.feature.StackableItems    | normal   | Quark-r1.6-177.jar            | false           |
| CraftTweaker2 | onLivingEntityUseItemFinishEvent | crafttweaker.mc1120.events.CommonEventHandler | normal   | CraftTweaker2-1.12-4.1.20.jar | false           |
| Alchemistry   | finishItemEvent                  | al132.alchemistry.EventHandler                | normal   | alchemistry-1.0.36.jar        | false           |
| NuclearCraft  | onFoodEaten                      | nc.handler.ItemUseHandler                     | normal   | NuclearCraft-2.18o-1.12.2.jar | false           |


## BlockEvent$BreakEvent
| Owner              | Method               | Location                                                            | Priority | Source                                          | RecieveCanceled |
|--------------------|----------------------|---------------------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| Reborn Core        | breakBlock           | reborncore.client.multiblock.MultiblockRenderEvent                  | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar      | false           |
| Techguns           | onBlockBreakEvent    | techguns.events.TGEventHandler                                      | high     | techguns-1.12.2-2.0.2.0_pre3.1.jar              | false           |
| Ice and Fire       | onBlockBreak         | slimeknights.tconstruct.tools.traits.TraitEstablished               | normal   | iceandfire-1.8.3.jar                            | false           |
| Chisel             | onBlockBreak         | team.chisel.common.item.ChiselController                            | normal   | Chisel-MC1.12.2-1.0.1.44.jar                    | false           |
| Advanced Rocketry  | onBlockBroken        | zmaster587.advancedRocketry.event.CableTickHandler                  | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| RFTools            | onBlockBreakEvent    | mcjty.rftools.blocks.blockprotector.BlockProtectorEventHandlers     | normal   | rftools-1.12-7.72.jar                           | false           |
| Ice and Fire       | onBreakBlock         | com.github.alexthe666.iceandfire.event.EventLiving                  | normal   | iceandfire-1.8.3.jar                            | false           |
| Refined Storage    | onBlockBreak         | com.raoulvdberge.refinedstorage.apiimpl.network.NetworkNodeListener | normal   | refinedstorage-1.6.15.jar                       | false           |
| Compact Machines 3 | preventBreakingInHub | org.dave.compactmachines3.skyworld.SkyWorldEvents                   | normal   | compactmachines3-1.12.2-3.0.18-b278.jar         | false           |
| EnderCore          | handleBlockBreak     | com.enderio.core.common.handlers.XPBoostHandler                     | normal   | EnderCore-1.12.2-0.5.73.jar                     | false           |
| Ender IO           | onBreakEvent         | crazypants.enderio.base.item.spawner.BrokenSpawnerHandler           | normal   | EnderIO-1.12.2-5.1.55.jar                       | false           |
| The Betweenlands   | onBlockBreak         | thebetweenlands.common.handler.BlockBreakHandler                    | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| The Betweenlands   | onBlockBreak         | thebetweenlands.common.handler.AdvancementHandler                   | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| Quark              | onBlockBroken        | vazkii.quark.tweaks.feature.HoeSickle                               | normal   | Quark-r1.6-177.jar                              | false           |
| CraftTweaker2      | onBlockBreakEvent    | crafttweaker.mc1120.events.CommonEventHandler                       | normal   | CraftTweaker2-1.12-4.1.20.jar                   | false           |
| The Betweenlands   | onBlockBreak         | thebetweenlands.common.block.farming.BlockGenericDugSoil            | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| OreExcavation      | onBlockBreak         | oreexcavation.handlers.EventHandler                                 | lowest   | OreExcavation-1.4.143.jar                       | false           |
| The Betweenlands   | onBlockBreak         | thebetweenlands.common.handler.LocationHandler                      | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| Tinkers' Construct | blockBreak           | slimeknights.tconstruct.tools.TraitEvents                           | normal   | TConstruct-1.12.2-2.13.0.171.jar                | false           |


## PlayerEvent$PlayerLoggedOutEvent
| Owner                         | Method               | Location                                                          | Priority | Source                                             | RecieveCanceled |
|-------------------------------|----------------------|-------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Chunk Pregenerator            | onPlayerLoggedOut    | pregenerator.ChunkPregenerator                                    | normal   | Chunk Pregenerator V1.12-2.2.jar                   | false           |
| CraftTweaker2                 | onPlayerLoggedOut    | crafttweaker.mc1120.events.CommonEventHandler                     | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Ice and Fire                  | onPlayerLeaveEvent   | com.github.alexthe666.iceandfire.event.EventLiving                | normal   | iceandfire-1.8.3.jar                               | false           |
| Storage Drawers               | onPlayerDisconnect   | com.jaquadro.minecraft.storagedrawers.StorageDrawers              | normal   | StorageDrawers-1.12.2-5.4.0.jar                    | false           |
| Advanced Rocketry             | onPlayerLoggedOut    | zmaster587.libVulpes.util.InputSyncHandler                        | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| LLibrary                      | onPlayerLogOut       | INSTANCE                                                          | normal   | llibrary-1.7.19-1.12.2.jar                         | false           |
| Mekanism                      | onPlayerLogoutEvent  | mekanism.common.CommonPlayerTracker                               | normal   | Mekanism-1.12.2-9.8.3.390.jar                      | false           |
| Quark                         | onPlayerLogoff       | vazkii.quark.misc.feature.LockDirectionHotkey                     | normal   | Quark-r1.6-177.jar                                 | false           |
| Morpheus                      | loggedOutEvent       | net.quetzi.morpheus.helpers.MorpheusEventHandler                  | normal   | Morpheus-1.12.2-3.5.106.jar                        | false           |
| Quark                         | playerLoggedOut      | vazkii.quark.tweaks.feature.JumpBoostStepAssist                   | normal   | Quark-r1.6-177.jar                                 | false           |
| Anvil Patch - Lawful          | onPlayerDisconnected | lumberwizard.anvilpatch.AnvilPatch                                | normal   | anvilpatch-1.0.0.jar                               | false           |
| PneumaticCraft: Repressurized | onPlayerLeave        | me.desht.pneumaticcraft.common.pneumatic_armor.CommonArmorHandler | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## CollectEntityDataEvent
| Owner    | Method              | Location | Priority | Source                     | RecieveCanceled |
|----------|---------------------|----------|----------|----------------------------|-----------------|
| LLibrary | onCollectEntityData | INSTANCE | normal   | llibrary-1.7.19-1.12.2.jar | false           |


## PlayerEvent$BreakSpeed
| Owner                         | Method                  | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-------------------------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| The Betweenlands              | onBreakSpeed            | thebetweenlands.common.handler.OverworldItemHandler             | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| CraftTweaker2                 | onPlayerBreakSpeedEvent | crafttweaker.mc1120.events.CommonEventHandler                   | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Chisel                        | onBreakSpeed            | team.chisel.common.block.BreakSpeedHandler                      | normal   | Chisel-MC1.12.2-1.0.1.44.jar                       | false           |
| Techguns                      | onBreakEventHigh        | techguns.events.TGEventHandler                                  | high     | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| The Betweenlands              | onBreakSpeed            | thebetweenlands.common.handler.ArmorHandler                     | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Techguns                      | onBreakEvent            | techguns.events.TGEventHandler                                  | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| Draconic Evolution            | getBreakSpeed           | com.brandon3055.draconicevolution.handlers.DEEventHandler       | low      | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| Apotheosis                    | breakSpeed              | shadows.ench.EnchModule                                         | normal   | Apotheosis-1.12.2-1.12.4.jar                       | false           |
| PneumaticCraft: Repressurized | breakSpeedCheck         | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticArmor | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Ender IO                      | onBreakSpeedEvent       | crazypants.enderio.base.item.darksteel.ItemDarkSteelShears      | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Tech Reborn                   | getBreakSpeedEvent      | techreborn.events.BlockBreakHandler                             | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar        | false           |
| Ender IO                      | onBreakSpeedEvent       | crazypants.enderio.base.item.darksteel.ItemDarkSteelAxe         | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Ender IO Conduits             | onBreakSpeed            | crazypants.enderio.conduits.handler.ConduitBreakSpeedHandler    | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Ender IO                      | onBreakSpeedEvent       | crazypants.enderio.base.item.darksteel.ItemDarkSteelAxe         | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| The Betweenlands              | onBreakSpeed            | thebetweenlands.common.handler.ElixirCommonHandler              | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| The Betweenlands              | onBreakSpeed            | thebetweenlands.common.handler.LocationHandler                  | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                         | calcBreakSpeed          | vazkii.quark.tweaks.feature.AxesBreakLeaves                     | normal   | Quark-r1.6-177.jar                                 | false           |
| Tinkers' Construct            | offhandBreakSpeed       | slimeknights.tconstruct.library.tools.DualToolHarvestUtils      | normal   | TConstruct-1.12.2-2.13.0.171.jar                   | false           |
| Reborn Core                   | getDigSpeed             | reborncore.common.blocks.BlockWrenchEventHandler                | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar         | false           |
| Tinkers' Construct            | mineSpeed               | slimeknights.tconstruct.tools.TraitEvents                       | normal   | TConstruct-1.12.2-2.13.0.171.jar                   | false           |


## FMLNetworkEvent$ServerDisconnectionFromClientEvent
| Owner           | Method               | Location | Priority | Source                    | RecieveCanceled |
|-----------------|----------------------|----------|----------|---------------------------|-----------------|
| Academy Monster | __onServerDisconnect | instance | normal   | AcademyMonster-1.1.0a.jar | false           |


## DynamicNetwork$ClientTickUpdate
| Owner    | Method             | Location                 | Priority | Source                        | RecieveCanceled |
|----------|--------------------|--------------------------|----------|-------------------------------|-----------------|
| Mekanism | onClientTickUpdate | mekanism.common.Mekanism | normal   | Mekanism-1.12.2-9.8.3.390.jar | false           |


## FillBucketEvent
| Owner                         | Method             | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|--------------------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| CraftTweaker2                 | onPlayerFillBucket | crafttweaker.mc1120.events.CommonEventHandler                   | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Vulpes library                | onBucketFill       | zmaster587.libVulpes.event.BucketHandler                        | normal   | LibVulpes-1.12.2-0.4.2-69-universal.jar            | false           |
| PneumaticCraft: Repressurized | onFillBucket       | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Minecraft Forge               | onFillBucket       | net.minecraftforge.fluids.UniversalBucket                       | low      | forge-1.12.2-14.23.5.2847.jar                      | false           |
| The Betweenlands              | onFillBucket       | thebetweenlands.common.item.tools.ItemSpecificBucket            | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |


## ProjectileImpactEvent
| Owner        | Method         | Location                                           | Priority | Source               | RecieveCanceled |
|--------------|----------------|----------------------------------------------------|----------|----------------------|-----------------|
| Ice and Fire | onArrowCollide | com.github.alexthe666.iceandfire.event.EventLiving | normal   | iceandfire-1.8.3.jar | false           |


## WorldEvent$CreateSpawnPosition
| Owner                  | Method             | Location                                                       | Priority | Source                                                    | RecieveCanceled |
|------------------------|--------------------|----------------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| Compact Machines 3     | createSpawnPoint   | org.dave.compactmachines3.skyworld.SkyWorldEvents              | normal   | compactmachines3-1.12.2-3.0.18-b278.jar                   | false           |
| The Lost Cities        | onCreateSpawnPoint | mcjty.lostcities.ForgeEventHandlers                            | normal   | lostcities-1.12-2.0.21.jar                                | false           |
| Just Enough Dimensions | onWorldCreateSpawn | fi.dy.masa.justenoughdimensions.event.JEDEventHandler          | high     | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |
| Dimensional Control    | onCreateSpawn      | com.bloodnbonesgaming.dimensionalcontrol.events.DCEventHandler | normal   | DimensionalControl-1.12.2-2.13.0.jar                      | false           |


## TickEvent$WorldTickEvent
| Owner                         | Method                     | Location                                                            | Priority | Source                                             | RecieveCanceled |
|-------------------------------|----------------------------|---------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| OMLib                         | tickEvent                  | omtteam.omlib.handler.OMLibEventHandler                             | normal   | omlib-1.12.2-3.1.3-246.jar                         | false           |
| ICBM-Classic                  | onWorldTick                | icbm.classic.content.explosive.thread2.BlockEditHandler             | normal   | ICBM-classic-1.12.2-3.3.0b63.jar                   | false           |
| RFTools                       | onPostWorldTick            | mcjty.rftools.ForgeEventHandlers                                    | normal   | rftools-1.12-7.72.jar                              | false           |
| Tick Dynamic                  | worldTickEvent             | com.wildex999.tickdynamic.WorldEventHandler                         | normal   | TickDynamic-1.12.2-1.0.2.jar                       | false           |
| NuclearCraft                  | updateRadiationEnvironment | nc.radiation.environment.RadiationEnvironmentHandler                | normal   | NuclearCraft-2.18o-1.12.2.jar                      | false           |
| Chunk Pregenerator            | onWorldTick                | pregenerator.impl.tracking.ServerTracker                            | normal   | Chunk Pregenerator V1.12-2.2.jar                   | false           |
| NuclearCraft                  | updateWorldRadiation       | nc.radiation.RadiationHandler                                       | normal   | NuclearCraft-2.18o-1.12.2.jar                      | false           |
| ICBM-Classic                  | onWorldTick                | icbm.classic.content.missile.MissileTrackerHandler                  | normal   | ICBM-classic-1.12.2-3.3.0b63.jar                   | false           |
| RFTools                       | tickEnd                    | mcjty.rftools.world.WorldTickHandler                                | normal   | rftools-1.12-7.72.jar                              | false           |
| Mekanism                      | onTick                     | mekanism.common.CommonWorldTickHandler                              | normal   | Mekanism-1.12.2-9.8.3.390.jar                      | false           |
| Refined Storage               | onWorldTick                | com.raoulvdberge.refinedstorage.apiimpl.network.NetworkNodeListener | normal   | refinedstorage-1.6.15.jar                          | false           |
| The Betweenlands              | onWorldTick                | thebetweenlands.common.handler.EnvironmentEventHandler              | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| The Betweenlands              | onWorldTick                | thebetweenlands.common.handler.WorldEventHandler                    | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| InControl                     | onWorldTick                | mcjty.incontrol.ForgeEventHandlers                                  | normal   | incontrol-1.12-3.9.16.jar                          | false           |
| Advanced Rocketry             | serverTickEvent            | zmaster587.advancedRocketry.event.PlanetEventHandler                | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| Ender IO                      | onWorldTick                | crazypants.enderio.base.material.material.MaterialCraftingHandler   | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| ICBM-Classic                  | worldUpdateTick            | icbm.classic.lib.radar.RadarRegistry                                | normal   | ICBM-classic-1.12.2-3.3.0b63.jar                   | false           |
| PneumaticCraft: Repressurized | onWorldTickEnd             | me.desht.pneumaticcraft.common.event.TickHandlerPneumaticCraft      | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Quark                         | onWorldTick                | vazkii.quark.automation.feature.PistonsMoveTEs                      | normal   | Quark-r1.6-177.jar                                 | false           |
| Reborn Core                   | onWorldTick                | reborncore.common.multiblock.MultiblockServerTickHandler            | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar         | false           |
| PneumaticCraft: Repressurized | worldTick                  | me.desht.pneumaticcraft.common.event.HackTickHandler                | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Tech Reborn                   | onWorldTick                | reborncore.common.multiblock.MultiblockServerTickHandler            | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar        | false           |
| Draconic Evolution            | tickEnd                    | com.brandon3055.draconicevolution.world.WorldTickHandler            | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| Tech Reborn                   | onWorldTick                | TechRebornRetroGen{chunksToRetroGen=[]}                             | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar        | false           |
| RFTools                       | onWorldTick                | mcjty.rftools.ForgeEventHandlers                                    | normal   | rftools-1.12-7.72.jar                              | false           |
| Morpheus                      | worldTickEvent             | net.quetzi.morpheus.helpers.MorpheusEventHandler                    | normal   | Morpheus-1.12.2-3.5.106.jar                        | false           |
| Aroma1997Core                 | worldTick                  | aroma1997.core.util.registry.TickRegistry$1                         | normal   | Aroma1997Core-1.12.2-2.0.0.2.jar                   | false           |
| PneumaticCraft: Repressurized | onWorldTick                | me.desht.pneumaticcraft.common.semiblock.SemiBlockManager           | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Advanced Rocketry             | serverTickEvent            | zmaster587.advancedRocketry.event.PlanetEventHandler                | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |


## RenderGameOverlayEvent$Pre
| Owner                         | Method                   | Location                                                               | Priority | Source                                             | RecieveCanceled |
|-------------------------------|--------------------------|------------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Mantle                        | renderHealthbar          | slimeknights.mantle.client.ExtraHeartRenderHandler                     | low      | Mantle-1.12-1.3.3.55.jar                           | false           |
| The Betweenlands              | onRenderGameOverlay      | thebetweenlands.client.handler.ScreenRenderHandler                     | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| PneumaticCraft: Repressurized | renderFirstPersonMinigun | me.desht.pneumaticcraft.client.ClientEventHandler                      | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Scannable                     | onPreRenderGameOverlay   | INSTANCE                                                               | normal   | Scannable-MC1.12.2-1.6.3.24.jar                    | false           |
| Better Combat Rebirth         | onRenderGameOverlay      | bettercombat.mod.client.handler.EventHandlersClient                    | lowest   | BetterCombat-1.12.2-1.5.6.jar                      | true            |
| Tinkers' Construct            | onCrosshairRender        | slimeknights.tconstruct.library.client.crosshair.CrosshairRenderEvents | normal   | TConstruct-1.12.2-2.13.0.171.jar                   | false           |
| Scannable                     | onPreRenderGameOverlay   | INSTANCE                                                               | normal   | Scannable-MC1.12.2-1.6.3.24.jar                    | false           |
| The One Probe                 | renderGameOverlayEvent   | mcjty.theoneprobe.proxy.ClientProxy                                    | highest  | theoneprobe-1.12-1.4.28.jar                        | true            |
| Quark                         | onRenderHUD              | vazkii.quark.client.feature.ImprovedMountHUD                           | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | hudPre                   | vazkii.quark.management.feature.ChangeHotbarKeybind                    | normal   | Quark-r1.6-177.jar                                 | false           |
| SCPCraft:Decline              | renderGameOverlayPre     | com.grandlovania.scp.SCPGuiOverlay                                     | normal   | SCPCraft-1.1-Decline.jar                           | false           |


## OverloadEvent
| Owner         | Method           | Location                              | Priority | Source                 | RecieveCanceled |
|---------------|------------------|---------------------------------------|----------|------------------------|-----------------|
| lambdalib2    | __onOverload     | instance                              | normal   | AcademyCraft-1.1.2.jar | false           |
| Academy Craft | onPlayerOverload | cn.academy.advancements.DispatcherAch | normal   | AcademyCraft-1.1.2.jar | false           |


## GasNetwork$GasTransferEvent
| Owner    | Method           | Location                 | Priority | Source                        | RecieveCanceled |
|----------|------------------|--------------------------|----------|-------------------------------|-----------------|
| Mekanism | onGasTransferred | mekanism.common.Mekanism | normal   | Mekanism-1.12.2-9.8.3.390.jar | false           |


## PlayerWakeUpEvent
| Owner      | Method       | Location | Priority | Source                 | RecieveCanceled |
|------------|--------------|----------|----------|------------------------|-----------------|
| lambdalib2 | playerWakeup | instance | normal   | AcademyCraft-1.1.2.jar | false           |


## AttachCapabilitiesEvent
| Owner                         | Method                          | Location                                                         | Priority | Source                                             | RecieveCanceled |
|-------------------------------|---------------------------------|------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| NuclearCraft                  | attachTileRadiationCapability   | nc.capability.radiation.RadiationCapabilityHandler               | normal   | NuclearCraft-2.18o-1.12.2.jar                      | false           |
| lambdalib2                    | onAttachCapabilitiesEntity      | cn.lambdalib2.datapart.CapDataPartHandler                        | normal   | AcademyCraft-1.1.2.jar                             | false           |
| Advanced Rocketry             | attachCapabilities              | zmaster587.advancedRocketry.capability.CapabilityProtectiveArmor | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| Better Combat Rebirth         | onEntityConstruct               | bettercombat.mod.handler.EventHandlers                           | normal   | BetterCombat-1.12.2-1.5.6.jar                      | false           |
| LLibrary                      | onFinishAttachCapabilities      | INSTANCE                                                         | lowest   | llibrary-1.7.19-1.12.2.jar                         | false           |
| PlusTiC                       | addPortalArmorCapability        | landmaster.plustic.api.Portal                                    | normal   | plustic-7.1.6.1.jar                                | false           |
| PneumaticCraft: Repressurized | onEntityConstruction            | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft  | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| The Betweenlands              | onAttachCapabilities            | thebetweenlands.common.capability.base.EntityCapabilityHandler   | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| OMLib                         | onWorldCaps                     | omtteam.omlib.api.permission.OwnerShareRegister                  | normal   | omlib-1.12.2-3.1.3-246.jar                         | false           |
| ICBM-Classic                  | attachCapabilityItem            | icbm.classic.lib.emp.CapabilityEMP                               | normal   | ICBM-classic-1.12.2-3.3.0b63.jar                   | false           |
| EnderCore                     | attachCapabilities              | com.enderio.core.common.tweaks.BottleFluidCapability$BottleTweak | normal   | EnderCore-1.12.2-0.5.73.jar                        | false           |
| Alchemistry                   | onEntityConstructing            | al132.alchemistry.EventHandler                                   | normal   | alchemistry-1.0.36.jar                             | false           |
| NuclearCraft                  | attachEntityRadiationCapability | nc.capability.radiation.RadiationCapabilityHandler               | normal   | NuclearCraft-2.18o-1.12.2.jar                      | false           |
| OMLib                         | onWorldCaps                     | omtteam.omlib.api.permission.GlobalTrustRegister                 | normal   | omlib-1.12.2-3.1.3-246.jar                         | false           |
| McJtyLib                      | onEntityConstructing            | mcjty.lib.McJtyLib$EventHandler                                  | normal   | mcjtylib-1.12-3.5.4.jar                            | false           |
| Quark                         | attachTileCapabilities          | vazkii.quark.base.capability.CapabilityHandler                   | normal   | Quark-r1.6-177.jar                                 | false           |
| NuclearCraft                  | attachChunkRadiationCapability  | nc.capability.radiation.RadiationCapabilityHandler               | normal   | NuclearCraft-2.18o-1.12.2.jar                      | false           |
| Mutant Beasts                 | onAttachEntityCapability        | chumbanotz.mutantbeasts.EventHandler                             | normal   | MutantBeasts-1.12.2-0.6.0.jar                      | false           |
| Tinkers' Construct            | attachCapability                | slimeknights.tconstruct.gadgets.item.ItemPiggybackPack           | normal   | TConstruct-1.12.2-2.13.0.171.jar                   | false           |
| TorchMaster                   | onWorldAttachCapabilityEvent    | net.xalcon.torchmaster.common.EventHandlerServer                 | normal   | torchmaster_1.12.2-1.8.1.81.jar                    | false           |
| RFTools                       | onEntityConstructing            | mcjty.rftools.ForgeEventHandlers                                 | normal   | rftools-1.12-7.72.jar                              | false           |
| LLibrary                      | onAttachCapabilities            | INSTANCE                                                         | highest  | llibrary-1.7.19-1.12.2.jar                         | false           |
| Techguns                      | attachCapabilities              | techguns.capabilities.CapabilityEventHandler                     | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| Quark                         | attachItemCapabilities          | vazkii.quark.base.capability.CapabilityHandler                   | normal   | Quark-r1.6-177.jar                                 | false           |
| Epic Siege Mod                | onAttachCapability              | funwayguy.epicsiegemod.handlers.MainHandler                      | normal   | EpicSiegeMod-13.165.jar                            | false           |
| TOP Addons                    | onAttachCapabilityEntity        | io.github.drmanganese.topaddons.config.capabilities.CapEvents    | normal   | topaddons-1.12.2-1.12.0.jar                        | false           |
| MCMultiPart                   | onAttachTile                    | mekanism.common.integration.multipart.MultipartMekanism          | normal   | MCMultiPart-2.5.3.jar                              | false           |
| The Betweenlands              | onWorldCapability               | thebetweenlands.common.world.storage.WorldStorageImpl$Handler    | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Tinkers Tool Leveling         | onCapabilityAttach              | slimeknights.toolleveling.EntityXpHandler                        | normal   | TinkerToolLeveling-1.12.2-1.1.0.jar                | false           |
| NuclearCraft                  | attachStackRadiationCapability  | nc.capability.radiation.RadiationCapabilityHandler               | normal   | NuclearCraft-2.18o-1.12.2.jar                      | false           |
| PlusTiC                       | addToggleArmorCapability        | landmaster.plustic.api.Toggle                                    | normal   | plustic-7.1.6.1.jar                                | false           |
| Ender IO                      | onWorldCaps                     | crazypants.enderio.base.transceiver.ServerChannelRegister        | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Quark                         | onAttachCapability              | vazkii.quark.management.feature.RightClickAddToShulkerBox        | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | onBoatGenesis                   | vazkii.quark.vanity.feature.BoatSails                            | normal   | Quark-r1.6-177.jar                                 | false           |
| The One Probe                 | onEntityConstructing            | mcjty.theoneprobe.ForgeEventHandlers                             | normal   | theoneprobe-1.12-1.4.28.jar                        | false           |
| Techguns                      | attachCapabilitiesClient        | techguns.capabilities.CapabilityEventHandler                     | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| The Betweenlands              | onAttachCapabilities            | thebetweenlands.common.capability.base.ItemCapabilityHandler     | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |


## EnderTeleportEvent
| Owner                         | Method               | Location                                                                     | Priority | Source                                             | RecieveCanceled |
|-------------------------------|----------------------|------------------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| CraftTweaker2                 | onEnderTeleportEvent | crafttweaker.mc1120.events.CommonEventHandler                                | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Ender IO Machines             | onEnderTeleport      | crazypants.enderio.machines.machine.obelisk.inhibitor.InhibitorHandler       | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| RFTools                       | onEntityTeleport     | mcjty.rftools.ForgeEventHandlers                                             | normal   | rftools-1.12-7.72.jar                              | false           |
| Ice and Fire                  | onEnderTeleport      | slimeknights.tconstruct.tools.traits.TraitEnderference                       | normal   | iceandfire-1.8.3.jar                               | false           |
| Compact Machines 3            | onEnderTeleport      | org.dave.compactmachines3.misc.MachineEventHandler                           | normal   | compactmachines3-1.12.2-3.0.18-b278.jar            | false           |
| PneumaticCraft: Repressurized | onEnderTeleport      | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft              | lowest   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Ender IO                      | onEndermanTeleport   | crazypants.enderio.machines.machine.obelisk.attractor.handlers.EndermanFixer | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Ender IO                      | onEnderTeleport      | crazypants.enderio.base.handler.darksteel.SwordHandler                       | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Epic Siege Mod                | onEnderTeleport      | funwayguy.epicsiegemod.handlers.entities.EndermanHandler                     | normal   | EpicSiegeMod-13.165.jar                            | false           |


## AbilityActivateEvent
| Owner      | Method          | Location | Priority | Source                 | RecieveCanceled |
|------------|-----------------|----------|----------|------------------------|-----------------|
| lambdalib2 | activateAbility | instance | normal   | AcademyCraft-1.1.2.jar | false           |


## RenderLivingEvent$Pre
| Owner                         | Method                 | Location                                                                         | Priority | Source                                             | RecieveCanceled |
|-------------------------------|------------------------|----------------------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Ender IO Machines             | onEntityRender         | crazypants.enderio.machines.machine.teleport.telepad.TeleportEntityRenderHandler | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| The Betweenlands              | onRenderLivingPre      | thebetweenlands.common.handler.PuppetHandler                                     | lowest   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Techguns                      | onRenderLivingEventPre | techguns.events.TGEventHandler                                                   | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| PlusTiC                       | renderBeam             | landmaster.plustic.tools.ToolLaserGun$ProxyClient                                | normal   | plustic-7.1.6.1.jar                                | false           |
| Quark                         | preRenderLiving        | vazkii.quark.vanity.feature.EmoteSystem                                          | highest  | Quark-r1.6-177.jar                                 | false           |
| Ice and Fire                  | onPreRenderLiving      | com.github.alexthe666.iceandfire.event.EventClient                               | normal   | iceandfire-1.8.3.jar                               | false           |
| Quark                         | colorize               | vazkii.quark.world.effects.PotionColorizer                                       | normal   | Quark-r1.6-177.jar                                 | false           |
| Tinkers' Construct            | onRenderPlayer         | slimeknights.tconstruct.tools.ranged.RangedRenderEvents                          | normal   | TConstruct-1.12.2-2.13.0.171.jar                   | false           |
| PneumaticCraft: Repressurized | onLivingRender         | me.desht.pneumaticcraft.client.ClientEventHandler                                | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## MatterUnitHarvestEvent
| Owner         | Method              | Location                              | Priority | Source                 | RecieveCanceled |
|---------------|---------------------|---------------------------------------|----------|------------------------|-----------------|
| Academy Craft | onMatterUnitHarvest | cn.academy.advancements.DispatcherAch | normal   | AcademyCraft-1.1.2.jar | false           |


## PlayerEvent$ItemCraftedEvent
| Owner              | Method              | Location                                                    | Priority | Source                                             | RecieveCanceled |
|--------------------|---------------------|-------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Academy Craft      | onItemCraft         | cn.academy.tutorial.Conditions                              | normal   | AcademyCraft-1.1.2.jar                             | false           |
| Advanced Rocketry  | onCrafting          | zmaster587.advancedRocketry.event.PlanetEventHandler        | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| Mekanism           | onCrafting          | mekanism.common.recipe.BinRecipe                            | normal   | Mekanism-1.12.2-9.8.3.390.jar                      | false           |
| Draconic Evolution | craftEvent          | com.brandon3055.draconicevolution.achievements.Achievements | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| CraftTweaker2      | onPlayerItemCrafted | crafttweaker.mc1120.events.CommonEventHandler               | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Reborn Core        | craft               | reborncore.shields.RebornCoreShields                        | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar         | false           |
| Tinkers' Construct | onCraft             | slimeknights.tconstruct.shared.AchievementEvents            | normal   | TConstruct-1.12.2-2.13.0.171.jar                   | false           |
| Advanced Rocketry  | onCrafting          | zmaster587.advancedRocketry.event.PlanetEventHandler        | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| Techguns           | onCraftEvent        | techguns.events.TGEventHandler                              | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| The Betweenlands   | onItemCrafting      | thebetweenlands.common.handler.AdvancementHandler           | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Academy Craft      | onItemCrafted       | cn.academy.advancements.DispatcherAch                       | normal   | AcademyCraft-1.1.2.jar                             | false           |


## DrawMultipartHighlightEvent
| Owner       | Method                  | Location                                                | Priority | Source                | RecieveCanceled |
|-------------|-------------------------|---------------------------------------------------------|----------|-----------------------|-----------------|
| MCMultiPart | drawBlockHighlightEvent | mekanism.common.integration.multipart.MultipartMekanism | normal   | MCMultiPart-2.5.3.jar | false           |


## PlayerEvent$StartTracking
| Owner            | Method                | Location                                                       | Priority | Source                              | RecieveCanceled |
|------------------|-----------------------|----------------------------------------------------------------|----------|-------------------------------------|-----------------|
| Ender IO         | onTracking            | crazypants.enderio.base.handler.darksteel.StateController      | normal   | EnderIO-1.12.2-5.1.55.jar           | false           |
| LLibrary         | onEntityStartTracking | INSTANCE                                                       | normal   | llibrary-1.7.19-1.12.2.jar          | false           |
| The Betweenlands | onEntityStartTracking | thebetweenlands.common.capability.base.EntityCapabilityHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| Quark            | onStartTracking       | vazkii.quark.world.effects.PotionColorizer                     | normal   | Quark-r1.6-177.jar                  | false           |
| Techguns         | onStartTracking       | techguns.events.TGEventHandler                                 | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar  | false           |
| Quark            | onVehicleSeen         | vazkii.quark.automation.feature.ChainLinkage                   | normal   | Quark-r1.6-177.jar                  | false           |
| Quark            | onBoatSeen            | vazkii.quark.vanity.feature.BoatSails                          | normal   | Quark-r1.6-177.jar                  | false           |


## EnderIOLifecycleEvent$PreInit
| Owner             | Method           | Location                                                              | Priority | Source                    | RecieveCanceled |
|-------------------|------------------|-----------------------------------------------------------------------|----------|---------------------------|-----------------|
| Ender IO          | init             | crazypants.enderio.base.gui.IoConfigRenderer                          | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | init             | crazypants.enderio.base.gui.tooltip.TooltipHandlerFluid               | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | onPreInit        | crazypants.enderio.base.block.charge.EntityPrimedCharge               | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | onPreInit        | crazypants.enderio.base.teleport.ChunkTicket                          | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO Conduits | register         | crazypants.enderio.conduits.autosave.ConduitHandlers                  | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | init             | crazypants.enderio.base.gui.tooltip.TooltipHandlerBurnTime            | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | register         | crazypants.enderio.base.paint.render.PaintRegistry                    | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | onPreInit        | crazypants.enderio.base.item.eggs.EntityOwlEgg                        | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | init             | crazypants.enderio.base.gui.tooltip.TooltipHandlerGrinding            | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | preInit          | crazypants.enderio.base.loot.Loot                                     | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | preInit          | crazypants.enderio.base.transceiver.ServerChannelRegister             | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | create           | crazypants.enderio.base.invpanel.capability.CapabilityDatabaseHandler | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | register         | crazypants.enderio.base.autosave.BaseHandlers                         | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | register         | crazypants.enderio.api.capacitor.CapabilityCapacitorData              | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | create           | crazypants.enderio.base.integration.top.TOPUtil                       | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO Machines | register         | crazypants.enderio.machines.autosave.MachineHandlers                  | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO Conduits | load             | crazypants.enderio.conduits.render.ConduitInOutRenderer               | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | create           | crazypants.enderio.base.filter.capability.CapabilityFilterHolder      | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | onPreInit        | crazypants.enderio.base.machine.entity.EntityFallingMachine           | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO          | registerRegistry | crazypants.enderio.base.init.ModObjectRegistry                        | normal   | EnderIO-1.12.2-5.1.55.jar | false           |


## MouseEvent
| Owner                         | Method       | Location                                                            | Priority | Source                                             | RecieveCanceled |
|-------------------------------|--------------|---------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Mekanism                      | onMouseEvent | mekanism.client.ClientTickHandler                                   | normal   | Mekanism-1.12.2-9.8.3.390.jar                      | false           |
| Better Combat Rebirth         | onMouseEvent | bettercombat.mod.client.handler.EventHandlersClient                 | normal   | BetterCombat-1.12.2-1.5.6.jar                      | true            |
| PneumaticCraft: Repressurized | onMouseEvent | me.desht.pneumaticcraft.client.render.pneumatic_armor.HUDHandler    | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| The Betweenlands              | onMouseInput | thebetweenlands.client.handler.WeedwoodRowboatHandler               | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                         | onMouseInput | vazkii.quark.management.feature.RotateArrowTypes                    | normal   | Quark-r1.6-177.jar                                 | false           |
| Techguns                      | onMouseEvent | techguns.events.TGEventHandler                                      | high     | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| Draconic Evolution            | onMouseInput | com.brandon3055.draconicevolution.client.keybinding.KeyInputHandler | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| The Betweenlands              | onMouseInput | thebetweenlands.client.handler.equipment.RadialMenuHandler          | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Vulpes library                | mouseEvent   | zmaster587.libVulpes.items.ItemProjector                            | normal   | LibVulpes-1.12.2-0.4.2-69-universal.jar            | false           |
| Morph-o-Tool                  | onMouseEvent | vazkii.morphtool.ClientHandler                                      | highest  | Morph-o-Tool-1.2-21.jar                            | false           |
| Ender IO                      | onMouseEvent | crazypants.enderio.base.item.conduitprobe.ToolTickHandler           | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| The Betweenlands              | onMouseClick | thebetweenlands.common.handler.ExtendedReachHandler                 | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |


## NetworkEvent$DisconnectedNetwork
| Owner                 | Method                 | Location                                          | Priority | Source                                  | RecieveCanceled |
|-----------------------|------------------------|---------------------------------------------------|----------|-----------------------------------------|-----------------|
| Practical Logistics 2 | onNetworksDisconnected | sonar.logistics.base.events.LogisticsEventHandler | highest  | practicallogistics2-1.12.2-3.0.8-11.jar | false           |


## RayShootingEvent
| Owner           | Method                  | Location                                         | Priority | Source                    | RecieveCanceled |
|-----------------|-------------------------|--------------------------------------------------|----------|---------------------------|-----------------|
| Academy Monster | onPlayerReflectRayShoot | cn.paindar.academymonster.core.GlobalEventHandle | normal   | AcademyMonster-1.1.0a.jar | false           |
| Academy Monster | onReflect               | cn.paindar.academymonster.ability.AMVecReflect   | normal   | AcademyMonster-1.1.0a.jar | false           |


## WorldEvent$Load
| Owner                         | Method          | Location                                                                 | Priority | Source                                                    | RecieveCanceled |
|-------------------------------|-----------------|--------------------------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| Quark                         | worldLoad       | vazkii.quark.management.gamerule.DropoffGamerule                         | normal   | Quark-r1.6-177.jar                                        | false           |
| Quark                         | onNewWorld      | vazkii.quark.world.feature.Crabs                                         | normal   | Quark-r1.6-177.jar                                        | false           |
| Advanced Rocketry             | worldLoadEvent  | zmaster587.advancedRocketry.event.PlanetEventHandler                     | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar           | false           |
| The Betweenlands              | onWorldLoad     | thebetweenlands.common.handler.BossHandler                               | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |
| Minecraft Forge               | onDimensionLoad | net.minecraftforge.common.ForgeInternalHandler                           | highest  | forge-1.12.2-14.23.5.2847.jar                             | false           |
| LLibrary                      | onWorldLoad     | INSTANCE                                                                 | normal   | llibrary-1.7.19-1.12.2.jar                                | false           |
| Epic Siege Mod                | onWorldLoad     | funwayguy.epicsiegemod.handlers.entities.GeneralEntityHandler            | normal   | EpicSiegeMod-13.165.jar                                   | false           |
| Default Options               | onWorldLoad     | net.blay09.mods.defaultoptions.DefaultDifficultyHandler                  | normal   | DefaultOptions_1.12.2-9.2.8.jar                           | false           |
| Compact Machines 3            | loadWorld       | org.dave.compactmachines3.world.WorldSavedDataMachines                   | normal   | compactmachines3-1.12.2-3.0.18-b278.jar                   | false           |
| Chunk Pregenerator            | onWorldLoad     | pregenerator.impl.structure.StructureManager                             | normal   | Chunk Pregenerator V1.12-2.2.jar                          | false           |
| Chunk Pregenerator            | onWorldLoad     | pregenerator.ChunkPregenerator                                           | normal   | Chunk Pregenerator V1.12-2.2.jar                          | false           |
| Brandon's Core                | worldLoad       | com.brandon3055.brandonscore.client.particle.BCEffectHandler             | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar              | false           |
| Dimensional Control           | onWorldLoad     | com.bloodnbonesgaming.dimensionalcontrol.client.event.ClientEventHandler | normal   | DimensionalControl-1.12.2-2.13.0.jar                      | false           |
| Compact Machines 3            | loadWorld       | org.dave.compactmachines3.skyworld.SkyWorldSavedData                     | normal   | compactmachines3-1.12.2-3.0.18-b278.jar                   | false           |
| The Betweenlands              | onWorldLoad     | thebetweenlands.common.world.storage.OfflinePlayerHandlerImpl            | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |
| Bookshelf                     | onWorldLoaded   | net.darkhax.bookshelf.Bookshelf                                          | normal   | Bookshelf-1.12.2-2.3.590.jar                              | false           |
| Mekanism                      | onWorldLoad     | mekanism.common.Mekanism                                                 | normal   | Mekanism-1.12.2-9.8.3.390.jar                             | false           |
| Just Enough Items             | onWorldLoad     | thebetweenlands.compat.jei.DynamicJEIRecipeHandler                       | normal   | jei_1.12.2-4.15.0.291.jar                                 | false           |
| Obsidian Animator             | onWorldSave     | com.dabigjoe.obsidianAnimator.EventHandler                               | normal   | ObsidianAnimator_v1.0.0.jar                               | false           |
| The Betweenlands              | onWorldLoad     | thebetweenlands.common.handler.EnvironmentEventOverridesHandler          | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |
| Chunk Pregenerator            | onWorldLoad     | pregenerator.impl.tracking.ServerTracker                                 | normal   | Chunk Pregenerator V1.12-2.2.jar                          | false           |
| Just Enough Dimensions        | onWorldLoad     | fi.dy.masa.justenoughdimensions.event.JEDEventHandler                    | high     | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |
| TorchMaster                   | onWorldLoad     | net.xalcon.torchmaster.compat.RegistryBackwardsCompat                    | normal   | torchmaster_1.12.2-1.8.1.81.jar                           | false           |
| PneumaticCraft: Repressurized | onWorldLoad     | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft          | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar        | false           |
| Tick Dynamic                  | onDimensionLoad | com.wildex999.tickdynamic.WorldEventHandler                              | highest  | TickDynamic-1.12.2-1.0.2.jar                              | false           |
| Advanced Rocketry             | worldLoadEvent  | zmaster587.advancedRocketry.event.PlanetEventHandler                     | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar           | false           |


## TinkerEvent$OnItemBuilding
| Owner                 | Method         | Location                                             | Priority | Source                              | RecieveCanceled |
|-----------------------|----------------|------------------------------------------------------|----------|-------------------------------------|-----------------|
| Ice and Fire          | onToolBuilding | slimeknights.tconstruct.tools.traits.TraitCheapskate | normal   | iceandfire-1.8.3.jar                | false           |
| Tinkers Tool Leveling | onToolBuild    | slimeknights.toolleveling.EventHandler               | normal   | TinkerToolLeveling-1.12.2-1.1.0.jar | false           |
| PlusTiC               | onToolBuilding | landmaster.plustic.traits.Light                      | normal   | plustic-7.1.6.1.jar                 | false           |


## InputEvent$KeyInputEvent
| Owner                         | Method              | Location                                                            | Priority | Source                                             | RecieveCanceled |
|-------------------------------|---------------------|---------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Techguns                      | onKeyInput          | techguns.debug.Keybinds                                             | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Smooth Font                   | onKeyInput          | bre.smoothfont.handler.KeyBindEventHandler                          | normal   | SmoothFont-mc1.12.2-2.1.1.jar                      | false           |
| The Betweenlands              | onTickAfterKeyboard | thebetweenlands.client.handler.WeedwoodRowboatHandler               | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Building Gadgets              | onKeyInput          | com.direwolf20.buildinggadgets.client.events.EventKeyInput          | normal   | BuildingGadgets-2.7.4.jar                          | false           |
| PlusTiC                       | testSetPortal       | landmaster.plustic.api.Portal                                       | normal   | plustic-7.1.6.1.jar                                | false           |
| Toast Control                 | keys                | shadows.toaster.ToastControl                                        | normal   | Toast Control-1.12.2-1.8.1.jar                     | false           |
| PlusTiC                       | onInput             | landmaster.plustic.tools.IToggleTool                                | normal   | plustic-7.1.6.1.jar                                | false           |
| The One Probe                 | onKeyInput          | mcjty.theoneprobe.keys.KeyInputHandler                              | normal   | theoneprobe-1.12-1.4.28.jar                        | false           |
| Quark                         | onKeyInput          | vazkii.quark.vanity.feature.EmoteSystem                             | normal   | Quark-r1.6-177.jar                                 | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| MoreOverlays                  | onKeyEvent          | at.feldim2425.moreoverlays.KeyBindings                              | normal   | moreoverlays-1.15.1-mc1.12.2.jar                   | true            |
| Techguns                      | onKeyInput          | techguns.keybind.TGKeybinds                                         | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| RFTools                       | onKeyInput          | mcjty.rftools.keys.KeyInputHandler                                  | normal   | rftools-1.12-7.72.jar                              | false           |
| Draconic Evolution            | onKeyInput          | com.brandon3055.draconicevolution.client.keybinding.KeyInputHandler | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| The Betweenlands              | onKey               | thebetweenlands.client.handler.DebugHandlerClient                   | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Refined Storage Addons        | onKeyInput          | com.raoulvdberge.refinedstorageaddons.proxy.ProxyClient             | normal   | refinedstorageaddons-0.4.4.jar                     | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onKey               | me.desht.pneumaticcraft.client.KeyHandler                           | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| World Stripper                | onKeyInput          | com.ewyboy.worldstripper.client.KeyBindingHandler                   | normal   | World-Stripper-1.6.0-1.12.2.jar                    | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Chunk Pregenerator            | onKeyPressed        | pregenerator.impl.client.ClientHandler                              | normal   | Chunk Pregenerator V1.12-2.2.jar                   | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Quark                         | onKeyInput          | vazkii.quark.client.feature.AutoJumpHotkey                          | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | onKeyInput          | vazkii.quark.management.feature.ChangeHotbarKeybind                 | normal   | Quark-r1.6-177.jar                                 | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| JourneyMap                    | onGameKeyboardEvent | INSTANCE                                                            | normal   | journeymap-1.12.2-5.5.6.jar                        | false           |
| The Betweenlands              | onKeyInput          | thebetweenlands.client.handler.equipment.RadialMenuHandler          | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Refined Storage               | onKeyInput          | com.raoulvdberge.refinedstorage.gui.KeyInputListener                | normal   | refinedstorage-1.6.15.jar                          | false           |
| Quark                         | onKeyInput          | vazkii.quark.misc.feature.LockDirectionHotkey                       | normal   | Quark-r1.6-177.jar                                 | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| LagGoggles                    | onKeyInput          | cf.terminator.laggoggles.client.gui.KeyHandler                      | normal   | LagGoggles-FAT-1.12.2-4.9.jar                      | false           |
| Ender IO                      | onKeyInput          | crazypants.enderio.base.handler.KeyTracker                          | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| PlusTiC                       | testAndToggle       | landmaster.plustic.api.Toggle                                       | normal   | plustic-7.1.6.1.jar                                | false           |
| Advanced Rocketry             | onKeyInput          | zmaster587.advancedRocketry.client.KeyBindings                      | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| PneumaticCraft: Repressurized | onKeyInput          | me.desht.pneumaticcraft.client.gui.widget.GuiKeybindCheckBox        | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## ClientChatReceivedEvent
| Owner      | Method | Location                                       | Priority | Source                      | RecieveCanceled |
|------------|--------|------------------------------------------------|----------|-----------------------------|-----------------|
| JourneyMap | invoke | journeymap.client.forge.event.ChatEventHandler | normal   | journeymap-1.12.2-5.5.6.jar | false           |


## AnvilRepairEvent
| Owner         | Method                   | Location                                      | Priority | Source                        | RecieveCanceled |
|---------------|--------------------------|-----------------------------------------------|----------|-------------------------------|-----------------|
| CraftTweaker2 | onPlayerAnvilRepairEvent | crafttweaker.mc1120.events.CommonEventHandler | normal   | CraftTweaker2-1.12-4.1.20.jar | false           |
| Apotheosis    | applyUnbreaking          | shadows.ench.EnchModule                       | normal   | Apotheosis-1.12.2-1.12.4.jar  | false           |


## NetworkEvent$ConnectedTile
| Owner                 | Method                  | Location                                          | Priority | Source                                  | RecieveCanceled |
|-----------------------|-------------------------|---------------------------------------------------|----------|-----------------------------------------|-----------------|
| Practical Logistics 2 | onWirelessTileConnected | sonar.logistics.base.events.LogisticsEventHandler | lowest   | practicallogistics2-1.12.2-3.0.8-11.jar | false           |
| Practical Logistics 2 | onTileConnected         | sonar.logistics.base.events.LogisticsEventHandler | high     | practicallogistics2-1.12.2-3.0.8-11.jar | false           |


## LivingDestroyBlockEvent
| Owner            | Method               | Location                                                        | Priority | Source                              | RecieveCanceled |
|------------------|----------------------|-----------------------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onLivingDestroyBlock | thebetweenlands.common.handler.LocationHandler                  | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| RFTools          | onLivingDestroyBlock | mcjty.rftools.blocks.blockprotector.BlockProtectorEventHandlers | normal   | rftools-1.12-7.72.jar               | false           |


## SkillExpAddedEvent
| Owner         | Method          | Location                              | Priority | Source                 | RecieveCanceled |
|---------------|-----------------|---------------------------------------|----------|------------------------|-----------------|
| Academy Craft | onSkillExpAdded | cn.academy.advancements.DispatcherAch | normal   | AcademyCraft-1.1.2.jar | false           |


## ChunkEvent$Load
| Owner              | Method           | Location                                             | Priority | Source                                          | RecieveCanceled |
|--------------------|------------------|------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| JourneyMap         | onChunkLoad      | INSTANCE                                             | normal   | journeymap-1.12.2-5.5.6.jar                     | false           |
| Advanced Rocketry  | chunkLoadedEvent | zmaster587.advancedRocketry.event.CableTickHandler   | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| The Betweenlands   | onChunkLoad      | thebetweenlands.common.handler.WorldEventHandler     | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| Advanced Rocketry  | chunkLoadEvent   | zmaster587.advancedRocketry.event.PlanetEventHandler | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Chunk Pregenerator | onChunkLoad      | pregenerator.ChunkPregenerator                       | normal   | Chunk Pregenerator V1.12-2.2.jar                | false           |
| Reborn Core        | onChunkLoad      | reborncore.common.multiblock.MultiblockEventHandler  | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar      | false           |
| Tech Reborn        | onChunkLoad      | reborncore.common.multiblock.MultiblockEventHandler  | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar     | false           |
| Advanced Rocketry  | chunkLoadEvent   | zmaster587.advancedRocketry.event.PlanetEventHandler | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |


## PlayerJoinedWorldEvent
| Owner             | Method                   | Location                                                        | Priority | Source                    | RecieveCanceled |
|-------------------|--------------------------|-----------------------------------------------------------------|----------|---------------------------|-----------------|
| Just Enough Items | onPlayerJoinedWorldEvent | mezz.jei.plugins.vanilla.ingredients.enchant.EnchantedBookCache | normal   | jei_1.12.2-4.15.0.291.jar | false           |
| Just Enough Items | onPlayerJoinedWorldEvent | mezz.jei.ingredients.IngredientFilter                           | normal   | jei_1.12.2-4.15.0.291.jar | false           |


## LivingKnockBackEvent
| Owner            | Method            | Location                                           | Priority | Source                              | RecieveCanceled |
|------------------|-------------------|----------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onEntityKnockback | thebetweenlands.common.handler.AttackDamageHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |


## WorldEvent$PotentialSpawns
| Owner              | Method            | Location                                             | Priority | Source                                          | RecieveCanceled |
|--------------------|-------------------|------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| Advanced Rocketry  | SpawnEntity       | zmaster587.advancedRocketry.event.PlanetEventHandler | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Advanced Rocketry  | SpawnEntity       | zmaster587.advancedRocketry.event.PlanetEventHandler | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Tinkers' Construct | extraSlimeSpawn   | slimeknights.tconstruct.world.WorldEvents            | normal   | TConstruct-1.12.2-2.13.0.171.jar                | false           |
| InControl          | onPotentialSpawns | mcjty.incontrol.ForgeEventHandlers                   | lowest   | incontrol-1.12-3.9.16.jar                       | false           |


## FlushControlEvent
| Owner      | Method       | Location | Priority | Source                 | RecieveCanceled |
|------------|--------------|----------|----------|------------------------|-----------------|
| lambdalib2 | flushControl | instance | normal   | AcademyCraft-1.1.2.jar | false           |


## RocketEvent$RocketLaunchEvent
| Owner             | Method         | Location                                             | Priority | Source                                          | RecieveCanceled |
|-------------------|----------------|------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| Advanced Rocketry | onRocketLaunch | zmaster587.advancedRocketry.event.RocketEventHandler | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |


## TickEvent$RenderTickEvent
| Owner                         | Method            | Location                                                         | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-------------------|------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Patchouli                     | renderTick        | vazkii.patchouli.client.base.ClientTicker                        | normal   | Patchouli-1.0-20.jar                               | false           |
| The Betweenlands              | onRenderTickStart | thebetweenlands.client.handler.CameraPositionHandler             | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                         | renderTick        | vazkii.quark.client.feature.PanoramaMaker                        | normal   | Quark-r1.6-177.jar                                 | false           |
| AutoRegLib                    | renderTick        | vazkii.arl.util.ClientTicker                                     | normal   | AutoRegLib-1.3-32.jar                              | false           |
| LLibrary                      | onRenderUpdate    | INSTANCE                                                         | normal   | llibrary-1.7.19-1.12.2.jar                         | false           |
| Quark                         | renderTick        | vazkii.quark.vanity.feature.EmoteSystem                          | normal   | Quark-r1.6-177.jar                                 | false           |
| Mekanism                      | tickEnd           | mekanism.client.render.RenderTickHandler                         | normal   | Mekanism-1.12.2-9.8.3.390.jar                      | false           |
| Quark                         | renderEvent       | vazkii.quark.world.feature.TreeVariants                          | normal   | Quark-r1.6-177.jar                                 | false           |
| The Betweenlands              | onPreRenderWorld  | thebetweenlands.client.handler.ShaderHandler                     | lowest   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Scannable                     | onPreWorldRender  | INSTANCE                                                         | normal   | Scannable-MC1.12.2-1.6.3.24.jar                    | false           |
| The Betweenlands              | onPostRender      | thebetweenlands.client.handler.ShaderHandler                     | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| PneumaticCraft: Repressurized | renderTick        | me.desht.pneumaticcraft.client.render.pneumatic_armor.HUDHandler | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Compact Machines 3            | onRenderTick      | org.dave.compactmachines3.misc.RenderTickCounter                 | normal   | compactmachines3-1.12.2-3.0.18-b278.jar            | false           |
| PneumaticCraft: Repressurized | tickEnd           | me.desht.pneumaticcraft.client.ClientEventHandler                | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| CodeChicken Lib               | renderTick        | codechicken.lib.render.CCRenderEventHandler                      | normal   | CodeChickenLib-1.12.2-3.2.3.358-universal.jar      | false           |
| Techguns                      | onRenderTick      | techguns.events.TGTickHandler                                    | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| Mouse Tweaks                  | onRenderTick      | yalter.mousetweaks.forge.MouseTweaksForge                        | normal   | MouseTweaks-2.10-mc1.12.2.jar                      | false           |
| The Betweenlands              | onRenderTick      | thebetweenlands.client.handler.WorldRenderHandler                | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| The Betweenlands              | onRender          | thebetweenlands.client.handler.WeedwoodRowboatHandler            | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |


## EnderIOLifecycleEvent$Init$Normal
| Owner    | Method | Location                                                  | Priority | Source                    | RecieveCanceled |
|----------|--------|-----------------------------------------------------------|----------|---------------------------|-----------------|
| Ender IO | init   | crazypants.enderio.base.integration.bigreactors.BRProxy   | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | init   | crazypants.enderio.base.gui.handler.GuiHelper             | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | init   | crazypants.enderio.base.integration.chiselsandbits.CABIMC | normal   | EnderIO-1.12.2-5.1.55.jar | false           |


## PlayerEvent$LoadFromFile
| Owner                  | Method               | Location                                              | Priority | Source                                                    | RecieveCanceled |
|------------------------|----------------------|-------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| Just Enough Dimensions | onPlayerLoadingEvent | fi.dy.masa.justenoughdimensions.event.JEDEventHandler | normal   | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |


## AdvancementEvent
| Owner         | Method                   | Location                                               | Priority | Source                        | RecieveCanceled |
|---------------|--------------------------|--------------------------------------------------------|----------|-------------------------------|-----------------|
| CraftTweaker2 | onPlayerAdvancementEvent | crafttweaker.mc1120.events.CommonEventHandler          | normal   | CraftTweaker2-1.12-4.1.20.jar | false           |
| EnderCore     | onAchievement            | com.enderio.core.common.handlers.FireworkHandler       | normal   | EnderCore-1.12.2-0.5.73.jar   | false           |
| Patchouli     | onAdvancement            | vazkii.patchouli.common.handler.AdvancementSyncHandler | normal   | Patchouli-1.0-20.jar          | false           |


## PlayerEvent$PlayerLoggedInEvent
| Owner                  | Method                | Location                                                        | Priority | Source                                                    | RecieveCanceled |
|------------------------|-----------------------|-----------------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| Epic Siege Mod         | onRespawn             | funwayguy.epicsiegemod.handlers.entities.PlayerHandler          | normal   | EpicSiegeMod-13.165.jar                                   | false           |
| Draconic Evolution     | login                 | com.brandon3055.draconicevolution.handlers.DEEventHandler       | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar        | false           |
| Morpheus               | loggedInEvent         | net.quetzi.morpheus.helpers.MorpheusEventHandler                | normal   | Morpheus-1.12.2-3.5.106.jar                               | false           |
| TorchMaster            | onPlayerLoggedInEvent | net.xalcon.torchmaster.client.EventHandlerClient                | normal   | torchmaster_1.12.2-1.8.1.81.jar                           | false           |
| Scannable              | onPlayerLoggedIn      | INSTANCE                                                        | normal   | Scannable-MC1.12.2-1.6.3.24.jar                           | false           |
| Brandon's Core         | playerLoggedIn        | com.brandon3055.brandonscore.handlers.BCEventHandler            | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar              | false           |
| Building Gadgets       | onPlayerLogin         | com.direwolf20.buildinggadgets.common.events.ConfigEventHandler | normal   | BuildingGadgets-2.7.4.jar                                 | false           |
| Ender IO               | onLogin               | crazypants.enderio.base.handler.darksteel.StateController       | normal   | EnderIO-1.12.2-5.1.55.jar                                 | false           |
| Compact Machines 3     | onLogin               | org.dave.compactmachines3.misc.PlayerEventHandler               | normal   | compactmachines3-1.12.2-3.0.18-b278.jar                   | false           |
| Quark                  | onPlayerLoggedIn      | vazkii.quark.tweaks.feature.AutomaticRecipeUnlock               | normal   | Quark-r1.6-177.jar                                        | false           |
| Quark                  | login                 | vazkii.quark.management.gamerule.DropoffGamerule                | normal   | Quark-r1.6-177.jar                                        | false           |
| PlusTiC                | onPlayerLogin         | landmaster.plustic.api.Toggle                                   | normal   | plustic-7.1.6.1.jar                                       | false           |
| Refined Storage        | onPlayerLoginEvent    | com.raoulvdberge.refinedstorage.proxy.ProxyClient               | normal   | refinedstorage-1.6.15.jar                                 | false           |
| Minecraft Forge        | playerLogin           | forge                                                           | normal   | forge-1.12.2-14.23.5.2847.jar                             | false           |
| Ender IO               | onPlayerLoggon        | crazypants.enderio.base.capacitor.CapacitorKeyRegistry          | normal   | EnderIO-1.12.2-5.1.55.jar                                 | false           |
| NuclearCraft           | configOnWorldLoad     | nc.config.NCConfig$ServerConfigEventHandler                     | normal   | NuclearCraft-2.18o-1.12.2.jar                             | false           |
| EnderCore              | onPlayerJoin          | com.enderio.core.common.handlers.JoinMessageHandler             | normal   | EnderCore-1.12.2-0.5.73.jar                               | false           |
| Chisel                 | onPlayerJoin          | INSTANCE                                                        | normal   | Chisel-MC1.12.2-1.0.1.44.jar                              | false           |
| Mekanism               | onPlayerLoginEvent    | mekanism.common.CommonPlayerTracker                             | normal   | Mekanism-1.12.2-9.8.3.390.jar                             | false           |
| AgriCraft              | onPlayerJoin          | com.infinityraider.agricraft.AgriCraft                          | normal   | AgriCraft-2.12.0-1.12.0-a6.jar                            | false           |
| Chunk Pregenerator     | onPlayerLoggedIn      | pregenerator.ChunkPregenerator                                  | normal   | Chunk Pregenerator V1.12-2.2.jar                          | false           |
| SCPCraft:Decline       | onPlayerLoggedIn      | com.grandlovania.scp.SCPGuiOverlay                              | normal   | SCPCraft-1.1-Decline.jar                                  | false           |
| The One Probe          | onPlayerLoggedIn      | mcjty.theoneprobe.ForgeEventHandlers                            | normal   | theoneprobe-1.12-1.4.28.jar                               | false           |
| Ender IO               | onPlayerLoggon        | info.loenwind.autoconfig.factory.FactoryManager                 | normal   | EnderIO-1.12.2-5.1.55.jar                                 | false           |
| Patchouli              | onLogin               | vazkii.patchouli.common.handler.AdvancementSyncHandler          | normal   | Patchouli-1.0-20.jar                                      | false           |
| CraftTweaker2          | onPlayerLoggedIn      | crafttweaker.mc1120.events.CommonEventHandler                   | normal   | CraftTweaker2-1.12-4.1.20.jar                             | false           |
| EnderCore              | onPlayerLogin         | com.enderio.core.common.config.ConfigHandler$1                  | normal   | EnderCore-1.12.2-0.5.73.jar                               | false           |
| Just Enough Dimensions | onPlayerLoggedIn      | fi.dy.masa.justenoughdimensions.event.JEDEventHandler           | normal   | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |
| Academy Craft          | loginListener         | cn.academy.analytic.AnalyticDataListener                        | normal   | AcademyCraft-1.1.2.jar                                    | false           |
| Tinkers' Construct     | onPlayerLoggedIn      | slimeknights.tconstruct.shared.PlayerDataEvents                 | normal   | TConstruct-1.12.2-2.13.0.171.jar                          | false           |
| Draconic Evolution     | onPlayerLogin         | com.brandon3055.draconicevolution.handlers.ContributorHandler   | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar        | false           |
| Ender IO               | onPlayerLoggon        | crazypants.enderio.base.config.Config                           | normal   | EnderIO-1.12.2-5.1.55.jar                                 | false           |
| The Betweenlands       | onPlayerLogin         | thebetweenlands.common.handler.PlayerJoinWorldHandler           | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |
| Ender IO               | onPlayerLoggon        | crazypants.enderio.base.transceiver.ConnectionHandler           | normal   | EnderIO-1.12.2-5.1.55.jar                                 | false           |
| Academy Craft          | onPlayerLoggedIn      | cn.academy.ACConfig$LoginEvents                                 | normal   | AcademyCraft-1.1.2.jar                                    | false           |
| Anvil Patch - Lawful   | onPlayerConnected     | lumberwizard.anvilpatch.AnvilPatch                              | normal   | anvilpatch-1.0.0.jar                                      | false           |


## LivingEvent$LivingJumpEvent
| Owner                         | Method                  | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-------------------------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | onPlayerJump            | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticArmor | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| The Betweenlands              | onLivingJump            | thebetweenlands.common.handler.ElixirCommonHandler              | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| The Betweenlands              | onEntityJump            | thebetweenlands.common.herblore.elixir.PotionRootBound          | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Tinkers' Construct            | onLivingJump            | slimeknights.tconstruct.shared.BlockEvents                      | normal   | TConstruct-1.12.2-2.13.0.171.jar                   | false           |
| CraftTweaker2                 | onEntityLivingJumpEvent | crafttweaker.mc1120.events.CommonEventHandler                   | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Techguns                      | onLivingJumpEvent       | techguns.events.TGEventHandler                                  | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| Draconic Evolution            | onLivingJumpEvent       | com.brandon3055.draconicevolution.handlers.CustomArmorHandler   | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |


## GuiScreenEvent$KeyboardInputEvent$Pre
| Owner                    | Method             | Location                                   | Priority | Source                               | RecieveCanceled |
|--------------------------|--------------------|--------------------------------------------|----------|--------------------------------------|-----------------|
| Just Enough Items        | onGuiKeyboardEvent | mezz.jei.input.InputHandler                | normal   | jei_1.12.2-4.15.0.291.jar            | false           |
| Quark                    | onKeypress         | vazkii.quark.client.feature.ChestSearchBar | normal   | Quark-r1.6-177.jar                   | false           |
| McJtyLib                 | onKeyboardInput    | mcjty.lib.ClientEventHandler               | normal   | mcjtylib-1.12-3.5.4.jar              | false           |
| Simple Inventory sorting | onKey              | cpw.mods.inventorysorter.KeyHandler        | lowest   | inventorysorter-1.12.2-1.13.3+57.jar | false           |


## GuiOpenEvent
| Owner                         | Method                | Location                                                                 | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-----------------------|--------------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Quark                         | onOpenGUI             | vazkii.quark.management.feature.ChestsInBoats                            | high     | Quark-r1.6-177.jar                                 | false           |
| The Betweenlands              | onGuiOpened           | thebetweenlands.client.gui.menu.GuiBLMainMenu                            | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Controlling                   | guiInit               | us.getfluxed.controlsearch.events.ClientEventHandler                     | normal   | Controlling-3.0.8.jar                              | false           |
| MekanismOres                  | onGuiOpen             | io.github.phantamanta44.mekores.client.ClientEventListener               | normal   | mekores-2.0.12.jar                                 | false           |
| Tinkers' Construct            | onWoodenHopperDrawGui | slimeknights.tconstruct.gadgets.WoodenHopperGUIDrawEvent                 | normal   | TConstruct-1.12.2-2.13.0.171.jar                   | false           |
| Dimensional Control           | onGuiOpen             | com.bloodnbonesgaming.dimensionalcontrol.client.event.ClientEventHandler | normal   | DimensionalControl-1.12.2-2.13.0.jar               | false           |
| The One Probe                 | onGuiOpen             | mcjty.theoneprobe.proxy.ClientProxy                                      | normal   | theoneprobe-1.12-1.4.28.jar                        | false           |
| Quark                         | onOpenGUI             | vazkii.quark.oddities.feature.Backpacks                                  | normal   | Quark-r1.6-177.jar                                 | false           |
| The Betweenlands              | onGuiOpen             | thebetweenlands.client.handler.MusicHandler                              | highest  | TheBetweenlands-3.5.5-universal.jar                | false           |
| PneumaticCraft: Repressurized | onGuiOpen             | me.desht.pneumaticcraft.common.thirdparty.patchouli.Patchouli            | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Quark                         | loadMainMenu          | vazkii.quark.client.feature.PanoramaMaker                                | normal   | Quark-r1.6-177.jar                                 | false           |
| Compact Machines 3            | onGuiClose            | org.dave.compactmachines3.misc.PlayerEventHandler                        | normal   | compactmachines3-1.12.2-3.0.18-b278.jar            | false           |
| RFTools                       | onGuiOpen             | mcjty.rftools.setup.ClientProxy                                          | normal   | rftools-1.12-7.72.jar                              | false           |
| OMLib                         | playerGUIEvent        | omtteam.omlib.handler.OMLibEventHandler                                  | normal   | omlib-1.12.2-3.1.3-246.jar                         | false           |
| Custom Main Menu              | openGui               | lumien.custommainmenu.handler.CMMEventHandler                            | lowest   | CustomMainMenu-MC1.12.2-2.0.9.1.jar                | false           |
| The Betweenlands              | onGuiOpened           | thebetweenlands.client.gui.menu.GuiDownloadTerrainBetweenlands           | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Just Enough Items             | onGuiOpen             | mezz.jei.gui.GuiEventHandler                                             | normal   | jei_1.12.2-4.15.0.291.jar                          | false           |
| Quark                         | onGuiOpen             | vazkii.quark.misc.feature.ExtraPotions                                   | normal   | Quark-r1.6-177.jar                                 | false           |
| BNBGamingLib                  | onGuiOpen             | com.bloodnbonesgaming.lib.events.ClientEventHandler                      | normal   | BNBGamingLib-1.12.2-2.17.6.jar                     | false           |
| Ice and Fire                  | onGuiOpened           | com.github.alexthe666.iceandfire.event.EventClient                       | normal   | iceandfire-1.8.3.jar                               | false           |
| MoreOverlays                  | onGuiOpen             | at.feldim2425.moreoverlays.itemsearch.GuiHandler                         | normal   | moreoverlays-1.15.1-mc1.12.2.jar                   | false           |
| Chunk Pregenerator            | onGuiOpen             | pregenerator.impl.client.ClientHandler                                   | normal   | Chunk Pregenerator V1.12-2.2.jar                   | false           |
| CraftTweaker2                 | onGuiOpenEvent        | crafttweaker.mc1120.events.ClientEventHandler                            | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Quark                         | onGuiOpen             | vazkii.quark.world.feature.Archaeologist                                 | highest  | Quark-r1.6-177.jar                                 | false           |
| Draconic Evolution            | guiOpenEvent          | com.brandon3055.draconicevolution.client.handler.ClientEventHandler      | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| SCPCraft:Decline              | onOpenGui             | com.grandlovania.scp.proxy.ClientProxy                                   | normal   | SCPCraft-1.1-Decline.jar                           | false           |
| TipTheScales                  | onGuiOpen             | com.blamejared.tipthescales.events.ClientEventHandler                    | normal   | TipTheScales-1.12.2-1.0.4.jar                      | false           |
| Quark                         | onOpenGUI             | vazkii.quark.client.feature.ImprovedSignEdit                             | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | onGuiOpen             | vazkii.quark.base.client.gui.config.ConfigEvents                         | normal   | Quark-r1.6-177.jar                                 | false           |


## GuiScreenEvent$InitGuiEvent
| Owner                         | Method                 | Location                                          | Priority | Source                                             | RecieveCanceled |
|-------------------------------|------------------------|---------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Quark                         | onGuiInit              | vazkii.quark.base.client.gui.config.ConfigEvents  | normal   | Quark-r1.6-177.jar                                 | false           |
| FastWorkbench                 | removeButton           | shadows.fastbench.proxy.BenchClientProxy          | normal   | FastWorkbench-1.12.2-1.7.3.jar                     | false           |
| PneumaticCraft: Repressurized | handleResolutionChange | me.desht.pneumaticcraft.client.ClientEventHandler | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## EntityViewRenderEvent$FOVModifier
| Owner     | Method        | Location                                             | Priority | Source                      | RecieveCanceled |
|-----------|---------------|------------------------------------------------------|----------|-----------------------------|-----------------|
| Ender IO  | onFov         | crazypants.enderio.base.handler.FovZoomHandler       | normal   | EnderIO-1.12.2-5.1.55.jar   | false           |
| EnderCore | onFOVModifier | com.enderio.core.client.handlers.FluidVisualsHandler | normal   | EnderCore-1.12.2-0.5.73.jar | false           |


## LivingSpawnEvent$SpecialSpawn
| Owner         | Method              | Location                                      | Priority | Source                        | RecieveCanceled |
|---------------|---------------------|-----------------------------------------------|----------|-------------------------------|-----------------|
| CraftTweaker2 | onSpecialSpawnEvent | crafttweaker.mc1120.events.CommonEventHandler | normal   | CraftTweaker2-1.12-4.1.20.jar | false           |


## RenderTooltipEvent$PostText
| Owner                         | Method             | Location                                                  | Priority | Source                                             | RecieveCanceled |
|-------------------------------|--------------------|-----------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Building Gadgets              | onDrawTooltip      | com.direwolf20.buildinggadgets.client.events.EventTooltip | normal   | BuildingGadgets-2.7.4.jar                          | false           |
| McJtyLib                      | onDrawTooltip      | mcjty.lib.tooltips.TooltipRender                          | normal   | mcjtylib-1.12-3.5.4.jar                            | false           |
| Quark                         | renderTooltip      | vazkii.quark.vanity.feature.DyeItemNames                  | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | renderTooltip      | vazkii.quark.client.feature.ShulkerBoxTooltip             | normal   | Quark-r1.6-177.jar                                 | false           |
| PneumaticCraft: Repressurized | renderTooltipEvent | me.desht.pneumaticcraft.client.ClientEventHandler         | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Quark                         | renderTooltip      | vazkii.quark.management.feature.FavoriteItems             | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | renderTooltip      | vazkii.quark.client.feature.EnchantedBooksShowItems       | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | renderTooltip      | vazkii.quark.client.feature.VisualStatDisplay             | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | renderTooltip      | vazkii.quark.client.feature.FoodTooltip                   | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | renderTooltip      | vazkii.quark.client.feature.MapTooltip                    | normal   | Quark-r1.6-177.jar                                 | false           |


## PlayerEvent$ItemPickupEvent
| Owner             | Method         | Location                                             | Priority | Source                                          | RecieveCanceled |
|-------------------|----------------|------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| Academy Craft     | onPlayerPickup | cn.academy.advancements.DispatcherAch                | normal   | AcademyCraft-1.1.2.jar                          | false           |
| The Betweenlands  | onItemPickup   | thebetweenlands.common.item.misc.ItemMagicItemMagnet | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| Academy Craft     | onItemPickup   | cn.academy.tutorial.Conditions                       | normal   | AcademyCraft-1.1.2.jar                          | false           |
| Advanced Rocketry | onPickup       | zmaster587.advancedRocketry.event.PlanetEventHandler | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Advanced Rocketry | onPickup       | zmaster587.advancedRocketry.event.PlanetEventHandler | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |


## SoundLoadEvent
| Owner            | Method            | Location                                    | Priority | Source                              | RecieveCanceled |
|------------------|-------------------|---------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onSoundSystemLoad | thebetweenlands.client.handler.MusicHandler | lowest   | TheBetweenlands-3.5.5-universal.jar | false           |


## SurvivalTabClickEvent
| Owner    | Method             | Location | Priority | Source                     | RecieveCanceled |
|----------|--------------------|----------|----------|----------------------------|-----------------|
| LLibrary | onSurvivalTabClick | INSTANCE | normal   | llibrary-1.7.19-1.12.2.jar | false           |


## RenderPlayerEvent$Post
| Owner                         | Method                | Location                                                            | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-----------------------|---------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Quark                         | onRenderPlayer        | vazkii.quark.base.client.ContributorRewardHandler                   | normal   | Quark-r1.6-177.jar                                 | false           |
| The Betweenlands              | onPlayerRenderPost    | thebetweenlands.client.render.entity.RenderGrapplingHookNode        | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| LLibrary                      | onRenderPlayer        | INSTANCE                                                            | normal   | llibrary-1.7.19-1.12.2.jar                         | false           |
| Draconic Evolution            | renderPlayerEvent     | com.brandon3055.draconicevolution.client.handler.ClientEventHandler | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| PneumaticCraft: Repressurized | playerPostRotateEvent | me.desht.pneumaticcraft.client.ClientEventHandler                   | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Advanced Rocketry             | renderPostSpecial     | zmaster587.advancedRocketry.client.render.RenderComponents          | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| Aroma1997Core                 | renderPlayer          | aroma1997.core.client.ClientEventListener                           | normal   | Aroma1997Core-1.12.2-2.0.0.2.jar                   | false           |


## LivingEntityUseItemEvent$Start
| Owner            | Method                          | Location                                           | Priority | Source                              | RecieveCanceled |
|------------------|---------------------------------|----------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onStartUseItem                  | thebetweenlands.common.handler.ElixirCommonHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| The Betweenlands | onStartItemUse                  | thebetweenlands.common.handler.FoodSicknessHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| The Betweenlands | onStartUsingItem                | thebetweenlands.common.handler.PlayerDecayHandler  | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| The Betweenlands | onStartUsingItem                | thebetweenlands.common.item.tools.ItemGreatsword   | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| CraftTweaker2    | onLivingEntityUseItemStartEvent | crafttweaker.mc1120.events.CommonEventHandler      | normal   | CraftTweaker2-1.12-4.1.20.jar       | false           |


## AmadronRetrievalEvent
| Owner                         | Method           | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|------------------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | onAmadronSuccess | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## TinkerEvent$OnToolPartReplacement
| Owner              | Method                | Location                                          | Priority | Source                           | RecieveCanceled |
|--------------------|-----------------------|---------------------------------------------------|----------|----------------------------------|-----------------|
| Tinkers' Construct | onToolPartReplacement | slimeknights.tconstruct.tools.ranged.RangedEvents | normal   | TConstruct-1.12.2-2.13.0.171.jar | false           |


## ModelBakeEvent
| Owner                         | Method           | Location                                                               | Priority | Source                                               | RecieveCanceled |
|-------------------------------|------------------|------------------------------------------------------------------------|----------|------------------------------------------------------|-----------------|
| FoamFix                       | onModelBake      | pl.asie.foamfix.client.ModelLoaderCleanup                              | normal   | foamfix-0.10.10-1.12.2.jar                           | false           |
| Mutant Beasts                 | onModelBaked     | chumbanotz.mutantbeasts.client.ClientEventHandler                      | normal   | MutantBeasts-1.12.2-0.6.0.jar                        | false           |
| Ender IO                      | invalidate       | crazypants.enderio.base.render.pipeline.EnderItemOverrideList          | normal   | EnderIO-1.12.2-5.1.55.jar                            | false           |
| Refined Storage Addons        | registerModels   | com.raoulvdberge.refinedstorageaddons.proxy.ProxyClient                | normal   | refinedstorageaddons-0.4.4.jar                       | false           |
| CTM                           | onModelBake      | INSTANCE                                                               | lowest   | CTM-MC1.12.2-1.0.1.30.jar                            | false           |
| QuarryPlus                    | onBake           | com.yogpc.qp.ProxyClient                                               | normal   | AdditionalEnchantedMiner-1.12.2-12.3.0-universal.jar | false           |
| Mekanism                      | onModelBake      | mekanism.client.render.obj.MekanismOBJLoader                           | normal   | Mekanism-1.12.2-9.8.3.390.jar                        | false           |
| lambdalib2                    | onModelBake      | cn.academy.item.ItemMagHook                                            | normal   | AcademyCraft-1.1.2.jar                               | false           |
| Chameleon                     | onModelBakeEvent | com.jaquadro.minecraft.chameleon.core.ClientProxy                      | normal   | Chameleon-1.12-4.1.3.jar                             | false           |
| Ender IO                      | bakeModels       | crazypants.enderio.base.render.registry.ItemModelRegistry              | normal   | EnderIO-1.12.2-5.1.55.jar                            | false           |
| Ender IO Conduits             | invalidate       | crazypants.enderio.conduits.render.BlockStateWrapperConduitBundle      | normal   | EnderIO-1.12.2-5.1.55.jar                            | false           |
| FoamFix                       | onModelBake      | pl.asie.foamfix.ProxyClient                                            | low      | foamfix-0.10.10-1.12.2.jar                           | false           |
| CTM                           | afterModelBaking | INSTANCE                                                               | lowest   | CTM-MC1.12.2-1.0.1.30.jar                            | false           |
| lambdalib2                    | onModelBake      | cn.academy.item.ItemDeveloper                                          | normal   | AcademyCraft-1.1.2.jar                               | false           |
| Tinkers' Construct            | onModelBake      | slimeknights.tconstruct.tools.ToolClientEvents                         | normal   | TConstruct-1.12.2-2.13.0.171.jar                     | false           |
| Quark                         | onModelBake      | vazkii.quark.decoration.feature.BetterVanillaFlowerPot                 | normal   | Quark-r1.6-177.jar                                   | false           |
| Refined Storage               | onModelBake      | com.raoulvdberge.refinedstorage.proxy.ProxyClient                      | normal   | refinedstorage-1.6.15.jar                            | false           |
| Quark                         | onModelBake      | vazkii.quark.decoration.feature.ColoredFlowerPots                      | normal   | Quark-r1.6-177.jar                                   | false           |
| MCMultiPart                   | onModelBake      | mcmultipart.client.MCMPClientProxy                                     | normal   | MCMultiPart-2.5.3.jar                                | false           |
| CodeChicken Lib               | onModelBake      | codechicken.lib.model.loader.blockstate.CCBlockStateLoader             | highest  | CodeChickenLib-1.12.2-3.2.3.358-universal.jar        | false           |
| lambdalib2                    | onModelBake      | cn.academy.item.ItemSilbarn                                            | normal   | AcademyCraft-1.1.2.jar                               | false           |
| Ender IO                      | bakeModels       | crazypants.enderio.base.render.registry.SmartModelAttacher             | normal   | EnderIO-1.12.2-5.1.55.jar                            | false           |
| Tinkers' Construct            | onModelBake      | slimeknights.tconstruct.smeltery.SmelteryClientEvents                  | normal   | TConstruct-1.12.2-2.13.0.171.jar                     | false           |
| Ender IO                      | bakeModels       | crazypants.enderio.base.paint.render.PaintRegistry$PaintRegistryClient | normal   | EnderIO-1.12.2-5.1.55.jar                            | false           |
| Techguns                      | onModelBake      | techguns.events.TGEventHandler                                         | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                   | false           |
| Tinkers' Construct            | onModelBake      | slimeknights.tconstruct.gadgets.GadgetClientEvents                     | normal   | TConstruct-1.12.2-2.13.0.171.jar                     | false           |
| Mekanism                      | onModelBake      | mekanism.client.ClientProxy                                            | normal   | Mekanism-1.12.2-9.8.3.390.jar                        | false           |
| Quark                         | onModelBake      | vazkii.quark.decoration.feature.VariedChests                           | normal   | Quark-r1.6-177.jar                                   | false           |
| Compact Machines 3            | modelBake        | org.dave.compactmachines3.render.BakeryHandler                         | normal   | compactmachines3-1.12.2-3.0.18-b278.jar              | false           |
| AgriCraft                     | onModelBakePost  | com.infinityraider.agricraft.utility.ModelErrorSuppressor              | normal   | AgriCraft-2.12.0-1.12.0-a6.jar                       | false           |
| PneumaticCraft: Repressurized | onModelBaking    | me.desht.pneumaticcraft.client.ClientEventHandler                      | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar   | false           |
| lambdalib2                    | onModelBake      | cn.academy.item.ItemCoin                                               | normal   | AcademyCraft-1.1.2.jar                               | false           |
| CodeChicken Lib               | onModelBake      | codechicken.lib.model.ModelRegistryHelper                              | normal   | CodeChickenLib-1.12.2-3.2.3.358-universal.jar        | false           |
| The Betweenlands              | onModelBake      | thebetweenlands.client.render.model.loader.CustomModelLoader           | normal   | TheBetweenlands-3.5.5-universal.jar                  | false           |
| Tomb Many Graves              | onModelBake      | com.m4thg33k.tombmanygraves.events.ClientEvents                        | normal   | TombManyGraves-1.12-4.2.0.jar                        | false           |
| Ender IO                      | invalidate       | crazypants.enderio.base.render.pipeline.BlockStateWrapperBase          | normal   | EnderIO-1.12.2-5.1.55.jar                            | false           |


## ScreenshotEvent
| Owner | Method         | Location                                  | Priority | Source             | RecieveCanceled |
|-------|----------------|-------------------------------------------|----------|--------------------|-----------------|
| Quark | takeScreenshot | vazkii.quark.client.feature.PanoramaMaker | normal   | Quark-r1.6-177.jar | false           |


## PlayerDestroyItemEvent
| Owner         | Method                   | Location                                                  | Priority | Source                        | RecieveCanceled |
|---------------|--------------------------|-----------------------------------------------------------|----------|-------------------------------|-----------------|
| CraftTweaker2 | onPlayerDestroyItemEvent | crafttweaker.mc1120.events.CommonEventHandler             | normal   | CraftTweaker2-1.12-4.1.20.jar | false           |
| Quark         | onToolBreak              | vazkii.quark.management.feature.AutomaticToolRestock      | normal   | Quark-r1.6-177.jar            | false           |
| Morph-o-Tool  | onItemBroken             | vazkii.morphtool.MorphingHandler                          | normal   | Morph-o-Tool-1.2-21.jar       | false           |
| Ender IO      | onItemDesctroyed         | crazypants.enderio.base.handler.darksteel.UpgradeRegistry | normal   | EnderIO-1.12.2-5.1.55.jar     | false           |


## NetworkPartEvent$RemovedPart
| Owner                 | Method        | Location                                          | Priority | Source                                  | RecieveCanceled |
|-----------------------|---------------|---------------------------------------------------|----------|-----------------------------------------|-----------------|
| Practical Logistics 2 | onPartRemoved | sonar.logistics.base.events.LogisticsEventHandler | highest  | practicallogistics2-1.12.2-3.0.8-11.jar | false           |


## DecorateBiomeEvent$Pre
| Owner               | Method             | Location                                                       | Priority | Source                               | RecieveCanceled |
|---------------------|--------------------|----------------------------------------------------------------|----------|--------------------------------------|-----------------|
| Dimensional Control | onDecorateBiomePre | com.bloodnbonesgaming.dimensionalcontrol.events.DCEventHandler | highest  | DimensionalControl-1.12.2-2.13.0.jar | false           |


## GuiScreenEvent$ActionPerformedEvent$Pre
| Owner               | Method            | Location                                                                 | Priority | Source                               | RecieveCanceled |
|---------------------|-------------------|--------------------------------------------------------------------------|----------|--------------------------------------|-----------------|
| Dimensional Control | onActionPerformed | com.bloodnbonesgaming.dimensionalcontrol.client.event.ClientEventHandler | normal   | DimensionalControl-1.12.2-2.13.0.jar | false           |
| ReAuth              | action            | technicianlp.reauth.GuiHandler                                           | normal   | reauth-3.6.0.jar                     | false           |
| LLibrary            | onButtonPressPre  | INSTANCE                                                                 | normal   | llibrary-1.7.19-1.12.2.jar           | false           |
| Quark               | performAction     | vazkii.quark.management.feature.ChestButtons                             | normal   | Quark-r1.6-177.jar                   | false           |
| Patchouli           | onActionPressed   | vazkii.patchouli.client.base.InventoryBookButtonHandler                  | normal   | Patchouli-1.0-20.jar                 | false           |
| Quark               | performAction     | vazkii.quark.vanity.feature.EmoteSystem                                  | normal   | Quark-r1.6-177.jar                   | false           |
| Quark               | performAction     | vazkii.quark.management.feature.StoreToChests                            | normal   | Quark-r1.6-177.jar                   | false           |


## PlayerBrewedPotionEvent
| Owner         | Method                    | Location                                      | Priority | Source                        | RecieveCanceled |
|---------------|---------------------------|-----------------------------------------------|----------|-------------------------------|-----------------|
| CraftTweaker2 | onPlayerBrewedPotionEvent | crafttweaker.mc1120.events.CommonEventHandler | normal   | CraftTweaker2-1.12-4.1.20.jar | false           |


## RenderSpecificHandEvent
| Owner              | Method                | Location                                                 | Priority | Source                                        | RecieveCanceled |
|--------------------|-----------------------|----------------------------------------------------------|----------|-----------------------------------------------|-----------------|
| Tinkers' Construct | handRenderEvent       | slimeknights.tconstruct.tools.common.client.RenderEvents | normal   | TConstruct-1.12.2-2.13.0.171.jar              | false           |
| CodeChicken Lib    | renderFirstPersonHand | codechicken.lib.render.item.map.MapRenderRegistry        | normal   | CodeChickenLib-1.12.2-3.2.3.358-universal.jar | false           |
| The Betweenlands   | onRenderHand          | thebetweenlands.client.handler.DecayRenderHandler        | lowest   | TheBetweenlands-3.5.5-universal.jar           | false           |
| The Betweenlands   | onRenderHand          | thebetweenlands.common.item.tools.ItemGreatsword         | high     | TheBetweenlands-3.5.5-universal.jar           | false           |


## GuiScreenEvent$KeyboardInputEvent$Post
| Owner             | Method             | Location                                                  | Priority | Source                      | RecieveCanceled |
|-------------------|--------------------|-----------------------------------------------------------|----------|-----------------------------|-----------------|
| Quark             | onKeyInput         | vazkii.quark.base.client.ModKeybinds$KeybindButtonHandler | normal   | Quark-r1.6-177.jar          | false           |
| Quark             | keyboardEvent      | vazkii.quark.management.feature.LinkItems                 | normal   | Quark-r1.6-177.jar          | false           |
| Quark             | keyboardEvent      | vazkii.quark.management.feature.DeleteItems               | normal   | Quark-r1.6-177.jar          | false           |
| Quark             | onKeyInput         | vazkii.quark.base.client.ModKeybinds$KeybindButtonHandler | normal   | Quark-r1.6-177.jar          | false           |
| Just Enough Items | onGuiKeyboardEvent | mezz.jei.input.InputHandler                               | normal   | jei_1.12.2-4.15.0.291.jar   | false           |
| Quark             | onKeyInput         | vazkii.quark.base.client.ModKeybinds$KeybindButtonHandler | normal   | Quark-r1.6-177.jar          | false           |
| JourneyMap        | onGuiKeyboardEvent | INSTANCE                                                  | normal   | journeymap-1.12.2-5.5.6.jar | false           |
| Quark             | onKeyInput         | vazkii.quark.base.client.ModKeybinds$KeybindButtonHandler | normal   | Quark-r1.6-177.jar          | false           |
| Quark             | onKeyInput         | vazkii.quark.base.client.ModKeybinds$KeybindButtonHandler | normal   | Quark-r1.6-177.jar          | false           |
| Quark             | keyboardEvent      | vazkii.quark.management.feature.FToSwitchItems            | normal   | Quark-r1.6-177.jar          | false           |
| Quark             | onKeyInput         | vazkii.quark.base.client.ModKeybinds$KeybindButtonHandler | normal   | Quark-r1.6-177.jar          | false           |


## Apotheosis$ApotheosisInit
| Owner      | Method | Location                    | Priority | Source                       | RecieveCanceled |
|------------|--------|-----------------------------|----------|------------------------------|-----------------|
| Apotheosis | init   | shadows.potion.PotionModule | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |
| Apotheosis | init   | shadows.deadly.DeadlyModule | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |
| Apotheosis | init   | shadows.ApotheosisClient    | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |
| Apotheosis | init   | shadows.spawn.SpawnerModule | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |
| Apotheosis | init   | shadows.ench.EnchModule     | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |


## EnderIOLifecycleEvent$Init$Post
| Owner    | Method                        | Location                                                   | Priority | Source                    | RecieveCanceled |
|----------|-------------------------------|------------------------------------------------------------|----------|---------------------------|-----------------|
| Ender IO | registerColoredBlocksAndItems | crazypants.enderio.base.render.registry.SmartModelAttacher | normal   | EnderIO-1.12.2-5.1.55.jar | false           |


## ConfigFileChangedEvent
| Owner                              | Method              | Location                                        | Priority | Source                       | RecieveCanceled |
|------------------------------------|---------------------|-------------------------------------------------|----------|------------------------------|-----------------|
| Ender IO Machines                  | onConfigFileChanged | crazypants.enderio.base.config.ConfigHandlerEIO | normal   | EnderIO-1.12.2-5.1.55.jar    | false           |
| EnderCore                          | onConfigFileChanged | com.enderio.core.common.config.ConfigHandler$1  | normal   | EnderCore-1.12.2-0.5.73.jar  | false           |
| Ender IO Integration with Forestry | onConfigFileChanged | crazypants.enderio.base.config.ConfigHandlerEIO | normal   | EnderIO-1.12.2-5.1.55.jar    | false           |
| Ender IO                           | onConfigFileChanged | crazypants.enderio.base.config.Config           | normal   | EnderIO-1.12.2-5.1.55.jar    | false           |
| Ender IO Conduits                  | onConfigFileChanged | crazypants.enderio.base.config.ConfigHandlerEIO | normal   | EnderIO-1.12.2-5.1.55.jar    | false           |
| Ender IO Powertools                | onConfigFileChanged | crazypants.enderio.base.config.ConfigHandlerEIO | normal   | EnderIO-1.12.2-5.1.55.jar    | false           |
| GasConduits                        | onConfigFileChanged | crazypants.enderio.base.config.ConfigHandlerEIO | normal   | GasConduits-1.12.2-1.2.2.jar | false           |
| EnderCore                          | onConfigFileChanged | com.enderio.core.common.config.ConfigHandler    | normal   | EnderCore-1.12.2-0.5.73.jar  | false           |


## RenderLivingEvent$Post
| Owner                         | Method             | Location                                                                         | Priority | Source                                             | RecieveCanceled |
|-------------------------------|--------------------|----------------------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | onLivingRender     | me.desht.pneumaticcraft.client.ClientEventHandler                                | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Ender IO Machines             | onEntityRender     | crazypants.enderio.machines.machine.teleport.telepad.TeleportEntityRenderHandler | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Ice and Fire                  | onPostRenderLiving | com.github.alexthe666.iceandfire.event.EventClient                               | normal   | iceandfire-1.8.3.jar                               | false           |
| Quark                         | postRenderLiving   | vazkii.quark.vanity.feature.EmoteSystem                                          | lowest   | Quark-r1.6-177.jar                                 | false           |
| The Betweenlands              | onRenderLivingPost | thebetweenlands.common.handler.PuppetHandler                                     | lowest   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                         | colorize           | vazkii.quark.world.effects.PotionColorizer                                       | normal   | Quark-r1.6-177.jar                                 | false           |


## LivingFallEvent
| Owner                         | Method                  | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-------------------------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| RFTools                       | onLivingFall            | mcjty.rftools.ForgeEventHandlers                                | normal   | rftools-1.12-7.72.jar                              | false           |
| Tinkers' Construct            | onFall                  | slimeknights.tconstruct.gadgets.item.ItemSlimeBoots             | normal   | TConstruct-1.12.2-2.13.0.171.jar                   | false           |
| PneumaticCraft: Repressurized | onPlayerFall            | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticArmor | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Advanced Rocketry             | fallEvent               | zmaster587.advancedRocketry.event.PlanetEventHandler            | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| Techguns                      | onLivingFallEvent       | techguns.events.TGEventHandler                                  | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| Ice and Fire                  | onEntityFall            | com.github.alexthe666.iceandfire.event.EventLiving              | normal   | iceandfire-1.8.3.jar                               | false           |
| Advanced Rocketry             | fallEvent               | zmaster587.advancedRocketry.event.PlanetEventHandler            | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| CraftTweaker2                 | onEntityLivingFallEvent | crafttweaker.mc1120.events.CommonEventHandler                   | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Ender IO                      | onFall                  | crazypants.enderio.base.handler.darksteel.DarkSteelController   | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |


## GuiScreenEvent$DrawScreenEvent$Post
| Owner             | Method                | Location                                                   | Priority | Source                              | RecieveCanceled |
|-------------------|-----------------------|------------------------------------------------------------|----------|-------------------------------------|-----------------|
| Item Blacklist    | drawScreenEvent       | net.doubledoordev.itemblacklist.client.ClientEventHandlers | normal   | ItemBlacklist-1.4.3.jar             | false           |
| Just Enough Items | onDrawScreenEventPost | mezz.jei.gui.GuiEventHandler                               | normal   | jei_1.12.2-4.15.0.291.jar           | false           |
| Just Enough Items | drawScreenEvent       | techreborn.compat.jei.TechRebornJeiPlugin                  | normal   | jei_1.12.2-4.15.0.291.jar           | false           |
| MoreOverlays      | onDrawScreen          | at.feldim2425.moreoverlays.itemsearch.GuiHandler           | normal   | moreoverlays-1.15.1-mc1.12.2.jar    | false           |
| ReAuth            | draw                  | technicianlp.reauth.GuiHandler                             | normal   | reauth-3.6.0.jar                    | false           |
| Quark             | onRender              | vazkii.quark.client.feature.ChestSearchBar                 | normal   | Quark-r1.6-177.jar                  | false           |
| LLibrary          | onDrawScreenPost      | INSTANCE                                                   | normal   | llibrary-1.7.19-1.12.2.jar          | false           |
| Custom Main Menu  | renderScreenPost      | lumien.custommainmenu.handler.CMMEventHandler              | highest  | CustomMainMenu-MC1.12.2-2.0.9.1.jar | false           |
| Quark             | drawEvent             | vazkii.quark.management.feature.FavoriteItems              | normal   | Quark-r1.6-177.jar                  | false           |
| The Betweenlands  | onRenderScreen        | thebetweenlands.client.handler.ScreenRenderHandler         | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| AutoRegLib        | onDrawScreen          | vazkii.arl.util.DropInHandler                              | normal   | AutoRegLib-1.3-32.jar               | false           |


## PlayerEvent$Clone
| Owner              | Method            | Location                                                       | Priority | Source                              | RecieveCanceled |
|--------------------|-------------------|----------------------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands   | onPlayerClone     | thebetweenlands.common.capability.base.EntityCapabilityHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| Tomb Many Graves   | onPlayerClone     | com.m4thg33k.tombmanygraves.events.CommonEvents                | highest  | TombManyGraves-1.12-4.2.0.jar       | false           |
| PlusTiC            | copyOnDeath       | landmaster.plustic.api.Portal                                  | normal   | plustic-7.1.6.1.jar                 | false           |
| LLibrary           | onPlayerClone     | INSTANCE                                                       | normal   | llibrary-1.7.19-1.12.2.jar          | false           |
| NuclearCraft       | onPlayerRespawn   | nc.handler.PlayerRespawnHandler                                | normal   | NuclearCraft-2.18o-1.12.2.jar       | false           |
| Ender IO           | onPlayerClone     | crazypants.enderio.base.enchantment.HandlerSoulBound           | highest  | EnderIO-1.12.2-5.1.55.jar           | false           |
| RFTools            | onPlayerCloned    | mcjty.rftools.ForgeEventHandlers                               | normal   | rftools-1.12-7.72.jar               | false           |
| Tinkers' Construct | onPlayerClone     | slimeknights.tconstruct.tools.modifiers.ModSoulbound           | high     | TConstruct-1.12.2-2.13.0.171.jar    | false           |
| Ender IO           | onPlayerCloneLast | crazypants.enderio.base.enchantment.HandlerSoulBound           | lowest   | EnderIO-1.12.2-5.1.55.jar           | false           |
| LLibrary           | onPlayerClone     | INSTANCE                                                       | normal   | llibrary-1.7.19-1.12.2.jar          | false           |
| TOP Addons         | onPlayerClone     | io.github.drmanganese.topaddons.config.capabilities.CapEvents  | normal   | topaddons-1.12.2-1.12.0.jar         | false           |
| PlusTiC            | copyOnDeath       | landmaster.plustic.api.Toggle                                  | normal   | plustic-7.1.6.1.jar                 | false           |
| The One Probe      | onPlayerCloned    | mcjty.theoneprobe.ForgeEventHandlers                           | normal   | theoneprobe-1.12-1.4.28.jar         | false           |
| lambdalib2         | onPlayerClone     | instance                                                       | normal   | AcademyCraft-1.1.2.jar              | false           |
| Techguns           | playerClone       | techguns.capabilities.CapabilityEventHandler                   | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar  | false           |


## DroneSuicideEvent
| Owner                         | Method         | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|----------------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | onDroneSuicide | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## GuiScreenEvent$BackgroundDrawnEvent
| Owner             | Method                    | Location                     | Priority | Source                    | RecieveCanceled |
|-------------------|---------------------------|------------------------------|----------|---------------------------|-----------------|
| Just Enough Items | onDrawBackgroundEventPost | mezz.jei.gui.GuiEventHandler | normal   | jei_1.12.2-4.15.0.291.jar | false           |


## BlockEvent$PlaceEvent
| Owner              | Method                 | Location                                                         | Priority | Source                                          | RecieveCanceled |
|--------------------|------------------------|------------------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| The Betweenlands   | onPlayerTorchPlacement | thebetweenlands.common.handler.OverworldItemHandler              | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| Advanced Rocketry  | blockPlacedEvent       | zmaster587.advancedRocketry.event.PlanetEventHandler             | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| The Betweenlands   | onBlockPlace           | thebetweenlands.common.handler.LocationHandler                   | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| Quark              | onBlockPlaced          | vazkii.quark.misc.feature.LockDirectionHotkey                    | normal   | Quark-r1.6-177.jar                              | false           |
| Academy Monster    | onBlockPlaced          | cn.paindar.academymonster.core.support.tile.AbilityInterfManager | normal   | AcademyMonster-1.1.0a.jar                       | false           |
| Advanced Rocketry  | blockPlacedEvent       | zmaster587.advancedRocketry.event.PlanetEventHandler             | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Quark              | onItemPlaced           | vazkii.quark.tweaks.feature.SpongeDriesInNether                  | normal   | Quark-r1.6-177.jar                              | false           |
| Compact Machines 3 | preventPlacingInHub    | org.dave.compactmachines3.skyworld.SkyWorldEvents                | normal   | compactmachines3-1.12.2-3.0.18-b278.jar         | false           |


## LivingEquipmentChangeEvent
| Owner                         | Method             | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|--------------------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | onEquipmentChanged | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Techguns                      | onItemSwitch       | techguns.events.TGEventHandler                                  | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |


## InputEvent$MouseInputEvent
| Owner             | Method          | Location                                                   | Priority | Source                                          | RecieveCanceled |
|-------------------|-----------------|------------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| Advanced Rocketry | mouseInputEvent | zmaster587.advancedRocketry.event.RocketEventHandler       | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Building Gadgets  | onMouseInput    | com.direwolf20.buildinggadgets.client.events.EventKeyInput | normal   | BuildingGadgets-2.7.4.jar                       | false           |


## AnalyticSkillEvent
| Owner         | Method        | Location                                 | Priority | Source                 | RecieveCanceled |
|---------------|---------------|------------------------------------------|----------|------------------------|-----------------|
| Academy Craft | skillListener | cn.academy.analytic.AnalyticDataListener | normal   | AcademyCraft-1.1.2.jar | false           |


## RenderGameOverlayEvent$BossInfo
| Owner            | Method          | Location                                   | Priority | Source                              | RecieveCanceled |
|------------------|-----------------|--------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onBossBarRender | thebetweenlands.common.handler.BossHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |


## PlaySoundEvent
| Owner                          | Method           | Location                                                              | Priority | Source                                      | RecieveCanceled |
|--------------------------------|------------------|-----------------------------------------------------------------------|----------|---------------------------------------------|-----------------|
| The Betweenlands               | onPlaySound      | thebetweenlands.client.handler.MusicHandler                           | normal   | TheBetweenlands-3.5.5-universal.jar         | false           |
| Ender IO                       | onPlaySoundEvent | crazypants.enderio.base.item.darksteel.upgrade.padding.PaddingHandler | lowest   | EnderIO-1.12.2-5.1.55.jar                   | false           |
| NuclearCraft                   | onTilePlaySound  | nc.handler.SoundHandler                                               | lowest   | NuclearCraft-2.18o-1.12.2.jar               | false           |
| TorchMaster                    | onPlaySoundEvent | net.xalcon.torchmaster.client.EventHandlerClient                      | normal   | torchmaster_1.12.2-1.8.1.81.jar             | false           |
| Quark                          | onSound          | vazkii.quark.world.feature.OceanGuardians                             | normal   | Quark-r1.6-177.jar                          | false           |
| Bad Wither No Cookie! Reloaded | onEvent          | com.kreezcraft.badwithernocookiereloaded.SoundEventHandler            | lowest   | badwithernocookiereloaded-1.12.2-3.3.16.jar | false           |
| Mekanism                       | onTilePlaySound  | mekanism.client.sound.SoundHandler                                    | lowest   | Mekanism-1.12.2-9.8.3.390.jar               | false           |


## GuiScreenEvent$DrawScreenEvent$Pre
| Owner                         | Method                   | Location                                          | Priority | Source                                             | RecieveCanceled |
|-------------------------------|--------------------------|---------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | drawCustomDurabilityBars | me.desht.pneumaticcraft.client.ClientEventHandler | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| MoreOverlays                  | onDrawScreen             | at.feldim2425.moreoverlays.itemsearch.GuiHandler  | normal   | moreoverlays-1.15.1-mc1.12.2.jar                   | false           |


## GuiScreenEvent$InitGuiEvent$Pre
| Owner       | Method       | Location                                                | Priority | Source                        | RecieveCanceled |
|-------------|--------------|---------------------------------------------------------|----------|-------------------------------|-----------------|
| Quark       | openGUI      | vazkii.quark.world.feature.RealisticWorldType           | normal   | Quark-r1.6-177.jar            | false           |
| Smooth Font | onInitGui    | bre.smoothfont.handler.GuiScreenEventHandler            | normal   | SmoothFont-mc1.12.2-2.1.1.jar | false           |
| Ender IO    | handle       | crazypants.enderio.base.handler.SplashTextHandler       | normal   | EnderIO-1.12.2-5.1.55.jar     | false           |
| Patchouli   | onGuiInitPre | vazkii.patchouli.client.base.InventoryBookButtonHandler | highest  | Patchouli-1.0-20.jar          | false           |


## BlockEvent$CropGrowEvent$Pre
| Owner            | Method     | Location                                                 | Priority | Source                              | RecieveCanceled |
|------------------|------------|----------------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onCropGrow | thebetweenlands.common.block.farming.BlockGenericDugSoil | normal   | TheBetweenlands-3.5.5-universal.jar | false           |


## EnderIOLifecycleEvent$Config$Post
| Owner    | Method   | Location                                                 | Priority | Source                    | RecieveCanceled |
|----------|----------|----------------------------------------------------------|----------|---------------------------|-----------------|
| Ender IO | preInit  | crazypants.enderio.base.conduit.geom.ConduitGeometryUtil | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | preInit2 | crazypants.enderio.base.Log                              | normal   | EnderIO-1.12.2-5.1.55.jar | false           |


## OreGenEvent$GenerateMinable
| Owner             | Method             | Location                                             | Priority | Source                                          | RecieveCanceled |
|-------------------|--------------------|------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| Advanced Rocketry | onWorldGen         | zmaster587.advancedRocketry.event.PlanetEventHandler | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Quark             | onOreGenerate      | vazkii.quark.world.feature.UndergroundBiomes         | normal   | Quark-r1.6-177.jar                              | false           |
| Advanced Rocketry | onWorldGen         | zmaster587.advancedRocketry.event.PlanetEventHandler | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Ore Veins         | onGenerateMineable | com.alcatrazescapee.oreveins.world.WorldGenReplacer  | normal   | oreveins-2.0.15.jar                             | false           |
| Quark             | onOreGenerate      | vazkii.quark.world.feature.RevampStoneGen            | normal   | Quark-r1.6-177.jar                              | false           |


## PlayerEvent$PlayerChangedDimensionEvent
| Owner                  | Method                   | Location                                                       | Priority | Source                                                    | RecieveCanceled |
|------------------------|--------------------------|----------------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| Morpheus               | changedDimensionEvent    | net.quetzi.morpheus.helpers.MorpheusEventHandler               | normal   | Morpheus-1.12.2-3.5.106.jar                               | false           |
| Mekanism               | onPlayerChangedDimension | mekanism.client.ClientPlayerTracker                            | normal   | Mekanism-1.12.2-9.8.3.390.jar                             | false           |
| TorchMaster            | onPlayerChangedDimension | net.xalcon.torchmaster.client.EventHandlerClient               | normal   | torchmaster_1.12.2-1.8.1.81.jar                           | false           |
| Epic Siege Mod         | onDimensionChange        | funwayguy.epicsiegemod.handlers.entities.PlayerHandler         | normal   | EpicSiegeMod-13.165.jar                                   | false           |
| The Betweenlands       | onEntityChangeDimension  | thebetweenlands.common.capability.base.EntityCapabilityHandler | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |
| CraftTweaker2          | onPlayerChangedDimension | crafttweaker.mc1120.events.CommonEventHandler                  | normal   | CraftTweaker2-1.12-4.1.20.jar                             | false           |
| PlusTiC                | changeDim                | landmaster.plustic.api.Toggle                                  | normal   | plustic-7.1.6.1.jar                                       | false           |
| Advanced Rocketry      | onDimChanged             | zmaster587.libVulpes.util.InputSyncHandler                     | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar           | false           |
| Advanced Rocketry      | playerTeleportEvent      | zmaster587.advancedRocketry.event.RocketEventHandler           | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar           | false           |
| Mekanism               | onPlayerDimChangedEvent  | mekanism.common.CommonPlayerTracker                            | normal   | Mekanism-1.12.2-9.8.3.390.jar                             | false           |
| Just Enough Dimensions | onPlayerChangedDimension | fi.dy.masa.justenoughdimensions.event.JEDEventHandler          | normal   | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |


## EnderIOLifecycleEvent$PostInit$Post
| Owner    | Method | Location                                                      | Priority | Source                    | RecieveCanceled |
|----------|--------|---------------------------------------------------------------|----------|---------------------------|-----------------|
| Ender IO | onPost | crazypants.enderio.base.integration.thaumcraft.ThaumcraftUtil | normal   | EnderIO-1.12.2-5.1.55.jar | false           |


## BlockDestroyEvent
| Owner         | Method         | Location                           | Priority | Source                 | RecieveCanceled |
|---------------|----------------|------------------------------------|----------|------------------------|-----------------|
| Academy Craft | onBlockDestroy | cn.academy.ability.AbilityPipeline | normal   | AcademyCraft-1.1.2.jar | false           |


## SpecialVariableRetrievalEvent$CoordinateVariable$Drone
| Owner                         | Method                      | Location                                                         | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-----------------------------|------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | onSpecialVariableRetrieving | me.desht.pneumaticcraft.common.event.DroneSpecialVariableHandler | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## PlayerDropsEvent
| Owner              | Method             | Location                                             | Priority | Source                             | RecieveCanceled |
|--------------------|--------------------|------------------------------------------------------|----------|------------------------------------|-----------------|
| Quark              | onPlayerDrops      | vazkii.quark.oddities.feature.TotemOfHolding         | lowest   | Quark-r1.6-177.jar                 | false           |
| Ender IO           | onPlayerDeathLate  | crazypants.enderio.base.enchantment.HandlerSoulBound | lowest   | EnderIO-1.12.2-5.1.55.jar          | false           |
| CraftTweaker2      | onPlayerDeathDrops | crafttweaker.mc1120.events.CommonEventHandler        | normal   | CraftTweaker2-1.12-4.1.20.jar      | false           |
| Ender IO           | onPlayerDeath      | crazypants.enderio.base.enchantment.HandlerSoulBound | highest  | EnderIO-1.12.2-5.1.55.jar          | false           |
| Techguns           | onPlayerDrops      | techguns.events.TGEventHandler                       | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar | false           |
| Tomb Many Graves   | onPlayerDrop       | com.m4thg33k.tombmanygraves.events.CommonEvents      | lowest   | TombManyGraves-1.12-4.2.0.jar      | false           |
| Tinkers' Construct | onPlayerDeath      | slimeknights.tconstruct.tools.modifiers.ModSoulbound | high     | TConstruct-1.12.2-2.13.0.171.jar   | false           |


## TransformCategoryEvent
| Owner         | Method                    | Location                              | Priority | Source                 | RecieveCanceled |
|---------------|---------------------------|---------------------------------------|----------|------------------------|-----------------|
| Academy Craft | onPlayerTransformCategory | cn.academy.advancements.DispatcherAch | normal   | AcademyCraft-1.1.2.jar | false           |


## CommandEvent
| Owner                  | Method         | Location                                              | Priority | Source                                                    | RecieveCanceled |
|------------------------|----------------|-------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| Just Enough Dimensions | onCommand      | fi.dy.masa.justenoughdimensions.event.JEDEventHandler | normal   | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |
| CraftTweaker2          | onCommandEvent | crafttweaker.mc1120.events.CommonEventHandler         | normal   | CraftTweaker2-1.12-4.1.20.jar                             | false           |


## RegistryEvent$NewRegistry
| Owner       | Method           | Location                                                  | Priority | Source                    | RecieveCanceled |
|-------------|------------------|-----------------------------------------------------------|----------|---------------------------|-----------------|
| Ender IO    | registerRegistry | crazypants.enderio.base.integration.IntegrationRegistry   | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO    | registerRegistry | crazypants.enderio.base.farming.registry.Registry         | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO    | registerRegistry | crazypants.enderio.base.farming.fertilizer.Fertilizer     | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO    | registerRegistry | crazypants.enderio.base.capacitor.CapacitorKeyRegistry    | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO    | registerRegistry | crazypants.enderio.base.init.ModObjectRegistry            | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO    | registerRegistry | crazypants.enderio.base.handler.darksteel.UpgradeRegistry | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| MCMultiPart | onRegistrySetup  | mcmultipart.MCMultiPart                                   | normal   | MCMultiPart-2.5.3.jar     | false           |


## FMLNetworkEvent$ServerConnectionFromClientEvent
| Owner                  | Method              | Location                                              | Priority | Source                                                    | RecieveCanceled |
|------------------------|---------------------|-------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| Advanced Rocketry      | playerLoggedInEvent | zmaster587.advancedRocketry.event.PlanetEventHandler  | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar           | false           |
| Just Enough Dimensions | onConnectionCreated | fi.dy.masa.justenoughdimensions.event.JEDEventHandler | normal   | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |
| Advanced Rocketry      | playerLoggedInEvent | zmaster587.advancedRocketry.event.PlanetEventHandler  | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar           | false           |


## RenderHandEvent
| Owner            | Method       | Location                                              | Priority | Source                              | RecieveCanceled |
|------------------|--------------|-------------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onHandRender | thebetweenlands.client.handler.WeedwoodRowboatHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| Techguns         | onRenderHand | techguns.events.TGEventHandler                        | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar  | false           |
| lambdalib2       | onRenderHand | instance                                              | normal   | AcademyCraft-1.1.2.jar              | false           |


## PlayerInteractEvent$RightClickBlock
| Owner                         | Method                       | Location                                                                                       | Priority | Source                                             | RecieveCanceled |
|-------------------------------|------------------------------|------------------------------------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Quark                         | onInteract                   | vazkii.quark.tweaks.feature.DeployLaddersDown                                                  | normal   | Quark-r1.6-177.jar                                 | false           |
| PneumaticCraft: Repressurized | onInteraction                | me.desht.pneumaticcraft.common.semiblock.SemiBlockManager                                      | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Morpheus                      | bedClicked                   | net.quetzi.morpheus.helpers.MorpheusEventHandler                                               | highest  | Morpheus-1.12.2-3.5.106.jar                        | false           |
| Quark                         | onPlayerInteract             | vazkii.quark.decoration.feature.FlatItemFrames                                                 | normal   | Quark-r1.6-177.jar                                 | false           |
| AgriCraft                     | applyVinesToGrate            | com.infinityraider.agricraft.handler.PlayerInteractEventHandler                                | highest  | AgriCraft-2.12.0-1.12.0-a6.jar                     | false           |
| lambdalib2                    | onInteract                   | Block{minecraft:air}                                                                           | normal   | AcademyCraft-1.1.2.jar                             | false           |
| Chisel                        | onRightClickBlock            | team.chisel.common.item.ChiselController                                                       | normal   | Chisel-MC1.12.2-1.0.1.44.jar                       | false           |
| The Betweenlands              | onBlockRightClick            | thebetweenlands.common.handler.LocationHandler                                                 | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Patchouli                     | onPlayerInteract             | vazkii.patchouli.client.handler.MultiblockVisualizationHandler                                 | normal   | Patchouli-1.0-20.jar                               | false           |
| Quark                         | onInteract                   | vazkii.quark.misc.feature.NoteBlockInterface                                                   | normal   | Quark-r1.6-177.jar                                 | false           |
| PneumaticCraft: Repressurized | onModdedWrenchBlock          | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft                                | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Mekanism                      | rightClickEvent              | Block{minecraft:air}                                                                           | normal   | Mekanism-1.12.2-9.8.3.390.jar                      | false           |
| Tinkers' Construct            | onInteract                   | slimeknights.tconstruct.tools.ToolEvents                                                       | normal   | TConstruct-1.12.2-2.13.0.171.jar                   | false           |
| Pam's HarvestCraft            | onPlayerInteract             | com.pam.harvestcraft.addons.RightClickHarvesting                                               | normal   | Pam's HarvestCraft 1.12.2zf.jar                    | false           |
| Apotheosis                    | rightClick                   | shadows.ench.EnchModule                                                                        | normal   | Apotheosis-1.12.2-1.12.4.jar                       | false           |
| Quark                         | onInteract                   | vazkii.quark.tweaks.feature.RightClickSignEdit                                                 | normal   | Quark-r1.6-177.jar                                 | false           |
| PlusTiC                       | onPlayerUse                  | landmaster.plustic.traits.Global                                                               | normal   | plustic-7.1.6.1.jar                                | false           |
| Patchouli                     | onRightClick                 | vazkii.patchouli.client.handler.BookRightClickHandler                                          | normal   | Patchouli-1.0-20.jar                               | false           |
| AgriCraft                     | waterPadCreation             | com.infinityraider.agricraft.handler.PlayerInteractEventHandler                                | highest  | AgriCraft-2.12.0-1.12.0-a6.jar                     | false           |
| Advanced Rocketry             | blockRightClicked            | zmaster587.advancedRocketry.event.PlanetEventHandler                                           | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| The Betweenlands              | onPlayerRightClick           | thebetweenlands.common.handler.AdvancementHandler                                              | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                         | onPlayerRightClick           | vazkii.quark.tweaks.feature.RemoveSnowLayers                                                   | normal   | Quark-r1.6-177.jar                                 | false           |
| PneumaticCraft: Repressurized | onPlayerInteract             | me.desht.pneumaticcraft.client.render.pneumatic_armor.upgrade_handler.CoordTrackUpgradeHandler | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Advanced Rocketry             | blockRightClicked            | zmaster587.advancedRocketry.event.PlanetEventHandler                                           | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| Storage Drawers               | playerRightClick             | com.jaquadro.minecraft.storagedrawers.core.ClientProxy                                         | normal   | StorageDrawers-1.12.2-5.4.0.jar                    | false           |
| Quark                         | onCropClick                  | vazkii.quark.tweaks.feature.RightClickHarvest                                                  | highest  | Quark-r1.6-177.jar                                 | false           |
| Tell Me                       | onRightClickBlock            | fi.dy.masa.tellme.event.InteractEventHandler                                                   | normal   | tellme-1.12.2-0.7.0-dev.20190610.165828.jar        | false           |
| Quark                         | onInteract                   | vazkii.quark.vanity.feature.SitInStairs                                                        | normal   | Quark-r1.6-177.jar                                 | false           |
| Apotheosis                    | handleUseItem                | shadows.spawn.SpawnerModule                                                                    | normal   | Apotheosis-1.12.2-1.12.4.jar                       | false           |
| AgriCraft                     | vanillaSeedPlanting          | com.infinityraider.agricraft.handler.PlayerInteractEventHandler                                | highest  | AgriCraft-2.12.0-1.12.0-a6.jar                     | false           |
| The Betweenlands              | onUseItem                    | thebetweenlands.common.handler.OverworldItemHandler                                            | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Dimensional Control           | onRightClickBlock            | com.bloodnbonesgaming.dimensionalcontrol.events.DCEventHandler                                 | normal   | DimensionalControl-1.12.2-2.13.0.jar               | false           |
| Quark                         | onRightClick                 | vazkii.quark.tweaks.feature.DirtToPath                                                         | normal   | Quark-r1.6-177.jar                                 | false           |
| Techguns                      | rightClickEvent              | techguns.events.TGEventHandler                                                                 | high     | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| Reborn Core                   | rightClickBlockEvent         | reborncore.common.blocks.BlockWrenchEventHandler                                               | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar         | false           |
| EnderCore                     | handleCropRightClick         | com.enderio.core.common.handlers.RightClickCropHandler                                         | normal   | EnderCore-1.12.2-0.5.73.jar                        | false           |
| Alchemistry                   | rightClickEvent              | al132.alchemistry.EventHandler                                                                 | normal   | alchemistry-1.0.36.jar                             | false           |
| Ice and Fire                  | onPlayerRightClick           | com.github.alexthe666.iceandfire.event.EventLiving                                             | normal   | iceandfire-1.8.3.jar                               | false           |
| Quark                         | onRightClick                 | vazkii.quark.decoration.feature.TieFences                                                      | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | onPlayerInteract             | vazkii.quark.tweaks.feature.DoubleDoors                                                        | lowest   | Quark-r1.6-177.jar                                 | false           |
| AgriCraft                     | denyBonemeal                 | com.infinityraider.agricraft.handler.PlayerInteractEventHandler                                | highest  | AgriCraft-2.12.0-1.12.0-a6.jar                     | false           |
| MCMultiPart                   | onPlayerRightClickBlock      | mcmultipart.client.MCMPClientProxy                                                             | normal   | MCMultiPart-2.5.3.jar                              | false           |
| Draconic Evolution            | rightClickBlock              | com.brandon3055.draconicevolution.handlers.DEEventHandler                                      | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| CraftTweaker2                 | onPlayerRightClickBlockEvent | crafttweaker.mc1120.events.CommonEventHandler                                                  | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |


## LivingEntityUseItemEvent$Stop
| Owner         | Method                         | Location                                      | Priority | Source                        | RecieveCanceled |
|---------------|--------------------------------|-----------------------------------------------|----------|-------------------------------|-----------------|
| CraftTweaker2 | onLivingEntityUseItemStopEvent | crafttweaker.mc1120.events.CommonEventHandler | normal   | CraftTweaker2-1.12-4.1.20.jar | false           |


## RocketEvent$RocketDeOrbitingEvent
| Owner             | Method          | Location                                             | Priority | Source                                          | RecieveCanceled |
|-------------------|-----------------|------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| Advanced Rocketry | onRocketDeorbit | zmaster587.advancedRocketry.event.RocketEventHandler | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |


## PotionColorCalculationEvent
| Owner | Method           | Location                                   | Priority | Source             | RecieveCanceled |
|-------|------------------|--------------------------------------------|----------|--------------------|-----------------|
| Quark | onCalculateColor | vazkii.quark.world.effects.PotionColorizer | normal   | Quark-r1.6-177.jar | false           |


## PopulateChunkEvent$Post
| Owner               | Method                 | Location                                                       | Priority | Source                                          | RecieveCanceled |
|---------------------|------------------------|----------------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| Dimensional Control | onPopulateChunkPost    | com.bloodnbonesgaming.dimensionalcontrol.events.DCEventHandler | highest  | DimensionalControl-1.12.2-2.13.0.jar            | false           |
| Advanced Rocketry   | chunkLoadEvent         | zmaster587.advancedRocketry.event.PlanetEventHandler           | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Chisel              | onLavaLakes            | INSTANCE                                                       | normal   | Chisel-MC1.12.2-1.0.1.44.jar                    | false           |
| Chisel              | onLavaLakes            | INSTANCE                                                       | normal   | Chisel-MC1.12.2-1.0.1.44.jar                    | false           |
| Advanced Rocketry   | populateChunkPostEvent | zmaster587.advancedRocketry.world.decoration.MapGenLander      | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Advanced Rocketry   | chunkLoadEvent         | zmaster587.advancedRocketry.event.PlanetEventHandler           | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |


## GuiContainerEvent$DrawForeground
| Owner                         | Method                 | Location                                          | Priority | Source                                             | RecieveCanceled |
|-------------------------------|------------------------|---------------------------------------------------|----------|----------------------------------------------------|-----------------|
| The Betweenlands              | onGuiDrawPost          | thebetweenlands.client.handler.DebugHandlerClient | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Just Enough Items             | onDrawForegroundEvent  | mezz.jei.gui.GuiEventHandler                      | normal   | jei_1.12.2-4.15.0.291.jar                          | false           |
| PneumaticCraft: Repressurized | guiContainerForeground | me.desht.pneumaticcraft.client.ClientEventHandler | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## GuiScreenEvent$MouseInputEvent$Post
| Owner        | Method          | Location                                                  | Priority | Source                        | RecieveCanceled |
|--------------|-----------------|-----------------------------------------------------------|----------|-------------------------------|-----------------|
| Quark        | onMouseInput    | vazkii.quark.base.client.ModKeybinds$KeybindButtonHandler | normal   | Quark-r1.6-177.jar            | false           |
| Quark        | onMouseInput    | vazkii.quark.base.client.ModKeybinds$KeybindButtonHandler | normal   | Quark-r1.6-177.jar            | false           |
| Quark        | onMouseInput    | vazkii.quark.base.client.ModKeybinds$KeybindButtonHandler | normal   | Quark-r1.6-177.jar            | false           |
| Mouse Tweaks | onGuiMouseInput | yalter.mousetweaks.forge.MouseTweaksForge                 | normal   | MouseTweaks-2.10-mc1.12.2.jar | false           |
| Quark        | onMouseInput    | vazkii.quark.base.client.ModKeybinds$KeybindButtonHandler | normal   | Quark-r1.6-177.jar            | false           |
| JourneyMap   | onGuiMouseEvent | INSTANCE                                                  | normal   | journeymap-1.12.2-5.5.6.jar   | false           |
| Quark        | onMouseInput    | vazkii.quark.base.client.ModKeybinds$KeybindButtonHandler | normal   | Quark-r1.6-177.jar            | false           |
| Quark        | onMouseInput    | vazkii.quark.base.client.ModKeybinds$KeybindButtonHandler | normal   | Quark-r1.6-177.jar            | false           |


## TextureStitchEvent
| Owner         | Method          | Location                                                  | Priority | Source                             | RecieveCanceled |
|---------------|-----------------|-----------------------------------------------------------|----------|------------------------------------|-----------------|
| Techguns      | onTextureStitch | techguns.events.TGEventHandler                            | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar | false           |
| Quark         | onTextureStitch | vazkii.quark.oddities.feature.TotemOfHolding              | normal   | Quark-r1.6-177.jar                 | false           |
| AgriCraft     | onTextureStitch | com.infinityraider.agricraft.handler.TextureStitchHandler | lowest   | AgriCraft-2.12.0-1.12.0-a6.jar     | false           |
| Mutant Beasts | onTextureStitch | chumbanotz.mutantbeasts.client.ClientEventHandler         | normal   | MutantBeasts-1.12.2-0.6.0.jar      | false           |


## PresetUpdateEvent
| Owner      | Method     | Location | Priority | Source                 | RecieveCanceled |
|------------|------------|----------|----------|------------------------|-----------------|
| lambdalib2 | presetEdit | instance | normal   | AcademyCraft-1.1.2.jar | false           |


## FOVUpdateEvent
| Owner                         | Method           | Location                                                             | Priority | Source                                             | RecieveCanceled |
|-------------------------------|------------------|----------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Ender IO                      | handleFovUpdate  | crazypants.enderio.base.item.darksteel.upgrade.speed.SpeedController | lowest   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Ender IO                      | onFovUpdateEvent | crazypants.enderio.base.item.darksteel.ItemDarkSteelBow              | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Tinkers' Construct            | onFovEvent       | slimeknights.tconstruct.tools.common.client.RenderEvents             | normal   | TConstruct-1.12.2-2.13.0.171.jar                   | false           |
| PneumaticCraft: Repressurized | adjustFOVEvent   | me.desht.pneumaticcraft.client.ClientEventHandler                    | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Ender IO                      | onFovUpdateEvent | crazypants.enderio.base.item.darksteel.ItemDarkSteelBow              | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| The Betweenlands              | onUpdateFov      | thebetweenlands.common.item.tools.bow.ItemBLBow                      | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Alchemistry                   | fovEvent         | al132.alchemistry.ClientEventHandler                                 | normal   | alchemistry-1.0.36.jar                             | false           |
| Brandon's Core                | fovUpdate        | com.brandon3055.brandonscore.client.BCClientEventHandler             | low      | BrandonsCore-1.12.2-2.4.17.208-universal.jar       | false           |
| The Betweenlands              | onFovUpdate      | thebetweenlands.common.herblore.elixir.PotionRootBound               | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| The Betweenlands              | onUpdateFov      | thebetweenlands.common.item.shields.ItemSwatShield                   | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Techguns                      | handleFovEvent   | techguns.events.TGEventHandler                                       | low      | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |


## TinkerToolEvent$OnScytheHarvest
| Owner                 | Method   | Location                                  | Priority | Source                              | RecieveCanceled |
|-----------------------|----------|-------------------------------------------|----------|-------------------------------------|-----------------|
| Tinkers Tool Leveling | onScythe | slimeknights.toolleveling.ModToolLeveling | normal   | TinkerToolLeveling-1.12.2-1.1.0.jar | false           |


## AbilityDeactivateEvent
| Owner      | Method            | Location | Priority | Source                 | RecieveCanceled |
|------------|-------------------|----------|----------|------------------------|-----------------|
| lambdalib2 | deactivateAbility | instance | normal   | AcademyCraft-1.1.2.jar | false           |


## UseHoeEvent
| Owner         | Method   | Location                                      | Priority | Source                        | RecieveCanceled |
|---------------|----------|-----------------------------------------------|----------|-------------------------------|-----------------|
| CraftTweaker2 | onUseHoe | crafttweaker.mc1120.events.CommonEventHandler | normal   | CraftTweaker2-1.12-4.1.20.jar | false           |


## TinkerRegisterEvent$ModifierRegisterEvent
| Owner   | Method             | Location                                        | Priority | Source              | RecieveCanceled |
|---------|--------------------|-------------------------------------------------|----------|---------------------|-----------------|
| PlusTiC | onModifierRegister | landmaster.plustic.util.ModifierRegisterPromise | lowest   | plustic-7.1.6.1.jar | false           |


## GuiScreenEvent$KeyboardInputEvent
| Owner       | Method        | Location                                                 | Priority | Source                                      | RecieveCanceled |
|-------------|---------------|----------------------------------------------------------|----------|---------------------------------------------|-----------------|
| Tech Reborn | keyboardEvent | reborncore.client.gui.builder.slot.GuiSlotConfiguration  | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar | false           |
| Tech Reborn | keyboardEvent | reborncore.client.gui.builder.slot.GuiFluidConfiguration | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar | false           |


## EnderIOLifecycleEvent$Config$Pre
| Owner    | Method    | Location                                                  | Priority | Source                    | RecieveCanceled |
|----------|-----------|-----------------------------------------------------------|----------|---------------------------|-----------------|
| Ender IO | preConfig | crazypants.enderio.base.integration.chickens.ChickensUtil | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | preInit   | crazypants.enderio.base.Log                               | normal   | EnderIO-1.12.2-5.1.55.jar | false           |


## NetworkEvent$DisconnectedTile
| Owner                 | Method                     | Location                                          | Priority | Source                                  | RecieveCanceled |
|-----------------------|----------------------------|---------------------------------------------------|----------|-----------------------------------------|-----------------|
| Practical Logistics 2 | onTileDisconnected         | sonar.logistics.base.events.LogisticsEventHandler | high     | practicallogistics2-1.12.2-3.0.8-11.jar | false           |
| Practical Logistics 2 | onWirelessTileDisconnected | sonar.logistics.base.events.LogisticsEventHandler | highest  | practicallogistics2-1.12.2-3.0.8-11.jar | false           |


## GuiScreenEvent$PotionShiftEvent
| Owner             | Method             | Location                                  | Priority | Source                    | RecieveCanceled |
|-------------------|--------------------|-------------------------------------------|----------|---------------------------|-----------------|
| Quark             | onPotionShiftEvent | vazkii.quark.client.feature.NoPotionShift | normal   | Quark-r1.6-177.jar        | false           |
| Just Enough Items | onPotionShiftEvent | mezz.jei.gui.GuiEventHandler              | normal   | jei_1.12.2-4.15.0.291.jar | false           |


## EntityFilterRegisterEvent$DreadLamp
| Owner       | Method                    | Location                                    | Priority | Source                          | RecieveCanceled |
|-------------|---------------------------|---------------------------------------------|----------|---------------------------------|-----------------|
| TorchMaster | registerDreadLampEntities | net.xalcon.torchmaster.compat.VanillaCompat | normal   | torchmaster_1.12.2-1.8.1.81.jar | false           |


## InitMapGenEvent
| Owner               | Method        | Location                                                         | Priority | Source                       | RecieveCanceled |
|---------------------|---------------|------------------------------------------------------------------|----------|------------------------------|-----------------|
| YUNG's Better Caves | onRavineEvent | com.yungnickyoung.minecraft.bettercaves.event.EventRavineGen     | normal   | bettercaves-1.12.2-1.6.0.jar | false           |
| YUNG's Better Caves | onCaveEvent   | com.yungnickyoung.minecraft.bettercaves.event.EventBetterCaveGen | normal   | bettercaves-1.12.2-1.6.0.jar | false           |


## TPSkillHelper$TPCritHitEvent
| Owner      | Method      | Location                                                       | Priority | Source                 | RecieveCanceled |
|------------|-------------|----------------------------------------------------------------|----------|------------------------|-----------------|
| lambdalib2 | onTPCritHit | cn.academy.ability.vanilla.teleporter.client.CriticalHitEffect | normal   | AcademyCraft-1.1.2.jar | false           |


## RenderLivingEvent$Specials$Post
| Owner            | Method         | Location                                                  | Priority | Source                              | RecieveCanceled |
|------------------|----------------|-----------------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onRenderPlayer | thebetweenlands.common.item.equipment.ItemLurkerSkinPouch | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| The Betweenlands | onRenderLiving | thebetweenlands.common.handler.BossHandler                | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| The Betweenlands | onRenderLiving | thebetweenlands.common.item.equipment.ItemAmulet          | lowest   | TheBetweenlands-3.5.5-universal.jar | false           |


## BiomeEvent$GetFoliageColor
| Owner                  | Method            | Location                                                    | Priority | Source                                                    | RecieveCanceled |
|------------------------|-------------------|-------------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| Just Enough Dimensions | onGetFoliageColor | fi.dy.masa.justenoughdimensions.event.JEDEventHandlerClient | normal   | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |


## PlayerInteractEvent
| Owner                         | Method                | Location                                                         | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-----------------------|------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | onPlayerClick         | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft  | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| The Betweenlands              | onItemUse             | thebetweenlands.common.handler.ItemEquipmentHandler              | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| RFTools                       | onPlayerInteractEvent | mcjty.rftools.ForgeEventHandlers                                 | normal   | rftools-1.12-7.72.jar                              | false           |
| The Betweenlands              | onPlayerInteract      | thebetweenlands.common.handler.PuppetHandler                     | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| CraftTweaker2                 | onPlayerInteract      | crafttweaker.mc1120.events.CommonEventHandler                    | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| PneumaticCraft: Repressurized | onInteraction         | me.desht.pneumaticcraft.common.event.EventHandlerUniversalSensor | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## LivingDropsEvent
| Owner                | Method                        | Location                                                            | Priority | Source                                             | RecieveCanceled |
|----------------------|-------------------------------|---------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Quark                | onDrops                       | vazkii.quark.misc.feature.BlackAsh                                  | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                | onDrops                       | vazkii.quark.vanity.feature.WitchHat                                | normal   | Quark-r1.6-177.jar                                 | false           |
| Tinkers' Construct   | onLivingDrops                 | slimeknights.tconstruct.tools.modifiers.ModBeheading                | normal   | TConstruct-1.12.2-2.13.0.171.jar                   | false           |
| The Betweenlands     | onLivingDrops                 | thebetweenlands.common.recipe.censer.CenserRecipeCremains           | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Ender IO             | onDrop                        | crazypants.enderio.base.block.infinity.InfinityFogDropHandler       | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Ender IO             | livingDropsEvent              | crazypants.enderio.base.item.darksteel.upgrade.direct.DirectUpgrade | lowest   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| CraftTweaker2        | mobDrop                       | crafttweaker.mc1120.events.CommonEventHandler                       | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| InControl            | onLivingDrops                 | mcjty.incontrol.ForgeEventHandlers                                  | lowest   | incontrol-1.12-3.9.16.jar                          | false           |
| Mutant Beasts        | onLivingDrops                 | chumbanotz.mutantbeasts.EventHandler                                | normal   | MutantBeasts-1.12.2-0.6.0.jar                      | false           |
| NuclearCraft         | addEntityDrop                 | nc.handler.DropHandler                                              | normal   | NuclearCraft-2.18o-1.12.2.jar                      | false           |
| The Betweenlands     | onDeathDrops                  | thebetweenlands.common.handler.ItemEquipmentHandler                 | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                | onDrops                       | vazkii.quark.misc.feature.SnowGolemPlayerHeads                      | normal   | Quark-r1.6-177.jar                                 | false           |
| Apotheosis           | drops                         | shadows.potion.PotionModule                                         | normal   | Apotheosis-1.12.2-1.12.4.jar                       | false           |
| PlusTiC              | dropEvent                     | landmaster.plustic.traits.Global                                    | lowest   | plustic-7.1.6.1.jar                                | false           |
| Quark                | onEntityDrops                 | vazkii.quark.tweaks.feature.ShearableChickens                       | normal   | Quark-r1.6-177.jar                                 | false           |
| Draconic Evolution   | onDropEvent                   | com.brandon3055.draconicevolution.handlers.DEEventHandler           | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| Quark                | onDrops                       | vazkii.quark.decoration.feature.TallowAndCandles                    | normal   | Quark-r1.6-177.jar                                 | false           |
| InstantUnify         | death                         | mrriegel.instantunify.InstantUnify                                  | lowest   | instantunify-1.12.2-1.1.2.jar                      | false           |
| Open Modular Turrets | lootEvent                     | omtteam.openmodularturrets.handler.OMTEventHandler                  | normal   | openmodularturrets-1.12.2-3.1.4-356.jar            | false           |
| Quark                | onLivingDrops                 | vazkii.quark.tweaks.feature.ChickensShedFeathers                    | normal   | Quark-r1.6-177.jar                                 | false           |
| Apotheosis           | handleCapturing               | shadows.spawn.SpawnerModule                                         | normal   | Apotheosis-1.12.2-1.12.4.jar                       | false           |
| Ice and Fire         | onEntityDrop                  | com.github.alexthe666.iceandfire.event.EventLiving                  | normal   | iceandfire-1.8.3.jar                               | false           |
| Apotheosis           | drops                         | shadows.ench.EnchModule                                             | low      | Apotheosis-1.12.2-1.12.4.jar                       | false           |
| CraftTweaker2        | onEntityLivingDeathDropsEvent | crafttweaker.mc1120.events.CommonEventHandler                       | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Ender IO             | onEntityDrop                  | crazypants.enderio.base.handler.darksteel.SwordHandler              | lowest   | EnderIO-1.12.2-5.1.55.jar                          | false           |


## BiomeEvent$GetGrassColor
| Owner                  | Method          | Location                                                    | Priority | Source                                                    | RecieveCanceled |
|------------------------|-----------------|-------------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| Just Enough Dimensions | onGetGrassColor | fi.dy.masa.justenoughdimensions.event.JEDEventHandlerClient | normal   | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |


## NetworkEvent$ConnectedLocalProvider
| Owner                 | Method                   | Location                                          | Priority | Source                                  | RecieveCanceled |
|-----------------------|--------------------------|---------------------------------------------------|----------|-----------------------------------------|-----------------|
| Practical Logistics 2 | onLocalProviderConnected | sonar.logistics.base.events.LogisticsEventHandler | highest  | practicallogistics2-1.12.2-3.0.8-11.jar | false           |


## PlayerInteractEvent$EntityInteractSpecific
| Owner        | Method                   | Location                                           | Priority | Source                                      | RecieveCanceled |
|--------------|--------------------------|----------------------------------------------------|----------|---------------------------------------------|-----------------|
| Quark        | onEntityInteract         | vazkii.quark.management.feature.ChestsInBoats      | normal   | Quark-r1.6-177.jar                          | false           |
| Tell Me      | onEntityInteractSpecific | fi.dy.masa.tellme.event.InteractEventHandler       | normal   | tellme-1.12.2-0.7.0-dev.20190610.165828.jar | false           |
| Ice and Fire | onEntityInteract         | com.github.alexthe666.iceandfire.event.EventLiving | normal   | iceandfire-1.8.3.jar                        | false           |
| Quark        | onClickEntity            | vazkii.quark.decoration.feature.VariedChests       | normal   | Quark-r1.6-177.jar                          | false           |
| Quark        | onEntityInteractSpecific | vazkii.quark.tweaks.feature.QuickArmorSwapping     | normal   | Quark-r1.6-177.jar                          | false           |


## DecorateBiomeEvent$Post
| Owner               | Method              | Location                                                       | Priority | Source                               | RecieveCanceled |
|---------------------|---------------------|----------------------------------------------------------------|----------|--------------------------------------|-----------------|
| Dimensional Control | onDecorateBiomePost | com.bloodnbonesgaming.dimensionalcontrol.events.DCEventHandler | highest  | DimensionalControl-1.12.2-2.13.0.jar | false           |


## PlayerSetSpawnEvent
| Owner         | Method                | Location                                      | Priority | Source                        | RecieveCanceled |
|---------------|-----------------------|-----------------------------------------------|----------|-------------------------------|-----------------|
| CraftTweaker2 | onPlayerSetSpawnEvent | crafttweaker.mc1120.events.CommonEventHandler | normal   | CraftTweaker2-1.12-4.1.20.jar | false           |


## Apotheosis$ApotheosisRecipeEvent
| Owner      | Method  | Location                    | Priority | Source                       | RecieveCanceled |
|------------|---------|-----------------------------|----------|------------------------------|-----------------|
| Apotheosis | recipes | shadows.garden.GardenModule | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |
| Apotheosis | recipes | shadows.ench.EnchModule     | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |
| Apotheosis | recipes | shadows.potion.PotionModule | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |


## TinkerToolEvent$OnShovelMakePath
| Owner                 | Method | Location                                  | Priority | Source                              | RecieveCanceled |
|-----------------------|--------|-------------------------------------------|----------|-------------------------------------|-----------------|
| Tinkers Tool Leveling | onPath | slimeknights.toolleveling.ModToolLeveling | normal   | TinkerToolLeveling-1.12.2-1.1.0.jar | false           |


## AttackEntityEvent
| Owner                         | Method                    | Location                                                            | Priority | Source                                             | RecieveCanceled |
|-------------------------------|---------------------------|---------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Better Combat Rebirth         | onAttack                  | bettercombat.mod.handler.EventHandlers                              | normal   | BetterCombat-1.12.2-1.5.6.jar                      | false           |
| Ice and Fire                  | onPlayerAttack            | com.github.alexthe666.iceandfire.event.EventLiving                  | normal   | iceandfire-1.8.3.jar                               | false           |
| Chisel                        | onLeftClickEntity         | team.chisel.common.item.ChiselController                            | normal   | Chisel-MC1.12.2-1.0.1.44.jar                       | false           |
| Ice and Fire                  | onPlayerAttackMob         | com.github.alexthe666.iceandfire.event.EventLiving                  | normal   | iceandfire-1.8.3.jar                               | false           |
| Ender IO                      | attackEntityEvent         | crazypants.enderio.base.item.darksteel.upgrade.direct.DirectUpgrade | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| CraftTweaker2                 | onPlayerAttackEntityEvent | crafttweaker.mc1120.events.CommonEventHandler                       | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Practical Logistics 2         | onEntityAttack            | sonar.logistics.network.PL2Client                                   | normal   | practicallogistics2-1.12.2-3.0.8-11.jar            | false           |
| PneumaticCraft: Repressurized | onPlayerAttack            | me.desht.pneumaticcraft.common.event.EventHandlerUniversalSensor    | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Apotheosis                    | trackCooldown             | shadows.Apotheosis                                                  | normal   | Apotheosis-1.12.2-1.12.4.jar                       | false           |
| Grimoire of Gaia 3            | onAttackWithWeaponBooks   | gaia.proxy.ClientProxy                                              | normal   | GrimoireOfGaia3-1.12.2-1.6.9.3.jar                 | false           |


## RenderGameOverlayEvent$Chat
| Owner | Method     | Location                                      | Priority | Source             | RecieveCanceled |
|-------|------------|-----------------------------------------------|----------|--------------------|-----------------|
| Quark | getChatPos | vazkii.quark.client.feature.RenderItemsInChat | normal   | Quark-r1.6-177.jar | false           |


## FMLNetworkEvent$ClientConnectedToServerEvent
| Owner             | Method                    | Location                                                     | Priority | Source                        | RecieveCanceled |
|-------------------|---------------------------|--------------------------------------------------------------|----------|-------------------------------|-----------------|
| Just Enough Items | onClientConnectedToServer | mezz.jei.startup.ProxyCommonClient                           | normal   | jei_1.12.2-4.15.0.291.jar     | false           |
| LagGoggles        | onLogin                   | cf.terminator.laggoggles.client.ClientProxy$2                | normal   | LagGoggles-FAT-1.12.2-4.9.jar | false           |
| Tick Dynamic      | clientJoinWorld           | com.wildex999.tickdynamic.TickDynamicMod                     | normal   | TickDynamic-1.12.2-1.0.2.jar  | false           |
| Mekanism          | onConnection              | mekanism.client.ClientConnectionHandler                      | normal   | Mekanism-1.12.2-9.8.3.390.jar | false           |
| JourneyMap        | onConnect                 | journeymap.client.forge.event.PlayerConnectHandler           | normal   | journeymap-1.12.2-5.5.6.jar   | false           |
| Building Gadgets  | onJoinWorld               | com.direwolf20.buildinggadgets.client.events.EventClientTick | normal   | BuildingGadgets-2.7.4.jar     | false           |


## InputEvent
| Owner                  | Method       | Location                                        | Priority | Source                                          | RecieveCanceled |
|------------------------|--------------|-------------------------------------------------|----------|-------------------------------------------------|-----------------|
| The Betweenlands       | onInput      | thebetweenlands.client.handler.InputHandler     | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| Better Builder's Wands | KeyEvent     | portablejim.bbw.core.client.KeyEvents           | normal   | BetterBuildersWands-1.12-0.11.1.245+69d0d70.jar | false           |
| OreExcavation          | onKeyPressed | oreexcavation.handlers.EventHandler             | normal   | OreExcavation-1.4.143.jar                       | false           |
| Rebind Narrator        | input        | quaternary.rebindnarrator.RebindNarrator$Events | normal   | rebindnarrator-1.0.jar                          | false           |
| Mekanism               | onTick       | mekanism.client.MekanismKeyHandler              | normal   | Mekanism-1.12.2-9.8.3.390.jar                   | false           |


## CalcEvent$MaxOverload
| Owner      | Method            | Location                           | Priority | Source                 | RecieveCanceled |
|------------|-------------------|------------------------------------|----------|------------------------|-----------------|
| lambdalib2 | recalcMaxOverload | <unassigned>.brain_course_advanced | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | recalcMaxOverload | <unassigned>.brain_course_advanced | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | recalcMaxOverload | <unassigned>.brain_course_advanced | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | recalcMaxOverload | <unassigned>.brain_course_advanced | normal   | AcademyCraft-1.1.2.jar | false           |


## EntityViewRenderEvent$CameraSetup
| Owner                         | Method        | Location                                           | Priority | Source                                             | RecieveCanceled |
|-------------------------------|---------------|----------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Ice and Fire                  | onCameraSetup | com.github.alexthe666.iceandfire.event.EventClient | normal   | iceandfire-1.8.3.jar                               | false           |
| PneumaticCraft: Repressurized | screenTilt    | me.desht.pneumaticcraft.client.ClientEventHandler  | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## UnlinkUserEvent
| Owner      | Method     | Location                              | Priority | Source                 | RecieveCanceled |
|------------|------------|---------------------------------------|----------|------------------------|-----------------|
| lambdalib2 | unlinkUser | cn.academy.energy.impl.WirelessSystem | normal   | AcademyCraft-1.1.2.jar | false           |


## DynamicNetwork$TransmittersAddedEvent
| Owner    | Method                   | Location                 | Priority | Source                        | RecieveCanceled |
|----------|--------------------------|--------------------------|----------|-------------------------------|-----------------|
| Mekanism | onTransmittersAddedEvent | mekanism.common.Mekanism | normal   | Mekanism-1.12.2-9.8.3.390.jar | false           |


## BlockEvent$FarmlandTrampleEvent
| Owner                         | Method            | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-------------------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | onFarmlandTrample | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticArmor | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## CalcEvent$CPRecoverSpeed
| Owner      | Method          | Location                 | Priority | Source                 | RecieveCanceled |
|------------|-----------------|--------------------------|----------|------------------------|-----------------|
| lambdalib2 | recalcCPRecover | <unassigned>.mind_course | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | recalcCPRecover | <unassigned>.mind_course | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | recalcCPRecover | <unassigned>.mind_course | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | recalcCPRecover | <unassigned>.mind_course | normal   | AcademyCraft-1.1.2.jar | false           |


## AnimalTameEvent
| Owner | Method | Location                               | Priority | Source             | RecieveCanceled |
|-------|--------|----------------------------------------|----------|--------------------|-----------------|
| Quark | onTame | vazkii.quark.tweaks.feature.PatTheDogs | normal   | Quark-r1.6-177.jar | false           |


## LivingSpawnEvent$CheckSpawn
| Owner              | Method             | Location                                                                   | Priority | Source                                          | RecieveCanceled |
|--------------------|--------------------|----------------------------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| RFTools            | onEntitySpawnEvent | mcjty.rftools.ForgeEventHandlers                                           | normal   | rftools-1.12-7.72.jar                           | false           |
| TorchMaster        | onEntityCheckSpawn | net.xalcon.torchmaster.common.EventHandlerServer                           | lowest   | torchmaster_1.12.2-1.8.1.81.jar                 | false           |
| InControl          | onEntitySpawnEvent | mcjty.incontrol.ForgeEventHandlers                                         | lowest   | incontrol-1.12-3.9.16.jar                       | false           |
| Grimoire of Gaia 3 | onSpawn            | gaia.init.GaiaSpawning$DimensionHandler                                    | normal   | GrimoireOfGaia3-1.12.2-1.6.9.3.jar              | false           |
| Advanced Rocketry  | CheckSpawn         | zmaster587.advancedRocketry.event.PlanetEventHandler                       | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Ender IO           | onEntitySpawn      | crazypants.enderio.machines.machine.obelisk.base.SpawningObeliskController | normal   | EnderIO-1.12.2-5.1.55.jar                       | false           |
| Mutant Beasts      | onLivingSpawnCheck | chumbanotz.mutantbeasts.EventHandler                                       | normal   | MutantBeasts-1.12.2-0.6.0.jar                   | false           |
| Grimoire of Gaia 3 | debugGaiaSpawn     | gaia.proxy.ClientProxy                                                     | normal   | GrimoireOfGaia3-1.12.2-1.6.9.3.jar              | false           |
| Advanced Rocketry  | CheckSpawn         | zmaster587.advancedRocketry.event.PlanetEventHandler                       | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| CraftTweaker2      | onCheckSpawnEvent  | crafttweaker.mc1120.events.CommonEventHandler                              | normal   | CraftTweaker2-1.12-4.1.20.jar                   | false           |
| NuclearCraft       | onLootTableLoad    | nc.handler.EntityHandler                                                   | normal   | NuclearCraft-2.18o-1.12.2.jar                   | false           |
| Quark              | onSpawnCheck       | vazkii.quark.misc.feature.BlackAsh                                         | normal   | Quark-r1.6-177.jar                              | false           |
| Quark              | onSpawn            | vazkii.quark.world.feature.Wraiths                                         | normal   | Quark-r1.6-177.jar                              | false           |
| Quark              | onSpawn            | vazkii.quark.world.feature.NaturalBlazesInNether                           | normal   | Quark-r1.6-177.jar                              | false           |
| EnderCore          | onEntitySpawn      | com.enderio.core.common.handlers.FluidSpawnHandler                         | normal   | EnderCore-1.12.2-0.5.73.jar                     | false           |


## PlayerSleepInBedEvent
| Owner              | Method                  | Location                                               | Priority | Source                                          | RecieveCanceled |
|--------------------|-------------------------|--------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| The Lost Cities    | onPlayerSleepInBedEvent | mcjty.lostcities.ForgeEventHandlers                    | normal   | lostcities-1.12-2.0.21.jar                      | false           |
| Epic Siege Mod     | onPlayerSleepInBed      | funwayguy.epicsiegemod.handlers.entities.PlayerHandler | normal   | EpicSiegeMod-13.165.jar                         | false           |
| Advanced Rocketry  | sleepEvent              | zmaster587.advancedRocketry.event.PlanetEventHandler   | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Advanced Rocketry  | sleepEvent              | zmaster587.advancedRocketry.event.PlanetEventHandler   | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Compact Machines 3 | onPlayerSleepInBed      | org.dave.compactmachines3.misc.PlayerEventHandler      | normal   | compactmachines3-1.12.2-3.0.18-b278.jar         | false           |
| CraftTweaker2      | onPlayerSleepInBed      | crafttweaker.mc1120.events.CommonEventHandler          | normal   | CraftTweaker2-1.12-4.1.20.jar                   | false           |


## EntityTravelToDimensionEvent
| Owner                  | Method                         | Location                                              | Priority | Source                                                    | RecieveCanceled |
|------------------------|--------------------------------|-------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| Just Enough Dimensions | onEntityTravelToDimensionEvent | fi.dy.masa.justenoughdimensions.event.JEDEventHandler | highest  | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |


## NetworkEvent$ConnectedNetwork
| Owner                 | Method              | Location                                          | Priority | Source                                  | RecieveCanceled |
|-----------------------|---------------------|---------------------------------------------------|----------|-----------------------------------------|-----------------|
| Practical Logistics 2 | onNetworksConnected | sonar.logistics.base.events.LogisticsEventHandler | highest  | practicallogistics2-1.12.2-3.0.8-11.jar | false           |


## ExplosionEvent$Start
| Owner          | Method    | Location                                                | Priority | Source                  | RecieveCanceled |
|----------------|-----------|---------------------------------------------------------|----------|-------------------------|-----------------|
| Epic Siege Mod | onExplode | funwayguy.epicsiegemod.handlers.entities.CreeperHandler | normal   | EpicSiegeMod-13.165.jar | false           |


## ItemExpireEvent
| Owner              | Method            | Location                                                                        | Priority | Source                           | RecieveCanceled |
|--------------------|-------------------|---------------------------------------------------------------------------------|----------|----------------------------------|-----------------|
| Tinkers' Construct | onItemExpire      | slimeknights.tconstruct.gadgets.block.BlockSlimeChannel$EventHandler            | normal   | TConstruct-1.12.2-2.13.0.171.jar | false           |
| CraftTweaker2      | onItemExpireEvent | crafttweaker.mc1120.events.CommonEventHandler                                   | normal   | CraftTweaker2-1.12-4.1.20.jar    | false           |
| Tinkers' Construct | onExpire          | slimeknights.tconstruct.library.tinkering.IndestructibleEntityItem$EventHandler | normal   | TConstruct-1.12.2-2.13.0.171.jar | false           |


## DynamicNetwork$NetworkClientRequest
| Owner    | Method                 | Location                 | Priority | Source                        | RecieveCanceled |
|----------|------------------------|--------------------------|----------|-------------------------------|-----------------|
| Mekanism | onNetworkClientRequest | mekanism.common.Mekanism | normal   | Mekanism-1.12.2-9.8.3.390.jar | false           |


## PopulateChunkEvent$Pre
| Owner               | Method             | Location                                                       | Priority | Source                               | RecieveCanceled |
|---------------------|--------------------|----------------------------------------------------------------|----------|--------------------------------------|-----------------|
| Chunk Pregenerator  | onPopulation       | pregenerator.impl.structure.StructureManager                   | normal   | Chunk Pregenerator V1.12-2.2.jar     | false           |
| Dimensional Control | onPopulateChunkPre | com.bloodnbonesgaming.dimensionalcontrol.events.DCEventHandler | highest  | DimensionalControl-1.12.2-2.13.0.jar | false           |
| Apotheosis          | terrainGen         | shadows.deadly.gen.WorldGenerator                              | normal   | Apotheosis-1.12.2-1.12.4.jar         | false           |


## ConfigChangedEvent$PostConfigChangedEvent
| Owner             | Method        | Location                                     | Priority | Source                    | RecieveCanceled |
|-------------------|---------------|----------------------------------------------|----------|---------------------------|-----------------|
| Just Enough Items | onPostChanged | com.yogpc.qp.integration.jei.QuarryJeiPlugin | normal   | jei_1.12.2-4.15.0.291.jar | false           |


## TinkerToolEvent$ExtraBlockBreak
| Owner              | Method            | Location                                 | Priority | Source                           | RecieveCanceled |
|--------------------|-------------------|------------------------------------------|----------|----------------------------------|-----------------|
| Tinkers' Construct | onExtraBlockBreak | slimeknights.tconstruct.tools.ToolEvents | normal   | TConstruct-1.12.2-2.13.0.171.jar | false           |


## GuiScreenEvent$MouseInputEvent$Pre
| Owner                    | Method           | Location                                         | Priority | Source                               | RecieveCanceled |
|--------------------------|------------------|--------------------------------------------------|----------|--------------------------------------|-----------------|
| Simple Inventory sorting | onMouse          | cpw.mods.inventorysorter.KeyHandler              | lowest   | inventorysorter-1.12.2-1.13.3+57.jar | false           |
| Quark                    | mouseEvent       | vazkii.quark.management.feature.DeleteItems      | normal   | Quark-r1.6-177.jar                   | false           |
| Just Enough Items        | handleMouseClick | techreborn.compat.jei.TechRebornJeiPlugin        | normal   | jei_1.12.2-4.15.0.291.jar            | false           |
| AutoRegLib               | onRightClick     | vazkii.arl.util.DropInHandler                    | normal   | AutoRegLib-1.3-32.jar                | false           |
| MoreOverlays             | onGuiClick       | at.feldim2425.moreoverlays.itemsearch.GuiHandler | normal   | moreoverlays-1.15.1-mc1.12.2.jar     | false           |
| Quark                    | onClick          | vazkii.quark.client.feature.ChestSearchBar       | normal   | Quark-r1.6-177.jar                   | false           |
| Quark                    | mouseEvent       | vazkii.quark.management.feature.FavoriteItems    | highest  | Quark-r1.6-177.jar                   | false           |
| Just Enough Items        | onGuiMouseEvent  | mezz.jei.input.InputHandler                      | normal   | jei_1.12.2-4.15.0.291.jar            | false           |


## ItemTooltipEvent
| Owner                         | Method                 | Location                                                   | Priority | Source                                             | RecieveCanceled |
|-------------------------------|------------------------|------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| The Betweenlands              | onItemTooltip          | thebetweenlands.client.handler.ItemTooltipHandler          | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                         | makeTooltip            | vazkii.quark.client.feature.FoodTooltip                    | normal   | Quark-r1.6-177.jar                                 | false           |
| PlusTiC                       | tooltip                | landmaster.plustic.traits.Global                           | normal   | plustic-7.1.6.1.jar                                | false           |
| Quark                         | makeTooltip            | vazkii.quark.client.feature.ShulkerBoxTooltip              | highest  | Quark-r1.6-177.jar                                 | false           |
| Apotheosis                    | tooltips               | shadows.ApotheosisClient                                   | normal   | Apotheosis-1.12.2-1.12.4.jar                       | false           |
| EnderCore                     | onItemTooltip          | com.enderio.core.client.handlers.OreDictTooltipHandler     | normal   | EnderCore-1.12.2-0.5.73.jar                        | false           |
| PlusTiC                       | tooltip                | landmaster.plustic.traits.Portly                           | normal   | plustic-7.1.6.1.jar                                | false           |
| Draconic Evolution            | itemTooltipEvent       | com.brandon3055.draconicevolution.handlers.DEEventHandler  | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| McJtyLib                      | onMakeTooltip          | mcjty.lib.tooltips.TooltipRender                           | normal   | mcjtylib-1.12-3.5.4.jar                            | false           |
| Ender IO                      | onTooltip              | crazypants.enderio.base.machine.recipes.ClearConfigRecipe  | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Ender IO                      | addTooltip             | crazypants.enderio.base.paint.PaintTooltipUtil             | highest  | EnderIO-1.12.2-5.1.55.jar                          | false           |
| JEI Integration               | onItemTooltip          | com.snowshock35.jeiintegration.TooltipEventHandler         | normal   | jeiintegration_1.12.2-1.5.1.36.jar                 | false           |
| Tinkers Tool Leveling         | onTooltip              | slimeknights.toolleveling.EventHandler                     | normal   | TinkerToolLeveling-1.12.2-1.1.0.jar                | false           |
| CraftTweaker2                 | onItemTooltip          | crafttweaker.mc1120.events.ClientEventHandler              | low      | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Quark                         | makeTooltip            | vazkii.quark.management.feature.FavoriteItems              | normal   | Quark-r1.6-177.jar                                 | false           |
| PneumaticCraft: Repressurized | onItemTooltip          | me.desht.pneumaticcraft.client.ClientEventHandler          | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| AgriCraft                     | addRegistryInfo        | com.infinityraider.agricraft.handler.ItemToolTipHandler    | normal   | AgriCraft-2.12.0-1.12.0-a6.jar                     | false           |
| Quark                         | makeTooltip            | vazkii.quark.client.feature.EnchantedBooksShowItems        | normal   | Quark-r1.6-177.jar                                 | false           |
| AgriCraft                     | addClipperTooltip      | com.infinityraider.agricraft.handler.ItemToolTipHandler    | normal   | AgriCraft-2.12.0-1.12.0-a6.jar                     | false           |
| Quark                         | removeCurseTooltip     | vazkii.quark.oddities.feature.Backpacks                    | normal   | Quark-r1.6-177.jar                                 | false           |
| Tech Reborn                   | toolTip                | techreborn.utils.StackWIPHandler                           | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar        | false           |
| Quark                         | makeTooltip            | vazkii.quark.client.feature.VisualStatDisplay              | normal   | Quark-r1.6-177.jar                                 | false           |
| EnderCore                     | addTooltip             | com.enderio.core.client.handlers.SpecialTooltipHandler     | normal   | EnderCore-1.12.2-0.5.73.jar                        | false           |
| PlusTiC                       | tooltip                | landmaster.plustic.modules.ModuleGems                      | normal   | plustic-7.1.6.1.jar                                | false           |
| Quark                         | makeTooltip            | vazkii.quark.vanity.feature.DyeItemNames                   | normal   | Quark-r1.6-177.jar                                 | false           |
| Tech Reborn                   | handleItemTooltipEvent | techreborn.events.StackToolTipEvent                        | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar        | false           |
| Quark                         | onTooltip              | vazkii.quark.vanity.feature.DyableElytra                   | normal   | Quark-r1.6-177.jar                                 | false           |
| AgriCraft                     | addNbtInfo             | com.infinityraider.agricraft.handler.ItemToolTipHandler    | normal   | AgriCraft-2.12.0-1.12.0-a6.jar                     | false           |
| Item Blacklist                | itemTooltipEvent       | net.doubledoordev.itemblacklist.client.ClientEventHandlers | normal   | ItemBlacklist-1.4.3.jar                            | false           |
| Quark                         | makeTooltip            | vazkii.quark.client.feature.MapTooltip                     | highest  | Quark-r1.6-177.jar                                 | false           |
| AgriCraft                     | addTrowelTooltip       | com.infinityraider.agricraft.handler.ItemToolTipHandler    | normal   | AgriCraft-2.12.0-1.12.0-a6.jar                     | false           |
| Mod Name Tooltip              | onToolTip              | mezz.modnametooltip.TooltipEventHandler                    | low      | modnametooltip_1.12.2-1.10.1.jar                   | false           |
| NuclearCraft                  | addAdditionalTooltips  | nc.handler.TooltipHandler                                  | normal   | NuclearCraft-2.18o-1.12.2.jar                      | false           |
| PlusTiC                       | tooltip                | landmaster.plustic.api.Portal                              | normal   | plustic-7.1.6.1.jar                                | false           |
| AgriCraft                     | addOreDictInfo         | com.infinityraider.agricraft.handler.ItemToolTipHandler    | normal   | AgriCraft-2.12.0-1.12.0-a6.jar                     | false           |
| TOP Addons                    | onItemTooltip          | io.github.drmanganese.topaddons.proxy.ClientProxy          | normal   | topaddons-1.12.2-1.12.0.jar                        | false           |
| PlusTiC                       | tooltip                | landmaster.plustic.api.Toggle                              | normal   | plustic-7.1.6.1.jar                                | false           |
| Techguns                      | ItemRadiationTooltip   | techguns.events.TGEventHandler                             | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| Enchantment Descriptions      | onTooltipDisplayed     | net.darkhax.enchdesc.client.TooltipHandler                 | normal   | EnchantmentDescriptions-1.12.2-1.1.15.jar          | false           |
| Tinkers' Construct            | addTooltip             | slimeknights.tconstruct.gadgets.WoodenHopperGUIDrawEvent   | normal   | TConstruct-1.12.2-2.13.0.171.jar                   | false           |
| Alchemistry                   | tooltipEvent           | al132.alchemistry.ClientEventHandler                       | normal   | alchemistry-1.0.36.jar                             | false           |
| EnderCore                     | handleTooltip          | com.enderio.core.client.handlers.EnchantTooltipHandler     | normal   | EnderCore-1.12.2-0.5.73.jar                        | false           |
| Quark                         | onTooltip              | vazkii.quark.oddities.feature.MatrixEnchanting             | normal   | Quark-r1.6-177.jar                                 | false           |
| AgriCraft                     | addSeedStatsTooltip    | com.infinityraider.agricraft.handler.ItemToolTipHandler    | normal   | AgriCraft-2.12.0-1.12.0-a6.jar                     | false           |


## LinkNodeEvent
| Owner      | Method   | Location                              | Priority | Source                 | RecieveCanceled |
|------------|----------|---------------------------------------|----------|------------------------|-----------------|
| lambdalib2 | linkNode | cn.academy.energy.impl.WirelessSystem | normal   | AcademyCraft-1.1.2.jar | false           |


## DestroyNetworkEvent
| Owner      | Method       | Location                              | Priority | Source                 | RecieveCanceled |
|------------|--------------|---------------------------------------|----------|------------------------|-----------------|
| lambdalib2 | onDestroyNet | cn.academy.energy.impl.WirelessSystem | normal   | AcademyCraft-1.1.2.jar | false           |


## FluidNetwork$FluidTransferEvent
| Owner    | Method              | Location                 | Priority | Source                        | RecieveCanceled |
|----------|---------------------|--------------------------|----------|-------------------------------|-----------------|
| Mekanism | onLiquidTransferred | mekanism.common.Mekanism | normal   | Mekanism-1.12.2-9.8.3.390.jar | false           |


## RenderItemInFrameEvent
| Owner           | Method                 | Location                                                   | Priority | Source                                        | RecieveCanceled |
|-----------------|------------------------|------------------------------------------------------------|----------|-----------------------------------------------|-----------------|
| Item Blacklist  | renderItemInFrameEvent | net.doubledoordev.itemblacklist.client.ClientEventHandlers | normal   | ItemBlacklist-1.4.3.jar                       | false           |
| CodeChicken Lib | onItemFrameRender      | codechicken.lib.render.item.map.MapRenderRegistry          | normal   | CodeChickenLib-1.12.2-3.2.3.358-universal.jar | false           |


## PlayerInteractEvent$LeftClickBlock
| Owner                         | Method           | Location                                             | Priority | Source                                             | RecieveCanceled |
|-------------------------------|------------------|------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Chisel                        | onPlayerInteract | team.chisel.common.item.ChiselController             | normal   | Chisel-MC1.12.2-1.0.1.44.jar                       | false           |
| SwingThroughGrass             | onLeftClick      | com.exidex.stg.LeftClickEventHandler                 | normal   | stg-1.12.2-1.2.3.jar                               | false           |
| PneumaticCraft: Repressurized | onBlockLeftClick | me.desht.pneumaticcraft.common.item.ItemGPSAreaTool  | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| McJtyLib                      | onPlayerInteract | mcjty.lib.McJtyLib$EventHandler                      | normal   | mcjtylib-1.12-3.5.4.jar                            | false           |
| Ender IO                      | onClick          | crazypants.enderio.base.block.coldfire.BlockColdFire | lowest   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Practical Logistics 2         | onLeftClick      | sonar.logistics.base.events.PL2Events                | normal   | practicallogistics2-1.12.2-3.0.8-11.jar            | false           |
| The Betweenlands              | onLeftClickBlock | thebetweenlands.common.handler.LocationHandler       | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                         | leftClick        | vazkii.quark.tweaks.feature.KnockOnDoors             | normal   | Quark-r1.6-177.jar                                 | false           |


## GuiScreenEvent$InitGuiEvent$Post
| Owner               | Method           | Location                                                                 | Priority | Source                               | RecieveCanceled |
|---------------------|------------------|--------------------------------------------------------------------------|----------|--------------------------------------|-----------------|
| Quark               | guiOpenEvent     | vazkii.quark.world.feature.DefaultWorldOptions                           | normal   | Quark-r1.6-177.jar                   | false           |
| Quark               | initGui          | vazkii.quark.client.feature.ChestSearchBar                               | normal   | Quark-r1.6-177.jar                   | false           |
| Just Enough Items   | onGuiInit        | mezz.jei.gui.GuiEventHandler                                             | normal   | jei_1.12.2-4.15.0.291.jar            | false           |
| MoreOverlays        | onGuiInit        | at.feldim2425.moreoverlays.itemsearch.GuiHandler                         | low      | moreoverlays-1.15.1-mc1.12.2.jar     | false           |
| Dimensional Control | onInitGui        | com.bloodnbonesgaming.dimensionalcontrol.client.event.ClientEventHandler | normal   | DimensionalControl-1.12.2-2.13.0.jar | false           |
| Quark               | initGui          | vazkii.quark.management.feature.StoreToChests                            | normal   | Quark-r1.6-177.jar                   | false           |
| Quark               | onInitGui        | vazkii.quark.tweaks.feature.AutomaticRecipeUnlock                        | normal   | Quark-r1.6-177.jar                   | false           |
| LLibrary            | onInitGuiPost    | INSTANCE                                                                 | normal   | llibrary-1.7.19-1.12.2.jar           | false           |
| Chunk Pregenerator  | onGuiOpened      | pregenerator.impl.client.ClientHandler                                   | normal   | Chunk Pregenerator V1.12-2.2.jar     | false           |
| Quark               | initGui          | vazkii.quark.vanity.feature.EmoteSystem                                  | normal   | Quark-r1.6-177.jar                   | false           |
| Quark               | initGui          | vazkii.quark.management.feature.ChestButtons                             | normal   | Quark-r1.6-177.jar                   | false           |
| Custom Main Menu    | initGuiPost      | lumien.custommainmenu.handler.CMMEventHandler                            | lowest   | CustomMainMenu-MC1.12.2-2.0.9.1.jar  | false           |
| Patchouli           | onGuiInitPost    | vazkii.patchouli.client.base.InventoryBookButtonHandler                  | highest  | Patchouli-1.0-20.jar                 | false           |
| Techguns            | guiPostInit      | micdoodle8.mods.galacticraft.api.client.tabs.TabRegistry                 | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar   | false           |
| ReAuth              | open             | technicianlp.reauth.GuiHandler                                           | normal   | reauth-3.6.0.jar                     | false           |
| Ender IO            | onGuiInit        | crazypants.enderio.base.handler.RecipeButtonHandler                      | normal   | EnderIO-1.12.2-5.1.55.jar            | false           |
| Just Enough Items   | onGuiInit        | mezz.jei.input.MouseHelper                                               | normal   | jei_1.12.2-4.15.0.291.jar            | false           |
| Custom Main Menu    | initGuiPostEarly | lumien.custommainmenu.handler.CMMEventHandler                            | highest  | CustomMainMenu-MC1.12.2-2.0.9.1.jar  | false           |
| No Recipe Book      | InitGui          | com.luwin.norecipebook.EventHandler                                      | highest  | noRecipeBook_v1.2.2formc1.12.2.jar   | false           |
| Quark               | initGui          | vazkii.quark.management.feature.DeleteItems                              | normal   | Quark-r1.6-177.jar                   | false           |


## PlayerContainerEvent$Open
| Owner              | Method                     | Location                                                                | Priority | Source                           | RecieveCanceled |
|--------------------|----------------------------|-------------------------------------------------------------------------|----------|----------------------------------|-----------------|
| CraftTweaker2      | onPlayerOpenContainer      | crafttweaker.mc1120.events.CommonEventHandler                           | normal   | CraftTweaker2-1.12-4.1.20.jar    | false           |
| Apotheosis         | enchContainer              | shadows.ench.EnchModule                                                 | normal   | Apotheosis-1.12.2-1.12.4.jar     | false           |
| Tinkers' Construct | onCraftingStationGuiOpened | slimeknights.tconstruct.tools.common.inventory.ContainerCraftingStation | normal   | TConstruct-1.12.2-2.13.0.171.jar | false           |


## CategoryChangeEvent
| Owner      | Method             | Location | Priority | Source                 | RecieveCanceled |
|------------|--------------------|----------|----------|------------------------|-----------------|
| lambdalib2 | __onCategoryChange | instance | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | changedCategory    | instance | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | onCategoryChanged  | instance | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | onCategoryChange   | instance | normal   | AcademyCraft-1.1.2.jar | false           |


## InfoEvent$ListChanged
| Owner                 | Method        | Location                                          | Priority | Source                                  | RecieveCanceled |
|-----------------------|---------------|---------------------------------------------------|----------|-----------------------------------------|-----------------|
| Practical Logistics 2 | onListChanged | sonar.logistics.base.events.LogisticsEventHandler | highest  | practicallogistics2-1.12.2-3.0.8-11.jar | false           |


## ConfigChangedEvent$OnConfigChangedEvent
| Owner                              | Method                      | Location                                                                        | Priority | Source                                                    | RecieveCanceled |
|------------------------------------|-----------------------------|---------------------------------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| Akashic Tome                       | onConfigChanged             | vazkii.akashictome.ConfigHandler$ChangeListener                                 | normal   | AkashicTome-1.2-12.jar                                    | false           |
| air_support                        | onConfigChanged             | air_support.configs.ConfigHandler                                               | normal   | AirSupport-0.3.0.jar                                      | false           |
| EnderCore                          | onConfigChanged             | com.enderio.core.common.config.ConfigHandler$1                                  | normal   | EnderCore-1.12.2-0.5.73.jar                               | false           |
| Ice and Fire                       | onConfigChanged             | com.github.alexthe666.iceandfire.ClientProxy                                    | normal   | iceandfire-1.8.3.jar                                      | false           |
| Default Options                    | onConfigChanged             | net.blay09.mods.defaultoptions.DefaultOptions                                   | normal   | DefaultOptions_1.12.2-9.2.8.jar                           | false           |
| FoamFix                            | configChanged               | pl.asie.foamfix.FoamFix                                                         | normal   | foamfix-0.10.10-1.12.2.jar                                | false           |
| SwingThroughGrass                  | onConfigChanged             | com.exidex.stg.STGConfig                                                        | normal   | stg-1.12.2-1.2.3.jar                                      | false           |
| Storage Drawers                    | onConfigChanged             | com.jaquadro.minecraft.storagedrawers.StorageDrawers                            | normal   | StorageDrawers-1.12.2-5.4.0.jar                           | false           |
| PneumaticCraft: Repressurized      | onConfigChange              | me.desht.pneumaticcraft.common.thirdparty.patchouli.Patchouli                   | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar        | false           |
| Morph-o-Tool                       | onConfigChanged             | vazkii.morphtool.ConfigHandler$ChangeListener                                   | normal   | Morph-o-Tool-1.2-21.jar                                   | false           |
| Building Gadgets                   | onConfigurationChanged      | com.direwolf20.buildinggadgets.common.events.ConfigEventHandler                 | normal   | BuildingGadgets-2.7.4.jar                                 | false           |
| Just Enough Items                  | onConfigChanged             | mezz.jei.startup.ProxyCommonClient                                              | normal   | jei_1.12.2-4.15.0.291.jar                                 | false           |
| JEI Integration                    | onConfigChanged             | com.snowshock35.jeiintegration.config.Config                                    | normal   | jeiintegration_1.12.2-1.5.1.36.jar                        | false           |
| Mod Name Tooltip                   | onConfigChanged             | mezz.modnametooltip.Config                                                      | normal   | modnametooltip_1.12.2-1.10.1.jar                          | false           |
| Mutant Beasts                      | onConfigChanged             | chumbanotz.mutantbeasts.EventHandler                                            | normal   | MutantBeasts-1.12.2-0.6.0.jar                             | false           |
| LLibrary                           | onConfigChanged             | INSTANCE                                                                        | normal   | llibrary-1.7.19-1.12.2.jar                                | false           |
| Better Combat Rebirth              | onConfigurationChangedEvent | bettercombat.mod.util.ConfigurationHandler                                      | normal   | BetterCombat-1.12.2-1.5.6.jar                             | false           |
| OreExcavation Integration          | onConfigChanged             | atm.bloodworkxgaming.oeintegration.MainConfig$ConfigurationHolder               | normal   | oeintegration-2.3.4.jar                                   | false           |
| Brandon's Core                     | onConfigChanges             | com.brandon3055.brandonscore.handlers.BCEventHandler                            | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar              | false           |
| TorchMaster                        | onConfigChanged             | net.xalcon.torchmaster.common.TorchmasterConfig                                 | normal   | torchmaster_1.12.2-1.8.1.81.jar                           | false           |
| Techguns                           | onConfigChanged             | techguns.TGConfig                                                               | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                        | false           |
| Patchouli                          | onConfigChanged             | vazkii.patchouli.common.base.PatchouliConfig$ChangeListener                     | normal   | Patchouli-1.0-20.jar                                      | false           |
| Progressive Automation             | onConfigChanged             | com.vanhal.progressiveautomation.ProgressiveAutomation                          | normal   | ProgressiveAutomation-1.12.2-1.7.8.jar                    | false           |
| Anvil Patch - Lawful               | onConfigChanged             | lumberwizard.anvilpatch.AnvilPatch                                              | normal   | anvilpatch-1.0.0.jar                                      | false           |
| Ender IO Conduits                  | onConfigChanged             | crazypants.enderio.base.config.ConfigHandlerEIO                                 | normal   | EnderIO-1.12.2-5.1.55.jar                                 | false           |
| ICBM-Classic                       | onConfigChangedEvent        | icbm.classic.config.ConfigMain                                                  | normal   | ICBM-classic-1.12.2-3.3.0b63.jar                          | false           |
| LunatriusCore                      | onConfigurationChangedEvent | com.github.lunatrius.core.handler.ConfigurationHandler                          | normal   | LunatriusCore-1.12.2-1.2.0.42-universal.jar               | false           |
| Ender IO Integration with Forestry | onConfigChanged             | crazypants.enderio.base.config.ConfigHandlerEIO                                 | normal   | EnderIO-1.12.2-5.1.55.jar                                 | false           |
| VanillaFix                         | onConfigChanged             | org.dimdev.vanillafix.ModConfig                                                 | normal   | VanillaFix-1.0.10-99.jar                                  | false           |
| Ender IO Machines                  | onConfigChanged             | crazypants.enderio.base.config.ConfigHandlerEIO                                 | normal   | EnderIO-1.12.2-5.1.55.jar                                 | false           |
| TOP Addons                         | onConfigChanged             | io.github.drmanganese.topaddons.TOPAddons                                       | normal   | topaddons-1.12.2-1.12.0.jar                               | false           |
| Stackie                            | onConfigurationChangedEvent | com.github.lunatrius.stackie.handler.ConfigurationHandler                       | normal   | Stackie-1.12.2-1.6.0.48-universal.jar                     | false           |
| Minecraft Forge                    | onConfigChanged             | forge                                                                           | normal   | forge-1.12.2-14.23.5.2847.jar                             | false           |
| AgriCraft                          | onConfigurationChangedEvent | com.infinityraider.agricraft.core.ModProvider                                   | normal   | AgriCraft-2.12.0-1.12.0-a6.jar                            | false           |
| Ender IO Powertools                | onConfigChanged             | crazypants.enderio.base.config.ConfigHandlerEIO                                 | normal   | EnderIO-1.12.2-5.1.55.jar                                 | false           |
| OreExcavation                      | onConfigChanged             | oreexcavation.handlers.EventHandler                                             | normal   | OreExcavation-1.4.143.jar                                 | false           |
| NuclearCraft                       | onEvent                     | nc.config.NCConfig$ClientConfigEventHandler                                     | lowest   | NuclearCraft-2.18o-1.12.2.jar                             | false           |
| GasConduits                        | onConfigChanged             | crazypants.enderio.base.config.ConfigHandlerEIO                                 | normal   | GasConduits-1.12.2-1.2.2.jar                              | false           |
| Just Enough Resources              | onConfigChangedEvent        | jeresources.config.ConfigHandler                                                | normal   | JustEnoughResources-1.12.2-0.9.2.60.jar                   | false           |
| Refined Storage                    | onConfigChangedEvent        | com.raoulvdberge.refinedstorage.RSConfig                                        | normal   | refinedstorage-1.6.15.jar                                 | false           |
| Bad Wither No Cookie! Reloaded     | onConfigChanged             | com.kreezcraft.badwithernocookiereloaded.BWNCR_Config                           | normal   | badwithernocookiereloaded-1.12.2-3.3.16.jar               | false           |
| Reborn Core                        | onChange                    | reborncore.common.registration.impl.ConfigRegistryFactory                       | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar                | false           |
| ReAuth                             | onConfigChanged             | technicianlp.reauth.Main                                                        | normal   | reauth-3.6.0.jar                                          | false           |
| Quark                              | onConfigChanged             | vazkii.quark.base.module.ModuleLoader$EventHandler                              | normal   | Quark-r1.6-177.jar                                        | false           |
| Toast Control                      | onConfigChanged             | shadows.toaster.ToastControl                                                    | normal   | Toast Control-1.12.2-1.8.1.jar                            | false           |
| World Stripper                     | onConfigChanged             | com.ewyboy.worldstripper.WorldStripper                                          | normal   | World-Stripper-1.6.0-1.12.2.jar                           | false           |
| Mekanism                           | onConfigChanged             | mekanism.common.Mekanism                                                        | normal   | Mekanism-1.12.2-9.8.3.390.jar                             | false           |
| LagGoggles                         | onConfigChanged             | cf.terminator.laggoggles.client.ClientConfig$ConfigurationHolder                | normal   | LagGoggles-FAT-1.12.2-4.9.jar                             | false           |
| Tinkers' Construct                 | update                      | slimeknights.tconstruct.common.config.Config                                    | normal   | TConstruct-1.12.2-2.13.0.171.jar                          | false           |
| YUNG's Better Caves                | onConfigReload              | com.yungnickyoung.minecraft.bettercaves.event.EventConfigReload                 | normal   | bettercaves-1.12.2-1.6.0.jar                              | false           |
| RefinedStorageRequestify           | onConfigChanged             | com.buuz135.refinedstoragerequestify.proxy.config.RequestifyConfig$EventHandler | normal   | refinedstoragerequestify-1.12.2-1.0.2-3.jar               | false           |
| LagGoggles                         | onConfigChanged             | cf.terminator.laggoggles.server.ServerConfig$ConfigurationHolder                | normal   | LagGoggles-FAT-1.12.2-4.9.jar                             | false           |
| Tell Me                            | onConfigChangedEvent        | fi.dy.masa.tellme.config.Configs                                                | normal   | tellme-1.12.2-0.7.0-dev.20190610.165828.jar               | false           |
| Scannable                          | handleConfigChanged         | li.cil.scannable.client.ProxyClient                                             | normal   | Scannable-MC1.12.2-1.6.3.24.jar                           | false           |
| EnderCore                          | onConfigChanged             | com.enderio.core.common.config.ConfigHandler                                    | normal   | EnderCore-1.12.2-0.5.73.jar                               | false           |
| PneumaticCraft: Repressurized      | onConfigChanged             | me.desht.pneumaticcraft.common.config.ConfigHandler$ConfigSyncHandler           | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar        | false           |
| TOP Addons                         | onConfigChanged             | io.github.drmanganese.topaddons.config.ConfigClient                             | normal   | topaddons-1.12.2-1.12.0.jar                               | false           |
| The Betweenlands                   | onConfigChanged             | thebetweenlands.common.config.ConfigHelper                                      | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |
| QuarryPlus                         | onChange                    | com.yogpc.qp.Config$                                                            | normal   | AdditionalEnchantedMiner-1.12.2-12.3.0-universal.jar      | false           |
| The One Probe                      | onConfigChanged             | mcjty.theoneprobe.ForgeEventHandlers                                            | normal   | theoneprobe-1.12-1.4.28.jar                               | false           |
| Ore Veins                          | configChanged               | com.alcatrazescapee.oreveins.OreVeins                                           | normal   | oreveins-2.0.15.jar                                       | false           |
| Ender IO                           | onConfigChanged             | crazypants.enderio.base.config.Config                                           | normal   | EnderIO-1.12.2-5.1.55.jar                                 | false           |
| Just Enough Dimensions             | onConfigChangedEvent        | fi.dy.masa.justenoughdimensions.config.Configs                                  | normal   | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |
| Simple Inventory sorting           | onConfigChanged             | cpw.mods.inventorysorter.SideProxy$ClientProxy$1                                | normal   | inventorysorter-1.12.2-1.13.3+57.jar                      | false           |
| Refined Storage Addons             | onConfigChangedEvent        | com.raoulvdberge.refinedstorageaddons.RSAddonsConfig                            | normal   | refinedstorageaddons-0.4.4.jar                            | false           |
| Grimoire of Gaia 3                 | onConfigChanged             | gaia.GaiaConfig$EventHandler                                                    | normal   | GrimoireOfGaia3-1.12.2-1.6.9.3.jar                        | false           |
| MoreOverlays                       | onConfigChanged             | at.feldim2425.moreoverlays.config.ConfigHandler                                 | normal   | moreoverlays-1.15.1-mc1.12.2.jar                          | false           |


## OreDictionary$OreRegisterEvent
| Owner             | Method              | Location                                      | Priority | Source                                          | RecieveCanceled |
|-------------------|---------------------|-----------------------------------------------|----------|-------------------------------------------------|-----------------|
| Reborn Core       | oreRegistationEvent | reborncore.common.util.OreRegistationEvent    | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar      | false           |
| CraftTweaker2     | onOreDictEvent      | crafttweaker.mc1120.events.CommonEventHandler | normal   | CraftTweaker2-1.12-4.1.20.jar                   | false           |
| Advanced Rocketry | registerOre         | zmaster587.advancedRocketry.AdvancedRocketry  | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| PlusTiC           | onOreRegister       | landmaster.plustic.util.OreRegisterPromise    | low      | plustic-7.1.6.1.jar                             | false           |


## AnalyticLevelUpEvent
| Owner         | Method          | Location                                 | Priority | Source                 | RecieveCanceled |
|---------------|-----------------|------------------------------------------|----------|------------------------|-----------------|
| Academy Craft | levelUpListener | cn.academy.analytic.AnalyticDataListener | normal   | AcademyCraft-1.1.2.jar | false           |


## EntityItemPickupEvent
| Owner                         | Method             | Location                                                         | Priority | Source                                             | RecieveCanceled |
|-------------------------------|--------------------|------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| The Betweenlands              | onItemPickup       | thebetweenlands.common.handler.OverworldItemHandler              | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Draconic Evolution            | entityPickupEvent  | com.brandon3055.draconicevolution.achievements.Achievements      | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| CraftTweaker2                 | onEntityItemPickup | crafttweaker.mc1120.events.CommonEventHandler                    | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Tech Reborn                   | pickupEvent        | techreborn.events.TRRecipeHandler                                | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar        | false           |
| Building Gadgets              | GetDrops           | com.direwolf20.buildinggadgets.common.events.ItemPickupHandler   | normal   | BuildingGadgets-2.7.4.jar                          | false           |
| Quark                         | itemPickedUp       | vazkii.quark.management.feature.FavoriteItems                    | normal   | Quark-r1.6-177.jar                                 | false           |
| PneumaticCraft: Repressurized | onItemPickUp       | me.desht.pneumaticcraft.common.event.EventHandlerUniversalSensor | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## InputUpdateEvent
| Owner | Method  | Location                                    | Priority | Source             | RecieveCanceled |
|-------|---------|---------------------------------------------|----------|--------------------|-----------------|
| Quark | onInput | vazkii.quark.tweaks.feature.LookDownLadders | normal   | Quark-r1.6-177.jar | false           |


## BiomeEvent$GetWaterColor
| Owner                  | Method          | Location                                                    | Priority | Source                                                    | RecieveCanceled |
|------------------------|-----------------|-------------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| Just Enough Dimensions | onGetWaterColor | fi.dy.masa.justenoughdimensions.event.JEDEventHandlerClient | normal   | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |


## EntityStruckByLightningEvent
| Owner         | Method                         | Location                                      | Priority | Source                        | RecieveCanceled |
|---------------|--------------------------------|-----------------------------------------------|----------|-------------------------------|-----------------|
| CraftTweaker2 | onEntityStruckByLightningEvent | crafttweaker.mc1120.events.CommonEventHandler | normal   | CraftTweaker2-1.12-4.1.20.jar | false           |


## ItemTossEvent
| Owner              | Method          | Location                                                  | Priority | Source                                             | RecieveCanceled |
|--------------------|-----------------|-----------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| CraftTweaker2      | onItemTossEvent | crafttweaker.mc1120.events.CommonEventHandler             | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Morph-o-Tool       | onItemDropped   | vazkii.morphtool.MorphingHandler                          | normal   | Morph-o-Tool-1.2-21.jar                            | false           |
| Tomb Many Graves   | itemToss        | com.m4thg33k.tombmanygraves.events.CommonEvents           | high     | TombManyGraves-1.12-4.2.0.jar                      | false           |
| Draconic Evolution | itemToss        | com.brandon3055.draconicevolution.handlers.DEEventHandler | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| Mutant Beasts      | onPlayerToss    | chumbanotz.mutantbeasts.EventHandler                      | normal   | MutantBeasts-1.12.2-0.6.0.jar                      | false           |
| Akashic Tome       | onItemDropped   | vazkii.akashictome.MorphingHandler                        | normal   | AkashicTome-1.2-12.jar                             | false           |


## CriticalHitEvent
| Owner      | Method | Location                              | Priority | Source                       | RecieveCanceled |
|------------|--------|---------------------------------------|----------|------------------------------|-----------------|
| Apotheosis | crit   | shadows.deadly.loot.affix.AffixEvents | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |


## LootTableLoadEvent
| Owner                         | Method           | Location                                                           | Priority | Source                                               | RecieveCanceled |
|-------------------------------|------------------|--------------------------------------------------------------------|----------|------------------------------------------------------|-----------------|
| Tech Reborn                   | lootLoad         | techreborn.init.ModLoot                                            | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar          | false           |
| Academy Craft                 | lootLoad         | cn.academy.item.ACItemAdditionalRegistry                           | normal   | AcademyCraft-1.1.2.jar                               | false           |
| Quark                         | onLootTableLoad  | vazkii.quark.misc.feature.AncientTomes                             | normal   | Quark-r1.6-177.jar                                   | false           |
| NuclearCraft                  | onLootTableLoad  | nc.handler.DungeonLootHandler                                      | normal   | NuclearCraft-2.18o-1.12.2.jar                        | false           |
| Pam's HarvestCraft            | onLootTableLoad  | com.pam.harvestcraft.loottables.LootTableLoadEventHandler          | normal   | Pam's HarvestCraft 1.12.2zf.jar                      | false           |
| Tinkers' Construct            | onLootTableLoad  | slimeknights.tconstruct.gadgets.GadgetEvents                       | normal   | TConstruct-1.12.2-2.13.0.171.jar                     | false           |
| Ender IO                      | onLootTableLoad  | crazypants.enderio.base.loot.LootManager                           | normal   | EnderIO-1.12.2-5.1.55.jar                            | false           |
| Ice and Fire                  | onChestGenerated | com.github.alexthe666.iceandfire.event.EventLiving                 | normal   | iceandfire-1.8.3.jar                                 | false           |
| Tinkers' Construct            | onLootTableLoad  | slimeknights.tconstruct.tools.ToolEvents                           | normal   | TConstruct-1.12.2-2.13.0.171.jar                     | false           |
| ICBM-Classic                  | registerLoot     | icbm.classic.ICBMClassic                                           | normal   | ICBM-classic-1.12.2-3.3.0b63.jar                     | false           |
| Reborn Core                   | onLootTableLoad  | reborncore.common.LootManager                                      | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar           | false           |
| Quark                         | onLootTableLoad  | vazkii.quark.misc.feature.ColorRunes                               | normal   | Quark-r1.6-177.jar                                   | false           |
| Placebo                       | loadTables       | shadows.placebo.loot.PlaceboLootSystem                             | normal   | Placebo-1.12.2-1.6.0.jar                             | false           |
| QuarryPlus                    | addEntry         | com.yogpc.qp.Loot$                                                 | normal   | AdditionalEnchantedMiner-1.12.2-12.3.0-universal.jar | false           |
| Quark                         | onLootTableLoad  | vazkii.quark.world.feature.BuriedTreasure                          | normal   | Quark-r1.6-177.jar                                   | false           |
| PneumaticCraft: Repressurized | onLootTableLoad  | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft    | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar   | false           |
| LootTweaker                   | onTableLoad      | leviathan143.loottweaker.common.zenscript.ZenLootTableTweakManager | lowest   | LootTweaker-0.1.4+MC1.12.2.jar                       | false           |


## PlaySoundSourceEvent
| Owner    | Method  | Location                                                           | Priority | Source                    | RecieveCanceled |
|----------|---------|--------------------------------------------------------------------|----------|---------------------------|-----------------|
| Ender IO | onSound | crazypants.enderio.base.item.darksteel.upgrade.sound.SoundDetector | normal   | EnderIO-1.12.2-5.1.55.jar | false           |


## LightOverlayReloadHandlerEvent
| Owner        | Method               | Location                                                                  | Priority | Source                           | RecieveCanceled |
|--------------|----------------------|---------------------------------------------------------------------------|----------|----------------------------------|-----------------|
| MoreOverlays | onLightOverlayEnable | at.feldim2425.moreoverlays.lightoverlay.integration.AlternateLightHandler | normal   | moreoverlays-1.15.1-mc1.12.2.jar | false           |


## ColorHandlerEvent$Block
| Owner                         | Method                     | Location                                               | Priority | Source                                             | RecieveCanceled |
|-------------------------------|----------------------------|--------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Quark                         | onModelRegister            | vazkii.quark.decoration.feature.BetterVanillaFlowerPot | normal   | Quark-r1.6-177.jar                                 | false           |
| RFTools                       | colorHandlerEventBlock     | mcjty.rftools.setup.ClientProxy                        | normal   | rftools-1.12-7.72.jar                              | false           |
| PneumaticCraft: Repressurized | registerBlockColorHandlers | me.desht.pneumaticcraft.common.block.Blockss           | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| AutoRegLib                    | onBlockColorRegister       | vazkii.arl.util.ModelHandler                           | normal   | AutoRegLib-1.3-32.jar                              | false           |


## TinkerToolEvent$OnRepair
| Owner              | Method   | Location                                  | Priority | Source                           | RecieveCanceled |
|--------------------|----------|-------------------------------------------|----------|----------------------------------|-----------------|
| Tinkers' Construct | onRepair | slimeknights.tconstruct.tools.TraitEvents | normal   | TConstruct-1.12.2-2.13.0.171.jar | false           |


## EnergyNetwork$EnergyTransferEvent
| Owner    | Method              | Location                 | Priority | Source                        | RecieveCanceled |
|----------|---------------------|--------------------------|----------|-------------------------------|-----------------|
| Mekanism | onEnergyTransferred | mekanism.common.Mekanism | normal   | Mekanism-1.12.2-9.8.3.390.jar | false           |


## PopulateChunkEvent$Populate
| Owner | Method         | Location                                  | Priority | Source             | RecieveCanceled |
|-------|----------------|-------------------------------------------|----------|--------------------|-----------------|
| Quark | onDungeonSpawn | vazkii.quark.world.feature.VariedDungeons | normal   | Quark-r1.6-177.jar | false           |


## RenderWorldLastEvent
| Owner                         | Method                   | Location                                                            | Priority | Source                                             | RecieveCanceled |
|-------------------------------|--------------------------|---------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Reborn Core                   | onWorldRenderLast        | reborncore.client.multiblock.MultiblockRenderEvent                  | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar         | false           |
| Patchouli                     | onWorldRenderLast        | vazkii.patchouli.client.handler.MultiblockVisualizationHandler      | normal   | Patchouli-1.0-20.jar                               | false           |
| Scannable                     | onRenderLast             | INSTANCE                                                            | normal   | Scannable-MC1.12.2-1.6.3.24.jar                    | false           |
| The Betweenlands              | onRenderWorldLast        | thebetweenlands.common.item.equipment.ItemLurkerSkinPouch           | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| NuclearCraft                  | highlightBlock           | nc.render.BlockHighlightHandler                                     | normal   | NuclearCraft-2.18o-1.12.2.jar                      | false           |
| Building Gadgets              | renderWorldLastEvent     | com.direwolf20.buildinggadgets.client.proxy.ClientProxy             | normal   | BuildingGadgets-2.7.4.jar                          | false           |
| PneumaticCraft: Repressurized | renderWorldLastEvent     | me.desht.pneumaticcraft.client.render.pneumatic_armor.HUDHandler    | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Progressive Automation        | renderWorldLast          | com.vanhal.progressiveautomation.client.events.EventRenderWorld     | highest  | ProgressiveAutomation-1.12.2-1.7.8.jar             | false           |
| PneumaticCraft: Repressurized | onWorldRender            | me.desht.pneumaticcraft.client.ClientEventHandler                   | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| RFTools                       | renderWorldLastEvent     | mcjty.rftools.setup.ClientProxy                                     | normal   | rftools-1.12-7.72.jar                              | false           |
| Compact Machines 3            | renderRotationIndicator  | org.dave.compactmachines3.misc.PlayerEventHandler                   | normal   | compactmachines3-1.12.2-3.0.18-b278.jar            | false           |
| The Betweenlands              | onRenderWorldLastLowest  | thebetweenlands.client.handler.ShaderHandler                        | lowest   | TheBetweenlands-3.5.5-universal.jar                | false           |
| JourneyMap                    | onRenderWorldLastEvent   | journeymap.client.forge.event.WaypointBeaconHandler                 | normal   | journeymap-1.12.2-5.5.6.jar                        | false           |
| The Betweenlands              | renderWorld              | thebetweenlands.client.handler.DebugHandlerClient                   | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Ender IO                      | onRender                 | crazypants.enderio.base.teleport.TravelController                   | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| MoreOverlays                  | renderWorldLastEvent     | at.feldim2425.moreoverlays.chunkbounds.ChunkBoundsHandler           | normal   | moreoverlays-1.15.1-mc1.12.2.jar                   | false           |
| The Betweenlands              | onRenderWorld            | thebetweenlands.client.handler.ElixirClientHandler                  | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Practical Logistics 2         | renderWorldLastEvent     | sonar.logistics.network.PL2Client                                   | normal   | practicallogistics2-1.12.2-3.0.8-11.jar            | false           |
| The Betweenlands              | onRenderWorldLastHighest | thebetweenlands.client.handler.ShaderHandler                        | highest  | TheBetweenlands-3.5.5-universal.jar                | false           |
| The Betweenlands              | onRenderWorldLastNormal  | thebetweenlands.client.handler.ShaderHandler                        | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Tinkers' Construct            | renderExtraBlockBreak    | slimeknights.tconstruct.tools.common.client.RenderEvents            | normal   | TConstruct-1.12.2-2.13.0.171.jar                   | false           |
| OMLib                         | renderWorldLastEvent     | omtteam.omlib.handler.OMLibEventHandler                             | normal   | omlib-1.12.2-3.1.3-246.jar                         | false           |
| NuclearCraft                  | onRenderWorldLastEvent   | nc.radiation.RadiationRenders                                       | normal   | NuclearCraft-2.18o-1.12.2.jar                      | false           |
| PneumaticCraft: Repressurized | renderWorldLastEvent     | me.desht.pneumaticcraft.client.semiblock.ClientSemiBlockManager     | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| The Betweenlands              | renderWorld              | thebetweenlands.client.handler.WorldRenderHandler                   | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| PneumaticCraft: Repressurized | renderWorldLastEvent     | me.desht.pneumaticcraft.client.AreaShowManager                      | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| The Betweenlands              | renderWorld              | thebetweenlands.util.GLUProjection                                  | highest  | TheBetweenlands-3.5.5-universal.jar                | false           |
| Tomb Many Graves              | onRenderWorldLast        | com.m4thg33k.tombmanygraves.events.ClientEvents                     | normal   | TombManyGraves-1.12-4.2.0.jar                      | false           |
| CodeChicken Lib               | renderWorldLast          | codechicken.lib.internal.HighlightHandler                           | normal   | CodeChickenLib-1.12.2-3.2.3.358-universal.jar      | false           |
| Scannable                     | onWorldRender            | INSTANCE                                                            | normal   | Scannable-MC1.12.2-1.6.3.24.jar                    | false           |
| Techguns                      | onRenderWorldLast        | techguns.events.TGEventHandler                                      | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| The Betweenlands              | onRenderWorldLast        | thebetweenlands.client.handler.ScreenRenderHandler                  | lowest   | TheBetweenlands-3.5.5-universal.jar                | true            |
| MoreOverlays                  | renderWorldLastEvent     | at.feldim2425.moreoverlays.lightoverlay.LightOverlayHandler         | normal   | moreoverlays-1.15.1-mc1.12.2.jar                   | false           |
| Draconic Evolution            | renderWorldEvent         | com.brandon3055.draconicevolution.client.handler.ClientEventHandler | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| The Betweenlands              | onRenderTick             | thebetweenlands.util.RenderUtils                                    | lowest   | TheBetweenlands-3.5.5-universal.jar                | false           |
| PlusTiC                       | renderBeam               | landmaster.plustic.tools.ToolLaserGun$ProxyClient                   | normal   | plustic-7.1.6.1.jar                                | false           |
| Brandon's Core                | renderWorld              | com.brandon3055.brandonscore.client.particle.BCEffectHandler        | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar       | false           |
| Brandon's Core                | renderWorldLast          | com.brandon3055.brandonscore.utils.BCProfiler                       | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar       | false           |
| TorchMaster                   | onRender                 | net.xalcon.torchmaster.client.renderer.TorchVolumeRenderHandler     | normal   | torchmaster_1.12.2-1.8.1.81.jar                    | false           |
| Academy Craft                 | onRender                 | cn.academy.client.CameraPosition                                    | normal   | AcademyCraft-1.1.2.jar                             | false           |


## EntityEvent$EntityConstructing
| Owner                         | Method               | Location                                                            | Priority | Source                                             | RecieveCanceled |
|-------------------------------|----------------------|---------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Ender IO                      | handleConstruct      | crazypants.enderio.base.handler.darksteel.PlayerAOEAttributeHandler | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Techguns                      | onEntityConstruction | techguns.events.TGEventHandler                                      | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| PneumaticCraft: Repressurized | onEntityConstruction | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft     | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Quark                         | entityConstruct      | vazkii.quark.tweaks.feature.ArmedArmorStands                        | normal   | Quark-r1.6-177.jar                                 | false           |


## PlayerEvent$ItemSmeltedEvent
| Owner              | Method              | Location                                                    | Priority | Source                                             | RecieveCanceled |
|--------------------|---------------------|-------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Academy Craft      | onItemSmelt         | cn.academy.tutorial.Conditions                              | normal   | AcademyCraft-1.1.2.jar                             | false           |
| Draconic Evolution | smeltEvent          | com.brandon3055.draconicevolution.achievements.Achievements | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| CraftTweaker2      | onPlayerItemSmelted | crafttweaker.mc1120.events.CommonEventHandler               | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |


## EventExcavate$Pre
| Owner                     | Method          | Location                                                | Priority | Source                  | RecieveCanceled |
|---------------------------|-----------------|---------------------------------------------------------|----------|-------------------------|-----------------|
| OreExcavation Integration | onExcavateEvent | atm.bloodworkxgaming.oeintegration.Handler.EventHandler | normal   | oeintegration-2.3.4.jar | false           |


## PreRenderShadersEvent
| Owner            | Method             | Location                                     | Priority | Source                              | RecieveCanceled |
|------------------|--------------------|----------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onPreRenderShaders | thebetweenlands.client.handler.ShaderHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |


## ArmSwingSpeedEvent
| Owner            | Method          | Location                                            | Priority | Source                              | RecieveCanceled |
|------------------|-----------------|-----------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onArmSwingSpeed | thebetweenlands.client.handler.ElixirClientHandler  | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| The Betweenlands | onArmSwingSpeed | thebetweenlands.common.handler.OverworldItemHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| The Betweenlands | onArmSwingSpeed | thebetweenlands.common.item.tools.ItemGreatsword    | normal   | TheBetweenlands-3.5.5-universal.jar | false           |


## LivingSpawnEvent$AllowDespawn
| Owner         | Method              | Location                                           | Priority | Source                        | RecieveCanceled |
|---------------|---------------------|----------------------------------------------------|----------|-------------------------------|-----------------|
| Ice and Fire  | onEntityDespawn     | com.github.alexthe666.iceandfire.event.EventLiving | normal   | iceandfire-1.8.3.jar          | false           |
| CraftTweaker2 | onAllowDespawnEvent | crafttweaker.mc1120.events.CommonEventHandler      | normal   | CraftTweaker2-1.12-4.1.20.jar | false           |


## TutorialActivatedEvent
| Owner         | Method              | Location | Priority | Source                 | RecieveCanceled |
|---------------|---------------------|----------|----------|------------------------|-----------------|
| Academy Craft | onAcquiredKnowledge | null     | normal   | AcademyCraft-1.1.2.jar | false           |


## ArrowLooseEvent
| Owner            | Method             | Location                                           | Priority | Source                              | RecieveCanceled |
|------------------|--------------------|----------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onShootArrow       | thebetweenlands.client.handler.ElixirClientHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| Mutant Beasts    | onPlayerShootArrow | chumbanotz.mutantbeasts.EventHandler               | normal   | MutantBeasts-1.12.2-0.6.0.jar       | false           |
| The Betweenlands | onShootArrow       | thebetweenlands.common.handler.ElixirCommonHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |


## ConfigRegistryFactory$RebornRegistryEvent
| Owner       | Method       | Location                              | Priority | Source                                      | RecieveCanceled |
|-------------|--------------|---------------------------------------|----------|---------------------------------------------|-----------------|
| Tech Reborn | handleConfig | techreborn.blocks.cable.EnumCableType | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar | false           |


## RenderBlockOverlayEvent
| Owner            | Method               | Location                                             | Priority | Source                              | RecieveCanceled |
|------------------|----------------------|------------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onRenderOverlay      | thebetweenlands.client.handler.OverlayHandler        | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| EnderCore        | onRenderBlockOverlay | com.enderio.core.client.handlers.FluidVisualsHandler | normal   | EnderCore-1.12.2-0.5.73.jar         | false           |
| The Betweenlands | onRenderBlockOverlay | thebetweenlands.client.handler.ShaderHandler         | highest  | TheBetweenlands-3.5.5-universal.jar | false           |


## LinkUserEvent
| Owner      | Method   | Location                              | Priority | Source                 | RecieveCanceled |
|------------|----------|---------------------------------------|----------|------------------------|-----------------|
| lambdalib2 | linkUser | cn.academy.energy.impl.WirelessSystem | normal   | AcademyCraft-1.1.2.jar | false           |


## LivingDeathEvent
| Owner                         | Method                   | Location                                                        | Priority | Source                                                    | RecieveCanceled |
|-------------------------------|--------------------------|-----------------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| Epic Siege Mod                | onEntityDeath            | funwayguy.epicsiegemod.handlers.entities.ZombieHandler          | normal   | EpicSiegeMod-13.165.jar                                   | false           |
| lambdalib2                    | playerDeath              | instance                                                        | lowest   | AcademyCraft-1.1.2.jar                                    | false           |
| Open Modular Turrets          | entityDeathEvent         | omtteam.openmodularturrets.handler.OMTEventHandler              | normal   | openmodularturrets-1.12.2-3.1.4-356.jar                   | false           |
| lambdalib2                    | onLivingDeath            | instance                                                        | normal   | AcademyCraft-1.1.2.jar                                    | false           |
| PneumaticCraft: Repressurized | onEntityDeath            | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticArmor | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar        | false           |
| Just Enough Dimensions        | onLivingDeath            | fi.dy.masa.justenoughdimensions.event.JEDEventHandler           | normal   | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |
| Quark                         | onDeath                  | vazkii.quark.decoration.feature.VariedChests                    | normal   | Quark-r1.6-177.jar                                        | false           |
| The Betweenlands              | onLivingDeath            | thebetweenlands.common.handler.PuppetHandler                    | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |
| Tomb Many Graves              | onPlayerDeath            | com.m4thg33k.tombmanygraves.events.CommonEvents                 | lowest   | TombManyGraves-1.12-4.2.0.jar                             | false           |
| EnderCore                     | handleEntityKill         | com.enderio.core.common.handlers.XPBoostHandler                 | normal   | EnderCore-1.12.2-0.5.73.jar                               | false           |
| Tinkers Tool Leveling         | onDeath                  | slimeknights.toolleveling.EntityXpHandler                       | normal   | TinkerToolLeveling-1.12.2-1.1.0.jar                       | false           |
| lambdalib2                    | onMobDeath               | instance                                                        | normal   | AcademyCraft-1.1.2.jar                                    | false           |
| PlusTiC                       | timing                   | landmaster.plustic.traits.NickOfTime                            | highest  | plustic-7.1.6.1.jar                                       | false           |
| CraftTweaker2                 | onEntityLivingDeathEvent | crafttweaker.mc1120.events.CommonEventHandler                   | normal   | CraftTweaker2-1.12-4.1.20.jar                             | false           |
| Techguns                      | onLivingDeathEvent       | techguns.events.TGEventHandler                                  | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                        | false           |
| Academy Monster               | onMonsterDied            | cn.paindar.academymonster.core.GlobalEventHandle                | normal   | AcademyMonster-1.1.0a.jar                                 | false           |
| Tinkers' Construct            | playerDrop               | slimeknights.tconstruct.tools.modifiers.ModBeheading            | lowest   | TConstruct-1.12.2-2.13.0.171.jar                          | false           |
| The Betweenlands              | onLivingDeath            | thebetweenlands.common.item.misc.ItemRingOfGathering            | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |
| Epic Siege Mod                | onEntityKilled           | funwayguy.epicsiegemod.handlers.entities.GeneralEntityHandler   | normal   | EpicSiegeMod-13.165.jar                                   | false           |
| Draconic Evolution            | onPlayerDeath            | com.brandon3055.draconicevolution.handlers.CustomArmorHandler   | highest  | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar        | false           |
| Ice and Fire                  | onEntityDie              | com.github.alexthe666.iceandfire.event.EventLiving              | normal   | iceandfire-1.8.3.jar                                      | false           |
| The Betweenlands              | onPlayerDeath            | thebetweenlands.common.handler.PlayerRespawnHandler             | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |


## ExplosionEvent$Detonate
| Owner                         | Method                 | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|------------------------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| The Betweenlands              | onExplosion            | thebetweenlands.common.handler.LocationHandler                  | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| RFTools                       | onDetonate             | mcjty.rftools.blocks.blockprotector.BlockProtectorEventHandlers | normal   | rftools-1.12-7.72.jar                              | false           |
| PlusTiC                       | onDetonate             | landmaster.plustic.traits.HailHydra                             | normal   | plustic-7.1.6.1.jar                                | false           |
| PneumaticCraft: Repressurized | explosionCraftingEvent | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## LivingEntityUseItemEvent$Tick
| Owner            | Method                         | Location                                           | Priority | Source                              | RecieveCanceled |
|------------------|--------------------------------|----------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onItemUsing                    | thebetweenlands.common.item.tools.ItemGreatsword   | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| The Betweenlands | onUseItemTick                  | thebetweenlands.common.handler.FoodSicknessHandler | low      | TheBetweenlands-3.5.5-universal.jar | false           |
| The Betweenlands | onUseItemTick                  | thebetweenlands.common.handler.PlayerDecayHandler  | low      | TheBetweenlands-3.5.5-universal.jar | false           |
| CraftTweaker2    | onLivingEntityUseItemTickEvent | crafttweaker.mc1120.events.CommonEventHandler      | normal   | CraftTweaker2-1.12-4.1.20.jar       | false           |
| Ice and Fire     | onEntityStopUsingItem          | com.github.alexthe666.iceandfire.event.EventLiving | normal   | iceandfire-1.8.3.jar                | false           |


## TinkerRegisterEvent$MeltingRegisterEvent
| Owner   | Method                  | Location                              | Priority | Source              | RecieveCanceled |
|---------|-------------------------|---------------------------------------|----------|---------------------|-----------------|
| PlusTiC | onMeltingRecipeRegister | landmaster.plustic.modules.ModuleBase | normal   | plustic-7.1.6.1.jar | false           |
| PlusTiC | meltingRecipeRegister   | landmaster.plustic.modules.ModulePsi  | normal   | plustic-7.1.6.1.jar | false           |


## PlayerInteractEvent$LeftClickEmpty
| Owner                         | Method            | Location                                            | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-------------------|-----------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Akashic Tome                  | onPlayerLeftClick | vazkii.akashictome.MorphingHandler                  | normal   | AkashicTome-1.2-12.jar                             | false           |
| PneumaticCraft: Repressurized | onLeftClickAir    | me.desht.pneumaticcraft.common.item.ItemGPSAreaTool | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## GameRuleChangeEvent
| Owner            | Method           | Location                                           | Priority | Source                              | RecieveCanceled |
|------------------|------------------|----------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onGameruleChange | thebetweenlands.common.registries.GameruleRegistry | normal   | TheBetweenlands-3.5.5-universal.jar | false           |


## PlayerInteractEvent$EntityInteract
| Owner                         | Method                 | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|------------------------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Ice and Fire                  | onEntityInteract       | com.github.alexthe666.iceandfire.event.EventLiving              | normal   | iceandfire-1.8.3.jar                               | false           |
| Quark                         | onEntityInteract       | vazkii.quark.tweaks.feature.ShearableChickens                   | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | onInteract             | vazkii.quark.tweaks.feature.PatTheDogs                          | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | onEntityInteract       | vazkii.quark.vanity.feature.BoatSails                           | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | onInteract             | vazkii.quark.misc.feature.PoisonPotatoUsage                     | normal   | Quark-r1.6-177.jar                                 | false           |
| CraftTweaker2                 | onPlayerInteractEntity | crafttweaker.mc1120.events.CommonEventHandler                   | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Draconic Evolution            | entityInteract         | com.brandon3055.draconicevolution.handlers.DEEventHandler       | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| Mutant Beasts                 | onEntityInteract       | chumbanotz.mutantbeasts.EventHandler                            | normal   | MutantBeasts-1.12.2-0.6.0.jar                      | false           |
| Quark                         | onEntityInteract       | vazkii.quark.tweaks.feature.MinecartInteraction                 | normal   | Quark-r1.6-177.jar                                 | false           |
| PneumaticCraft: Repressurized | onModdedWrenchEntity   | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Tell Me                       | onEntityInteract       | fi.dy.masa.tellme.event.InteractEventHandler                    | normal   | tellme-1.12.2-0.7.0-dev.20190610.165828.jar        | false           |
| The Betweenlands              | onEntityInteract       | thebetweenlands.common.handler.ItemEquipmentHandler             | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| PlusTiC                       | captureEntity          | landmaster.plustic.traits.Portly                                | lowest   | plustic-7.1.6.1.jar                                | false           |
| Quark                         | entityInteract         | vazkii.quark.misc.feature.SlimeBucket                           | normal   | Quark-r1.6-177.jar                                 | false           |
| The Betweenlands              | onEntityInteract       | thebetweenlands.common.handler.PuppetHandler                    | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                         | onInteract             | vazkii.quark.automation.feature.ChainLinkage                    | normal   | Quark-r1.6-177.jar                                 | false           |
| Techguns                      | rightClickEvent        | techguns.events.TGEventHandler                                  | high     | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| Pam's HarvestCraft            | onInteraction          | com.pam.harvestcraft.events.AnimalBreedingEvent                 | normal   | Pam's HarvestCraft 1.12.2zf.jar                    | false           |
| Quark                         | entityInteract         | vazkii.quark.misc.feature.ParrotEggs                            | normal   | Quark-r1.6-177.jar                                 | false           |


## ArrowNockEvent
| Owner     | Method      | Location                                 | Priority | Source                      | RecieveCanceled |
|-----------|-------------|------------------------------------------|----------|-----------------------------|-----------------|
| EnderCore | onArrowNock | com.enderio.core.common.tweaks.InfiniBow | lowest   | EnderCore-1.12.2-0.5.73.jar | false           |


## RenderGameOverlayEvent$Text
| Owner          | Method               | Location                                                     | Priority | Source                                       | RecieveCanceled |
|----------------|----------------------|--------------------------------------------------------------|----------|----------------------------------------------|-----------------|
| Brandon's Core | debugOverlay         | com.brandon3055.brandonscore.client.particle.BCEffectHandler | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar | false           |
| JourneyMap     | onRenderOverlayDebug | journeymap.client.forge.event.MiniMapOverlayHandler          | normal   | journeymap-1.12.2-5.5.6.jar                  | false           |


## SplashPotionEvent
| Owner            | Method         | Location                                                  | Priority | Source                              | RecieveCanceled |
|------------------|----------------|-----------------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onSplashPotion | thebetweenlands.common.item.shields.ItemDentrothystShield | normal   | TheBetweenlands-3.5.5-universal.jar | false           |


## Apotheosis$ApotheosisPreInit
| Owner      | Method  | Location                    | Priority | Source                       | RecieveCanceled |
|------------|---------|-----------------------------|----------|------------------------------|-----------------|
| Apotheosis | preInit | shadows.potion.PotionModule | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |
| Apotheosis | preInit | shadows.spawn.SpawnerModule | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |
| Apotheosis | preInit | shadows.ench.EnchModule     | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |
| Apotheosis | preInit | shadows.deadly.DeadlyModule | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |
| Apotheosis | preInit | shadows.garden.GardenModule | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |


## EntityTrackEvent
| Owner                         | Method           | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|------------------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | onEntityTracking | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## PlayerEvent$HarvestCheck
| Owner           | Method         | Location                                          | Priority | Source                    | RecieveCanceled |
|-----------------|----------------|---------------------------------------------------|----------|---------------------------|-----------------|
| Refined Storage | onHarvestCheck | com.raoulvdberge.refinedstorage.proxy.ProxyClient | normal   | refinedstorage-1.6.15.jar | false           |


## EnderIOLifecycleEvent$Init$Pre
| Owner    | Method           | Location                                                                        | Priority | Source                    | RecieveCanceled |
|----------|------------------|---------------------------------------------------------------------------------|----------|---------------------------|-----------------|
| Ender IO | registerHoes     | crazypants.enderio.base.integration.techreborn.TechRebornUtil                   | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | registerTreetaps | crazypants.enderio.base.integration.ic2c.IC2cUtil                               | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | registerHoes     | crazypants.enderio.base.integration.te.TEUtil                                   | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | registerHoes     | crazypants.enderio.base.integration.mekanism.MekanismUtil                       | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | registerHoes     | crazypants.enderio.base.integration.actuallyadditions.ActuallyadditionsUtil     | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | registerHoes     | crazypants.enderio.base.integration.railcraft.RailcraftUtil                     | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | registerHoes     | crazypants.enderio.base.integration.thaumcraft.ThaumcraftUtil                   | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | registerHoes     | crazypants.enderio.base.integration.basemetals.BaseMetalsUtil                   | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | init             | crazypants.enderio.base.init.ModObjectRegistry                                  | high     | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | registerHoes     | crazypants.enderio.base.integration.matteroverdrive.MatterOverdriveUtil         | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | registerTools    | crazypants.enderio.base.integration.ic2e.IC2eUtil                               | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | registerHoes     | crazypants.enderio.base.integration.mysticalagriculture.MysticalAgricultureUtil | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | registerHoes     | crazypants.enderio.base.integration.ae2.AE2Util                                 | normal   | EnderIO-1.12.2-5.1.55.jar | false           |
| Ender IO | registerHoes     | crazypants.enderio.base.integration.draconic.DraconicUtil                       | normal   | EnderIO-1.12.2-5.1.55.jar | false           |


## RenderGameOverlayEvent
| Owner            | Method                  | Location                                              | Priority | Source                                     | RecieveCanceled |
|------------------|-------------------------|-------------------------------------------------------|----------|--------------------------------------------|-----------------|
| Reborn Core      | onRenderExperienceBar   | reborncore.client.hud.StackInfoHUD                    | low      | RebornCore-1.12.2-3.16.4.478-universal.jar | false           |
| Techguns         | onRenderCrosshairs      | techguns.events.TGGuiEvents                           | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar         | false           |
| Academy Monster  | drawHudEvent            | cn.lambdalib2.auxgui.AuxGuiHandler                    | normal   | AcademyMonster-1.1.0a.jar                  | true            |
| RFTools          | renderGameOverlayEvent  | mcjty.rftools.setup.ClientProxy                       | normal   | rftools-1.12-7.72.jar                      | false           |
| PlusTiC          | render                  | landmaster.plustic.tools.ToolKatana                   | normal   | plustic-7.1.6.1.jar                        | false           |
| JourneyMap       | onRenderOverlay         | journeymap.client.forge.event.MiniMapOverlayHandler   | normal   | journeymap-1.12.2-5.5.6.jar                | false           |
| Techguns         | onRenderGameOverlyEvent | techguns.events.TGGuiEvents                           | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar         | false           |
| The Betweenlands | onOverlayRender         | thebetweenlands.client.handler.WeedwoodRowboatHandler | normal   | TheBetweenlands-3.5.5-universal.jar        | false           |


## NetworkCableEvent$AddedCable
| Owner                 | Method       | Location                                          | Priority | Source                                  | RecieveCanceled |
|-----------------------|--------------|---------------------------------------------------|----------|-----------------------------------------|-----------------|
| Practical Logistics 2 | onCableAdded | sonar.logistics.base.events.LogisticsEventHandler | highest  | practicallogistics2-1.12.2-3.0.8-11.jar | false           |


## FMLNetworkEvent$ClientDisconnectionFromServerEvent
| Owner                         | Method                          | Location                                                          | Priority | Source                                             | RecieveCanceled |
|-------------------------------|---------------------------------|-------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Academy Craft                 | onClientDisconnectionFromServer | cn.academy.AcademyCraft                                           | normal   | AcademyCraft-1.1.2.jar                             | false           |
| PneumaticCraft: Repressurized | onClientDisconnect              | me.desht.pneumaticcraft.common.pneumatic_armor.CommonArmorHandler | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Ender IO                      | onPlayerLogout                  | crazypants.enderio.base.capacitor.CapacitorKeyRegistry            | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| lambdalib2                    | __onDisconnect                  | instance                                                          | normal   | AcademyCraft-1.1.2.jar                             | false           |
| Scannable                     | onClientDisconnectionFromServer | INSTANCE                                                          | normal   | Scannable-MC1.12.2-1.6.3.24.jar                    | false           |
| Practical Logistics 2         | clientDisconnection             | sonar.logistics.base.events.PL2Events                             | normal   | practicallogistics2-1.12.2-3.0.8-11.jar            | false           |
| CodeChicken Lib               | onClientDisconnected            | codechicken.lib.configuration.ConfigSyncManager                   | normal   | CodeChickenLib-1.12.2-3.2.3.358-universal.jar      | false           |
| Academy Monster               | __onClientDisconnect            | instance                                                          | normal   | AcademyMonster-1.1.0a.jar                          | false           |
| Brandon's Core                | disconnectEvent                 | com.brandon3055.brandonscore.handlers.BCEventHandler              | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar       | false           |
| Academy Monster               | disconnected                    | cn.lambdalib2.auxgui.AuxGuiHandler                                | normal   | AcademyMonster-1.1.0a.jar                          | false           |
| lambdalib2                    | __onDisconnect                  | instance                                                          | normal   | AcademyCraft-1.1.2.jar                             | false           |
| Advanced Rocketry             | disconnected                    | zmaster587.advancedRocketry.event.PlanetEventHandler              | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| Ender IO                      | onDisconnectedFromServer        | crazypants.enderio.base.transceiver.ConnectionHandler             | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Ender IO                      | onPlayerLogout                  | info.loenwind.autoconfig.factory.FactoryManager                   | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| EnderCore                     | onPlayerLogout                  | com.enderio.core.common.config.ConfigHandler$1                    | normal   | EnderCore-1.12.2-0.5.73.jar                        | false           |
| Advanced Rocketry             | disconnected                    | zmaster587.advancedRocketry.event.PlanetEventHandler              | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |


## NetworkEvent$DisconnectedLocalProvider
| Owner                 | Method                      | Location                                          | Priority | Source                                  | RecieveCanceled |
|-----------------------|-----------------------------|---------------------------------------------------|----------|-----------------------------------------|-----------------|
| Practical Logistics 2 | onLocalProviderDisconnected | sonar.logistics.base.events.LogisticsEventHandler | highest  | practicallogistics2-1.12.2-3.0.8-11.jar | false           |


## TinkerToolEvent$OnBowShoot
| Owner        | Method        | Location                                                        | Priority | Source               | RecieveCanceled |
|--------------|---------------|-----------------------------------------------------------------|----------|----------------------|-----------------|
| Ice and Fire | onBowShooting | slimeknights.tconstruct.tools.traits.TraitSplitting             | normal   | iceandfire-1.8.3.jar | false           |
| Ice and Fire | onBowShooting | slimeknights.tconstruct.tools.traits.TraitEndspeed              | normal   | iceandfire-1.8.3.jar | false           |
| Ice and Fire | onBowShooting | com.github.alexthe666.iceandfire.compat.tinkers.TraitSplitting2 | normal   | iceandfire-1.8.3.jar | false           |


## ProjectileEvent$OnHitBlock
| Owner        | Method     | Location                                            | Priority | Source               | RecieveCanceled |
|--------------|------------|-----------------------------------------------------|----------|----------------------|-----------------|
| Ice and Fire | onHitBlock | slimeknights.tconstruct.tools.traits.TraitBreakable | normal   | iceandfire-1.8.3.jar | false           |


## PlayerPickupXpEvent
| Owner              | Method           | Location                                               | Priority | Source                           | RecieveCanceled |
|--------------------|------------------|--------------------------------------------------------|----------|----------------------------------|-----------------|
| Tinkers' Construct | onPickupXp       | slimeknights.tconstruct.tools.modifiers.ModMendingMoss | normal   | TConstruct-1.12.2-2.13.0.171.jar | false           |
| CraftTweaker2      | onPlayerPickupXp | crafttweaker.mc1120.events.CommonEventHandler          | normal   | CraftTweaker2-1.12-4.1.20.jar    | false           |


## ChunkWatchEvent$UnWatch
| Owner                 | Method           | Location                                                 | Priority | Source                                  | RecieveCanceled |
|-----------------------|------------------|----------------------------------------------------------|----------|-----------------------------------------|-----------------|
| Practical Logistics 2 | onChunkUnwatched | sonar.logistics.core.tiles.displays.DisplayViewerHandler | normal   | practicallogistics2-1.12.2-3.0.8-11.jar | false           |
| The Betweenlands      | onUnwatchChunk   | thebetweenlands.common.handler.WorldEventHandler         | normal   | TheBetweenlands-3.5.5-universal.jar     | false           |


## CTGUIEvent
| Owner  | Method  | Location                                       | Priority | Source                        | RecieveCanceled |
|--------|---------|------------------------------------------------|----------|-------------------------------|-----------------|
| CT-GUI | onCTGUI | com.blamejared.ctgui.events.CommonEventHandler | normal   | CraftTweaker2-1.12-4.1.20.jar | false           |


## EntityViewRenderEvent$RenderFogEvent
| Owner             | Method           | Location                                             | Priority | Source                                          | RecieveCanceled |
|-------------------|------------------|------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| The Betweenlands  | onFogRenderEvent | thebetweenlands.client.handler.FogHandler            | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| Advanced Rocketry | fogColor         | zmaster587.advancedRocketry.event.PlanetEventHandler | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Advanced Rocketry | fogColor         | zmaster587.advancedRocketry.event.PlanetEventHandler | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |


## SkillLearnEvent
| Owner         | Method             | Location                                 | Priority | Source                 | RecieveCanceled |
|---------------|--------------------|------------------------------------------|----------|------------------------|-----------------|
| lambdalib2    | learnedSkill       | instance                                 | normal   | AcademyCraft-1.1.2.jar | false           |
| Academy Craft | skillLearnListener | cn.academy.analytic.AnalyticDataListener | normal   | AcademyCraft-1.1.2.jar | false           |
| Academy Craft | onSkillLearn       | cn.academy.advancements.DispatcherAch    | normal   | AcademyCraft-1.1.2.jar | false           |
| Academy Craft | onPlayerLearnSkill | cn.academy.advancements.DispatcherAch    | normal   | AcademyCraft-1.1.2.jar | false           |


## EntityMountEvent
| Owner                         | Method             | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|--------------------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| The Betweenlands              | onMountEvent       | thebetweenlands.common.entity.EntityVolarkite                   | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| PneumaticCraft: Repressurized | entityMounting     | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Ice and Fire                  | onEntityMount      | com.github.alexthe666.iceandfire.event.EventLiving              | normal   | iceandfire-1.8.3.jar                               | false           |
| The Betweenlands              | onEntityMountEvent | thebetweenlands.common.entity.mobs.EntitySludgeMenace           | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |


## ChunkDataEvent$Save
| Owner                         | Method               | Location                                                  | Priority | Source                                             | RecieveCanceled |
|-------------------------------|----------------------|-----------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | onChunkSave          | me.desht.pneumaticcraft.common.semiblock.SemiBlockManager | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Mekanism                      | chunkSave            | mekanism.common.Mekanism                                  | normal   | Mekanism-1.12.2-9.8.3.390.jar                      | false           |
| Chisel                        | onChunkSave          | INSTANCE                                                  | normal   | Chisel-MC1.12.2-1.0.1.44.jar                       | false           |
| RFTools                       | handleChunkSaveEvent | mcjty.rftools.world.RFToolsWorldGenerator                 | normal   | rftools-1.12-7.72.jar                              | false           |
| Tech Reborn                   | onChunkSave          | TechRebornRetroGen{chunksToRetroGen=[]}                   | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar        | false           |
| Chunk Pregenerator            | onChunkSave          | pregenerator.impl.retrogen.RetrogenHandler                | normal   | Chunk Pregenerator V1.12-2.2.jar                   | false           |
| The Betweenlands              | onChunkSave          | thebetweenlands.common.handler.WorldEventHandler          | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Draconic Evolution            | chunkSaveEvent       | com.brandon3055.draconicevolution.world.DEWorldGenHandler | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |


## ServerChatEvent
| Owner                         | Method    | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-----------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | quetziMoo | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |


## LivingEntityUseItemEvent
| Owner         | Method                     | Location                                      | Priority | Source                        | RecieveCanceled |
|---------------|----------------------------|-----------------------------------------------|----------|-------------------------------|-----------------|
| CraftTweaker2 | onLivingEntityUseItemEvent | crafttweaker.mc1120.events.CommonEventHandler | normal   | CraftTweaker2-1.12-4.1.20.jar | false           |


## PlayerSPPushOutOfBlocksEvent
| Owner            | Method            | Location                                                                     | Priority | Source                              | RecieveCanceled |
|------------------|-------------------|------------------------------------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onSPPlayerPushOut | thebetweenlands.common.capability.collision.RingOfDispersionEntityCapability | normal   | TheBetweenlands-3.5.5-universal.jar | false           |


## ConfigChangedEvent
| Owner          | Method          | Location                                    | Priority | Source                         | RecieveCanceled |
|----------------|-----------------|---------------------------------------------|----------|--------------------------------|-----------------|
| CTM            | onConfigChange  | team.chisel.ctm.Configurations              | normal   | CTM-MC1.12.2-1.0.1.30.jar      | false           |
| LootTweaker    | syncConfig      | leviathan143.loottweaker.common.LTConfig    | normal   | LootTweaker-0.1.4+MC1.12.2.jar | false           |
| Epic Siege Mod | onConfigChanged | funwayguy.epicsiegemod.handlers.MainHandler | normal   | EpicSiegeMod-13.165.jar        | false           |


## NetworkPartEvent$AddedPart
| Owner                 | Method      | Location                                          | Priority | Source                                  | RecieveCanceled |
|-----------------------|-------------|---------------------------------------------------|----------|-----------------------------------------|-----------------|
| Practical Logistics 2 | onPartAdded | sonar.logistics.base.events.LogisticsEventHandler | highest  | practicallogistics2-1.12.2-3.0.8-11.jar | false           |


## CreateNetworkEvent
| Owner      | Method      | Location                              | Priority | Source                 | RecieveCanceled |
|------------|-------------|---------------------------------------|----------|------------------------|-----------------|
| lambdalib2 | onCreateNet | cn.academy.energy.impl.WirelessSystem | normal   | AcademyCraft-1.1.2.jar | false           |


## BonemealEvent
| Owner            | Method           | Location                                            | Priority | Source                              | RecieveCanceled |
|------------------|------------------|-----------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onBonemeal       | thebetweenlands.common.handler.OverworldItemHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| CraftTweaker2    | onPlayerBonemeal | crafttweaker.mc1120.events.CommonEventHandler       | normal   | CraftTweaker2-1.12-4.1.20.jar       | false           |


## BlockEvent$EntityPlaceEvent
| Owner           | Method       | Location                                                            | Priority | Source                    | RecieveCanceled |
|-----------------|--------------|---------------------------------------------------------------------|----------|---------------------------|-----------------|
| Refined Storage | onBlockPlace | com.raoulvdberge.refinedstorage.apiimpl.network.NetworkNodeListener | normal   | refinedstorage-1.6.15.jar | false           |


## FurnaceFuelBurnTimeEvent
| Owner                         | Method                | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-----------------------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | handleFuelEvent       | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| CraftTweaker2                 | onFurnaceFuelBurnTime | crafttweaker.mc1120.events.CommonEventHandler                   | highest  | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| The Betweenlands              | onFuelEvent           | thebetweenlands.common.handler.FuelHandler                      | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                         | burnTorch             | vazkii.quark.tweaks.feature.TorchesBurnInFurnaces               | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | onFurnaceTimeCheck    | vazkii.quark.decoration.feature.TallowAndCandles                | normal   | Quark-r1.6-177.jar                                 | false           |


## GetCollisionBoxesEvent
| Owner            | Method                 | Location                                                     | Priority | Source                              | RecieveCanceled |
|------------------|------------------------|--------------------------------------------------------------|----------|-------------------------------------|-----------------|
| Ice and Fire     | onGatherCollisionBoxes | com.github.alexthe666.iceandfire.event.EventLiving           | normal   | iceandfire-1.8.3.jar                | false           |
| The Betweenlands | onGatherCollisionBoxes | thebetweenlands.common.handler.CustomEntityCollisionsHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |


## EntityFilterRegisterEvent$MegaTorch
| Owner       | Method                | Location                                         | Priority | Source                          | RecieveCanceled |
|-------------|-----------------------|--------------------------------------------------|----------|---------------------------------|-----------------|
| TorchMaster | registerTorchEntities | net.xalcon.torchmaster.compat.LycaniteMobsCompat | normal   | torchmaster_1.12.2-1.8.1.81.jar | false           |
| TorchMaster | registerTorchEntities | net.xalcon.torchmaster.compat.VanillaCompat      | normal   | torchmaster_1.12.2-1.8.1.81.jar | false           |
| TorchMaster | registerTorchEntities | net.xalcon.torchmaster.compat.MoCreaturesCompat  | normal   | torchmaster_1.12.2-1.8.1.81.jar | false           |


## ZombieEvent$SummonAidEvent
| Owner             | Method           | Location                                              | Priority | Source                    | RecieveCanceled |
|-------------------|------------------|-------------------------------------------------------|----------|---------------------------|-----------------|
| InControl         | onSummonAidEvent | mcjty.incontrol.ForgeEventHandlers                    | lowest   | incontrol-1.12-3.9.16.jar | false           |
| Ender IO Machines | onSummonAid      | crazypants.enderio.machines.machine.killera.Attackera | normal   | EnderIO-1.12.2-5.1.55.jar | false           |


## CalcEvent$MaxCP
| Owner      | Method      | Location                           | Priority | Source                 | RecieveCanceled |
|------------|-------------|------------------------------------|----------|------------------------|-----------------|
| lambdalib2 | recalcMaxCP | <unassigned>.brain_course          | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | recalcMaxCP | <unassigned>.brain_course          | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | recalcMaxCP | <unassigned>.brain_course          | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | recalcMaxCP | <unassigned>.brain_course          | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | recalcMaxCP | <unassigned>.brain_course_advanced | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | recalcMaxCP | <unassigned>.brain_course_advanced | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | recalcMaxCP | <unassigned>.brain_course_advanced | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2 | recalcMaxCP | <unassigned>.brain_course_advanced | normal   | AcademyCraft-1.1.2.jar | false           |


## PlayerEvent$StopTracking
| Owner            | Method               | Location                                                       | Priority | Source                              | RecieveCanceled |
|------------------|----------------------|----------------------------------------------------------------|----------|-------------------------------------|-----------------|
| LLibrary         | onEntityStopTracking | INSTANCE                                                       | normal   | llibrary-1.7.19-1.12.2.jar          | false           |
| The Betweenlands | onEntityStopTracking | thebetweenlands.common.capability.base.EntityCapabilityHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |
| Techguns         | onStopTracking       | techguns.events.TGEventHandler                                 | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar  | false           |


## EntityViewRenderEvent$FogColors
| Owner             | Method      | Location                                                  | Priority | Source                                          | RecieveCanceled |
|-------------------|-------------|-----------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| Advanced Rocketry | fogColor    | zmaster587.advancedRocketry.event.PlanetEventHandler      | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| The Betweenlands  | onFogColor  | thebetweenlands.client.handler.FogHandler                 | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| The Lost Cities   | onFogEvent  | mcjty.lostcities.ClientEventHandlers                      | normal   | lostcities-1.12-2.0.21.jar                      | false           |
| The Betweenlands  | onFogColors | thebetweenlands.common.item.equipment.ItemLurkerSkinPouch | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| EnderCore         | onFogColor  | com.enderio.core.client.handlers.FluidVisualsHandler      | normal   | EnderCore-1.12.2-0.5.73.jar                     | false           |
| Advanced Rocketry | fogColor    | zmaster587.advancedRocketry.event.PlanetEventHandler      | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |


## BlockEvent$NeighborNotifyEvent
| Owner           | Method          | Location                                                          | Priority | Source                    | RecieveCanceled |
|-----------------|-----------------|-------------------------------------------------------------------|----------|---------------------------|-----------------|
| Fast Leaf Decay | UpdateNeighbour | net.olafkeijsers.fastleafdecay.LeafUpdateEventHandler             | normal   | FastLeafDecay-v14.jar     | false           |
| Ender IO        | on              | crazypants.enderio.base.material.material.MaterialCraftingHandler | normal   | EnderIO-1.12.2-5.1.55.jar | false           |


## PlayerInteractEvent$RightClickItem
| Owner              | Method           | Location                                                  | Priority | Source                                             | RecieveCanceled |
|--------------------|------------------|-----------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Quark              | rightClick       | vazkii.quark.misc.feature.MapMarkers                      | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark              | playerRightClick | vazkii.quark.misc.feature.ThrowableDragonBreath           | normal   | Quark-r1.6-177.jar                                 | false           |
| Chisel             | onRightClickItem | team.chisel.common.item.ChiselController                  | normal   | Chisel-MC1.12.2-1.0.1.44.jar                       | false           |
| Quark              | onRightClick     | vazkii.quark.misc.feature.PlaceVanillaDusts               | normal   | Quark-r1.6-177.jar                                 | false           |
| Ice and Fire       | onEntityUseItem  | com.github.alexthe666.iceandfire.event.EventLiving        | normal   | iceandfire-1.8.3.jar                               | false           |
| Tell Me            | onRightClickItem | fi.dy.masa.tellme.event.InteractEventHandler              | normal   | tellme-1.12.2-0.7.0-dev.20190610.165828.jar        | false           |
| Quark              | onRightClick     | vazkii.quark.misc.feature.ReacharoundPlacing              | normal   | Quark-r1.6-177.jar                                 | false           |
| Draconic Evolution | rightClickItem   | com.brandon3055.draconicevolution.handlers.DEEventHandler | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| The Betweenlands   | onStartUsingItem | thebetweenlands.common.item.tools.ItemGreatsword          | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Techguns           | rightClickEvent  | techguns.events.TGEventHandler                            | high     | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| RFTools            | onRightClickItem | mcjty.rftools.ForgeEventHandlers                          | normal   | rftools-1.12-7.72.jar                              | false           |
| Quark              | onRightClick     | vazkii.quark.decoration.feature.PlaceBlazeRods            | normal   | Quark-r1.6-177.jar                                 | false           |


## TinkerRegisterEvent$TableCastingRegisterEvent
| Owner   | Method                  | Location                              | Priority | Source              | RecieveCanceled |
|---------|-------------------------|---------------------------------------|----------|---------------------|-----------------|
| PlusTiC | onCastingRecipeRegister | landmaster.plustic.modules.ModuleBase | normal   | plustic-7.1.6.1.jar | false           |


## InfoEvent$InfoChanged
| Owner                 | Method        | Location                                          | Priority | Source                                  | RecieveCanceled |
|-----------------------|---------------|---------------------------------------------------|----------|-----------------------------------------|-----------------|
| Practical Logistics 2 | onInfoChanged | sonar.logistics.base.events.LogisticsEventHandler | highest  | practicallogistics2-1.12.2-3.0.8-11.jar | false           |


## ConfigModifyEvent
| Owner         | Method           | Location                     | Priority | Source                 | RecieveCanceled |
|---------------|------------------|------------------------------|----------|------------------------|-----------------|
| Academy Craft | onConfigModified | instance                     | normal   | AcademyCraft-1.1.2.jar | false           |
| Academy Craft | onConfigModified | cn.academy.util.ACKeyManager | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2    | onConfigModify   | instance                     | normal   | AcademyCraft-1.1.2.jar | false           |


## DecorateBiomeEvent$Decorate
| Owner           | Method           | Location                                     | Priority | Source                     | RecieveCanceled |
|-----------------|------------------|----------------------------------------------|----------|----------------------------|-----------------|
| The Lost Cities | onCreateDecorate | mcjty.lostcities.TerrainEventHandlers        | normal   | lostcities-1.12-2.0.21.jar | false           |
| Quark           | decorate         | vazkii.quark.world.feature.MushroomsInSwamps | normal   | Quark-r1.6-177.jar         | false           |


## SoundEvent$SoundSourceEvent
| Owner            | Method      | Location                                                | Priority | Source                              | RecieveCanceled |
|------------------|-------------|---------------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onPlaySound | thebetweenlands.client.handler.AmbienceSoundPlayHandler | normal   | TheBetweenlands-3.5.5-universal.jar | false           |


## EntityViewRenderEvent$FogDensity
| Owner                         | Method          | Location                                             | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-----------------|------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| The Betweenlands              | onFogDensity    | thebetweenlands.client.handler.FogHandler            | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| EnderCore                     | onFogDensity    | com.enderio.core.client.handlers.FluidVisualsHandler | normal   | EnderCore-1.12.2-0.5.73.jar                        | false           |
| PneumaticCraft: Repressurized | fogDensityEvent | me.desht.pneumaticcraft.client.ClientEventHandler    | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| The Lost Cities               | onFogDensity    | mcjty.lostcities.ClientEventHandlers                 | normal   | lostcities-1.12-2.0.21.jar                         | false           |


## TextureCollectedEvent
| Owner | Method          | Location | Priority | Source                    | RecieveCanceled |
|-------|-----------------|----------|----------|---------------------------|-----------------|
| CTM   | onTextureStitch | INSTANCE | normal   | CTM-MC1.12.2-1.0.1.30.jar | false           |


## TextureStitchEvent$Pre
| Owner                         | Method               | Location                                                     | Priority | Source                                               | RecieveCanceled |
|-------------------------------|----------------------|--------------------------------------------------------------|----------|------------------------------------------------------|-----------------|
| The Betweenlands              | onTextureStitchPre   | thebetweenlands.client.handler.TextureStitchHandler          | normal   | TheBetweenlands-3.5.5-universal.jar                  | false           |
| TOP Addons                    | onTextureStitch      | io.github.drmanganese.topaddons.proxy.ClientProxy            | normal   | topaddons-1.12.2-1.12.0.jar                          | false           |
| Building Gadgets              | registerSprites      | com.direwolf20.buildinggadgets.client.proxy.ClientProxy      | normal   | BuildingGadgets-2.7.4.jar                            | false           |
| QuarryPlus                    | registerTexture      | com.yogpc.qp.render.Sprites$                                 | normal   | AdditionalEnchantedMiner-1.12.2-12.3.0-universal.jar | false           |
| Ender IO                      | onIconLoad           | crazypants.enderio.base.render.registry.TextureResolver      | normal   | EnderIO-1.12.2-5.1.55.jar                            | false           |
| CodeChicken Lib               | textureLoad          | codechicken.lib.texture.TextureUtils                         | normal   | CodeChickenLib-1.12.2-3.2.3.358-universal.jar        | false           |
| EnderCore                     | onIconLoad           | com.enderio.core.client.render.IconUtil                      | normal   | EnderCore-1.12.2-0.5.73.jar                          | false           |
| The Betweenlands              | onTextureStitch      | thebetweenlands.client.render.model.loader.CustomModelLoader | normal   | TheBetweenlands-3.5.5-universal.jar                  | false           |
| Mekanism                      | onStitch             | mekanism.client.render.MekanismRenderer                      | normal   | Mekanism-1.12.2-9.8.3.390.jar                        | false           |
| Ender IO                      | onIconLoad           | crazypants.enderio.base.fluid.Fluids                         | normal   | EnderIO-1.12.2-5.1.55.jar                            | false           |
| Compact Machines 3            | onTextureStitch      | org.dave.compactmachines3.misc.TextureStitchHandler          | normal   | compactmachines3-1.12.2-3.0.18-b278.jar              | false           |
| Just Enough Items             | handleTextureRemap   | mezz.jei.startup.ProxyCommonClient                           | normal   | jei_1.12.2-4.15.0.291.jar                            | false           |
| PneumaticCraft: Repressurized | onTextureStitch      | me.desht.pneumaticcraft.client.ClientEventHandler            | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar   | false           |
| JAOPCA                        | onTextureStitchPre   | thelm.jaopca.utils.JAOPCAEventHandler                        | normal   | JAOPCA-1.12.2-2.2.8.103.jar                          | false           |
| Reborn Core                   | preTextureStitch     | reborncore.client.IconSupplier                               | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar           | false           |
| CodeChicken Lib               | onTextureStitchPre   | codechicken.lib.model.loader.blockstate.CCBlockStateLoader   | highest  | CodeChickenLib-1.12.2-3.2.3.358-universal.jar        | false           |
| Tinkers' Construct            | createCustomTextures | slimeknights.tconstruct.library.client.CustomTextureCreator  | low      | TConstruct-1.12.2-2.13.0.171.jar                     | false           |
| Chameleon                     | onPreTextureStitch   | com.jaquadro.minecraft.chameleon.core.ClientProxy            | normal   | Chameleon-1.12-4.1.3.jar                             | false           |
| Tomb Many Graves              | stitchTextures       | com.m4thg33k.tombmanygraves.events.ClientEvents              | normal   | TombManyGraves-1.12-4.2.0.jar                        | false           |
| Mutant Beasts                 | onTextureStitchPre   | chumbanotz.mutantbeasts.client.ClientEventHandler            | normal   | MutantBeasts-1.12.2-0.6.0.jar                        | false           |


## UnlinkNodeEvent
| Owner      | Method     | Location                              | Priority | Source                 | RecieveCanceled |
|------------|------------|---------------------------------------|----------|------------------------|-----------------|
| lambdalib2 | unlinkNode | cn.academy.energy.impl.WirelessSystem | normal   | AcademyCraft-1.1.2.jar | false           |


## EnchantmentLevelSetEvent
| Owner      | Method    | Location                | Priority | Source                       | RecieveCanceled |
|------------|-----------|-------------------------|----------|------------------------------|-----------------|
| Apotheosis | enchLevel | shadows.ench.EnchModule | normal   | Apotheosis-1.12.2-1.12.4.jar | false           |


## LivingSetAttackTargetEvent
| Owner                         | Method            | Location                                                        | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-------------------|-----------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| PneumaticCraft: Repressurized | onMobTargetSet    | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticArmor | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| The Betweenlands              | onSetAttackTarget | thebetweenlands.common.handler.ElixirCommonHandler              | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Epic Siege Mod                | onTargetSet       | funwayguy.epicsiegemod.handlers.MainHandler                     | normal   | EpicSiegeMod-13.165.jar                            | false           |
| Ice and Fire                  | onLivingSetTarget | com.github.alexthe666.iceandfire.event.EventLiving              | normal   | iceandfire-1.8.3.jar                               | false           |


## GuiScreenEvent$ActionPerformedEvent
| Owner | Method        | Location                                         | Priority | Source             | RecieveCanceled |
|-------|---------------|--------------------------------------------------|----------|--------------------|-----------------|
| Quark | onButtonClick | vazkii.quark.base.client.gui.config.ConfigEvents | normal   | Quark-r1.6-177.jar | false           |


## LivingEvent$LivingUpdateEvent
| Owner                 | Method                 | Location                                                          | Priority | Source                                          | RecieveCanceled |
|-----------------------|------------------------|-------------------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| Quark                 | onEntityTick           | vazkii.quark.misc.feature.EnderdragonScales                       | normal   | Quark-r1.6-177.jar                              | false           |
| Apotheosis            | lifeMend               | shadows.ench.EnchModule                                           | normal   | Apotheosis-1.12.2-1.12.4.jar                    | false           |
| Quark                 | updatePlayerStepStatus | vazkii.quark.tweaks.feature.JumpBoostStepAssist                   | normal   | Quark-r1.6-177.jar                              | false           |
| The Betweenlands      | onLivingUpdated        | thebetweenlands.common.handler.ItemEquipmentHandler               | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| lambdalib2            | onLivingUpdate         | instance                                                          | normal   | AcademyCraft-1.1.2.jar                          | false           |
| Quark                 | onEntityTick           | vazkii.quark.tweaks.feature.DoubleDoors                           | normal   | Quark-r1.6-177.jar                              | false           |
| Ender IO              | onLivingUpdate         | Block{minecraft:air}                                              | normal   | EnderIO-1.12.2-5.1.55.jar                       | false           |
| Classic Combat        | handleLivingUpdate     | com.radixshock.classiccombat.ForgeEventHandler                    | normal   | ClassicCombat-1.0.1.jar                         | false           |
| The Betweenlands      | onUpdateLiving         | thebetweenlands.common.handler.PuppetHandler                      | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| Quark                 | onEntityTick           | vazkii.quark.world.feature.Biotite                                | normal   | Quark-r1.6-177.jar                              | false           |
| Quark                 | onEntityTick           | vazkii.quark.automation.feature.AnimalsEatFloorFood               | normal   | Quark-r1.6-177.jar                              | false           |
| Epic Siege Mod        | onLivingUpdate         | funwayguy.epicsiegemod.handlers.MainHandler                       | normal   | EpicSiegeMod-13.165.jar                         | false           |
| Quark                 | entityUpdate           | vazkii.quark.tweaks.feature.BabyZombiesBurn                       | normal   | Quark-r1.6-177.jar                              | false           |
| Advanced Rocketry     | playerTick             | zmaster587.advancedRocketry.event.PlanetEventHandler              | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Quark                 | entityUpdate           | vazkii.quark.misc.feature.ParrotEggs                              | normal   | Quark-r1.6-177.jar                              | false           |
| Ice and Fire          | onLivingUpdate         | com.github.alexthe666.iceandfire.event.EventClient                | normal   | iceandfire-1.8.3.jar                            | false           |
| Brandon's Core        | livingUpdate           | com.brandon3055.brandonscore.handlers.BCEventHandler              | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar    | false           |
| The Betweenlands      | onEntityUpdate         | thebetweenlands.common.handler.ElixirCommonHandler                | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| Epic Siege Mod        | onLivingUpdate         | funwayguy.epicsiegemod.handlers.entities.PlayerHandler            | normal   | EpicSiegeMod-13.165.jar                         | false           |
| LLibrary              | onEntityUpdate         | INSTANCE                                                          | normal   | llibrary-1.7.19-1.12.2.jar                      | false           |
| Quark                 | onPlayerTick           | vazkii.quark.tweaks.feature.LookDownLadders                       | normal   | Quark-r1.6-177.jar                              | false           |
| The Betweenlands      | onEntityLivingUpdate   | thebetweenlands.common.herblore.elixir.PotionRootBound            | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| Ice and Fire          | onEntityUpdate         | com.github.alexthe666.iceandfire.event.EventLiving                | normal   | iceandfire-1.8.3.jar                            | false           |
| Quark                 | onEntityTick           | vazkii.quark.tweaks.feature.VillagerPursueEmeralds                | normal   | Quark-r1.6-177.jar                              | false           |
| Quark                 | onEntityUpdate         | vazkii.quark.misc.feature.PoisonPotatoUsage                       | normal   | Quark-r1.6-177.jar                              | false           |
| The Betweenlands      | teleportCheck          | thebetweenlands.common.handler.PlayerPortalHandler                | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| Quark                 | onUpdate               | vazkii.quark.world.feature.BuriedTreasure                         | normal   | Quark-r1.6-177.jar                              | false           |
| Prehistoric Eclipse   | onEntityUpdate         | com.dabigjoe.obsidianAPI.ObsidianEventHandler                     | normal   | Prehistoric Eclipsev0.9-BETA.jar                | false           |
| Advanced Rocketry     | playerTick             | zmaster587.advancedRocketry.event.PlanetEventHandler              | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar | false           |
| Better Combat Rebirth | onLivingUpdate         | bettercombat.mod.handler.EventHandlers                            | normal   | BetterCombat-1.12.2-1.5.6.jar                   | true            |
| The Betweenlands      | onLivingUpdate         | thebetweenlands.common.block.plant.BlockWeedwoodBush              | normal   | TheBetweenlands-3.5.5-universal.jar             | false           |
| Academy Craft         | onLivingUpdate         | cn.academy.ability.vanilla.meltdowner.skill.MDDamageHelper$Events | normal   | AcademyCraft-1.1.2.jar                          | false           |
| Quark                 | onLivingUpdate         | vazkii.quark.tweaks.feature.ChickensShedFeathers                  | normal   | Quark-r1.6-177.jar                              | false           |
| Academy Craft         | onUpdateClient         | cn.academy.ability.vanilla.meltdowner.skill.MDDamageHelper$Events | normal   | AcademyCraft-1.1.2.jar                          | false           |
| Quark                 | onUpdate               | vazkii.quark.tweaks.feature.EndermenAntiCheese                    | normal   | Quark-r1.6-177.jar                              | false           |
| Quark                 | onEntityTick           | vazkii.quark.misc.feature.HorseWhistle                            | normal   | Quark-r1.6-177.jar                              | false           |
| Quark                 | onUpdate               | vazkii.quark.tweaks.feature.SheepArmor                            | normal   | Quark-r1.6-177.jar                              | false           |


## LivingHurtEvent
| Owner                | Method                  | Location                                                          | Priority | Source                                  | RecieveCanceled |
|----------------------|-------------------------|-------------------------------------------------------------------|----------|-----------------------------------------|-----------------|
| lambdalib2           | onLivingHurt            | <unassigned>.vec_deviation                                        | normal   | AcademyCraft-1.1.2.jar                  | false           |
| The Betweenlands     | onEntityOnFire          | thebetweenlands.common.handler.ArmorHandler                       | normal   | TheBetweenlands-3.5.5-universal.jar     | false           |
| Academy Monster      | onLivingHurt            | cn.paindar.academymonster.ability.AMVecReflect                    | normal   | AcademyMonster-1.1.0a.jar               | false           |
| PlusTiC              | defend                  | landmaster.plustic.traits.BlindBandit                             | highest  | plustic-7.1.6.1.jar                     | false           |
| Academy Craft        | onLivingAttack          | cn.academy.ability.vanilla.meltdowner.skill.MDDamageHelper$Events | normal   | AcademyCraft-1.1.2.jar                  | false           |
| The Betweenlands     | onEntityAttacked        | thebetweenlands.common.handler.PuppetHandler                      | normal   | TheBetweenlands-3.5.5-universal.jar     | false           |
| PlusTiC              | defend                  | landmaster.plustic.traits.HailHydra                               | normal   | plustic-7.1.6.1.jar                     | false           |
| Quark                | onDamage                | vazkii.quark.vanity.feature.WitchHat                              | normal   | Quark-r1.6-177.jar                      | false           |
| Epic Siege Mod       | onAttacked              | funwayguy.epicsiegemod.handlers.entities.SpiderHandler            | normal   | EpicSiegeMod-13.165.jar                 | false           |
| Apotheosis           | livingHurt              | shadows.ench.EnchModule                                           | normal   | Apotheosis-1.12.2-1.12.4.jar            | false           |
| The Betweenlands     | onEntityAttacked        | thebetweenlands.common.handler.AttackDamageHandler                | normal   | TheBetweenlands-3.5.5-universal.jar     | false           |
| CraftTweaker2        | onEntityLivingHurtEvent | crafttweaker.mc1120.events.CommonEventHandler                     | normal   | CraftTweaker2-1.12-4.1.20.jar           | false           |
| Quark                | onHurt                  | vazkii.quark.tweaks.feature.SquidsInkYou                          | normal   | Quark-r1.6-177.jar                      | false           |
| Tinkers' Construct   | onDamageEntity          | slimeknights.tconstruct.shared.AchievementEvents                  | normal   | TConstruct-1.12.2-2.13.0.171.jar        | false           |
| lambdalib2           | onPlayerAttacked        | <unassigned>.light_shield                                         | normal   | AcademyCraft-1.1.2.jar                  | false           |
| Tinkers' Construct   | reducedDamageBlocked    | slimeknights.tconstruct.tools.melee.item.BattleSign               | low      | TConstruct-1.12.2-2.13.0.171.jar        | false           |
| PlusTiC              | attack                  | landmaster.plustic.traits.RudeAwakening                           | lowest   | plustic-7.1.6.1.jar                     | false           |
| Techguns             | onLivingHurt            | techguns.events.TGEventHandler                                    | high     | techguns-1.12.2-2.0.2.0_pre3.1.jar      | false           |
| Ice and Fire         | onEntityDamage          | com.github.alexthe666.iceandfire.event.EventLiving                | normal   | iceandfire-1.8.3.jar                    | false           |
| Tinkers' Construct   | playerBlockOrHurtEvent  | slimeknights.tconstruct.tools.TraitEvents                         | normal   | TConstruct-1.12.2-2.13.0.171.jar        | false           |
| The Betweenlands     | onEntityAttacked        | thebetweenlands.common.handler.PlayerDecayHandler                 | normal   | TheBetweenlands-3.5.5-universal.jar     | false           |
| Open Modular Turrets | entityHurtEvent         | omtteam.openmodularturrets.handler.OMTEventHandler                | normal   | openmodularturrets-1.12.2-3.1.4-356.jar | false           |


## TickEvent$PlayerTickEvent
| Owner                         | Method                | Location                                                                     | Priority | Source                                             | RecieveCanceled |
|-------------------------------|-----------------------|------------------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| The Betweenlands              | onPlayerTick          | thebetweenlands.common.handler.PlayerDecayHandler                            | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| CraftTweaker2                 | onPlayerTickEvent     | crafttweaker.mc1120.events.CommonEventHandler                                | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Draconic Evolution            | onPlayerTick          | com.brandon3055.draconicevolution.handlers.CustomArmorHandler                | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| NuclearCraft                  | updatePlayerRadiation | nc.radiation.RadiationHandler                                                | normal   | NuclearCraft-2.18o-1.12.2.jar                      | false           |
| The Betweenlands              | onPlayerTick          | thebetweenlands.common.handler.OverworldItemHandler                          | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| PneumaticCraft: Repressurized | playJetbootsParticles | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticArmor              | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| EnderCore                     | onPlayerTick          | com.enderio.core.common.handlers.FireworkHandler                             | normal   | EnderCore-1.12.2-0.5.73.jar                        | false           |
| RFTools                       | onPlayerTickEvent     | mcjty.rftools.ForgeEventHandlers                                             | normal   | rftools-1.12-7.72.jar                              | false           |
| Flood Lights                  | onPlayerJoin          | de.keridos.floodlights.compatability.IGWSupportNotifier                      | normal   | FloodLights-1.12.2-1.4.1-17.jar                    | false           |
| Quark                         | onPlayerTick          | vazkii.quark.management.feature.AutomaticToolRestock                         | normal   | Quark-r1.6-177.jar                                 | false           |
| Mutant Beasts                 | onPlayerUpdate        | chumbanotz.mutantbeasts.EventHandler                                         | normal   | MutantBeasts-1.12.2-0.6.0.jar                      | false           |
| Ender IO                      | onTick                | crazypants.enderio.base.item.darksteel.ItemInventoryCharger                  | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Compact Machines 3            | onPlayerTick          | org.dave.compactmachines3.misc.PlayerEventHandler                            | normal   | compactmachines3-1.12.2-3.0.18-b278.jar            | false           |
| The Betweenlands              | onPlayerUpdate        | thebetweenlands.common.handler.PuppetHandler                                 | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Compact Machines 3            | onPlayerTick          | org.dave.compactmachines3.skyworld.SkyWorldEvents                            | normal   | compactmachines3-1.12.2-3.0.18-b278.jar            | false           |
| Ender IO                      | onPlayerTick          | crazypants.enderio.base.handler.darksteel.DarkSteelController                | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Quark                         | tick                  | vazkii.quark.misc.feature.MapMarkers                                         | normal   | Quark-r1.6-177.jar                                 | false           |
| Ender IO                      | onPlayerTick          | crazypants.enderio.base.power.wireless.WirelessChargerController             | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Placebo                       | onPlayerTick          | shadows.placebo.patreon.PatreonManager                                       | normal   | Placebo-1.12.2-1.6.0.jar                           | false           |
| McJtyLib                      | onPlayerTickEvent     | mcjty.lib.McJtyLib$EventHandler                                              | normal   | mcjtylib-1.12-3.5.4.jar                            | false           |
| Ender IO                      | onPlayerTick          | crazypants.enderio.base.item.magnet.MagnetController                         | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Advanced Rocketry             | onPlayerTick          | zmaster587.advancedRocketry.stations.SpaceObjectManager                      | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |
| Apotheosis                    | tick                  | shadows.deadly.loot.affix.AffixEvents                                        | normal   | Apotheosis-1.12.2-1.12.4.jar                       | false           |
| The Betweenlands              | onPlayerTick          | thebetweenlands.common.handler.LocationHandler                               | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| PneumaticCraft: Repressurized | jetBootsEvent         | me.desht.pneumaticcraft.client.ClientEventHandler                            | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| The Betweenlands              | onPlayerUpdate        | thebetweenlands.common.capability.collision.RingOfDispersionEntityCapability | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| PneumaticCraft: Repressurized | playerTick            | me.desht.pneumaticcraft.client.render.pneumatic_armor.HUDHandler             | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| PneumaticCraft: Repressurized | onPlayerTick          | me.desht.pneumaticcraft.common.pneumatic_armor.CommonArmorHandler            | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Techguns                      | localClientPlayerTick | techguns.events.TGTickHandler                                                | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| Chisel                        | speedupPlayer         | team.chisel.client.handler.BlockSpeedHandler                                 | normal   | Chisel-MC1.12.2-1.0.1.44.jar                       | false           |
| The Betweenlands              | onEntityUpdate        | thebetweenlands.common.capability.base.EntityCapabilityHandler               | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                         | onUpdate              | vazkii.quark.tweaks.feature.CompassesWorkEverywhere                          | normal   | Quark-r1.6-177.jar                                 | false           |
| The Betweenlands              | onPlayerTick          | thebetweenlands.common.handler.AdvancementHandler                            | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Tech Reborn                   | onPlayerTick          | techreborn.events.TRTickHandler                                              | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar        | true            |
| Mekanism                      | onTick                | mekanism.common.CommonPlayerTickHandler                                      | normal   | Mekanism-1.12.2-9.8.3.390.jar                      | false           |
| Aroma1997Core                 | playerTick            | aroma1997.core.util.registry.TickRegistry$2                                  | normal   | Aroma1997Core-1.12.2-2.0.0.2.jar                   | false           |
| InstantUnify                  | player                | mrriegel.instantunify.InstantUnify                                           | normal   | instantunify-1.12.2-1.1.2.jar                      | false           |
| lambdalib2                    | onPlayerTick          | cn.academy.item.ItemCoin                                                     | normal   | AcademyCraft-1.1.2.jar                             | false           |
| The Betweenlands              | onPlayerCltTick       | thebetweenlands.client.handler.AmbienceSoundPlayHandler                      | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| The Betweenlands              | onPlayerTick          | thebetweenlands.common.item.equipment.ItemRingOfFlight                       | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Techguns                      | onPlayerTick          | techguns.events.TGTickHandler                                                | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |


## PlayerContainerEvent
| Owner        | Method | Location                           | Priority | Source                        | RecieveCanceled |
|--------------|--------|------------------------------------|----------|-------------------------------|-----------------|
| InstantUnify | open   | mrriegel.instantunify.InstantUnify | normal   | instantunify-1.12.2-1.1.2.jar | false           |


## LivingAttackEvent
| Owner                         | Method              | Location                                                                     | Priority | Source                                             | RecieveCanceled |
|-------------------------------|---------------------|------------------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Academy Monster               | onLivingAttack      | cn.paindar.academymonster.ability.AMVecReflect                               | normal   | AcademyMonster-1.1.0a.jar                          | false           |
| The Betweenlands              | onLivingAttacked    | thebetweenlands.common.entity.rowboat.EntityWeedwoodRowboat                  | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Tinkers' Construct            | reflectProjectiles  | slimeknights.tconstruct.tools.melee.item.BattleSign                          | normal   | TConstruct-1.12.2-2.13.0.171.jar                   | false           |
| Ice and Fire                  | onLivingAttacked    | com.github.alexthe666.iceandfire.event.EventLiving                           | normal   | iceandfire-1.8.3.jar                               | false           |
| PneumaticCraft: Repressurized | onLivingAttack      | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticArmor              | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Open Modular Turrets          | entityAttackedEvent | omtteam.openmodularturrets.handler.OMTEventHandler                           | normal   | openmodularturrets-1.12.2-3.1.4-356.jar            | false           |
| Mutant Beasts                 | onLivingAttack      | chumbanotz.mutantbeasts.EventHandler                                         | normal   | MutantBeasts-1.12.2-0.6.0.jar                      | false           |
| The Betweenlands              | onLivingAttack      | thebetweenlands.common.handler.PuppetHandler                                 | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| The Betweenlands              | onLivingAttacked    | thebetweenlands.common.capability.collision.RingOfDispersionEntityCapability | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| CraftTweaker2                 | onLivingAttackEvent | crafttweaker.mc1120.events.CommonEventHandler                                | normal   | CraftTweaker2-1.12-4.1.20.jar                      | false           |
| Mekanism                      | onEntityAttacked    | mekanism.common.item.ItemGasMask                                             | normal   | Mekanism-1.12.2-9.8.3.390.jar                      | false           |
| The Betweenlands              | onLivingAttacked    | INSTANCE                                                                     | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Tinkers Tool Leveling         | onLivingHurt        | slimeknights.toolleveling.ModToolLeveling                                    | lowest   | TinkerToolLeveling-1.12.2-1.1.0.jar                | true            |
| Techguns                      | OnLivingAttack      | techguns.events.TGEventHandler                                               | high     | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| Mekanism                      | onEntityAttacked    | mekanism.common.item.ItemFreeRunners                                         | normal   | Mekanism-1.12.2-9.8.3.390.jar                      | false           |
| The Betweenlands              | onEntityAttack      | thebetweenlands.common.handler.AttackDamageHandler                           | low      | TheBetweenlands-3.5.5-universal.jar                | false           |
| Draconic Evolution            | onPlayerAttacked    | com.brandon3055.draconicevolution.handlers.CustomArmorHandler                | low      | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |


## LevelChangeEvent
| Owner         | Method              | Location                                 | Priority | Source                 | RecieveCanceled |
|---------------|---------------------|------------------------------------------|----------|------------------------|-----------------|
| Academy Craft | onLevelChange       | cn.academy.advancements.DispatcherAch    | normal   | AcademyCraft-1.1.2.jar | false           |
| lambdalib2    | changedLevel        | instance                                 | normal   | AcademyCraft-1.1.2.jar | false           |
| Academy Craft | levelChangeListener | cn.academy.analytic.AnalyticDataListener | normal   | AcademyCraft-1.1.2.jar | false           |


## DrawBlockHighlightEvent
| Owner                  | Method               | Location                                                                | Priority | Source                                          | RecieveCanceled |
|------------------------|----------------------|-------------------------------------------------------------------------|----------|-------------------------------------------------|-----------------|
| MCMultiPart            | onDrawHighlight      | mcmultipart.client.MCMPClientProxy                                      | normal   | MCMultiPart-2.5.3.jar                           | false           |
| Chisel                 | onBlockHighlight     | team.chisel.common.item.ChiselController                                | normal   | Chisel-MC1.12.2-1.0.1.44.jar                    | false           |
| Techguns               | onBlockHighlight     | techguns.events.TGEventHandler                                          | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar              | false           |
| OMLib                  | drawBlockOutline     | omtteam.omlib.handler.OMLibEventHandler                                 | normal   | omlib-1.12.2-3.1.3-246.jar                      | false           |
| lambdalib2             | onDrawBlockHighlight | instance                                                                | normal   | AcademyCraft-1.1.2.jar                          | false           |
| RFTools                | onRenderBlockOutline | mcjty.rftools.setup.ClientProxy                                         | normal   | rftools-1.12-7.72.jar                           | false           |
| Tomb Many Graves       | onBlockHighlight     | com.m4thg33k.tombmanygraves.events.RenderEvents                         | normal   | TombManyGraves-1.12-4.2.0.jar                   | false           |
| Ender IO               | onHighlight          | crazypants.enderio.base.handler.darksteel.PlayerAOEAttributeHandler     | normal   | EnderIO-1.12.2-5.1.55.jar                       | false           |
| Chisel                 | onBlockHighlight     | team.chisel.common.item.ItemOffsetTool                                  | normal   | Chisel-MC1.12.2-1.0.1.44.jar                    | false           |
| Refined Storage        | onBlockDrawHighlight | com.raoulvdberge.refinedstorage.render.collision.BlockHighlightListener | lowest   | refinedstorage-1.6.15.jar                       | false           |
| CodeChicken Lib        | onBlockHighlight     | codechicken.lib.render.CCRenderEventHandler                             | low      | CodeChickenLib-1.12.2-3.2.3.358-universal.jar   | false           |
| Better Builder's Wands | blockHighlightEvent  | portablejim.bbw.core.BlockEvents                                        | normal   | BetterBuildersWands-1.12-0.11.1.245+69d0d70.jar | false           |
| Practical Logistics 2  | renderHighlight      | sonar.logistics.network.PL2Client                                       | normal   | practicallogistics2-1.12.2-3.0.8-11.jar         | false           |


## EditModeToggleEvent
| Owner             | Method                | Location                              | Priority | Source                    | RecieveCanceled |
|-------------------|-----------------------|---------------------------------------|----------|---------------------------|-----------------|
| Just Enough Items | onEditModeToggleEvent | mezz.jei.ingredients.IngredientFilter | normal   | jei_1.12.2-4.15.0.291.jar | false           |


## NetworkCableEvent$RemovedCable
| Owner                 | Method         | Location                                          | Priority | Source                                  | RecieveCanceled |
|-----------------------|----------------|---------------------------------------------------|----------|-----------------------------------------|-----------------|
| Practical Logistics 2 | onCableRemoved | sonar.logistics.base.events.LogisticsEventHandler | highest  | practicallogistics2-1.12.2-3.0.8-11.jar | false           |


## PlayerEvent$PlayerRespawnEvent
| Owner                  | Method          | Location                                               | Priority | Source                                                    | RecieveCanceled |
|------------------------|-----------------|--------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| Compact Machines 3     | onPlayerSpawn   | org.dave.compactmachines3.misc.PlayerEventHandler      | normal   | compactmachines3-1.12.2-3.0.18-b278.jar                   | false           |
| Just Enough Dimensions | onPlayerRespawn | fi.dy.masa.justenoughdimensions.event.JEDEventHandler  | normal   | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |
| The Betweenlands       | onRespawn       | thebetweenlands.common.handler.PlayerRespawnHandler    | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |
| PlusTiC                | onPlayerRespawn | landmaster.plustic.api.Toggle                          | normal   | plustic-7.1.6.1.jar                                       | false           |
| CraftTweaker2          | onPlayerRespawn | crafttweaker.mc1120.events.CommonEventHandler          | normal   | CraftTweaker2-1.12-4.1.20.jar                             | false           |
| The Betweenlands       | onPlayerTick    | thebetweenlands.common.handler.PlayerDecayHandler      | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |
| Epic Siege Mod         | onPlayerRespawn | funwayguy.epicsiegemod.handlers.entities.PlayerHandler | normal   | EpicSiegeMod-13.165.jar                                   | false           |


## RenderPlayerEvent$Pre
| Owner                         | Method               | Location                                                             | Priority | Source                                             | RecieveCanceled |
|-------------------------------|----------------------|----------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Reborn Core                   | holidayRender        | reborncore.client.models.HolidayRenderEvent                          | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar         | false           |
| The Betweenlands              | onLivingRender       | thebetweenlands.client.render.entity.RenderWeedwoodRowboat           | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| PneumaticCraft: Repressurized | playerPreRotateEvent | me.desht.pneumaticcraft.client.ClientEventHandler                    | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| The Betweenlands              | onPlayerRenderPre    | thebetweenlands.client.render.entity.RenderVolarkite                 | highest  | TheBetweenlands-3.5.5-universal.jar                | false           |
| The Betweenlands              | onPlayerRenderPre    | thebetweenlands.client.render.entity.RenderGrapplingHookNode         | lowest   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Aroma1997Core                 | tick                 | aroma1997.core.client.ClientEventListener                            | normal   | Aroma1997Core-1.12.2-2.0.0.2.jar                   | false           |
| PlusTiC                       | renderBeam           | landmaster.plustic.tools.ToolLaserGun$ProxyClient                    | normal   | plustic-7.1.6.1.jar                                | false           |
| Ender IO                      | onPlayerRenderPre    | crazypants.enderio.base.handler.darksteel.UpgradeRenderManager       | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Ice and Fire                  | playerRender         | com.github.alexthe666.iceandfire.client.render.entity.RenderModCapes | normal   | iceandfire-1.8.3.jar                               | false           |
| Tech Reborn                   | renderPlayer         | techreborn.client.ClientEventHandler                                 | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar        | false           |
| The Betweenlands              | onPreRenderPlayer    | thebetweenlands.client.handler.DecayRenderHandler                    | lowest   | TheBetweenlands-3.5.5-universal.jar                | false           |


## RenderGameOverlayEvent$Post
| Owner                 | Method                   | Location                                                              | Priority | Source                                             | RecieveCanceled |
|-----------------------|--------------------------|-----------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| Draconic Evolution    | renderGameOverlay        | com.brandon3055.draconicevolution.client.handler.ClientEventHandler   | normal   | Draconic-Evolution-1.12.2-2.3.24.349-universal.jar | false           |
| Brandon's Core        | render                   | com.brandon3055.brandonscore.utils.BCProfiler                         | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar       | false           |
| Patchouli             | onRenderHUD              | vazkii.patchouli.client.handler.BookRightClickHandler                 | normal   | Patchouli-1.0-20.jar                               | false           |
| Ender IO              | renderOverlay            | crazypants.enderio.base.item.yetawrench.YetaWrenchOverlayRenderer     | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Item Blacklist        | onRenderGUI              | net.doubledoordev.itemblacklist.client.ClientEventHandlers            | normal   | ItemBlacklist-1.4.3.jar                            | false           |
| Chunk Pregenerator    | onRender                 | pregenerator.impl.client.ClientHandler                                | normal   | Chunk Pregenerator V1.12-2.2.jar                   | false           |
| Quark                 | renderHUD                | vazkii.quark.client.feature.UsageTicker                               | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                 | drawHUD                  | vazkii.quark.vanity.feature.EmoteSystem                               | normal   | Quark-r1.6-177.jar                                 | false           |
| LLibrary              | onRenderOverlayPost      | INSTANCE                                                              | normal   | llibrary-1.7.19-1.12.2.jar                         | false           |
| Quark                 | hudPost                  | vazkii.quark.management.feature.ChangeHotbarKeybind                   | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                 | onRender                 | vazkii.quark.misc.feature.ReacharoundPlacing                          | normal   | Quark-r1.6-177.jar                                 | false           |
| Practical Logistics 2 | renderInteractionOverlay | sonar.logistics.network.PL2Client                                     | normal   | practicallogistics2-1.12.2-3.0.8-11.jar            | false           |
| The Betweenlands      | renderGui                | thebetweenlands.client.handler.equipment.RadialMenuHandler            | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| SCPCraft:Decline      | renderGameOverlayPost    | com.grandlovania.scp.SCPGuiOverlay                                    | normal   | SCPCraft-1.1-Decline.jar                           | false           |
| The Betweenlands      | onRenderGameOverlay      | thebetweenlands.client.handler.ScreenRenderHandler                    | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| NuclearCraft          | addRadiationInfo         | nc.radiation.RadiationRenders                                         | lowest   | NuclearCraft-2.18o-1.12.2.jar                      | false           |
| Quark                 | onHUDRender              | vazkii.quark.misc.feature.LockDirectionHotkey                         | normal   | Quark-r1.6-177.jar                                 | false           |
| The Betweenlands      | onCrosshairRenderPost    | thebetweenlands.common.handler.ExtendedReachHandler                   | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                 | onRenderOverlay          | vazkii.quark.client.feature.AutoJumpHotkey                            | normal   | Quark-r1.6-177.jar                                 | false           |
| Brandon's Core        | renderScreen             | com.brandon3055.brandonscore.client.BCClientEventHandler              | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar       | false           |
| MoreOverlays          | onOverlayRender          | at.feldim2425.moreoverlays.chunkbounds.ChunkBoundsHandler             | normal   | moreoverlays-1.15.1-mc1.12.2.jar                   | false           |
| Akashic Tome          | onDrawScreen             | vazkii.akashictome.client.HUDHandler                                  | normal   | AkashicTome-1.2-12.jar                             | false           |
| Ender IO              | renderOverlay            | crazypants.enderio.base.item.conduitprobe.ConduitProbeOverlayRenderer | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Scannable             | onOverlayRender          | INSTANCE                                                              | normal   | Scannable-MC1.12.2-1.6.3.24.jar                    | false           |
| Quark                 | renderSymbols            | vazkii.quark.client.feature.RenderItemsInChat                         | normal   | Quark-r1.6-177.jar                                 | false           |
| Patchouli             | onRenderHUD              | vazkii.patchouli.client.handler.MultiblockVisualizationHandler        | normal   | Patchouli-1.0-20.jar                               | false           |
| Advanced Rocketry     | onScreenRender           | zmaster587.advancedRocketry.event.RocketEventHandler                  | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar    | false           |


## BlockEvent$HarvestDropsEvent
| Owner              | Method                   | Location                                                            | Priority | Source                                      | RecieveCanceled |
|--------------------|--------------------------|---------------------------------------------------------------------|----------|---------------------------------------------|-----------------|
| PlusTiC            | blockDrops               | landmaster.plustic.traits.Global                                    | lowest   | plustic-7.1.6.1.jar                         | false           |
| NuclearCraft       | addBlockDrop             | nc.handler.DropHandler                                              | normal   | NuclearCraft-2.18o-1.12.2.jar               | false           |
| Techguns           | MilitaryCrateDrops       | techguns.events.TGEventHandler                                      | high     | techguns-1.12.2-2.0.2.0_pre3.1.jar          | false           |
| EnderCore          | handleBlockBreak         | com.enderio.core.common.handlers.AutoSmeltHandler                   | normal   | EnderCore-1.12.2-0.5.73.jar                 | false           |
| The Betweenlands   | onHarvestBlock           | thebetweenlands.common.block.farming.BlockGenericDugSoil            | normal   | TheBetweenlands-3.5.5-universal.jar         | false           |
| Quark              | onDrops                  | vazkii.quark.tweaks.feature.GlassShards                             | low      | Quark-r1.6-177.jar                          | false           |
| Quark              | onDrops                  | vazkii.quark.world.feature.Archaeologist                            | normal   | Quark-r1.6-177.jar                          | false           |
| Ender IO           | onHarvest                | crazypants.enderio.base.item.darksteel.ItemDarkSteelCrook           | normal   | EnderIO-1.12.2-5.1.55.jar                   | false           |
| EnderCore          | onHarvestDrop            | com.enderio.core.common.handlers.RightClickCropHandler              | normal   | EnderCore-1.12.2-0.5.73.jar                 | false           |
| InstantUnify       | harvest                  | mrriegel.instantunify.InstantUnify                                  | lowest   | instantunify-1.12.2-1.1.2.jar               | false           |
| CraftTweaker2      | onBlockHarvestDropsEvent | crafttweaker.mc1120.events.CommonEventHandler                       | normal   | CraftTweaker2-1.12-4.1.20.jar               | false           |
| Building Gadgets   | GetDrops                 | com.direwolf20.buildinggadgets.common.events.BreakEventHandler      | normal   | BuildingGadgets-2.7.4.jar                   | false           |
| Tech Reborn        | onBlockHarvest           | techreborn.events.BlockBreakHandler                                 | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar | false           |
| Tinkers' Construct | blockDropEvent           | slimeknights.tconstruct.tools.TraitEvents                           | normal   | TConstruct-1.12.2-2.13.0.171.jar            | false           |
| Ender IO           | onHarvestDropsEvent      | crazypants.enderio.base.item.spawner.BrokenSpawnerHandler           | normal   | EnderIO-1.12.2-5.1.55.jar                   | false           |
| Apotheosis         | drops                    | shadows.spawn.SpawnerModule                                         | lowest   | Apotheosis-1.12.2-1.12.4.jar                | false           |
| Techguns           | onBlockDrops             | techguns.events.TGEventHandler                                      | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar          | false           |
| AgriCraft          | interceptGrassDrop       | com.infinityraider.agricraft.handler.GrassDropHandler               | highest  | AgriCraft-2.12.0-1.12.0-a6.jar              | false           |
| Apotheosis         | harvest                  | shadows.deadly.loot.affix.AffixEvents                               | normal   | Apotheosis-1.12.2-1.12.4.jar                | false           |
| Ender IO           | blockDropEvent           | crazypants.enderio.base.item.darksteel.upgrade.direct.DirectUpgrade | lowest   | EnderIO-1.12.2-5.1.55.jar                   | false           |


## EntityJoinWorldEvent
| Owner                         | Method                 | Location                                                            | Priority | Source                                             | RecieveCanceled |
|-------------------------------|------------------------|---------------------------------------------------------------------|----------|----------------------------------------------------|-----------------|
| EnderCore                     | handleArrowFire        | com.enderio.core.common.handlers.XPBoostHandler                     | normal   | EnderCore-1.12.2-0.5.73.jar                        | false           |
| Ice and Fire                  | onEntityJoinWorld      | com.github.alexthe666.iceandfire.event.EventLiving                  | normal   | iceandfire-1.8.3.jar                               | false           |
| Clumps                        | updateEntities         | com.blamejared.clumps.events.CommonEventHandler                     | normal   | Clumps-3.1.2.jar                                   | false           |
| Brandon's Core                | joinWorld              | com.brandon3055.brandonscore.client.BCClientEventHandler            | normal   | BrandonsCore-1.12.2-2.4.17.208-universal.jar       | false           |
| The Betweenlands              | onEntitySpawn          | thebetweenlands.common.handler.EntitySpawnHandler                   | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Quark                         | onVehicleArrive        | vazkii.quark.automation.feature.ChainLinkage                        | normal   | Quark-r1.6-177.jar                                 | false           |
| PneumaticCraft: Repressurized | onPlayerJoinWorld      | me.desht.pneumaticcraft.common.pneumatic_armor.CommonArmorHandler   | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Storage Drawers               | onEntityJoinWorldEvent | com.jaquadro.minecraft.storagedrawers.core.ClientProxy              | normal   | StorageDrawers-1.12.2-5.4.0.jar                    | false           |
| PneumaticCraft: Repressurized | onEntityJoinWorld      | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticCraft     | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Quark                         | onEntityJoinWorld      | vazkii.quark.decoration.feature.VariedChests                        | normal   | Quark-r1.6-177.jar                                 | false           |
| AI Improvements               | onEntityJoinWorld      | com.builtbroken.ai.improvements.AIImprovements                      | normal   | AIImprovements-1.12-0.0.1b3.jar                    | false           |
| Ender IO                      | handleJoin             | crazypants.enderio.base.handler.darksteel.PlayerAOEAttributeHandler | normal   | EnderIO-1.12.2-5.1.55.jar                          | false           |
| Epic Siege Mod                | onEntityConstruct      | funwayguy.epicsiegemod.handlers.MainHandler                         | normal   | EpicSiegeMod-13.165.jar                            | false           |
| Epic Siege Mod                | onSpawn                | funwayguy.epicsiegemod.handlers.entities.CreeperHandler             | normal   | EpicSiegeMod-13.165.jar                            | false           |
| InstantUnify                  | spawn                  | mrriegel.instantunify.InstantUnify                                  | lowest   | instantunify-1.12.2-1.1.2.jar                      | false           |
| PneumaticCraft: Repressurized | onEntityJoinWorld      | me.desht.pneumaticcraft.common.event.EventHandlerPneumaticArmor     | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Quark                         | onWolfAppear           | vazkii.quark.tweaks.feature.PatTheDogs                              | normal   | Quark-r1.6-177.jar                                 | false           |
| Quark                         | onBoatArrive           | vazkii.quark.vanity.feature.BoatSails                               | normal   | Quark-r1.6-177.jar                                 | false           |
| Brandon's Core                | entityJoinWorld        | com.brandon3055.brandonscore.handlers.BCEventHandler                | low      | BrandonsCore-1.12.2-2.4.17.208-universal.jar       | false           |
| The Betweenlands              | joinWorld              | thebetweenlands.common.handler.AspectSyncHandler                    | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| InControl                     | onEntityJoinWorldLast  | mcjty.incontrol.ForgeEventHandlers                                  | highest  | incontrol-1.12-3.9.16.jar                          | false           |
| Tomb Many Graves              | onPlayerJoin           | com.m4thg33k.tombmanygraves.friends.FriendHandler                   | normal   | TombManyGraves-1.12-4.2.0.jar                      | false           |
| The Betweenlands              | onPlayerJoin           | thebetweenlands.common.capability.base.EntityCapabilityHandler      | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| The Betweenlands              | joinWorld              | thebetweenlands.common.handler.EnvironmentEventHandler              | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Epic Siege Mod                | onEntitySpawn          | funwayguy.epicsiegemod.handlers.entities.SkeletonHandler            | normal   | EpicSiegeMod-13.165.jar                            | false           |
| Epic Siege Mod                | onEntitySpawn          | funwayguy.epicsiegemod.handlers.entities.WitchHandler               | normal   | EpicSiegeMod-13.165.jar                            | false           |
| LLibrary                      | onJoinWorld            | INSTANCE                                                            | normal   | llibrary-1.7.19-1.12.2.jar                         | false           |
| Minecraft Forge               | onEntityJoinWorld      | net.minecraftforge.common.ForgeInternalHandler                      | highest  | forge-1.12.2-14.23.5.2847.jar                      | false           |
| Mekanism                      | onEntitySpawn          | mekanism.common.item.ItemBlockCardboardBox                          | normal   | Mekanism-1.12.2-9.8.3.390.jar                      | false           |
| OreExcavation                 | onEntitySpawn          | oreexcavation.handlers.EventHandler                                 | lowest   | OreExcavation-1.4.143.jar                          | false           |
| Progressive Automation        | onJoin                 | com.vanhal.progressiveautomation.client.events.EventPlayers         | normal   | ProgressiveAutomation-1.12.2-1.7.8.jar             | false           |
| Epic Siege Mod                | onEntitySpawn          | funwayguy.epicsiegemod.handlers.entities.GeneralEntityHandler       | lowest   | EpicSiegeMod-13.165.jar                            | false           |
| InControl                     | onEntityJoinWorld      | mcjty.incontrol.ForgeEventHandlers                                  | lowest   | incontrol-1.12-3.9.16.jar                          | false           |
| TOP Addons                    | onEntityJoinWorld      | io.github.drmanganese.topaddons.config.capabilities.CapEvents       | normal   | topaddons-1.12.2-1.12.0.jar                        | false           |
| OMLib                         | playerJoinEvent        | omtteam.omlib.handler.OMLibEventHandler                             | normal   | omlib-1.12.2-3.1.3-246.jar                         | false           |
| The Betweenlands              | onEntitySpawn          | thebetweenlands.common.handler.OverworldItemHandler                 | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Calm Down Zombie Guy          | onEntityJoinWorld      | com.bloodnbonesgaming.calmdownzombieguy.CalmDownZombieGuy           | normal   | CalmDownZombieGuy-1.12.2-1.0.0.jar                 | false           |
| FastWorkbench                 | normalRemoval          | shadows.fastbench.FastBench                                         | normal   | FastWorkbench-1.12.2-1.7.3.jar                     | false           |
| The Betweenlands              | onEntityJoin           | thebetweenlands.common.registries.GameruleRegistry                  | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| Academy Monster               | onEntityJoinedWorld    | cn.paindar.academymonster.core.GlobalEventHandle                    | normal   | AcademyMonster-1.1.0a.jar                          | false           |
| Mutant Beasts                 | onEntityJoinWorld      | chumbanotz.mutantbeasts.EventHandler                                | normal   | MutantBeasts-1.12.2-0.6.0.jar                      | false           |
| Practical Logistics 2         | onEntityJoin           | sonar.logistics.base.events.PL2Events                               | normal   | practicallogistics2-1.12.2-3.0.8-11.jar            | false           |
| Phosphor Lighting Engine      | onPlayerJoinWorld      | me.jellysquid.mods.phosphor.mod.PhosphorEvents                      | normal   | phosphor-1.12.2-0.2.6+build50-universal.jar        | false           |
| Techguns                      | onEntityJoinWorld      | techguns.events.TGEventHandler                                      | normal   | techguns-1.12.2-2.0.2.0_pre3.1.jar                 | false           |
| The Betweenlands              | onEntityJoin           | thebetweenlands.common.handler.PlayerJoinWorldHandler               | normal   | TheBetweenlands-3.5.5-universal.jar                | false           |
| PneumaticCraft: Repressurized | onPlayerLogin          | me.desht.pneumaticcraft.client.ClientEventHandler                   | normal   | pneumaticcraft-repressurized-1.12.2-0.11.8-380.jar | false           |
| Prehistoric Eclipse           | onEntityJoin           | com.dabigjoe.obsidianAPI.ObsidianEventHandler                       | normal   | Prehistoric Eclipsev0.9-BETA.jar                   | false           |


## WorldEvent$Save
| Owner                  | Method          | Location                                                      | Priority | Source                                                    | RecieveCanceled |
|------------------------|-----------------|---------------------------------------------------------------|----------|-----------------------------------------------------------|-----------------|
| The Betweenlands       | onWorldSave     | thebetweenlands.common.handler.WorldEventHandler              | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |
| Just Enough Dimensions | onWorldSave     | fi.dy.masa.justenoughdimensions.event.JEDEventHandler         | normal   | justenoughdimensions-1.12.2-1.6.0-dev.20191019.172643.jar | false           |
| Advanced Rocketry      | worldSaveEvent  | zmaster587.advancedRocketry.event.PlanetEventHandler          | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar           | false           |
| The Betweenlands       | onWorldSave     | thebetweenlands.common.world.storage.OfflinePlayerHandlerImpl | normal   | TheBetweenlands-3.5.5-universal.jar                       | false           |
| LLibrary               | onWorldSave     | INSTANCE                                                      | normal   | llibrary-1.7.19-1.12.2.jar                                | false           |
| Minecraft Forge        | onDimensionSave | net.minecraftforge.common.ForgeInternalHandler                | highest  | forge-1.12.2-14.23.5.2847.jar                             | false           |
| Obsidian Animator      | onWorldSave     | com.dabigjoe.obsidianAnimator.EventHandler                    | normal   | ObsidianAnimator_v1.0.0.jar                               | false           |
| Epic Siege Mod         | onWorldSave     | funwayguy.epicsiegemod.handlers.entities.GeneralEntityHandler | normal   | EpicSiegeMod-13.165.jar                                   | false           |
| Just Enough Items      | onWorldSave     | mezz.jei.startup.ProxyCommonClient                            | normal   | jei_1.12.2-4.15.0.291.jar                                 | false           |
| Advanced Rocketry      | worldSaveEvent  | zmaster587.advancedRocketry.event.PlanetEventHandler          | normal   | AdvancedRocketry-1.12.2-1.6.0-196-universal.jar           | false           |


## UpdateFogEvent
| Owner            | Method      | Location                                       | Priority | Source                              | RecieveCanceled |
|------------------|-------------|------------------------------------------------|----------|-------------------------------------|-----------------|
| The Betweenlands | onUpdateFog | thebetweenlands.common.world.event.EventWinter | low      | TheBetweenlands-3.5.5-universal.jar | false           |
| The Betweenlands | updateFog   | thebetweenlands.client.handler.FogHandler      | normal   | TheBetweenlands-3.5.5-universal.jar | false           |


## TextureStitchEvent$Post
| Owner            | Method              | Location                                                   | Priority | Source                              | RecieveCanceled |
|------------------|---------------------|------------------------------------------------------------|----------|-------------------------------------|-----------------|
| JourneyMap       | onTextureStiched    | journeymap.client.forge.event.TextureAtlasHandler          | normal   | journeymap-1.12.2-5.5.6.jar         | false           |
| Mekanism         | onStitch            | mekanism.client.render.MekanismRenderer                    | normal   | Mekanism-1.12.2-9.8.3.390.jar       | false           |
| FoamFix          | onTextureStitchPost | pl.asie.foamfix.client.FoamFixModelRegistryDuplicateWipe   | normal   | foamfix-0.10.10-1.12.2.jar          | false           |
| MekanismOres     | onTextureStitch     | io.github.phantamanta44.mekores.client.ClientEventListener | normal   | mekores-2.0.12.jar                  | false           |
| The Betweenlands | onTextureStitchPost | thebetweenlands.client.handler.TextureStitchHandler        | normal   | TheBetweenlands-3.5.5-universal.jar | false           |


## CoinThrowEvent
| Owner           | Method      | Location             | Priority | Source                    | RecieveCanceled |
|-----------------|-------------|----------------------|----------|---------------------------|-----------------|
| lambdalib2      | onThrowCoin | <unassigned>.railgun | normal   | AcademyCraft-1.1.2.jar    | false           |
| Academy Monster | onThrowCoin | <unassigned>.railgun | normal   | AcademyMonster-1.1.0a.jar | false           |


## ModSoundRegisterEvent
| Owner    | Method         | Location                                    | Priority | Source                    | RecieveCanceled |
|----------|----------------|---------------------------------------------|----------|---------------------------|-----------------|
| Ender IO | registerSounds | crazypants.enderio.base.sound.SoundRegistry | normal   | EnderIO-1.12.2-5.1.55.jar | false           |


## RegisterPacketEvent
| Owner       | Method          | Location              | Priority | Source                                      | RecieveCanceled |
|-------------|-----------------|-----------------------|----------|---------------------------------------------|-----------------|
| Reborn Core | registerPackets | reborncore.RebornCore | normal   | RebornCore-1.12.2-3.16.4.478-universal.jar  | false           |
| Tech Reborn | LoadPackets     | techreborn.Core       | normal   | TechReborn-1.12.2-2.24.3.1043-universal.jar | false           |


## ChangePassEvent
| Owner      | Method     | Location                              | Priority | Source                 | RecieveCanceled |
|------------|------------|---------------------------------------|----------|------------------------|-----------------|
| lambdalib2 | changePass | cn.academy.energy.impl.WirelessSystem | normal   | AcademyCraft-1.1.2.jar | false           |


## TinkerToolEvent$OnMattockHoe
| Owner                 | Method    | Location                                  | Priority | Source                              | RecieveCanceled |
|-----------------------|-----------|-------------------------------------------|----------|-------------------------------------|-----------------|
| Tinkers Tool Leveling | onMattock | slimeknights.toolleveling.ModToolLeveling | normal   | TinkerToolLeveling-1.12.2-1.1.0.jar | false           |


## PlayerEvent$NameFormat
| Owner         | Method             | Location                                  | Priority | Source                           | RecieveCanceled |
|---------------|--------------------|-------------------------------------------|----------|----------------------------------|-----------------|
| Ender IO      | getKillDisplayName | Block{minecraft:air}                      | normal   | EnderIO-1.12.2-5.1.55.jar        | false           |
| Aroma1997Core | nameFormat         | aroma1997.core.client.ClientEventListener | normal   | Aroma1997Core-1.12.2-2.0.0.2.jar | false           |
