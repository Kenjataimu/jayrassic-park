# Error Reports

This file contains errors that were found with the pack. If there are no errors
listed below, none were detected. This data is anonymous, and is not
automatically submitted to any online service.

# Recipes with Air

If a recipe has air in it as an ingredient or output it can cause many issues
such as crashes and uncraftable recipes.

Recipe: akashictome:attachment Class: public class vazkii.akashictome.AttachementRecipe
Recipe: morphtool:attachment Class: public class vazkii.morphtool.AttachementRecipe
Recipe: refinedstorage:cover Class: public class com.raoulvdberge.refinedstorage.recipe.RecipeCover
Recipe: refinedstorage:hollow_cover Class: public class com.raoulvdberge.refinedstorage.recipe.RecipeHollowCover
Recipe: draconicevolution:recipe_dislocator_clone Class: public class com.brandon3055.draconicevolution.lib.RecipeDislocatorClone# Bad Version

The version number @version@ is a placeholder. If a mod has this version number
something went wrong in the build process.

Mod: TombManyPlugins Techguns File: tombmanypluginstechguns-1.0.0.jar# Enchantment Descriptions

Many mods have systems for showing descriptions for enchantments. Adding
translations for these can improve mod compatibility.

Enchant: enderio:witherarrow Translation Key: enchantment.enderio.witherarrow.desc
Enchant: enderio:repellent Translation Key: enchantment.enderio.repellent.desc
Enchant: enderio:shimmer Translation Key: enchantment.enderio.shimmer.desc
Enchant: enderio:witherweapon Translation Key: enchantment.enderio.witherweapon.desc
Enchant: enderio:soulbound Translation Key: enchantment.enderio.soulbound.desc