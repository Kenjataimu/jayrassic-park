# Dimension Registry Analysis

This file contains info about the various dimensions in a pack. This data is
anonymous, and is not automatically submitted to any online service.


| Name                 | Id   | Spawn |
|----------------------|------|-------|
| overworld            | 0    | true  |
| the nether           | -1   | false |
| the end              | 1    | false |
| planet               | 2    | false |
| space                | 3    | false |
| asteroid             | 4    | false |
| JED Surface          | 7891 | false |
| JED Surface 0        | 0    | false |
| JED Surface Loaded 0 | 0    | true  |
| JED Hell             | -1   | false |
| JED End              | 1    | false |
| CompactMachines      | 144  | false |
| lostcities           | 111  | false |
| pe                   | 46   | false |
| betweenlands         | 20   | false |
| nc wasteland         | 4598 | false |
