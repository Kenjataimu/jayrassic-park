# potion Registry Analysis

This file contains information about how mods are using the potion registry. 167
out of 256 ids available. 65.234% of this registry is still available. This data
is anonymous, and is not automatically submitted to any online service.


| Mod Name        | Entries | Utilization |
|-----------------|---------|-------------|
| thebetweenlands | 37      | 14.453%     |
| minecraft       | 27      | 10.547%     |
| tconstruct      | 7       | 2.734%      |
| scpcraft        | 7       | 2.734%      |
| quark           | 6       | 2.344%      |
| techguns        | 3       | 1.172%      |
| apotheosis      | 2       | 0.781%      |
