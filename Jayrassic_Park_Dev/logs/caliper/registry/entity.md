# entity Registry Analysis

This file contains information about how mods are using the entity registry.
67108341 out of 67108863 ids available. 99.999% of this registry is still
available. This data is anonymous, and is not automatically submitted to any
online service.


| Mod Name            | Entries | Utilization |
|---------------------|---------|-------------|
| thebetweenlands     | 90      | <0.001%     |
| minecraft           | 83      | <0.001%     |
| grimoireofgaia      | 78      | <0.001%     |
| techguns            | 50      | <0.001%     |
| iceandfire          | 40      | <0.001%     |
| scpcraft            | 40      | <0.001%     |
| quark               | 24      | <0.001%     |
| pe                  | 20      | <0.001%     |
| icbmclassic         | 16      | <0.001%     |
| mutantbeasts        | 14      | <0.001%     |
| advancedrocketry    | 10      | <0.001%     |
| draconicevolution   | 9       | <0.001%     |
| tconstruct          | 7       | <0.001%     |
| pneumaticcraft      | 7       | <0.001%     |
| academy             | 6       | <0.001%     |
| mekanism            | 5       | <0.001%     |
| academymonster      | 5       | <0.001%     |
| openmodularturrets  | 4       | <0.001%     |
| enderio             | 3       | <0.001%     |
| plustic             | 2       | <0.001%     |
| buildinggadgets     | 2       | <0.001%     |
| enderiomachines     | 1       | <0.001%     |
| obsidian_animator   | 1       | <0.001%     |
| air_support         | 1       | <0.001%     |
| clumps              | 1       | <0.001%     |
| nuclearcraft        | 1       | <0.001%     |
| practicallogistics2 | 1       | <0.001%     |
| techreborn          | 1       | <0.001%     |
