# recipes Registry Analysis

This file contains information about how mods are using the recipes registry.
67100272 out of 67108863 ids available. 99.987% of this registry is still
available. This data is anonymous, and is not automatically submitted to any
online service.


| Mod Name                            | Entries | Utilization |
|-------------------------------------|---------|-------------|
| nuclearcraft                        | 1357    | 0.002%      |
| harvestcraft                        | 1285    | 0.002%      |
| quark                               | 722     | 0.001%      |
| enderio                             | 643     | <0.001%     |
| techreborn                          | 480     | <0.001%     |
| minecraft                           | 443     | <0.001%     |
| mekanism                            | 436     | <0.001%     |
| thebetweenlands                     | 369     | <0.001%     |
| techguns                            | 350     | <0.001%     |
| iceandfire                          | 285     | <0.001%     |
| tconstruct                          | 264     | <0.001%     |
| jaopca                              | 235     | <0.001%     |
| advancedrocketry                    | 200     | <0.001%     |
| rftools                             | 154     | <0.001%     |
| pneumaticcraft                      | 150     | <0.001%     |
| icbmclassic                         | 117     | <0.001%     |
| scpcraft                            | 103     | <0.001%     |
| unidict                             | 93      | <0.001%     |
| refinedstorage                      | 91      | <0.001%     |
| sonarcore                           | 86      | <0.001%     |
| draconicevolution                   | 84      | <0.001%     |
| progressiveautomation               | 79      | <0.001%     |
| nuclearcraft:item.draconicevolution | 72      | <0.001%     |
| storagedrawers                      | 69      | <0.001%     |
| openmodularturrets                  | 60      | <0.001%     |
| academy                             | 50      | <0.001%     |
| chisel                              | 50      | <0.001%     |
| practicallogistics2                 | 29      | <0.001%     |
| alchemistry                         | 26      | <0.001%     |
| grimoireofgaia                      | 25      | <0.001%     |
| agricraft                           | 19      | <0.001%     |
| apotheosis                          | 17      | <0.001%     |
| libvulpes                           | 17      | <0.001%     |
| plustic                             | 16      | <0.001%     |
| endercore                           | 15      | <0.001%     |
| nuclearcraft:item.quark             | 12      | <0.001%     |
| floodlights                         | 12      | <0.001%     |
| scannable                           | 11      | <0.001%     |
| buildinggadgets                     | 9       | <0.001%     |
| notenoughrtgs                       | 8       | <0.001%     |
| quarryplus                          | 7       | <0.001%     |
| mutantbeasts                        | 6       | <0.001%     |
| betterbuilderswands                 | 6       | <0.001%     |
| torchmaster                         | 5       | <0.001%     |
| theoneprobe                         | 5       | <0.001%     |
| air_support                         | 4       | <0.001%     |
| akashictome                         | 3       | <0.001%     |
| compactmachines3                    | 2       | <0.001%     |
| topaddons                           | 2       | <0.001%     |
| morphtool                           | 2       | <0.001%     |
| omlib                               | 1       | <0.001%     |
| oeintegration                       | 1       | <0.001%     |
| nuclearcraft:item.minecraft         | 1       | <0.001%     |
| refinedstoragerequestify            | 1       | <0.001%     |
| academymonster                      | 1       | <0.001%     |
| refinedstorageaddons                | 1       | <0.001%     |
