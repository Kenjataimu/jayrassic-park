# enchantment Registry Analysis

This file contains information about how mods are using the enchantment
registry. 32708 out of 32766 ids available. 99.823% of this registry is still
available. This data is anonymous, and is not automatically submitted to any
online service.


| Mod Name          | Entries | Utilization |
|-------------------|---------|-------------|
| minecraft         | 30      | 0.092%      |
| apotheosis        | 18      | 0.055%      |
| enderio           | 5       | 0.015%      |
| endercore         | 2       | 0.006%      |
| draconicevolution | 1       | 0.003%      |
| oeintegration     | 1       | 0.003%      |
| advancedrocketry  | 1       | 0.003%      |
