# biome Registry Analysis

This file contains information about how mods are using the biome registry. 161
out of 256 ids available. 62.891% of this registry is still available. This data
is anonymous, and is not automatically submitted to any online service.


| Mod Name         | Entries | Utilization |
|------------------|---------|-------------|
| minecraft        | 62      | 24.219%     |
| advancedrocketry | 12      | 4.688%      |
| pe               | 10      | 3.906%      |
| thebetweenlands  | 9       | 3.516%      |
| iceandfire       | 1       | 0.391%      |
| nuclearcraft     | 1       | 0.391%      |
