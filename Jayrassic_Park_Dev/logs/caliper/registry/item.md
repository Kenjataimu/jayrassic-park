# item Registry Analysis

This file contains information about how mods are using the item registry. 25888
out of 32000 ids available. 80.900% of this registry is still available. This
data is anonymous, and is not automatically submitted to any online service.


| Mod Name                 | Entries | Utilization |
|--------------------------|---------|-------------|
| harvestcraft             | 1310    | 4.094%      |
| thebetweenlands          | 611     | 1.909%      |
| minecraft                | 417     | 1.303%      |
| quark                    | 385     | 1.203%      |
| nuclearcraft             | 349     | 1.091%      |
| iceandfire               | 340     | 1.063%      |
| jaopca                   | 320     | 1.000%      |
| chisel                   | 253     | 0.791%      |
| enderio                  | 211     | 0.659%      |
| techreborn               | 198     | 0.619%      |
| techguns                 | 146     | 0.456%      |
| tconstruct               | 145     | 0.453%      |
| advancedrocketry         | 144     | 0.450%      |
| rftools                  | 140     | 0.438%      |
| pneumaticcraft           | 138     | 0.431%      |
| scpcraft                 | 128     | 0.400%      |
| grimoireofgaia           | 105     | 0.328%      |
| draconicevolution        | 77      | 0.241%      |
| mekanism                 | 72      | 0.225%      |
| sonarcore                | 66      | 0.206%      |
| academy                  | 57      | 0.178%      |
| progressiveautomation    | 55      | 0.172%      |
| refinedstorage           | 51      | 0.159%      |
| practicallogistics2      | 37      | 0.116%      |
| icbmclassic              | 33      | 0.103%      |
| quarryplus               | 31      | 0.097%      |
| libvulpes                | 29      | 0.091%      |
| alchemistry              | 28      | 0.088%      |
| plustic                  | 27      | 0.084%      |
| pe                       | 23      | 0.072%      |
| storagedrawers           | 22      | 0.069%      |
| openmodularturrets       | 20      | 0.063%      |
| agricraft                | 18      | 0.056%      |
| mutantbeasts             | 15      | 0.047%      |
| buildinggadgets          | 15      | 0.047%      |
| apotheosis               | 15      | 0.047%      |
| floodlights              | 14      | 0.044%      |
| scannable                | 11      | 0.034%      |
| compactmachines3         | 9       | 0.028%      |
| notenoughrtgs            | 8       | 0.025%      |
| air_support              | 7       | 0.022%      |
| theoneprobe              | 6       | 0.019%      |
| torchmaster              | 5       | 0.016%      |
| omlib                    | 4       | 0.013%      |
| betterbuilderswands      | 4       | 0.013%      |
| tombmanygraves           | 1       | 0.003%      |
| akashictome              | 1       | 0.003%      |
| mcjtylib_ng              | 1       | 0.003%      |
| forge                    | 1       | 0.003%      |
| patchouli                | 1       | 0.003%      |
| itemblacklist            | 1       | 0.003%      |
| mekores                  | 1       | 0.003%      |
| gasconduits              | 1       | 0.003%      |
| oeintegration            | 1       | 0.003%      |
| refinedstoragerequestify | 1       | 0.003%      |
| academymonster           | 1       | 0.003%      |
| refinedstorageaddons     | 1       | 0.003%      |
| morphtool                | 1       | 0.003%      |
