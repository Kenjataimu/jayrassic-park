# block Registry Analysis

This file contains information about how mods are using the block registry. 1069
out of 4096 ids available. 26.099% of this registry is still available. This
data is anonymous, and is not automatically submitted to any online service.


| Mod Name                 | Entries | Utilization |
|--------------------------|---------|-------------|
| thebetweenlands          | 444     | 10.840%     |
| quark                    | 443     | 10.815%     |
| minecraft                | 254     | 6.201%      |
| chisel                   | 249     | 6.079%      |
| nuclearcraft             | 236     | 5.762%      |
| harvestcraft             | 223     | 5.444%      |
| enderio                  | 160     | 3.906%      |
| techreborn               | 118     | 2.881%      |
| tconstruct               | 113     | 2.759%      |
| advancedrocketry         | 94      | 2.295%      |
| rftools                  | 78      | 1.904%      |
| iceandfire               | 68      | 1.660%      |
| sonarcore                | 66      | 1.611%      |
| pneumaticcraft           | 57      | 1.392%      |
| scpcraft                 | 33      | 0.806%      |
| draconicevolution        | 32      | 0.781%      |
| progressiveautomation    | 32      | 0.781%      |
| refinedstorage           | 29      | 0.708%      |
| techguns                 | 29      | 0.708%      |
| practicallogistics2      | 26      | 0.635%      |
| academy                  | 23      | 0.562%      |
| quarryplus               | 21      | 0.513%      |
| mekanism                 | 20      | 0.488%      |
| alchemistry              | 18      | 0.439%      |
| libvulpes                | 16      | 0.391%      |
| jaopca                   | 15      | 0.366%      |
| plustic                  | 14      | 0.342%      |
| icbmclassic              | 14      | 0.342%      |
| grimoireofgaia           | 14      | 0.342%      |
| openmodularturrets       | 14      | 0.342%      |
| pe                       | 12      | 0.293%      |
| agricraft                | 10      | 0.244%      |
| storagedrawers           | 9       | 0.220%      |
| notenoughrtgs            | 8       | 0.195%      |
| floodlights              | 7       | 0.171%      |
| compactmachines3         | 7       | 0.171%      |
| torchmaster              | 5       | 0.122%      |
| buildinggadgets          | 5       | 0.122%      |
| air_support              | 3       | 0.073%      |
| apotheosis               | 2       | 0.049%      |
| tombmanygraves           | 1       | 0.024%      |
| dimensionalcontrol       | 1       | 0.024%      |
| omlib                    | 1       | 0.024%      |
| mcmultipart              | 1       | 0.024%      |
| refinedstoragerequestify | 1       | 0.024%      |
| mcjtylib_ng              | 1       | 0.024%      |
