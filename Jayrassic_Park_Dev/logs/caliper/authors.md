# Author Count

This is a list of all the authors of the mods in the instance sorted by how many
they have. This data is anonymous, and is not automatically submitted to any
online service.

14 - tterrag
     - Chisel
     - CTM
     - EnderCore
     - Ender IO
     - Ender IO Base
     - Ender IO Applied Energistics Conduits
     - Ender IO Open Computers Conduits
     - Ender IO Refined Storage Conduits
     - Ender IO Conduits
     - Ender IO Integration with Forestry
     - Ender IO Integration with Tinkers' Construct
     - Ender IO Integration with Tinkers' Construct
     - Ender IO Machines
     - Ender IO Powertools
12 - crazypants
     - EnderCore
     - Ender IO
     - Ender IO Base
     - Ender IO Applied Energistics Conduits
     - Ender IO Open Computers Conduits
     - Ender IO Refined Storage Conduits
     - Ender IO Conduits
     - Ender IO Integration with Forestry
     - Ender IO Integration with Tinkers' Construct
     - Ender IO Integration with Tinkers' Construct
     - Ender IO Machines
     - Ender IO Powertools
12 - henryloenwind
     - EnderCore
     - Ender IO
     - Ender IO Base
     - Ender IO Applied Energistics Conduits
     - Ender IO Open Computers Conduits
     - Ender IO Refined Storage Conduits
     - Ender IO Conduits
     - Ender IO Integration with Forestry
     - Ender IO Integration with Tinkers' Construct
     - Ender IO Integration with Tinkers' Construct
     - Ender IO Machines
     - Ender IO Powertools
11 - epicsquid
     - Ender IO
     - Ender IO Base
     - Ender IO Applied Energistics Conduits
     - Ender IO Open Computers Conduits
     - Ender IO Refined Storage Conduits
     - Ender IO Conduits
     - Ender IO Integration with Forestry
     - Ender IO Integration with Tinkers' Construct
     - Ender IO Integration with Tinkers' Construct
     - Ender IO Machines
     - Ender IO Powertools
9 - matthiasm
     - Ender IO
     - Ender IO Base
     - Ender IO Applied Energistics Conduits
     - Ender IO Open Computers Conduits
     - Ender IO Refined Storage Conduits
     - Ender IO Conduits
     - Ender IO Integration with Forestry
     - Ender IO Machines
     - Ender IO Powertools
6 - jaredlll08
     - Clumps
     - Controlling
     - CraftTweaker2
     - Mod Tweaker
     - MTLib
     - Surge
5 - cyanidex
     - Ender IO
     - Ender IO Base
     - Ender IO Conduits
     - Ender IO Machines
     - Ender IO Powertools
5 - mcjty
     - InControl
     - The Lost Cities
     - McJtyLib
     - RFTools
     - The One Probe
5 - darkhax
     - Attribute Fix
     - Bookshelf
     - Caliper
     - Enchantment Descriptions
     - Surge
5 - vazkii
     - Akashic Tome
     - AutoRegLib
     - Morph-o-Tool
     - Patchouli
     - Quark
4 - shadows_of_fire
     - Apotheosis
     - FastFurnace
     - FastWorkbench
     - Placebo
4 - lumien
     - Custom Backgrounds
     - Custom Main Menu
     - Loading Profiler
     - Resource Loader
3 - thelmiffy1111
     - JAOPCA
     - OreDictInit
     - WrapUp
3 - m4thg33k
     - TombManyGraves2 API
     - TombManyPlugins Techguns
     - TombManyPlugins The Betweenlands
3 - cpw
     - Forge Mod Loader
     - Minecraft Forge
     - Simple Inventory sorting
3 - boni
     - Mantle
     - Tinkers' Construct
     - Tinkers Tool Leveling
3 - mezz
     - Ender IO Integration with Forestry
     - Just Enough Items
     - Mod Name Tooltip
2 - brandon3055
     - Brandon's Core
     - Draconic Evolution
2 - blargerist
     - Calm Down Zombie Guy
     - Dimensional Control
2 - lexmanos
     - Forge Mod Loader
     - Minecraft Forge
2 - lclc98
     - Bookshelf
     - Surge
2 - minecreatr
     - Chisel
     - CTM
2 - jaquadro
     - Chameleon
     - Storage Drawers
2 - dries007
     - Item Blacklist
     - MineTweakerRecipeMaker
2 - modmuss50
     - Reborn Core
     - Tech Reborn
2 - funwayguy
     - Epic Siege Mod
     - OreExcavation
2 - bre2el
     - Smooth Font Core
     - Smooth Font
2 - pupnewfster
     - GasConduits
     - Mekanism
2 - gigabit101
     - Reborn Core
     - Tech Reborn
2 - drullkus
     - Chisel
     - CTM
2 - masa
     - Just Enough Dimensions
     - Tell Me
2 - lunatrius
     - LunatriusCore
     - Stackie
2 - wanioncane
     - UniDict
     - WanionLib
2 - prospector
     - Reborn Core
     - Tech Reborn
1 - droidicus
     - Bad Wither No Cookie! Reloaded
1 - thiakil
     - Mekanism
1 - lambda innovation
     - Academy Craft
1 - darkosto
     - OreExcavation
1 - soggymustache, knightworm
     - Bookworm API
1 - way2muchnoise
     - Just Enough Resources
1 - feldim2425
     - MoreOverlays
1 - team cofh
     - Redstone Flux
1 - claycorp
     - Item Blacklist
1 - dabigjoe
     - Obsidian Animator
1 - alexbegt
     - Mantle
1 - ilexiconn
     - LLibrary
1 - cubex2
     - Obsidian Animator
1 - geometrically
     - Prehistoric Eclipse
1 - m4thg33k, tiffit
     - Tomb Many Graves
1 - leviathan143
     - LootTweaker
1 - chumbanotz
     - Mutant Beasts
1 - refined storage contributors
     - Refined Storage
1 - blaytheninth
     - Default Options
1 - landmaster (phuong0429)
     - PlusTiC
1 - fry
     - Minecraft Forge
1 - blargerist, superckl
     - BNBGamingLib
1 - techbrew
     - JourneyMap
1 - desht
     - PneumaticCraft: Repressurized
1 - lumber wizard
     - Anvil Patch - Lawful
1 - quetzi
     - Morpheus
1 - yalter
     - Mouse Tweaks
1 - thommy101
     - Mekanism
1 - asiekierka
     - FoamFix
1 - sangar
     - Scannable
1 - stanhebben
     - CraftTweaker2
1 - shadowfacts
     - Shadowfacts' Forgelin
1 - fluffy taco
     - Prehistoric Eclipse
1 - wiiv
     - Quark
1 - wildex999
     - Tick Dynamic
1 - alexthe666
     - Ice and Fire
1 - tfarecnim
     - Not Enough RTGs
1 - yungnickyoung
     - YUNG's Better Caves
1 - 
    - infinityraider
     - AgriCraft
1 - minemaarten
     - PneumaticCraft: Repressurized
1 - quaternary
     - Rebind Narrator
1 - bdew
     - Ender IO Integration with Forestry
1 - shewolfdeadly
     - Classic Combat
1 - gegy1000
     - LLibrary
1 - raptorfarian
     - Ice and Fire
1 - covers1624
     - CodeChicken Lib
1 - exidex
     - SwingThroughGrass
1 - ironfists
     - Better Combat Rebirth
1 - buuz135
     - RefinedStorageRequestify
1 - knightminer
     - Tinkers' Construct
1 - kreezxil
     - Bad Wither No Cookie! Reloaded
1 - runemoro
     - VanillaFix
1 - alcatrazescapee
     - Ore Veins
1 - terminator_nl
     - LagGoggles
1 - snowshock35
     - JEI Integration
1 - pwn3d_1337, th3_st4lk3r
     - Techguns
1 - vanhal
     - Progressive Automation
1 - ewyboy
     - World Stripper
1 - bloodworkxgaming
     - OreExcavation Integration
1 - mrriegel
     - InstantUnify
1 - angeline (@jellysquid)
     - Phosphor Lighting Engine
1 - angry pixel
     - The Betweenlands
1 - sanandreasp (maintainer)
     - Better Combat Rebirth
1 - doubledoordev
     - Item Blacklist
1 - zmaster587
     - Advanced Rocketry Core
1 - drmanganese
     - TOP Addons
1 - luwin
     - No Recipe Book
1 - the_fireplace
     - Tick Dynamic
1 - olafski
     - Fast Leaf Decay
1 - darkguardsman aka darkcow
     - ICBM-Classic
1 - unnoen
     - Unloader
1 - progwml6
     - Mantle
1 - phantamanta44
     - MekanismOres
1 - xalcon
     - TorchMaster
1 - silentine
     - Grimoire of Gaia 3
1 - direwolf20
     - Building Gadgets
1 - mrapplexz
     - Just Enough HarvestCraft
1 - baushawat
     - SCPCraft:Decline
1 - possible_triangle
     - Tinker's JEI
1 - matthewprenger
     - HelpFixer
1 - infinityraider
     - InfinityLib
1 - calclavia
     - ICBM-Classic
1 - aidancbrady
     - Mekanism
1 - mysticdrew
     - JourneyMap
1 - al132
     - Alchemistry
1 - pamela collins
     - Pam's HarvestCraft
1 - portablejim
     - Better Builder's Wands
1 - wildbamaboy
     - Classic Combat
1 - gigabit_101
     - MineTweakerRecipeMaker
1 - tomdodd4598
     - NuclearCraft
1 - player
     - Forge Mod Loader
1 - doubledoordevelopment
     - MineTweakerRecipeMaker
1 - asie
     - FoamFixCore
1 - vadis365
     - air_support
1 - chickenbones
     - CodeChicken Lib
1 - technicianlp
     - ReAuth
1 - davenonymous
     - Compact Machines 3
1 - dizzyd
     - Mekanism
1 - grandlovania
     - SCPCraft:Decline
1 - refined storage addons contributors
     - Refined Storage Addons
1 - jared
     - TipTheScales
